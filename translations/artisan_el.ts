<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="el" sourcelanguage="">
<context>
    <name>About</name>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>About</source>
        <translation>Περι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11007"/>
        <source>Version:</source>
        <translation type="obsolete">Εκδοση:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>Core developers:</source>
        <translation>Προγραμματιστες:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>Contributors:</source>
        <translation>Συντελεστες:</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="artisanlib/main.py" line="7372"/>
        <source>ON</source>
        <translation>ΕΝΕΡΓΟΠΟΙΗΣΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7385"/>
        <source>START</source>
        <translation>ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3526"/>
        <source>OFF</source>
        <translation>ΠΑΥΣΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7398"/>
        <source>FC
START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7405"/>
        <source>FC
END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7412"/>
        <source>SC
START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7419"/>
        <source>SC
END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7427"/>
        <source>RESET</source>
        <translation>ΑΡΧΙΚΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7440"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7448"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7456"/>
        <source>Control</source>
        <translation>Ελεγχος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7463"/>
        <source>EVENT</source>
        <translation>ΣΥΜΒΑΝ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7471"/>
        <source>SV +5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7479"/>
        <source>SV +10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7487"/>
        <source>SV +20</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7495"/>
        <source>SV -20</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7503"/>
        <source>SV -10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7511"/>
        <source>SV -5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7519"/>
        <source>HUD</source>
        <translation>ΠΡΟΒ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7534"/>
        <source>DRY
END</source>
        <translation>ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7543"/>
        <source>COOL
END</source>
        <translation>ΛΗΞΗ ΨΥΞΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26608"/>
        <source>Update</source>
        <translation>Ενημέρωση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14566"/>
        <source>PID Help</source>
        <translation type="obsolete">PID Βοήθεια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31249"/>
        <source>OK</source>
        <translation>Ενταξει</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28813"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14895"/>
        <source>Color</source>
        <translation>Χρωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14898"/>
        <source>Plot</source>
        <translation>Σχεδιο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25931"/>
        <source>Background</source>
        <translation>Φοντο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14904"/>
        <source>Virtual Device</source>
        <translation>Εικονικη Συσκευη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19555"/>
        <source>Save Image</source>
        <translation>Αποθηκευση Εικονας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27124"/>
        <source>Help</source>
        <translation>Βοηθεια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14963"/>
        <source>Info</source>
        <translation>Πληροφοριες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31101"/>
        <source>Set</source>
        <translation>Ορισμος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25886"/>
        <source>Defaults</source>
        <translation>Προκαθορισμενα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15869"/>
        <source>Order</source>
        <translation>Εντολη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26830"/>
        <source>Add</source>
        <translation>Προσθηκη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26839"/>
        <source>Delete</source>
        <translation>Καταργηση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16129"/>
        <source>in</source>
        <translation>Εισοδο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16136"/>
        <source>out</source>
        <translation>Εξοδο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17013"/>
        <source>Search</source>
        <translation>Αναζητηση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17166"/>
        <source>Path</source>
        <translation>Διαδρομη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18001"/>
        <source>Transfer To</source>
        <translation>Μεταφορα προς</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18003"/>
        <source>Restore From</source>
        <translation>Επαναφορα απο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31229"/>
        <source>Save</source>
        <translation>Αποθηκευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31226"/>
        <source>Load</source>
        <translation>Φορτωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19552"/>
        <source>Del</source>
        <translation>Ακυρωση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19784"/>
        <source>Align</source>
        <translation>Ευθυγραμμιση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19834"/>
        <source>Up</source>
        <translation>Πανω</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19836"/>
        <source>Down</source>
        <translation>Κατω</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19838"/>
        <source>Left</source>
        <translation>Απιστερα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19840"/>
        <source>Right</source>
        <translation>Δεξια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24656"/>
        <source>Reset</source>
        <translation>Επαναφορα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27501"/>
        <source>Close</source>
        <translation>Κλεισιμο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23270"/>
        <source>Create</source>
        <translation>Δημιουργια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19880"/>
        <source>Scan for Ports</source>
        <translation type="obsolete">Ανιχνευση Θυρων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26610"/>
        <source>Select</source>
        <translation>Επιλογη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25775"/>
        <source>Grid</source>
        <translation>Πλεγμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25782"/>
        <source>Title</source>
        <translation>Τιτλος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25789"/>
        <source>Y Label</source>
        <translation>Επιγραφη Αξονα Y</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25796"/>
        <source>X Label</source>
        <translation>Επιγραφη Αξονα Χ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25803"/>
        <source>Drying Phase</source>
        <translation>Φαση Ξηρανσης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25810"/>
        <source>Maillard Phase</source>
        <translation>Φαση Μaillard</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25817"/>
        <source>Development Phase</source>
        <translation>Φαση Αναπτυξης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25824"/>
        <source>Cooling Phase</source>
        <translation>Φαση Ψυξης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25831"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25838"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25845"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25852"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25859"/>
        <source>Markers</source>
        <translation>Δεικτες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25866"/>
        <source>Text</source>
        <translation>Κειμενο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25873"/>
        <source>Watermarks</source>
        <translation>Υδατογραφηματα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25880"/>
        <source>C Lines</source>
        <translation>Γραμμες C</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25889"/>
        <source>Grey</source>
        <translation>Γκριζο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25943"/>
        <source>LED</source>
        <translation>LED</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25975"/>
        <source>B/W</source>
        <translation>Ασπρομαυρο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26299"/>
        <source>Reset Parents</source>
        <translation>Επαναφορα Parents</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26303"/>
        <source>Reverse Hierarchy</source>
        <translation>Αντιστροφη Ιερα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26317"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26320"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26335"/>
        <source>Line Color</source>
        <translation>Χρωμα Γραμμης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26348"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26351"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26354"/>
        <source>Save File</source>
        <translation>Αποθυκευση Αρχειου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26357"/>
        <source>Save Img</source>
        <translation>Αποθυκευση Εικονας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26360"/>
        <source>View Mode</source>
        <translation>Λειτυργια Προβολης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26363"/>
        <source>Open</source>
        <translation>Ανοιγμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26633"/>
        <source>Set Color</source>
        <translation>Ορισμος Χρωματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26824"/>
        <source>All On</source>
        <translation>Λειτουργια Ολων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26827"/>
        <source>All Off</source>
        <translation>Παυση Ολων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27496"/>
        <source>Read Ra/So values</source>
        <translation>Αναγνωση Τιμων  Ra/So </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28550"/>
        <source>RampSoak ON</source>
        <translation>Εναρξη Γραφηματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28552"/>
        <source>RampSoak OFF</source>
        <translation>Παυση Γραφηματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28556"/>
        <source>PID OFF</source>
        <translation>Εναρξη PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28558"/>
        <source>PID ON</source>
        <translation>Παυση PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28673"/>
        <source>Write SV</source>
        <translation>Εγγραφη SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27512"/>
        <source>SV Buttons ON</source>
        <translation>Εναρξη Πληκτρα SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27514"/>
        <source>SV Buttons OFF</source>
        <translation>Παυση Πληκτρα SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27516"/>
        <source>Read SV</source>
        <translation>Αναγνωση SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27533"/>
        <source>Set p</source>
        <translation>Ορισμος p</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27534"/>
        <source>Set i</source>
        <translation>Ορισμος i</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27535"/>
        <source>Set d</source>
        <translation>Ορισμος d</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28809"/>
        <source>Autotune ON</source>
        <translation>Eναρξη αυτοματου συντονισμου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28811"/>
        <source>Autotune OFF</source>
        <translation>Παυση αυτοματου συντονισμου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27550"/>
        <source>Read PID Values</source>
        <translation>Αναγνωση Τιμων PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31380"/>
        <source>Read</source>
        <translation>Αναγνωση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28940"/>
        <source>Set ET PID to 1 decimal point</source>
        <translation>Ορισμος ΕΤ PID σε 1 δεκαδικο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28942"/>
        <source>Set BT PID to 1 decimal point</source>
        <translation>Ορισμος BT PID σε 1 δεκαδικο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28546"/>
        <source>Read RS values</source>
        <translation>Αναγνωση Τιμων RS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28609"/>
        <source>Write SV1</source>
        <translation>Εγγραφη SV1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28611"/>
        <source>Write SV2</source>
        <translation>Εγγραφη SV2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28613"/>
        <source>Write SV3</source>
        <translation>Εγγραφη SV3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28615"/>
        <source>Write SV4</source>
        <translation>Εγγραφη SV4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28617"/>
        <source>Write SV5</source>
        <translation>Εγγραφη SV5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28619"/>
        <source>Write SV6</source>
        <translation>Εγγραφη SV6</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28621"/>
        <source>Write SV7</source>
        <translation>Εγγραφη SV7</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28677"/>
        <source>ON SV buttons</source>
        <translation>Εναρξη Πληκτρα SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28680"/>
        <source>OFF SV buttons</source>
        <translation>Παυση Πληκτρα SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28683"/>
        <source>Read SV (7-0)</source>
        <translation>Αναγνωση SV(7-0)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28791"/>
        <source>pid 1</source>
        <translation>pid1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28793"/>
        <source>pid 2</source>
        <translation>pid2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28795"/>
        <source>pid 3</source>
        <translation>pid3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28797"/>
        <source>pid 4</source>
        <translation>pid4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28799"/>
        <source>pid 5</source>
        <translation>pid5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28801"/>
        <source>pid 6</source>
        <translation>pid6</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28803"/>
        <source>pid 7</source>
        <translation>pid7</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23923"/>
        <source>Read All</source>
        <translation type="obsolete">Αναγνωση Ολων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28944"/>
        <source>Set ET PID to MM:SS time units</source>
        <translation>Ορισμος ET PID σε χρονικη μορφη ΛΛ:ΔΔ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31381"/>
        <source>Write</source>
        <translation>Εγγραφη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16150"/>
        <source>scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18208"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26834"/>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26858"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28542"/>
        <source>Write All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28548"/>
        <source>Write RS values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28685"/>
        <source>Write SV (7-0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28805"/>
        <source>Read PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28807"/>
        <source>Write PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31251"/>
        <source>On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31253"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckBox</name>
    <message>
        <location filename="artisanlib/main.py" line="16089"/>
        <source>Heavy FC</source>
        <translation>Eντονο FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16092"/>
        <source>Low FC</source>
        <translation>Αδυναμο FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16095"/>
        <source>Light Cut</source>
        <translation>Ανοικτοχρωμο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16098"/>
        <source>Dark Cut</source>
        <translation>Σκουροχρωμο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16101"/>
        <source>Drops</source>
        <translation>Στιγματα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16104"/>
        <source>Oily</source>
        <translation>Ελαιωδης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16107"/>
        <source>Uneven</source>
        <translation>Ανομοιο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16109"/>
        <source>Tipping</source>
        <translation>Tipping</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16111"/>
        <source>Scorching</source>
        <translation>scorching</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16113"/>
        <source>Divots</source>
        <translation>Καψιματα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19772"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19773"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14644"/>
        <source>Smooth Spikes</source>
        <translation>Λειανση Παρασιτων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14654"/>
        <source>Drop Spikes</source>
        <translation>Απορριψη Παρασιτων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14659"/>
        <source>Limits</source>
        <translation>Ορια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14688"/>
        <source>Projection</source>
        <translation>Προβολη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19769"/>
        <source>Show</source>
        <translation>Εμφανιση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15092"/>
        <source>Beep</source>
        <translation>Beep</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15860"/>
        <source>Delete roast properties on RESET</source>
        <translation>Διαγραφη Δεδομενων στην Επαναφορα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17064"/>
        <source>Serial Log ON/OFF</source>
        <translation>Serial Log ON/OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17156"/>
        <source>Autosave [a]</source>
        <translation>Αυτοματη Αποθυκευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17290"/>
        <source>Lock Max</source>
        <translation>Κλειδωμα Τιμης Max</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17764"/>
        <source>Button</source>
        <translation>Πληκτρο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17770"/>
        <source>Mini Editor</source>
        <translation>Συντακτης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15140"/>
        <source>Automatic CHARGE/DROP</source>
        <translation type="obsolete">Αυτοματο ΦΟΡΤΩΜΑ/ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18251"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23070"/>
        <source>DRY END</source>
        <translation>ΤΕΛΟΣ ΞΗΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23072"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23074"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23076"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23078"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18305"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18314"/>
        <source>COOL END</source>
        <translation>ΛΗΞΗ ΨΥΞΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19362"/>
        <source>Auto Adjusted</source>
        <translation>Αυτοματη Ρυθμιση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19365"/>
        <source>Watermarks</source>
        <translation>Υδατογραφηματα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19560"/>
        <source>Background</source>
        <translation>Φοντο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19770"/>
        <source>Text</source>
        <translation>Κειμενο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19771"/>
        <source>Events</source>
        <translation>Συμβαντα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19868"/>
        <source>Playback Aid</source>
        <translation>Βοηθημα Playback</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20225"/>
        <source>Time</source>
        <translation>Χρονος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20226"/>
        <source>Bar</source>
        <translation>Μπαρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20227"/>
        <source>d/m</source>
        <translation>d/m</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20228"/>
        <source>ETBTa</source>
        <translation>ETBTa</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20229"/>
        <source>Evaluation</source>
        <translation>Αξιολογηση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20230"/>
        <source>Characteristics</source>
        <translation>Χαρακτηριστικα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24521"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24524"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14649"/>
        <source>Smooth2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14765"/>
        <source>Decimal Places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17946"/>
        <source>Auto CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17949"/>
        <source>Auto DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17952"/>
        <source>Mark TP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19367"/>
        <source>Phases LCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19369"/>
        <source>Auto DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19371"/>
        <source>Auto FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24595"/>
        <source>Modbus Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26862"/>
        <source>Load alarms from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31182"/>
        <source>Start PID on CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31237"/>
        <source>Load Ramp/Soak table from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24628"/>
        <source>Control Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24703"/>
        <source>Ratiometric</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComboBox</name>
    <message>
        <location filename="artisanlib/main.py" line="810"/>
        <source>Speed</source>
        <translation>Ταχυτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="811"/>
        <source>Power</source>
        <translation>Δυναμη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="812"/>
        <source>Damper</source>
        <translation>Κλαπετο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="813"/>
        <source>Fan</source>
        <translation>Ανεμιστηρας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>linear</source>
        <translation>Γραμμικο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14690"/>
        <source>newton</source>
        <translation>newton</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15688"/>
        <source>metrics</source>
        <translation>μετρικα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15690"/>
        <source>thermal</source>
        <translation>θερμικο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>cubic</source>
        <translation>κυβικο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>nearest</source>
        <translation>κοντινοτερο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17555"/>
        <source>g</source>
        <translation>γραμ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17556"/>
        <source>Kg</source>
        <translation>Κιλα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16771"/>
        <source>ml</source>
        <translation>μιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16004"/>
        <source>l</source>
        <translation>λ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27196"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27197"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17268"/>
        <source>upper right</source>
        <translation>πανω δεξια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17269"/>
        <source>upper left</source>
        <translation>πανω αριστερα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17270"/>
        <source>lower left</source>
        <translation>κατω αριστερα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17271"/>
        <source>lower right</source>
        <translation>κατω δεξια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17272"/>
        <source>right</source>
        <translation>δεξια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17273"/>
        <source>center left</source>
        <translation>κεντρο αριστερα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17274"/>
        <source>center right</source>
        <translation>κεντρο δεξια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17275"/>
        <source>lower center</source>
        <translation>κατω κεντρικα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17276"/>
        <source>upper center</source>
        <translation>πανω κεντρικα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17277"/>
        <source>center</source>
        <translation>κεντρο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17295"/>
        <source>30 seconds</source>
        <translation>30 δευτερολεπτα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17296"/>
        <source>1 minute</source>
        <translation>1 λεπτο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17297"/>
        <source>2 minute</source>
        <translation>2 λεπτα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17298"/>
        <source>3 minute</source>
        <translation>3 λεπτα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17299"/>
        <source>4 minute</source>
        <translation>4 λεπτα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17300"/>
        <source>5 minute</source>
        <translation>5 λεπτα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17323"/>
        <source>solid</source>
        <translation>Συμπαγες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17324"/>
        <source>dashed</source>
        <translation>διακεκομμενο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17325"/>
        <source>dashed-dot</source>
        <translation>διακεκομμενο-τελειες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17326"/>
        <source>dotted</source>
        <translation>με τελειες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>None</source>
        <translation>Κανενα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17535"/>
        <source>Event #0</source>
        <translation>Συμβαν#0</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17537"/>
        <source>Event #%1</source>
        <translation>Συμβαν#%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17557"/>
        <source>lb</source>
        <translation>λιμπρες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17578"/>
        <source>liter</source>
        <translation>λιτρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17579"/>
        <source>gallon</source>
        <translation>γαλονια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17580"/>
        <source>quart</source>
        <translation>τεταρτο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17581"/>
        <source>pint</source>
        <translation>πιντα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17582"/>
        <source>cup</source>
        <translation>φλυτζανι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17583"/>
        <source>cm^3</source>
        <translation>εκ.^3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17776"/>
        <source>Type</source>
        <translation>Τυπος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17777"/>
        <source>Value</source>
        <translation>Αξια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Serial Command</source>
        <translation>Σειριακη Εντολη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Modbus Command</source>
        <translation>Εντολη Modbus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>DTA Command</source>
        <translation>Εντολη DTA</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Call Program</source>
        <translation>Ανακληση Προγραμματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>OFF</source>
        <translation>ΤΕΛΟΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>ON</source>
        <translation>ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Multiple Event</source>
        <translation>Πολλαπλα Συμβαντα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27195"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27194"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23252"/>
        <source>SV Commands</source>
        <translation>Εντολες SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23252"/>
        <source>Ramp Commands</source>
        <translation>Εντολες Γραφηματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23912"/>
        <source>little-endian</source>
        <translation>little-endian</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25896"/>
        <source>grey</source>
        <translation>γκριζο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25897"/>
        <source>Dark Grey</source>
        <translation>Σκουρο Γκριζο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25898"/>
        <source>Slate Grey</source>
        <translation>Σαγρε Γκριζο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25899"/>
        <source>Light Gray</source>
        <translation>Ανοικτο Γκριζο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25900"/>
        <source>Black</source>
        <translation>Μαυρο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25901"/>
        <source>White</source>
        <translation>Λευκο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25902"/>
        <source>Transparent</source>
        <translation>Διαφανο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Flat</source>
        <translation>Φλατ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Perpendicular</source>
        <translation>Καθετα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Radial</source>
        <translation>Ακτινωτα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>START</source>
        <translation>ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>TP</source>
        <translation>ΤΡ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>DRY END</source>
        <translation>ΤΕΛΟΣ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>COOL</source>
        <translation>ΨΥΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27254"/>
        <source>below</source>
        <translation>αποκατω</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27254"/>
        <source>above</source>
        <translation>αποπανω</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Pop Up</source>
        <translation>Εμφανιση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Event Button</source>
        <translation>Πληκτρο Συμβαντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Slider</source>
        <translation>Ολισθητης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14831"/>
        <source>classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14831"/>
        <source>xkcd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Default</source>
        <translation type="unfinished">Αρχικο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Humor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>FCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>SCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>SCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>COOL END</source>
        <translation type="unfinished">ΛΗΞΗ ΨΥΞΗΣ</translation>
    </message>
</context>
<context>
    <name>Contextual Menu</name>
    <message>
        <location filename="artisanlib/main.py" line="5080"/>
        <source>Create</source>
        <translation>Δημιουργια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5084"/>
        <source>Config...</source>
        <translation>Διαμορφωση...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5094"/>
        <source>Add point</source>
        <translation>Προσθηκη σημειου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5098"/>
        <source>Remove point</source>
        <translation>Καταργηση σημειου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5104"/>
        <source>Reset Designer</source>
        <translation>Επαναφορα Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5108"/>
        <source>Exit Designer</source>
        <translation>Εξοδος Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5685"/>
        <source>Add to Cupping Notes</source>
        <translation>Προσθηκη Σημειωσεων cupping</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5689"/>
        <source>Add to Roasting Notes</source>
        <translation>Προσθηκη Σημειωσεων Ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5693"/>
        <source>Cancel selection</source>
        <translation>Καταργηση επιλογης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5697"/>
        <source>Edit Mode</source>
        <translation>Επεξεργασια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5701"/>
        <source>Exit</source>
        <translation>Εξοδος</translation>
    </message>
</context>
<context>
    <name>Directory</name>
    <message>
        <location filename="artisanlib/main.py" line="804"/>
        <source>edit text</source>
        <translation type="obsolete">επεξεργασια  κειμενου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14034"/>
        <source>profiles</source>
        <translation>προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14024"/>
        <source>other</source>
        <translation>Αλλα</translation>
    </message>
</context>
<context>
    <name>Error Message</name>
    <message>
        <location filename="artisanlib/main.py" line="31311"/>
        <source>Exception:</source>
        <translation>Εξαιρεση:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22958"/>
        <source>Value Error:</source>
        <translation>Σφαλμα Τιμης:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26787"/>
        <source>IO Error:</source>
        <translation>Σφαλμα ΙΟ:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12447"/>
        <source>Error</source>
        <translation>Σφαλμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16804"/>
        <source>Unable to move CHARGE to a value that does not exist</source>
        <translation>Αδυνατη μετακινηση ΦΟΡΤΩΜΑ σε τιμη μη υπαρκτη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20865"/>
        <source>Modbus Error:</source>
        <translation>Σφαλμα Modbus:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23014"/>
        <source>Serial Exception:</source>
        <translation>Σειριακη Εξαιρεση:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21150"/>
        <source>F80h Error</source>
        <translation>Σφαλμα F80h</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21160"/>
        <source>CRC16 data corruption ERROR. TX does not match RX. Check wiring</source>
        <translation>Σφαλμα πληροφοριας CRC16.TX δεν αντιστοιχει RX.Ελεγξτε συνδεσμολογια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21163"/>
        <source>No RX data received</source>
        <translation>Δεν εγινε ληψη πληροφοριων RX</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21291"/>
        <source>DTAcommand(): %1 bytes received but 15 needed</source>
        <translation>ΕντοληDTA():%1bytes εληφθη αλλα χρειζει15</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21564"/>
        <source>Unable to open serial port</source>
        <translation>Αδυνατη επικοινωνια με σειριακη θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18065"/>
        <source>HH806AUtemperature(): %1 bytes received but 14 needed</source>
        <translation type="obsolete">HH806AUθερμο():%1bytes εληφθησαν αλλα χρειζει14</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21714"/>
        <source>HH806Wtemperature(): Unable to initiate device</source>
        <translation>HH806Wθερμο():Αδυνατη συνδεση συσκευης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21843"/>
        <source>HH506RAGetID: %1 bytes received but 5 needed</source>
        <translation>HH506RAGetID:%1bytes εληφθη αλλα χρειζει5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21868"/>
        <source>HH506RAtemperature(): Unable to get id from HH506RA device </source>
        <translation>HH506RAθερμο():Αδυνατη ληψη id απο συσκευη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21888"/>
        <source>HH506RAtemperature(): %1 bytes received but 14 needed</source>
        <translation>HH506RAθερμο():%1bytes εληφθη αλλα χρειζει 14</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21943"/>
        <source>CENTER302temperature(): %1 bytes received but 7 needed</source>
        <translation>CENTER302θερμο():%1bytes εληφθη αλλα χρειζει7</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22013"/>
        <source>CENTER303temperature(): %1 bytes received but 8 needed</source>
        <translation>CENTER303θερμο():%1bytes εληφθη αλλα χρειζει8</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22083"/>
        <source>CENTER306temperature(): %1 bytes received but 10 needed</source>
        <translation>CENTER306θερμο():%1bytes εληφθη αλλα χρειζει10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22169"/>
        <source>CENTER309temperature(): %1 bytes received but 45 needed</source>
        <translation>CENTER309θερμο():%1bytes εληφθη αλλα χρειζει45</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22594"/>
        <source>Arduino could not set channels</source>
        <translation>Το arduino αδυνατει να διαμορφωση καναλια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22605"/>
        <source>Arduino could not set temperature unit</source>
        <translation>Το arduino αδυνατει να επικοινωνησει με θερμομετρο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24398"/>
        <source>Serial Exception: invalid comm port</source>
        <translation>Εξαιρεση σειριακη:ακυρη θυρα επικοινωνιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24403"/>
        <source>Serial Exception: timeout</source>
        <translation>Εξαιρεση σειριακη:τελος χρονου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20915"/>
        <source>Device error</source>
        <translation type="obsolete">Σφαλμα συσκευης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30912"/>
        <source>Segment values could not be written into PID</source>
        <translation>Τιμη δεν εγγραφεται στο PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30690"/>
        <source>RampSoak could not be changed</source>
        <translation>Αδυνατη η αλλαγη του γραφηματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30968"/>
        <source>pid.readoneword(): %1 RX bytes received (7 needed) for unit ID=%2</source>
        <translation>pid.readoneword():%1 RX bytes εληφθη(χρειζει7)για συσκευη ID=%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15340"/>
        <source>Univariate: no profile data available</source>
        <translation>μονοπαραγοντική:Δεν ευρεθει προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15512"/>
        <source>Polyfit: no profile data available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21614"/>
        <source>MS6514temperature(): %1 bytes received but 16 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21659"/>
        <source>HH806AUtemperature(): %1 bytes received but 16 needed</source>
        <translation type="unfinished">HH806AUθερμο():%1bytes εληφθησαν αλλα χρειζει14 {806A?} {1 ?} {16 ?}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9247"/>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Flavor Scope Label</name>
    <message>
        <location filename="artisanlib/main.py" line="13236"/>
        <source>OK</source>
        <translation>Ενταξει</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13237"/>
        <source>Grassy</source>
        <translation>Χορτοειδες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13238"/>
        <source>Leathery</source>
        <translation>Δερματοειδες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13239"/>
        <source>Toasty</source>
        <translation>Καμμενο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13240"/>
        <source>Bready</source>
        <translation>Ψωμοειδες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13241"/>
        <source>Acidic</source>
        <translation>Οξινο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13242"/>
        <source>Flat</source>
        <translation>Φλατ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13243"/>
        <source>Fracturing</source>
        <translation>Θρυμματοειδες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13244"/>
        <source>Sweet</source>
        <translation>Γλυκο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13245"/>
        <source>Less Sweet</source>
        <translation>Λιγοτερο γλυκο</translation>
    </message>
</context>
<context>
    <name>Form Caption</name>
    <message>
        <location filename="artisanlib/main.py" line="14586"/>
        <source>Extras</source>
        <translation>Εξτρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15722"/>
        <source>Roast Properties</source>
        <translation>Ιδιοτητες ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16964"/>
        <source>Artisan Platform</source>
        <translation>Πλατφορμα artisan</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17004"/>
        <source>Settings Viewer</source>
        <translation>Ρυθμισεις</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17063"/>
        <source>Serial Log</source>
        <translation>Σειριακο αρχειο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17107"/>
        <source>Error Log</source>
        <translation>Αρχειο σφαλματων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17132"/>
        <source>Message History</source>
        <translation>Ιστορικο μυνηματων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17153"/>
        <source>Keyboard Autosave [a]</source>
        <translation>Πληκτρολογιο αυτοματης αποθυκευσης[a]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17191"/>
        <source>AutoSave Path</source>
        <translation>Διαδρομη αυτοματης αποθυκευσης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17214"/>
        <source>Axes</source>
        <translation>Αξονες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17520"/>
        <source>Roast Calculator</source>
        <translation>Υπολογιστης Ψσιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17760"/>
        <source>Events</source>
        <translation>Συμβαντα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19310"/>
        <source>Roast Phases</source>
        <translation>Φασεις Ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19533"/>
        <source>Cup Profile</source>
        <translation>Προφιλ cup</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19761"/>
        <source>Profile Background</source>
        <translation>Προφιλ Φοντου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20222"/>
        <source>Statistics</source>
        <translation>Στατιστικα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23064"/>
        <source>Designer Config</source>
        <translation>Διαμορφωση σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23603"/>
        <source>Manual Temperature Logger</source>
        <translation>Χειροκινητη καταγραφη δερμοκρασιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23729"/>
        <source>Serial Ports Configuration</source>
        <translation>Διαμορφωση σειριακων θυρων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24511"/>
        <source>Device Assignment</source>
        <translation>Καταχωρηση συσκευης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25762"/>
        <source>Colors</source>
        <translation>Χρωματα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26290"/>
        <source>Wheel Graph Editor</source>
        <translation>Διαμορφωση Γραφηματος Ροδας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26819"/>
        <source>Alarms</source>
        <translation>Συναγερμοι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27467"/>
        <source>Fuji PXR PID Control</source>
        <translation>Ελεγχος Fuji PXR PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28483"/>
        <source>Fuji PXG PID Control</source>
        <translation>Ελεγχος Fuji PXG PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31373"/>
        <source>Delta DTA PID Control</source>
        <translation>Ελεγχος Delta DTA PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31007"/>
        <source>Arduino Control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupBox</name>
    <message>
        <location filename="artisanlib/main.py" line="24533"/>
        <source>Curves</source>
        <translation>Καμπυλες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24542"/>
        <source>LCDs</source>
        <translation>LCDς</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14799"/>
        <source>HUD</source>
        <translation>ΠΡΟΒ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14813"/>
        <source>Input Filters</source>
        <translation>Φιλτρα Εισοδων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15015"/>
        <source>Interpolate</source>
        <translation>Παρεμβολη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15021"/>
        <source>Univariate</source>
        <translation>Μονοπαραγων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15079"/>
        <source>Appearance</source>
        <translation>Εμφανιση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15087"/>
        <source>Resolution</source>
        <translation>Αναλυση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15099"/>
        <source>Sound</source>
        <translation>Ηχος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16336"/>
        <source>Times</source>
        <translation>Χρονοι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17386"/>
        <source>Time Axis</source>
        <translation>Αξονας Χρονου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17388"/>
        <source>Temperature Axis</source>
        <translation>Αξονας Θερμοκρασιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17390"/>
        <source>DeltaBT/DeltaET Axis</source>
        <translation>Αξονες DeltaBT/DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17392"/>
        <source>Legend Location</source>
        <translation>Θεση Εμφανισης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17394"/>
        <source>Grid</source>
        <translation>Πλεγμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17633"/>
        <source>Rate of Change</source>
        <translation>Ρυθμος Μεταβολης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17635"/>
        <source>Temperature Conversion</source>
        <translation>Μετατροπη Θερμοκρασιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17637"/>
        <source>Weight Conversion</source>
        <translation>Μετατροπη Βαρους</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17639"/>
        <source>Volume Conversion</source>
        <translation>Μετατροπη Ογκου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18244"/>
        <source>Event Types</source>
        <translation>Ειδη Συμβαντων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18351"/>
        <source>Default Buttons</source>
        <translation>Προεπιλεγμενα Πληκτρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18394"/>
        <source>Management</source>
        <translation>Διαχειρηση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20339"/>
        <source>Evaluation</source>
        <translation>Αξιολογηση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20341"/>
        <source>Display</source>
        <translation>Εμφανιση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23326"/>
        <source>Initial Settings</source>
        <translation>Αρχικες Ρυθμισεις</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24113"/>
        <source>Input 1</source>
        <translation>Εισοδος 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24127"/>
        <source>Input 2</source>
        <translation>Εισοδος 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24140"/>
        <source>Input 3</source>
        <translation>Εισοδος 3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24153"/>
        <source>Input 4</source>
        <translation>Εισοδος 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24801"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24816"/>
        <source>Arduino TC4</source>
        <translation>Arduino TC4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24823"/>
        <source>External Program</source>
        <translation>Εξωτερικο Προγραμμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24829"/>
        <source>Symbolic Assignments</source>
        <translation>Συμβολικες αναθεσεις</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26057"/>
        <source>Timer LCD</source>
        <translation>Χρονικο LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26060"/>
        <source>ET LCD</source>
        <translation>ET LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26063"/>
        <source>BT LCD</source>
        <translation>BT LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26066"/>
        <source>DeltaET LCD</source>
        <translation>DeltaET LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26069"/>
        <source>DeltaBT LCD</source>
        <translation>DeltaBT LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26072"/>
        <source>Extra Devices / PID SV LCD</source>
        <translation>Εξτρα Συσκευες/PID SV LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26376"/>
        <source>Label Properties</source>
        <translation>Ιδιοτητες Επιγραφων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15048"/>
        <source>Polyfit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14858"/>
        <source>Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24698"/>
        <source>1048 Probe Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24777"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24784"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31010"/>
        <source>p-i-d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31173"/>
        <source>Set Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24749"/>
        <source>Phidget IO</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTML Report Template</name>
    <message>
        <location filename="artisanlib/main.py" line="12657"/>
        <source>Roasting Report</source>
        <translation>Λεπτομερειες Ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12682"/>
        <source>Date:</source>
        <translation>Ημερομηνια:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12686"/>
        <source>Beans:</source>
        <translation>Κοκκοι:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12690"/>
        <source>Size:</source>
        <translation>Mεγεθος:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12694"/>
        <source>Weight:</source>
        <translation>Βαρος:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12698"/>
        <source>Degree:</source>
        <translation>Βαθμος:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12702"/>
        <source>Volume:</source>
        <translation>Ογκος:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12706"/>
        <source>Density:</source>
        <translation>Πυκνοτητα:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12710"/>
        <source>Humidity:</source>
        <translation>Υγρασια:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12714"/>
        <source>Roaster:</source>
        <translation>Ψηστηρι:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12718"/>
        <source>Operator:</source>
        <translation>Χειρηστης:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12722"/>
        <source>Cupping:</source>
        <translation>Cupping:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12726"/>
        <source>Color:</source>
        <translation>Χρωμα:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12734"/>
        <source>CHARGE:</source>
        <translation>ΦΟΡΤΩΜΑ:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12742"/>
        <source>DRY:</source>
        <translation>ΞΗΡΑΝΣΗ:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12746"/>
        <source>FCs:</source>
        <translation>FCε:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12750"/>
        <source>FCe:</source>
        <translation>FCλ:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12754"/>
        <source>SCs:</source>
        <translation>SCε:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12758"/>
        <source>SCe:</source>
        <translation>SCλ:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12762"/>
        <source>DROP:</source>
        <translation>ΞΕΦΟΡΤΩΜΑ:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12766"/>
        <source>COOL:</source>
        <translation>ΨΗΞΗ:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12770"/>
        <source>RoR:</source>
        <translation>RoR:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12774"/>
        <source>ETBTa:</source>
        <translation>ETBTa:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12786"/>
        <source>Drying:</source>
        <translation>Ξηρανση:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12790"/>
        <source>Maillard:</source>
        <translation>Maillard:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12794"/>
        <source>Development:</source>
        <translation>Αναπτυξη:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12798"/>
        <source>Cooling:</source>
        <translation>Ψυξη:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12824"/>
        <source>Roasting Notes</source>
        <translation>Σημειωσεις Ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12834"/>
        <source>Cupping Notes</source>
        <translation>Σημειωσεις cupping</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12738"/>
        <source>TP:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12778"/>
        <source>CM:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12804"/>
        <source>Background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12804"/>
        <source>Events</source>
        <translation type="unfinished">Συμβαντα</translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="artisanlib/main.py" line="6036"/>
        <source>deg/min</source>
        <translation>βαθμ/λεπτ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23238"/>
        <source>ET</source>
        <translation>ΕΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23614"/>
        <source>BT</source>
        <translation>ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2912"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2919"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16066"/>
        <source>at</source>
        <translation>στους</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23067"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15758"/>
        <source>DRY END</source>
        <translation>ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15772"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15787"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15801"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15815"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23080"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1302"/>
        <source>EVENT</source>
        <translation>ΣΥΜΒΑΝ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2709"/>
        <source>BackgroundET</source>
        <translation>ΦοντοET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2713"/>
        <source>BackgroundBT</source>
        <translation>ΦοντοBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2743"/>
        <source>BackgroundDeltaET</source>
        <translation>ΦοντοDeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2747"/>
        <source>BackgroundDeltaBT</source>
        <translation>ΦοντοDeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12910"/>
        <source>d/m</source>
        <translation>d/m</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23095"/>
        <source>Time</source>
        <translation>Χρονος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5519"/>
        <source>BT %1 d/m for %2</source>
        <translation>BT%1d/mfor%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5535"/>
        <source>ET %1 d/m for %2</source>
        <translation>ET%1d/mfor%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7624"/>
        <source>PID SV</source>
        <translation>PID SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7628"/>
        <source>PID %</source>
        <translation>PID %</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7681"/>
        <source>Event #&lt;b&gt;0 &lt;/b&gt;</source>
        <translation>Event#&lt;b&gt;0&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9279"/>
        <source>Event #&lt;b&gt;%1 &lt;/b&gt;</source>
        <translation>Event#&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13801"/>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13803"/>
        <source>City+</source>
        <translation>City+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13805"/>
        <source>Full City</source>
        <translation>Full City</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13807"/>
        <source>Full City+</source>
        <translation>Full City+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13809"/>
        <source>Light French</source>
        <translation>Light French</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13811"/>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14098"/>
        <source>%1 to reach ET target %2</source>
        <translation>%1 to reach ET target%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14109"/>
        <source> at %1</source>
        <translation>at%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14111"/>
        <source>%1 to reach BT target %2</source>
        <translation>%1 to reach BT target%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14121"/>
        <source>%1 after FCs</source>
        <translation>%1afterFCS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14128"/>
        <source>%1 after FCe</source>
        <translation>%1afterFCe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14168"/>
        <source>ET - BT = %1</source>
        <translation>ET-BT=%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14219"/>
        <source>ET - BT = %1%2</source>
        <translation>ET-BT=%1%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14602"/>
        <source>ET Target 1</source>
        <translation>ET Στοχος1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14604"/>
        <source>BT Target 1</source>
        <translation>BT Στοχος1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14606"/>
        <source>ET Target 2</source>
        <translation>ET Στοχος2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14608"/>
        <source>BT Target 2</source>
        <translation>BT Στοχος2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31104"/>
        <source>Mode</source>
        <translation>Τροπος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14612"/>
        <source>ET p-i-d 1</source>
        <translation>ETp-i-d1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14619"/>
        <source>Smooth Deltas</source>
        <translation>Αμβλυνση των Delta</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14627"/>
        <source>Smooth Curves</source>
        <translation>Αμβλυνση Καμπυλων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31125"/>
        <source>min</source>
        <translation>Ελαχιστο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31133"/>
        <source>max</source>
        <translation>Μεγιστο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14873"/>
        <source>Y(x)</source>
        <translation>Y(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15846"/>
        <source>COOL</source>
        <translation>ΨΥΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15889"/>
        <source>Title</source>
        <translation>Τιτλος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15892"/>
        <source>Date</source>
        <translation>Ημερομηνια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15898"/>
        <source>Beans</source>
        <translation>Κοκκοι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15908"/>
        <source>Weight</source>
        <translation>Βαρος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15943"/>
        <source> in</source>
        <translation>Εισοδος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15944"/>
        <source> out</source>
        <translation>Εξοδος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15957"/>
        <source> %</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15942"/>
        <source>Volume</source>
        <translation>Ογκος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15980"/>
        <source>Density</source>
        <translation>Πυκνοτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15995"/>
        <source>per</source>
        <translation>προς</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16015"/>
        <source>Bean Size</source>
        <translation>Μεγεθος Κοκκου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16021"/>
        <source>mm</source>
        <translation>μμ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16023"/>
        <source>Whole Color</source>
        <translation>Χρωμα Κοκκων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16029"/>
        <source>Ground Color</source>
        <translation>Χρωμα Αλεσμενου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16040"/>
        <source>Storage Conditions</source>
        <translation>Συνθηκες Αποθυκευσης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16060"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16058"/>
        <source>Ambient Conditions</source>
        <translation>Συνθηκες Περιβαλλοντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16077"/>
        <source>Roaster</source>
        <translation>Ψηστηρι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16079"/>
        <source>Operator</source>
        <translation>Χειρηστης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16080"/>
        <source>Roasting Notes</source>
        <translation>Σημειωσεις Ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16084"/>
        <source>Cupping Notes</source>
        <translation>Σημειωσεις Cupping</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16161"/>
        <source>Ambient Source</source>
        <translation>Πηγη Περιβαλλοντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16761"/>
        <source>                 Density in: %1  g/l   =&gt;   Density out: %2 g/l</source>
        <translation>Πυκνοτητα εισ:%1g/l=&gt;Πυκνοτητα εξοδ:%2g/l</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16775"/>
        <source>(%1 g/l)</source>
        <translation>(%1 g/l)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17114"/>
        <source>Number of errors found %1</source>
        <translation>Αριθμος σφαλματων%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18126"/>
        <source>Max</source>
        <translation>Μεγιστο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18124"/>
        <source>Min</source>
        <translation>Ελαχιστο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17258"/>
        <source>Rotation</source>
        <translation>Περιστροφη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17281"/>
        <source>Initial Max</source>
        <translation>Αρχικο Μεγιστο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17313"/>
        <source>Step</source>
        <translation>Βημα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17321"/>
        <source>Style</source>
        <translation>Στυλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17331"/>
        <source>Width</source>
        <translation>Πλατος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19793"/>
        <source>Opaqueness</source>
        <translation>Διαφανεια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17522"/>
        <source>Enter two times along profile</source>
        <translation>Εισοδος δυο χρονων κατα μηκος προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17525"/>
        <source>Start (00:00)</source>
        <translation>Εναρξη(00:00)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17526"/>
        <source>End (00:00)</source>
        <translation>Ληξη(00:00)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17545"/>
        <source>Fahrenheit</source>
        <translation>Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17546"/>
        <source>Celsius</source>
        <translation>Κελσιου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17673"/>
        <source>Time syntax error. Time not valid</source>
        <translation>Σφαλμα χρονικης συνταξης.Χρονος μη συμβατος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17677"/>
        <source>Error: End time smaller than Start time</source>
        <translation>Σφαλμα:Χρονος ληξης μικροτερος εναρξης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17694"/>
        <source>Best approximation was made from %1 to %2</source>
        <translation>Βελτιστη προσεγγιση υπηρξε απο %1 εως %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17699"/>
        <source>No profile found</source>
        <translation>Δεν ευρεθει προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17774"/>
        <source>Bars</source>
        <translation>Μπαρες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17846"/>
        <source>Color</source>
        <translation>Χρωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23093"/>
        <source>Marker</source>
        <translation>Σημανση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17850"/>
        <source>Thickness</source>
        <translation>Παχος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17852"/>
        <source>Opacity</source>
        <translation>Διαφανεια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17854"/>
        <source>Size</source>
        <translation>Μεγεθος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17965"/>
        <source>Max buttons per row</source>
        <translation>Μεγιστος αριθμος πληκτρων ανα σειρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17993"/>
        <source>Color Pattern</source>
        <translation>Προτυπο χρωματων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18005"/>
        <source>palette #</source>
        <translation>Προτυπο#</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18120"/>
        <source>Event</source>
        <translation>Συμβαν</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18028"/>
        <source>Action</source>
        <translation>Δραση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18030"/>
        <source>Command</source>
        <translation>Εντολη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18032"/>
        <source>Offset</source>
        <translation>Οφσετ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18034"/>
        <source>Factor</source>
        <translation>Παραγων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20271"/>
        <source>Drying</source>
        <translation>Ξυρανση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20272"/>
        <source>Maillard</source>
        <translation>Maillard</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20273"/>
        <source>Development</source>
        <translation>Αναπτυξη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19534"/>
        <source>Default</source>
        <translation>Αρχικο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19564"/>
        <source>Aspect Ratio</source>
        <translation>Αναλογια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19806"/>
        <source>ET Color</source>
        <translation>Χρωμα ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19813"/>
        <source>BT Color</source>
        <translation>Χρωμα ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19820"/>
        <source>DeltaET Color</source>
        <translation>Χρωμα DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19827"/>
        <source>DeltaBT Color</source>
        <translation>Χρωμα DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19872"/>
        <source>Text Warning</source>
        <translation>Κειμενο Προειδοποιησης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19873"/>
        <source>sec</source>
        <translation>δευτ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20274"/>
        <source>Cooling</source>
        <translation>Ψυξη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23237"/>
        <source>Curviness</source>
        <translation>Καμπυλοτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23250"/>
        <source>Events Playback</source>
        <translation>Αναπαραγωγη Συμβαντων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24011"/>
        <source>Comm Port</source>
        <translation>Θυρα Επικοινωνιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24015"/>
        <source>Baud Rate</source>
        <translation>Ρυθμος Baud</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24021"/>
        <source>Byte Size</source>
        <translation>Mεγεθος Byte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24027"/>
        <source>Parity</source>
        <translation>Ισοτιμια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24034"/>
        <source>Stopbits</source>
        <translation>Stopbits</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24040"/>
        <source>Timeout</source>
        <translation>Xρονικη Ληξη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23764"/>
        <source>Settings for non-Modbus devices</source>
        <translation>Ρυθμισεις για μη Modbus συσκευες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23885"/>
        <source>Slave</source>
        <translation>Υποχειριο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23890"/>
        <source>Register</source>
        <translation>Καταχωρηση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23895"/>
        <source>Float</source>
        <translation>Πλευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23896"/>
        <source>Function</source>
        <translation>Λειτουργια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24001"/>
        <source>Device</source>
        <translation>Συσκευη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24575"/>
        <source>Control ET</source>
        <translation>Ελεγχος ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24579"/>
        <source>Read BT</source>
        <translation>Αναγνωση ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24583"/>
        <source>Type</source>
        <translation>Τυπος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24584"/>
        <source>RS485 Unit ID</source>
        <translation>RS485 Μοναδα ID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24600"/>
        <source>ET Channel</source>
        <translation>Καναλι ΕΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24603"/>
        <source>BT Channel</source>
        <translation>Καναλι ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24623"/>
        <source>AT Channel</source>
        <translation>Καναλι ΑΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24637"/>
        <source>ET Y(x)</source>
        <translation>ΕΤ Υ(χ)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24638"/>
        <source>BT Y(x)</source>
        <translation>ΒΤ Υ(χ)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26309"/>
        <source>Ratio</source>
        <translation>Αναλογια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26316"/>
        <source>Text</source>
        <translation>Κειμενο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26323"/>
        <source>Edge</source>
        <translation>Ακρο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26329"/>
        <source>Line</source>
        <translation>Γραμμη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26338"/>
        <source>Color pattern</source>
        <translation>Προτυπο Χρωματων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26618"/>
        <source> dg</source>
        <translation>dg</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26981"/>
        <source>Enter description</source>
        <translation>Εισαγωγη περιγραφης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27472"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(1-4)</source>
        <translation>Γραφημα ΩΩ:ΔΔ&lt;br&gt;(1-4)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27477"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(5-8)</source>
        <translation>Γραφημα ΩΩ:ΔΔ&lt;br&gt;(5-8)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27479"/>
        <source>Ramp/Soak Pattern</source>
        <translation>Προτυπο Γραφηματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>WARNING</source>
        <translation>ΠΡΟΕΙΔΟΠΟΙΗΣΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>Writing eeprom memory</source>
        <translation>Εγγραφη Μνημης eeprom</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>&lt;u&gt;Max life&lt;/u&gt; 10,000 writes</source>
        <translation>&lt;u&gt;ΜεγΖωη&lt;/u&gt;10,000 εγγραφες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>Infinite read life.</source>
        <translation>Αρχικη αναγνωση ζωης.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>After &lt;u&gt;writing&lt;/u&gt; an adjustment,&lt;br&gt;never power down the pid&lt;br&gt;for the next 5 seconds &lt;br&gt;or the pid may never recover.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>Read operations manual</source>
        <translation>Διαβαστε οδηγιες λειτουργιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29056"/>
        <source>ET Thermocouple type</source>
        <translation>Τυπος θερμοστοιχειου ΕΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29063"/>
        <source>BT Thermocouple type</source>
        <translation>Τυπος θερμοστοιχειου ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28846"/>
        <source>NOTE: BT Thermocouple type is not stored in the Artisan settings</source>
        <translation>ΣΗΜΕΙΩΣΗ:Ο Τυπος Θερμοστοιχειου δεν περιεχεται στις ρυθμισεις του artisan</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28949"/>
        <source>Artisan uses 1 decimal point</source>
        <translation>Το artisan χρησιμοποιει 1 δεκαδικο στοιχειο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28491"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(1-7)</source>
        <translation>Γραφημα (ΩΩ:ΔΔ)&lt;br&gt;(1-7)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28497"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(8-16)</source>
        <translation>Γραφημα (ΩΩ:ΔΔ)&lt;br&gt;(8-16)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28544"/>
        <source>Pattern</source>
        <translation>Προτυπο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28600"/>
        <source>SV (7-0)</source>
        <translation>SV(7-0)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28722"/>
        <source>Write</source>
        <translation>Εγγραφη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28704"/>
        <source>P</source>
        <translation>Ρ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28710"/>
        <source>I</source>
        <translation>Ι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28716"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28950"/>
        <source>Artisan Fuji PXG uses MINUTES:SECONDS units in Ramp/Soaks</source>
        <translation>Το artisan Fuji PXG χρησιμοποιει ΛΕΠΤΑ:ΔΕΥΤΕΡΑ στο γραφημα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31377"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6041"/>
        <source>Curves</source>
        <translation>Καμπυλες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6045"/>
        <source>Delta Curves</source>
        <translation>Delta Καμπυλες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>RoR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>ETBTa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17695"/>
        <source>&lt;b&gt;%1&lt;/b&gt; deg/sec, &lt;b&gt;%2&lt;/b&gt; deg/min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14984"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14985"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4626"/>
        <source>CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8436"/>
        <source>TP</source>
        <translation type="unfinished">ΤΡ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8464"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8498"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10205"/>
        <source>Start recording</source>
        <translation type="unfinished">Εναρξη καταγραφης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10219"/>
        <source>Charge the beans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14635"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14816"/>
        <source>Path Effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14836"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17162"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31037"/>
        <source>Source</source>
        <translation type="unfinished">Πηγη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24673"/>
        <source>1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24677"/>
        <source>2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24681"/>
        <source>3:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24685"/>
        <source>4:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31053"/>
        <source>Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31099"/>
        <source>Lookahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31106"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31107"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31108"/>
        <source>Background</source>
        <translation type="unfinished">Φοντο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31113"/>
        <source>SV Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31116"/>
        <source>SV Slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18128"/>
        <source>Coarse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23928"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23933"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14598"/>
        <source>HUD Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24735"/>
        <source>Raw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24736"/>
        <source>Data rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24737"/>
        <source>Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24756"/>
        <source>ServerId:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24758"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="const/UIconst.py" line="35"/>
        <source>Services</source>
        <translation>Υπηρεσιες</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="36"/>
        <source>Hide %1</source>
        <translation>Αποκρυψη%1</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="37"/>
        <source>Hide Others</source>
        <translation>Αποκρυψη Αλλων</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="38"/>
        <source>Show All</source>
        <translation>Εμφανιση Ολων</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="39"/>
        <source>Preferences...</source>
        <translation>Προτιμησεις...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="61"/>
        <source>Quit %1</source>
        <translation>Κλεισιμο%1</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="141"/>
        <source>About %1</source>
        <translation>Περι%1</translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="artisanlib/main.py" line="17813"/>
        <source>Circle</source>
        <translation>Κυκλος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17814"/>
        <source>Square</source>
        <translation>Τετραγωνο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17815"/>
        <source>Pentagon</source>
        <translation>Πενταγωνο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17816"/>
        <source>Diamond</source>
        <translation>Διαμαντι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17817"/>
        <source>Star</source>
        <translation>Αστερι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17818"/>
        <source>Hexagon 1</source>
        <translation>Εξαγωνο1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17819"/>
        <source>Hexagon 2</source>
        <translation>Εξαγωνο2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17820"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17821"/>
        <source>x</source>
        <translation>χ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17822"/>
        <source>None</source>
        <translation>Κανενα</translation>
    </message>
</context>
<context>
    <name>Menu</name>
    <message>
        <location filename="artisanlib/main.py" line="6923"/>
        <source>CSV...</source>
        <translation>CSV...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6927"/>
        <source>JSON...</source>
        <translation>JSON...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6931"/>
        <source>RoastLogger...</source>
        <translation>Καταγραφικο...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6896"/>
        <source>HH506RA...</source>
        <translation>HH506RA...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6900"/>
        <source>K202...</source>
        <translation>K202...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6904"/>
        <source>K204...</source>
        <translation>K204...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="44"/>
        <source>File</source>
        <translation>Αρχειο</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="47"/>
        <source>New</source>
        <translation>Νεο</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="48"/>
        <source>Open...</source>
        <translation>Ανοιγμα...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="49"/>
        <source>Open Recent</source>
        <translation>Ανοιγμα Προσφατου</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="50"/>
        <source>Import</source>
        <translation>Εισαγωγη</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="51"/>
        <source>Save</source>
        <translation>Αποθηκευση</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="52"/>
        <source>Save As...</source>
        <translation>Αποθηκευση ως...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="53"/>
        <source>Export</source>
        <translation>Εξαγωγη</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="54"/>
        <source>Save Graph</source>
        <translation>Αποθυκευση Γραφηματος</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="55"/>
        <source>Full Size...</source>
        <translation>Πληρες Μεγεθος...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="56"/>
        <source>Roasting Report</source>
        <translation>Εκθεση Ψησιματος</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="57"/>
        <source>Print...</source>
        <translation>Εκτυπωση...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="64"/>
        <source>Edit</source>
        <translation>Επεξεργασια</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="67"/>
        <source>Cut</source>
        <translation>Αποκοπη</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="68"/>
        <source>Copy</source>
        <translation>Αντιγραφη</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="69"/>
        <source>Paste</source>
        <translation>Επικολληση</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="72"/>
        <source>Roast</source>
        <translation>Ψησιμο</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="75"/>
        <source>Properties...</source>
        <translation>Ιδιοτητες...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="76"/>
        <source>Background...</source>
        <translation>Φοντο...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="77"/>
        <source>Cup Profile...</source>
        <translation>Προφιλ cup...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="78"/>
        <source>Temperature</source>
        <translation>Θερμοκρασια</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="79"/>
        <source>Convert to Fahrenheit</source>
        <translation>Μετατροπη σε Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="80"/>
        <source>Convert to Celsius</source>
        <translation>Μετατροπη σε Κελσιου</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="81"/>
        <source>Fahrenheit Mode</source>
        <translation>Κατασταση Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="82"/>
        <source>Celsius Mode</source>
        <translation>Κατασταση Κελσιου</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="86"/>
        <source>Config</source>
        <translation>Διαμορφωση</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="89"/>
        <source>Device...</source>
        <translation>Συσκευη...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="90"/>
        <source>Serial Port...</source>
        <translation>Σειριακη Θυρα...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="91"/>
        <source>Sampling Interval...</source>
        <translation>Διαστημα Δειγματοληψειας...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="93"/>
        <source>Colors...</source>
        <translation>Χρωματα...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="94"/>
        <source>Phases...</source>
        <translation>Φασεις...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="95"/>
        <source>Events...</source>
        <translation>Συμβαντα...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="96"/>
        <source>Statistics...</source>
        <translation>Στατιστικα...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="97"/>
        <source>Axes...</source>
        <translation>Αξονες...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="98"/>
        <source>Autosave...</source>
        <translation>Αυτοματη αποθυκευση...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="99"/>
        <source>Alarms...</source>
        <translation>Συναγερμοι...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="100"/>
        <source>Language</source>
        <translation>Γλωσσα</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="124"/>
        <source>Tools</source>
        <translation>Εργαλεια</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="127"/>
        <source>Designer</source>
        <translation>Σχεδιαστηριο</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="128"/>
        <source>Calculator</source>
        <translation>Υπολογιστης</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="129"/>
        <source>Wheel Graph</source>
        <translation>Γραφημα Ροδας</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="130"/>
        <source>Extras...</source>
        <translation>Εξτρα...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="134"/>
        <source>Help</source>
        <translation>Βοηθεια</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="143"/>
        <source>Documentation</source>
        <translation>Τεκμηριωση</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="145"/>
        <source>Keyboard Shortcuts</source>
        <translation>Συντομευσεις Πληκτρολογιου</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="146"/>
        <source>Errors</source>
        <translation>Σφαλματα</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="147"/>
        <source>Messages</source>
        <translation>Μυνηματα</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="148"/>
        <source>Serial</source>
        <translation>Σειριακο</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="152"/>
        <source>Settings</source>
        <translation>Ρυθμισεις</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="153"/>
        <source>Platform</source>
        <translation>Πλατφορμα</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="154"/>
        <source>Factory Reset</source>
        <translation>Εργοστασιακη επαναφορα</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="83"/>
        <source>Switch Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="92"/>
        <source>Oversampling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="142"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="artisanlib/main.py" line="1495"/>
        <source>HUD OFF</source>
        <translation>ΠΡΟΒΟΛΕΑΣ ΑΝΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1508"/>
        <source>HUD ON</source>
        <translation>ΠΡΟΒΟΛΕΑΣ ΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1587"/>
        <source>Alarm notice</source>
        <translation>Σημειωμα συναγερμου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1592"/>
        <source>Alarm is calling: %1</source>
        <translation>Καλει συναγερμος %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1599"/>
        <source>Alarm trigger button error, description &apos;%1&apos; not a number</source>
        <translation>Σφαλμα πληκτρου συναγερμου,περιγραφη &apos;%1&apos;δεν ειναι αριθμος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1627"/>
        <source>Alarm trigger slider error, description &apos;%1&apos; not a valid number [0-100]</source>
        <translation>Σφαλμα ολισθητηρα συναγερμου,περιγραφη &apos;%1&apos;δεν ειναι πρεπων αριθμος[0-100]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2001"/>
        <source>Save the profile, Discard the profile (Reset), or Cancel?</source>
        <translation>Αποθηκευση προφιλ,Επαναφορα η Απορριψη?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2002"/>
        <source>Profile unsaved</source>
        <translation>Προφιλ μη αποθηκευμενο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2010"/>
        <source>Action canceled</source>
        <translation>Ενεργεια καταργηθηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2076"/>
        <source>Scope has been reset</source>
        <translation>Επαναφορα καταγραφεα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3181"/>
        <source>Time format error encountered</source>
        <translation>Σφαλμα χρονικου τυπου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3282"/>
        <source>Convert profile data to Fahrenheit?</source>
        <translation>Μετατροπη προφιλ σε Φαρεναιτ?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3360"/>
        <source>Convert Profile Temperature</source>
        <translation>Μετατροπη Θερμοκρασιων Προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3319"/>
        <source>Profile changed to Fahrenheit</source>
        <translation>Το Προφιλ μετατραπηκε σε Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3322"/>
        <source>Unable to comply. You already are in Fahrenheit</source>
        <translation>Αδυνατη ενεργεια.Ειναι ηδη Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3362"/>
        <source>Profile not changed</source>
        <translation>Καμια αλλαγη στο Προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3328"/>
        <source>Convert profile data to Celsius?</source>
        <translation>Μετατροπη πληροφοριων προφιλ σε Κελσιου?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3360"/>
        <source>Unable to comply. You already are in Celsius</source>
        <translation>Αδυνατη ενεργεια.Ειναι ηδη Κελσιου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3366"/>
        <source>Profile changed to Celsius</source>
        <translation>Το Προφιλ μετατραπηκε σε Κελσιου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3371"/>
        <source>Convert Profile Scale</source>
        <translation>Μετατροπη κλιμακας Προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3371"/>
        <source>No profile data found</source>
        <translation>Δεν ευρεθησαν πληροφοριες του προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3388"/>
        <source>Colors set to defaults</source>
        <translation>Καθορισμος αρχικων χρωματων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3393"/>
        <source>Colors set to grey</source>
        <translation>Καθορισμος χρωματων σε κλιμακα του Γκρι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3521"/>
        <source>Scope monitoring...</source>
        <translation>Καταγραφεας ενεργοςι...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3564"/>
        <source>Scope stopped</source>
        <translation>Καταγραφεας σταματησε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3629"/>
        <source>Scope recording...</source>
        <translation>Καταγραφεας λειτουργει...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3669"/>
        <source>Scope recording stopped</source>
        <translation>Λειτουργια καταγραφεα σταματησε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3719"/>
        <source>Not enough variables collected yet. Try again in a few seconds</source>
        <translation>Μη συλλογη ικανων πληροφοριων.Δοκιμαστε ξανα  σε μερικα δευτερα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3752"/>
        <source>Roast time starts now 00:00 BT = %1</source>
        <translation>Εναρξη χρονου ψησιματος τωρα 00:00 ΒΤ%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4172"/>
        <source>Scope is OFF</source>
        <translation>Καταγραφεας ανενεργος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3833"/>
        <source>[DRY END] recorded at %1 BT = %2</source>
        <translation>[ΛΗΞΗ ΞΥΡΑΝΣΗΣ]Καταγραφηκε στις %1 ΒΤ = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3892"/>
        <source>[FC START] recorded at %1 BT = %2</source>
        <translation>[ΕΝΡΞΗ FC]Καταγραφηκε στις %1 ΒΤ = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3946"/>
        <source>[FC END] recorded at %1 BT = %2</source>
        <translation>[ΛΗΞΗ FC]Καταγραφηκε στις %1 ΒΤ = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4004"/>
        <source>[SC START] recorded at %1 BT = %2</source>
        <translation>[ΕΝΑΡΞΗ SC]Καταγραφηκε στις %1 ΒΤ = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4061"/>
        <source>[SC END] recorded at %1 BT = %2</source>
        <translation>[ΛΗΞΗ SC]Καταγραφηκε στις %1 ΒΤ = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4141"/>
        <source>Roast ended at %1 BT = %2</source>
        <translation>Τελος Ψησιματος στις %1ΒΤ=%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4205"/>
        <source>[COOL END] recorded at %1 BT = %2</source>
        <translation>[ΛΗΞΗ ΨΥΞΗΣ]Καταγραφηκε στις %1 ΒΤ = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4319"/>
        <source>Event # %1 recorded at BT = %2 Time = %3</source>
        <translation>Συμβαν # %1 καταγραφηκε στις ΒΤ=%2 Χρονος = %3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4334"/>
        <source>Timer is OFF</source>
        <translation>Χρονομετρο Ανενεργο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4366"/>
        <source>Computer Event # %1 recorded at BT = %2 Time = %3</source>
        <translation>Συμβαν Υπολογιστη # %1 καταγραφηκε στις ΒΤ=%2 Χρονος = %3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4445"/>
        <source>Statistics cancelled: need complete profile [CHARGE] + [DROP]</source>
        <translation>Ακυρωση στατιστικων:Χρειζει ολοκληρωμενου προφιλ [ΦΟΡΤΩΜΑ] + [ΞΕΦΟΡΤΩΜΑ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4676"/>
        <source>Unable to move background</source>
        <translation>Αδυνατη μετακινηση φοντου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4735"/>
        <source>No finished profile found</source>
        <translation>Δεν ευρεθησαν ολοκληρωμενα προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4753"/>
        <source>Polynomial coefficients (Horner form):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4756"/>
        <source>Knots:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4759"/>
        <source>Residual:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4762"/>
        <source>Roots:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4766"/>
        <source>Profile information</source>
        <translation>Πληροφοριες Προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4919"/>
        <source>Designer Start</source>
        <translation>Εναρξη Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4161"/>
        <source>Importing a profile in to Designer will decimate
all data except the main [points].
Continue?</source>
        <translation type="obsolete">Εισαγωγη προφιλ στο Σχεδιαστηριο θα διαγραψει ολες πληροφοριες εκτος κυριων σημειων[points].Συνεχεια?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4963"/>
        <source>Designer Init</source>
        <translation>Προκαθορισμενο Σχεδιαστηριο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4963"/>
        <source>Unable to start designer.
Profile missing [CHARGE] or [DROP]</source>
        <translation>Αδυνατη εναρξη σχεδιαστηριου.Ελλειψη στο προφιλ[ΦΟΡΤΩΜΑ] η [ΞΕΦΟΡΤΩΜΑ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5179"/>
        <source>[ CHARGE ]</source>
        <translation>[ΦΟΡΤΩΜΑ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5182"/>
        <source>[ DRY END ]</source>
        <translation>[ΛΗΞΗ ΞΥΡΑΝΣΗΣ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5185"/>
        <source>[ FC START ]</source>
        <translation>[ΕΝΑΡΞΗ FC]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5188"/>
        <source>[ FC END ]</source>
        <translation>[ΛΗΞΗ FC]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5191"/>
        <source>[ SC START ]</source>
        <translation>[ENARJH ΣΨ]]]]

[ΕΝΑΡΞΗ SC]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5194"/>
        <source>[ SC END ]</source>
        <translation>[ΛΗΞΗ SC]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5197"/>
        <source>[ DROP ]</source>
        <translation>[ΞΕΦΟΡΤΩΜΑ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5460"/>
        <source>New profile created</source>
        <translation>Δημιουργηθηκε νεο προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26791"/>
        <source>Open Wheel Graph</source>
        <translation>Ανοιγμα γραφικου ροδας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5647"/>
        <source> added to cupping notes</source>
        <translation>Προστεθηκε σε σημειωσεις cupping</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5653"/>
        <source> added to roasting notes</source>
        <translation>Προστεθηκε σε σημειωσεις ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5920"/>
        <source>Mouse Cross ON: move mouse around</source>
        <translation>Κερσορας ποντικου ΕΝΕΡΓΟΣ:μετακινηστε ποντικι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5932"/>
        <source>Mouse cross OFF</source>
        <translation>Κερσορας ποντικου ΑΝΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8775"/>
        <source>Do you want to reset all settings?</source>
        <translation>Επιθυμειτε επαναφορα ολων των ρυθμισεων?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8776"/>
        <source>Factory Reset</source>
        <translation>Εργοστασιακη επαναφορα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8967"/>
        <source>Exit Designer?</source>
        <translation>Εξοδος Σχεδιαστηριου?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8968"/>
        <source>Designer Mode ON</source>
        <translation>Λειτουργια σχεδιαστηριου ΕΝΕΡΓΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8931"/>
        <source>Extra Event Button Palette</source>
        <translation type="obsolete">Εξτρα Πληκτρο Συμβαντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9145"/>
        <source>Keyboard moves turned ON</source>
        <translation>Κινηση Πληκτρολογιου ΕΝΕΡΓΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9154"/>
        <source>Keyboard moves turned OFF</source>
        <translation>Κινηση Πληκτρολογιου ΑΝΕΝΕΡΓΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9236"/>
        <source>Profile %1 saved in: %2</source>
        <translation>Προφιλ %1 αποθυκευτηκε σε %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9243"/>
        <source>Empty path or box unchecked in Autosave</source>
        <translation>Κενη Διαδρομη η κουτι αμαρκαριστο στην Αυτοματη Αποθυκευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9250"/>
        <source>&lt;b&gt;[ENTER]&lt;/b&gt; = Turns ON/OFF Keyboard Shortcuts</source>
        <translation>&lt;b&gt;[ΕΙΣΑΓΩΓΗ]&lt;/b&gt; = ΑΝΟΙΓΕΙ/ΚΛΕΙΝΕΙ Συντομευσεις Πληκτρολογιου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9251"/>
        <source>&lt;b&gt;[SPACE]&lt;/b&gt; = Choses current button</source>
        <translation>&lt;b&gt;[ΔΙΑΣΤΗΜΑ]&lt;/b&gt; = Επιλογη τωρινου πληκτρου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9252"/>
        <source>&lt;b&gt;[LEFT]&lt;/b&gt; = Move to the left</source>
        <translation>&lt;b&gt;[ΑΡΙΣΤΕΡΑ]&lt;/b&gt; = Μετακινηση Αριστερα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9253"/>
        <source>&lt;b&gt;[RIGHT]&lt;/b&gt; = Move to the right</source>
        <translation>&lt;b&gt;[ΔΕΞΙΑ]&lt;/b&gt; = Μετακινηση Δεξια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9254"/>
        <source>&lt;b&gt;[a]&lt;/b&gt; = Autosave</source>
        <translation>&lt;b&gt;[a]&lt;/b&gt; = Aυτοματη Αποθυκευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9255"/>
        <source>&lt;b&gt;[CRTL N]&lt;/b&gt; = Autosave + Reset + START</source>
        <translation>&lt;b&gt;[CRTL N]&lt;/b&gt; = Αυτοματη Αποθυκευση + Επαναφορα + ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9256"/>
        <source>&lt;b&gt;[t]&lt;/b&gt; = Mouse cross lines</source>
        <translation>&lt;b&gt;[t]&lt;/b&gt; = Μετακινηση Σταυρου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9258"/>
        <source>&lt;b&gt;[b]&lt;/b&gt; = Shows/Hides Extra Event Buttons</source>
        <translation>&lt;b&gt;[b]&lt;/b&gt; = Εμφανιση/Αποκρυψη Πληκτρων Εξτρα Συμβαντων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9259"/>
        <source>&lt;b&gt;[s]&lt;/b&gt; = Shows/Hides Event Sliders</source>
        <translation>&lt;b&gt;[s]&lt;/b&gt; = Εμφανιση/Αποκρυψη Σλαιντερ Συμβαντων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9260"/>
        <source>&lt;b&gt;[i]&lt;/b&gt; = Retrieve Weight In from Scale</source>
        <translation>&lt;b&gt;[i]&lt;/b&gt; = Ανακτηση Βαρους Εισοδου απο Ζυγο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9261"/>
        <source>&lt;b&gt;[o]&lt;/b&gt; = Retrieve Weight Out from Scale</source>
        <translation>&lt;b&gt;[o]&lt;/b&gt; = Ανακτηση Βαρους Εξοδου απο Ζυγο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9262"/>
        <source>&lt;b&gt;[0-9]&lt;/b&gt; = Changes Event Button Palettes</source>
        <translation>&lt;b&gt;[0-9]&lt;/b&gt; = Αλλαγη Προτυπων Πληκτρων Συμβαντων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9263"/>
        <source>&lt;b&gt;[;]&lt;/b&gt; = Application ScreenShot</source>
        <translation>&lt;b&gt;[;]&lt;/b&gt; = Φωτογραφια Εφαρμογης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9264"/>
        <source>&lt;b&gt;[:]&lt;/b&gt; = Desktop ScreenShot</source>
        <translation>&lt;b&gt;[:]&lt;/b&gt; = Φωτογραφια Επιφανειας Εργασιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9268"/>
        <source>Keyboard Shotcuts</source>
        <translation>Συντομευσεις Πληκτρολογιου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9349"/>
        <source>Event #%1:  %2 has been updated</source>
        <translation>Συμβαν #%1: %2 εχει ενημερωθει</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9436"/>
        <source>Save</source>
        <translation>Αποθυκευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9445"/>
        <source>Select Directory</source>
        <translation>Επιλεξτε Ευρετηριο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16692"/>
        <source>No profile found</source>
        <translation>Δεν ευρεθει Προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9498"/>
        <source>%1 has been saved. New roast has started</source>
        <translation>%1 εχει αποθυκευτει.Εναρξη νεου ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9680"/>
        <source>Invalid artisan format</source>
        <translation>Μη αποδεκτη φορμα artisan</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9540"/>
        <source>%1  loaded </source>
        <translation>%1 φορτωθηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9676"/>
        <source>Background %1 loaded successfully %2</source>
        <translation>Το Φοντο %1  φορτωθηκε  με επιτυχια %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9790"/>
        <source>Artisan CSV file loaded successfully</source>
        <translation>Αρχειο artisan csv φορτωθηκε με επιτυχια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10369"/>
        <source>To load this profile the extra devices configuration needs to be changed.
Continue?</source>
        <translation>Για επιτυχη φορτωση του προφιλ χρειαζεται αλλαγη διαμορφωσης των εξτρα συσκευων.Συνεχεια?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10370"/>
        <source>Found a different number of curves</source>
        <translation>Ευρεθει διαφορετικος αριθμος καμπυλων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11063"/>
        <source>Save Profile</source>
        <translation>Αποθυκευση Προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11070"/>
        <source>Profile saved</source>
        <translation>Προφιλ Αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11109"/>
        <source>Cancelled</source>
        <translation>Ακυρωθηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11086"/>
        <source>Readings exported</source>
        <translation>Εγινε εξαγωγη δεδομενων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11094"/>
        <source>Export CSV</source>
        <translation>Εξαγωγη CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11097"/>
        <source>Export JSON</source>
        <translation>Εξαγωγη JSON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11100"/>
        <source>Export RoastLogger</source>
        <translation>Εξαγωγη Καταγραφεα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11107"/>
        <source>Readings imported</source>
        <translation>Δεδομενα εισηχθησαν</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11115"/>
        <source>Import CSV</source>
        <translation>Εισαγωγη CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11118"/>
        <source>Import JSON</source>
        <translation>Εισαγωγη JSON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11121"/>
        <source>Import RoastLogger</source>
        <translation>Εισαγωγη  Καταγραφεα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13524"/>
        <source>Sampling Interval</source>
        <translation>Διαστημα Δειγματοληψειας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13524"/>
        <source>Seconds</source>
        <translation>Δευτερολεπτα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13730"/>
        <source>Alarm Config</source>
        <translation>Διαμορφωση Συναγερμου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13730"/>
        <source>Alarms are not available for device None</source>
        <translation>Συναγερμοι δεν ειναι διαθεσιμοι για συσκευη Καμια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13783"/>
        <source>Switch Language</source>
        <translation>Αλλαγη Γλωσσας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13783"/>
        <source>Language successfully changed. Restart the application.</source>
        <translation>Επιτυχης αλλαγη γλωσσας.Επανεκκινηστε Εφαρμογη.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13815"/>
        <source>Import K202 CSV</source>
        <translation>Εισαγωγη Κ202 CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13865"/>
        <source>K202 file loaded successfully</source>
        <translation>Αρχειο Κ202 φορτωθηκε με επιτυχια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13879"/>
        <source>Import K204 CSV</source>
        <translation>Εισαγωγη K204 CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13943"/>
        <source>K204 file loaded successfully</source>
        <translation>Αρχειο Κ204 φορτωθηκε με επιτυχια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13957"/>
        <source>Import HH506RA CSV</source>
        <translation>Εισαγωγη HH506RA CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14006"/>
        <source>HH506RA file loaded successfully</source>
        <translation>Αρχειο HH506RA φορτωθηκε με επιτυχια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14047"/>
        <source>Save Graph as PNG</source>
        <translation>Αποθυκευση Γραφικου ως PNG</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14054"/>
        <source>%1  size(%2,%3) saved</source>
        <translation>%1 μεγεθος(%2,%3)αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14063"/>
        <source>Save Graph as SVG</source>
        <translation>Αποθυκευση Γραφικου ως SVG</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14068"/>
        <source>%1 saved</source>
        <translation>%1 Αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14276"/>
        <source>Invalid Wheel graph format</source>
        <translation>Μη αποδεκτο φορμα Γραφικου Ροδας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14279"/>
        <source>Wheel Graph succesfully open</source>
        <translation>Γραφικο Ροδας ανοιξε με επιτυχια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14298"/>
        <source>Return the absolute value of x.</source>
        <translation>Επιστροφη απολυτης τιμης χ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14299"/>
        <source>Return the arc cosine (measured in radians) of x.</source>
        <translation>Επιστροφη arc cosine του χ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14300"/>
        <source>Return the arc sine (measured in radians) of x.</source>
        <translation>Επιστροφη arc cine του χ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14301"/>
        <source>Return the arc tangent (measured in radians) of x.</source>
        <translation>Επιστροφη arc tangent του χ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14302"/>
        <source>Return the cosine of x (measured in radians).</source>
        <translation>Επιστροφη  cosine του χ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14303"/>
        <source>Convert angle x from radians to degrees.</source>
        <translation>Μετατροπη γωνιας χ απο radian σε degrees.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14304"/>
        <source>Return e raised to the power of x.</source>
        <translation>Eπιστροφη e raised στην δυναμη του χ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14305"/>
        <source>Return the logarithm of x to the given base. </source>
        <translation>Επιστροφη λογαριθμου χ στην δεδομενη βαση.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14306"/>
        <source>Return the base 10 logarithm of x.</source>
        <translation>Επιστροφη βασης 10 σε λογαριθμο του χ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14309"/>
        <source>Return x**y (x to the power of y).</source>
        <translation>Επιστροφη x**y.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14310"/>
        <source>Convert angle x from degrees to radians.</source>
        <translation>Μετατροπη γωνιας χ απο degrees σε radians.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14311"/>
        <source>Return the sine of x (measured in radians).</source>
        <translation>Επιστροφη sine του χ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14312"/>
        <source>Return the square root of x.</source>
        <translation>Επιστροφη τετραγωνικης ριζας του x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14313"/>
        <source>Return the tangent of x (measured in radians).</source>
        <translation>Eπιστροφη tangent του x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14316"/>
        <source>previous ET value</source>
        <translation>προηγουμενη τιμη ΕΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14317"/>
        <source>previous BT value</source>
        <translation>προηγουμενη τιμη ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14318"/>
        <source>previous Extra #1 T1 value</source>
        <translation>προηγουμενη τιμη Εξτρα #1 Τ1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14319"/>
        <source>previous Extra #1 T2 value</source>
        <translation>προηγουμενη τιμη Εξτρα #1 Τ2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14320"/>
        <source>previous Extra #2 T1 value</source>
        <translation>προηγουμενη τιμη Εξτρα #2 Τ1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14321"/>
        <source>previous Extra #2 T2 value</source>
        <translation>προηγουμενη τιμη Εξτρα #2 Τ2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14328"/>
        <source>MATHEMATICAL FUNCTIONS</source>
        <translation>MAΘΗΜΑΤΙΚΕΣ ΛΕΙΤΟΥΡΓΙΕΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14330"/>
        <source>SYMBOLIC VARIABLES</source>
        <translation>ΣΥΜΒΟΛΙΚΑ ΚΥΜΑΙΝΟΜΕΝΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14332"/>
        <source>Symbolic Functions</source>
        <translation>Συμβολικες Λειτουργιες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14470"/>
        <source>Save Palettes</source>
        <translation>Αποθυκευση Προτυπων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14474"/>
        <source>Palettes saved</source>
        <translation>Προτυπα Αποθυκευτηκαν</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14514"/>
        <source>Invalid palettes file format</source>
        <translation>Μη Αποδεκτη φορμα Προτυπων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14517"/>
        <source>Palettes loaded</source>
        <translation>Προτυπα Φορτωθηκαν</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14529"/>
        <source>Load Palettes</source>
        <translation>Φορτωση Προτυπων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14550"/>
        <source>Alarms loaded</source>
        <translation>Συναγερμοι Φορτωθηκαν</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15526"/>
        <source>Interpolation failed: no profile available</source>
        <translation>Σφαλμα:Δεν ευρεθει προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15535"/>
        <source>Sound turned ON</source>
        <translation>Ηχος ΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15539"/>
        <source>Sound turned OFF</source>
        <translation>Ηχος ΑΝΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15705"/>
        <source>[ET target 1 = %1] [BT target 1 = %2] [ET target 2 = %3] [BT target 2 = %4]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16689"/>
        <source>Event #%1 added</source>
        <translation>Συμβαν #%1 προστεθηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16712"/>
        <source> Event #%1 deleted</source>
        <translation>Συμβαν #%1 διαγραφηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16717"/>
        <source>No events found</source>
        <translation>Δεν ευρεθησαν συμβαντα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16952"/>
        <source>Roast properties updated but profile not saved to disk</source>
        <translation>Ιδιοτητες Ψησιματος ενημερωθηκαν αλλα δεν εγινε αποθυκευση του Προφιλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17199"/>
        <source>Autosave ON. Prefix: %1</source>
        <translation>Αυτοματη Αποθυκευση ΕΝΕΡΓΗ.Προκαθορισμενο: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17203"/>
        <source>Autosave OFF</source>
        <translation>Αυτοματη Αποθυκευση ΑΝΕΝΕΡΓΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17491"/>
        <source>xlimit = (%3,%4) ylimit = (%1,%2) zlimit = (%5,%6)</source>
        <translation>οριοχ=(%3,%4) οριοy = (%1,%2) οριοz = (%5,%6)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18536"/>
        <source>&lt;b&gt;Event&lt;/b&gt; hide or show the corresponding slider</source>
        <translation>&lt;b&gt;Συμβαν&lt;/b&gt;αποκρυψη η εμφανιση του αντιστοιχου σλαιντερ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18537"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action on slider release</source>
        <translation>&lt;b&gt;Ενεργεια&lt;/b&gt;Ενεργοποιηστε κατα την απελευθερωση του σλαιντερ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18538"/>
        <source>&lt;b&gt;Command&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt;)</source>
        <translation>&lt;b&gt;Εντολη&lt;/b&gt;εξαρταται απο ειδος εντολης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19294"/>
        <source>Serial Command: ASCII serial command or binary a2b_uu(serial command)</source>
        <translation>Σειριακη Εντολη:ASCII σειριακη εντολη η δυαδικο a2b_uu</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19297"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) writes values to the registers in slaves specified by the given ids</source>
        <translation>Eγγραφη Εντολης Modbus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19298"/>
        <source>DTA Command: Insert Data address : value, ex. 4701:1000 and sv is 100. always multiply with 10 if value Unit: 0.1 / ex. 4719:0 stops heating</source>
        <translation>Εντολη DTA:Χρειζει εισαγωγης τιμων,δεδομενων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18542"/>
        <source>&lt;b&gt;Offset&lt;/b&gt; added as offset to the slider value</source>
        <translation>&lt;b&gt;Οφσετ&lt;/b&gt;προστεθηκε ως οφσετ στην τιμη του σλαιντερ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18543"/>
        <source>&lt;b&gt;Factor&lt;/b&gt; multiplicator of the slider value</source>
        <translation>&lt;b&gt;Παραγων&lt;/b&gt;πολλαπλασιαστικο τιμης σλαιντερ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19301"/>
        <source>Event custom buttons</source>
        <translation>Πληκτρα Συμβαντων Χρηστη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19265"/>
        <source>Event configuration saved</source>
        <translation>Διαμορφωση Συμβαντων Αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19268"/>
        <source>Found empty event type box</source>
        <translation>Ευρεθει κενο κουτι τυπου συμβαντων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19288"/>
        <source>&lt;b&gt;Button Label&lt;/b&gt; Enter \n to create labels with multiple lines.</source>
        <translation>&lt;b&gt;Επιγραφη Πληκτρου&lt;/b&gt;Εισαγετε \n για δημιουργια επιγραφων με πολλαπλες γραμμες.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19289"/>
        <source>&lt;b&gt;Event Description&lt;/b&gt; Description of the Event to be recorded.</source>
        <translation>&lt;b&gt;Περιγραφη Συμβαντων&lt;/b&gt;Περιγραφει το συμβαν προς καταγραφη.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19290"/>
        <source>&lt;b&gt;Event type&lt;/b&gt; Type of event to be recorded.</source>
        <translation>&lt;b&gt;Τυπος Συμβαν&lt;/b&gt;Τυπος του συμβαντος προς καταγραφη.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19291"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-10) to be recorded</source>
        <translation>&lt;b&gt;Τιμη Συμβαντος&lt;/b&gt;Τιμη του συμβαντος (1-10) προς καταγραφη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19292"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action at the time of the event</source>
        <translation>&lt;b&gt;Ενεργεια&lt;/b&gt;Επιλεξτε ενεργεια κατα το συμβαν</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19293"/>
        <source>&lt;b&gt;Documentation&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by the event value):</source>
        <translation>&lt;b&gt;Τεκμηριωση&lt;/b&gt;Εξαρταται απο ειδος ενεργειας:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19295"/>
        <source>Call Program: A program/script path (absolute or relative)</source>
        <translation>Aνακληση Προγραμματος:Διαδρομη προγραμματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19296"/>
        <source>Multiple Event: Adds events of other button numbers separated by a comma: 1,2,3, etc.</source>
        <translation>Πολλαπλα Συμβαντα:Προσθετει συμβαντα η αλλους αριθμους πληκτρων χωρισμενα με κομμα:1,2,3, κλπ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19299"/>
        <source>&lt;b&gt;Button Visibility&lt;/b&gt; Hides/shows individual button</source>
        <translation>&lt;b&gt;Εμφανιση Πληκτρων&lt;/b&gt;Εμφανιση/Αποκρυψη μεμονομενων πληκτρων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19300"/>
        <source>&lt;b&gt;Keyboard Shorcut: &lt;/b&gt; [b] Hides/shows Extra Button Rows</source>
        <translation>&lt;b&gt;Συντομευση Πληκτρολογιου&lt;/b&gt;Εμφανιση/Αποκρυψη Εξτρα Σειρων Πληκτρων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19516"/>
        <source>Phases changed to %1 default: %2</source>
        <translation>Αλλαγη Φασεων σε %1 προκαθορισμενο: %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19651"/>
        <source>Background profile not found</source>
        <translation>Προφιλ Φοντου δεν ευρεθει</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19656"/>
        <source>Background does not match number of labels</source>
        <translation>Το Φοντο Δεν ταιριαζει ως προς αριθμο επιγραφων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19974"/>
        <source>Playback Aid set ON at %1 secs</source>
        <translation>Βοηθημα αναπαραγωγης ΕΝΕΡΓΟ στα %1 δευτερολεπτα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19982"/>
        <source>No profile background found</source>
        <translation>Προφιλ Φοντου δεν ευρεθει</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20076"/>
        <source>Reading background profile...</source>
        <translation>Αναγνωση προφιλ φοντου...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23346"/>
        <source>Not enough time points for an ET curviness of %1. Set curviness to %2</source>
        <translation>Μη επαρκη χρονικα σημεια για καμπυλη ΕΤ του %1.Επιλεξτε καμπυλη του %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23556"/>
        <source>Designer Config</source>
        <translation>Διαμορφωση Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23353"/>
        <source>Not enough time points for an BT curviness of %1. Set curviness to %2</source>
        <translation>Μη επαρκη χρονικα σημεια για καμπυληΒΤ του %1.Επιλεξτε καμπυλη του %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23548"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23549"/>
        <source>DRY END</source>
        <translation>ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23550"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23551"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23552"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23553"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23554"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23368"/>
        <source>Incorrect time format. Please recheck %1 time</source>
        <translation>Μη αποδεκτο χρονικο φορμα.Παρακαλω ελεγξτε %1 χρονο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23555"/>
        <source>Times need to be in ascending order. Please recheck %1 time</source>
        <translation>Χρονοι χρειζουν ανοδικη κατευθυνση.Παρακαλω ελεγξτε %1 χρονο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23520"/>
        <source>Designer has been reset</source>
        <translation>Επαναφορα Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23873"/>
        <source>These serial settings are used for all Modbus communication.</source>
        <translation type="obsolete">Ρυθμισεις σειριακες για χρηση ολων επικοινωνιων Modbus.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23874"/>
        <source>The MODBUS device corresponds to input channels 1 and 2.</source>
        <translation type="obsolete">Συσκευη MODBUS αντιστοιχει στα καναλια εισαγωγης 1 και 2.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23875"/>
        <source>The +MODBUS_34 extra device adds input channels 3 and 4.</source>
        <translation type="obsolete">Συσκευη +MODBUS_34 προσθετει καναλια καναλια εισαγωγης 3 και 4.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23876"/>
        <source>Inputs with slave id set to 0 are turned off.</source>
        <translation type="obsolete">Εισερχομενα με slave id καθορισμαενα στο 0 ειναι ανενεργα.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23877"/>
        <source>Modbus function 3 &apos;read holding register&apos; is the standard.</source>
        <translation type="obsolete">Λειτουργια 3 modbus &apos;read holding register&apos; ειναι στανταρτ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23878"/>
        <source>Modbus function 4 triggers the use of &apos;read input register&apos;.</source>
        <translation type="obsolete">Λειτουργια 4 Modbus καθοριζει τη χρηση &apos;read input register&apos;.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23879"/>
        <source>Input registers (fct 4) usually are from the range 30000-39999.</source>
        <translation type="obsolete">Συνηθη δεδομενα εισαγωγης(fct 4) στη κλιμακα 30000-39999.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23880"/>
        <source>Most devices hold data in 2 byte integer registers.</source>
        <translation type="obsolete">Οι περισσοτερες συσκευες παρακρατουν δεδομενα σε μητρωα 2 bytes.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23882"/>
        <source>In that case you have to use the symbolic assignment &apos;x/10&apos;.</source>
        <translation type="obsolete">Σε αυτη την περιπτωση χρησιμοποιηστε symbolic assignment &apos;x/10&apos;.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23883"/>
        <source>Few devices hold data as 4 byte floats in two registers.</source>
        <translation type="obsolete">Λιγες συσκευες κρατουν δεδομενα σε διπλα μητρωα των 4 bytes.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23952"/>
        <source>Tick the Float flag in this case.</source>
        <translation>Κλικαρετε τη σημαια float σε αυτη την περιπτωση.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24396"/>
        <source>Serial Port Settings: %1, %2, %3, %4, %5, %6</source>
        <translation>Ρυθμισεις Σειριακων Θυρων: %1,%2,%3,%4,%5,%6</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20233"/>
        <source>Port scan on this platform not yet supported</source>
        <translation type="obsolete">Ανιχνευση θυρας σε αυτη την πλατφορμα δεν υποστηριζεται</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25038"/>
        <source>External program</source>
        <translation>Εξωτερικο Προγραμμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25245"/>
        <source>Device not set</source>
        <translation>Συσκευη δεν καθοριστηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25302"/>
        <source>PID to control ET set to %1 %2 ; PID to read BT set to %3 %4</source>
        <translation>PID για ελεγχο ΕΤ καθοριστηκε σε %1 %2; PID για αναγνωση ΒΤ καθοριστηκε σε %3 %4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25526"/>
        <source>Device set to %1. Now, check Serial Port settings</source>
        <translation>Συσκευη καθοριστηκε σε %1.Τωρα ελεγξτε ρυθμισεις θυρων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25621"/>
        <source>Device set to %1. Now, chose serial port</source>
        <translation>Συσκευη καθοριστηκε σε %1.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25378"/>
        <source>Device set to CENTER 305, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation>Συσκευη καθοριστηκε στο CENTER 305, που ειναι ιδιο με CENTER 306.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25432"/>
        <source>Device set to %1, which is equivalent to CENTER 309. Now, chose serial port</source>
        <translation>Συσκευη καθοριστηκε στο %1, που ειναι ιδιο με CENTER 309.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25459"/>
        <source>Device set to %1, which is equivalent to CENTER 303. Now, chose serial port</source>
        <translation>Συσκευη καθοριστηκε στο %1, που ειναι ιδιο με CENTER 303.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25441"/>
        <source>Device set to %1, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation>Συσκευη καθοριστηκε στο %1, που ειναι ιδιο με CENTER 306.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25468"/>
        <source>Device set to %1, which is equivalent to Omega HH506RA. Now, chose serial port</source>
        <translation>Συσκευη καθοριστηκε στο %1, που ειναι ιδιο με Omega HH506RA.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25569"/>
        <source>Device set to %1, which is equivalent to Omega HH806AU. Now, chose serial port</source>
        <translation>Συσκευη καθοριστηκε στο %1, που ειναι ιδιο με Omega HH806AU.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25603"/>
        <source>Device set to %1</source>
        <translation>Συσκευη καθοριστηκε στο %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25499"/>
        <source>Device set to %1%2</source>
        <translation>Συσκευη καθοριστηκε στο %1%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25560"/>
        <source>Device set to %1, which is equivalent to CENTER 302. Now, chose serial port</source>
        <translation>Συσκευη καθοριστηκε στο %1, που ειναι ιδιο με CENTER 302.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26276"/>
        <source>Color of %1 set to %2</source>
        <translation>Το χρωμα του %1 καθοριστηκε σε %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26781"/>
        <source>Save Wheel graph</source>
        <translation>Αποθυκευση Γραφηματος Ροδας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26785"/>
        <source>Wheel Graph saved</source>
        <translation>Γραφημα Ροδας Αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27043"/>
        <source>Load Alarms</source>
        <translation>Φορτωση Συναγερμων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27081"/>
        <source>Save Alarms</source>
        <translation>Αποθυκευση Συναγερμων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27113"/>
        <source>&lt;b&gt;Status:&lt;/b&gt; activate or deactive alarm</source>
        <translation>&lt;b&gt;Θεσις&lt;/b&gt;Ενεργοποιηση η απενεργοποιηση συναγερμου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27114"/>
        <source>&lt;b&gt;If Alarm:&lt;/b&gt; alarm triggered only if the alarm with the given number was triggered before. Use 0 for no guard.</source>
        <translation>&lt;b&gt;Εαν Συναγερμος&lt;/b&gt;συναγερμος ανταποκρινεται μονο εαν ο συγκεκριμενος εχει ανταποκριθει πρωτυτερα.Χρηση 0 για μη προφυλαξη.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27116"/>
        <source>&lt;b&gt;From:&lt;/b&gt; alarm only triggered after the given event</source>
        <translation>&lt;b&gt;Απο&lt;/b&gt;συναγερμος καθοριζεται μετα απο δεδομενο συμβαν</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27117"/>
        <source>&lt;b&gt;Time:&lt;/b&gt; if not 00:00, alarm is triggered mm:ss after the event &apos;From&apos; happend</source>
        <translation>&lt;b&gt;Χρονος&lt;/b&gt;εαν οχι 00:00,συναγερμος ανταποκρινεταιι λλ:δδ μετα το συμβαν &apos;Απο&apos;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27118"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; the temperature source that is observed</source>
        <translation>&lt;b&gt;Πηγη&lt;/b&gt;Παρατηρηση πηγης θερμοκρασιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27119"/>
        <source>&lt;b&gt;Condition:&lt;/b&gt; alarm is triggered if source rises above or below the specified temperature</source>
        <translation>&lt;b&gt;Κατασταση&lt;/b&gt;συναγερμος ανταποκριναταιι εαν η πηγη ειναι πανω η κατω απο ειδικη θερμοκρασια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27120"/>
        <source>&lt;b&gt;Temp:&lt;/b&gt; the speficied temperature limit</source>
        <translation>&lt;b&gt;Θερμοκρασια&lt;/b&gt;Προκαθορισμενο οριο θερμοκρασιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27121"/>
        <source>&lt;b&gt;Action:&lt;/b&gt; if all conditions are fulfilled the alarm triggeres the corresponding action</source>
        <translation>&lt;b&gt;Ενεργεια&lt;/b&gt;Εαν εχουν επιτευχθει ολα τα ζητουμενα οι συναγερμοι ανταποκρινονται σε δεδομενη θερμοκρασια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22506"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button (if 0 the COOL event is triggered ) or the new value of the slider</source>
        <translation type="obsolete">&lt;b&gt;Περιγραφη&lt;/b&gt;κειμενο.ονομα προγραμματος,αριθμος πληκτρου συμβαντος η νεα τιμη σλαιντερ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27123"/>
        <source>&lt;b&gt;NOTE:&lt;/b&gt; each alarm is only triggered once</source>
        <translation>&lt;b&gt;ΣΗΜΕΙΩΣΗ&lt;/b&gt;καθε συναγερμος ανταποκρινεται απαξ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30102"/>
        <source>OFF</source>
        <translation>ΑΝΕΝΕΡΓΟ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30095"/>
        <source>CONTINUOUS CONTROL</source>
        <translation>ΣΥΝΕΧΗΣ ΕΛΕΓΧΟΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30108"/>
        <source>ON</source>
        <translation>ΕΝΕΡΓΟ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30107"/>
        <source>STANDBY MODE</source>
        <translation>ΚΑΤΑΣΤΑΣΗ STANDBY</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28144"/>
        <source>The rampsoak-mode tells how to start and end the ramp/soak</source>
        <translation>Θεση γραφηματος καθοριζει πως αρχιζει και τελειωνει το γραφημα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28145"/>
        <source>Your rampsoak mode in this pid is:</source>
        <translation>Η κατασταση γραφηματος σε αυτο το pid ειναι:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28146"/>
        <source>Mode = %1</source>
        <translation>Κατασταση = %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28148"/>
        <source>Start to run from PV value: %1</source>
        <translation>Εναρξη απο τιμη PV: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28149"/>
        <source>End output status at the end of ramp/soak: %1</source>
        <translation>Τελος θεσης εξοδου στη ληξη ραμπας: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28150"/>
        <source>Output status while ramp/soak operation set to OFF: %1</source>
        <translation>Θεση εξοδου οταν λειτουργια ραμπασ εχει καθοριστει ΑΝΕΝΕΡΓΗ: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28151"/>
        <source>
Repeat Operation at the end: %1</source>
        <translation>Επαναληψη Λειτουργιας στη ληξη:%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28153"/>
        <source>Recomended Mode = 0</source>
        <translation>Συνιστωμενι θεση = 0</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28154"/>
        <source>If you need to change it, change it now and come back later</source>
        <translation>Εαν χριζει αλλαγης,αλλαξτε το τωρα και επανελθετε αργοτερα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28155"/>
        <source>Use the Parameter Loader Software by Fuji if you need to

</source>
        <translation>Χρησιμοποιηστε προγραμμα φορτωσης παραμετρων της Fuji</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28156"/>
        <source>Continue?</source>
        <translation>Συνεχεια?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28157"/>
        <source>RampSoak Mode</source>
        <translation>Θεση Ραμπας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29512"/>
        <source>Current sv = %1. Change now to sv = %2?</source>
        <translation>Τρεχων sv = %1.Αλλαξτε τωρα σε sv = %2?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29574"/>
        <source>Change svN</source>
        <translation>Αλλαξτε svN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29573"/>
        <source>Current pid = %1. Change now to pid =%2?</source>
        <translation>Τρεχων pid = %1.Αλλαξτε τωρα σε pid = %2?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30328"/>
        <source>Ramp Soak start-end mode</source>
        <translation>Θεση εναρξης-Ληξης ραμπας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30234"/>
        <source>Pattern changed to %1</source>
        <translation>Προτυπο αλλαχτηκε σε %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30236"/>
        <source>Pattern did not changed</source>
        <translation>Προτυπο δεν αλλαξε</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30239"/>
        <source>Ramp/Soak was found ON! Turn it off before changing the pattern</source>
        <translation>Ενεργη Ραμπα!Απενεργοποιηστε πριν την αλλαγη προτυπου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30241"/>
        <source>Ramp/Soak was found in Hold! Turn it off before changing the pattern</source>
        <translation> Ραμπα σε αναμονη!Απενεργοποιηστε πριν την αλλαγη προτυπου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30582"/>
        <source>Activate PID front buttons</source>
        <translation>Ενεργοποιηστε εμπροσθια πληκτρα PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30582"/>
        <source>Remember SV memory has a finite
life of ~10,000 writes.

Proceed?</source>
        <translation>Θυμηθειτε οτι η μνημη SV εχει διαρκεια ~10,000 εγγραφες.Συνεχεια?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30684"/>
        <source>RS ON</source>
        <translation>RS ΕΝΕΡΓΟ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30686"/>
        <source>RS OFF</source>
        <translation>RS ΑΝΕΝΕΡΓΟ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30688"/>
        <source>RS on HOLD</source>
        <translation>RS σε ΑΝΑΜΟΝΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30709"/>
        <source>PXG sv#%1 set to %2</source>
        <translation>PXG sv#%1 καθοριστηκε σε %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30730"/>
        <source>PXR sv set to %1</source>
        <translation>PXR sv καθοριστηκε σε %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30765"/>
        <source>SV%1 changed from %2 to %3)</source>
        <translation>SV%1 αλλαξε απο %2 σε %3)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30773"/>
        <source>Unable to set sv%1</source>
        <translation>Αδυνατος καθορισμος sv%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30784"/>
        <source>SV changed from %1 to %2</source>
        <translation>SV αλλαξε απο %1 σε %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30792"/>
        <source>Unable to set sv</source>
        <translation>Aδυνατος καθορισμος sv</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30794"/>
        <source>Unable to set new sv</source>
        <translation>Aδυνατος καθορισμος νεου sv</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9266"/>
        <source>&lt;b&gt;[f]&lt;/b&gt; = Full Screen Mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14061"/>
        <source>Save Graph as PDF</source>
        <translation>Αποθυκευση Γραφικου ως PDF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23881"/>
        <source>A temperature of 145.2C is often sent as 1452.</source>
        <translation type="obsolete">Θερμοκρασια των 145.2 κ συχνακις εμφανιζεται ως 1452.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1581"/>
        <source>Alarm %1 triggered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3772"/>
        <source>[TP] recorded at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4919"/>
        <source>Importing a profile in to Designer will decimate all data except the main [points].
Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9257"/>
        <source>&lt;b&gt;[d]&lt;/b&gt; = Toggle xy scale (T/Delta)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13531"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13514"/>
        <source>Oversampling is only active with a sampling interval equal or larger than 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13531"/>
        <source>A tight sampling interval might lead to instability on some machines. We suggest a minimum of 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14323"/>
        <source>current background ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14324"/>
        <source>current background BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22227"/>
        <source>Phidget Temperature Sensor 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22235"/>
        <source>Phidget Temperature Sensor 4-input not attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22323"/>
        <source>Phidget Bridge 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22331"/>
        <source>Phidget Bridge 4-input not attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22415"/>
        <source>Phidget 1018 IO attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22423"/>
        <source>Phidget 1018 IO not attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25551"/>
        <source>Device set to %1. Now, chose Modbus serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27115"/>
        <source>&lt;b&gt;But Not:&lt;/b&gt; alarm triggered only if the alarm with the given number was not triggered before. Use 0 for no guard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27122"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button or the new value of the slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29111"/>
        <source>Load PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29195"/>
        <source>Save PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31275"/>
        <source>Load Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31294"/>
        <source>Save Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31500"/>
        <source>PID turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31513"/>
        <source>PID turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9265"/>
        <source>&lt;b&gt;[q,w,e,r + &lt;i&gt;nn&lt;/i&gt;]&lt;/b&gt; = Quick Custom Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14307"/>
        <source>Return the minimum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14308"/>
        <source>Return the maximum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23940"/>
        <source>The MODBUS device corresponds to input channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23941"/>
        <source>1 and 2.. The MODBUS_34 extra device adds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23942"/>
        <source>input channels 3 and 4. Inputs with slave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23943"/>
        <source>id set to 0 are turned off. Modbus function 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23944"/>
        <source>&apos;read holding register&apos; is the standard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23945"/>
        <source>Modbus function 4 triggers the use of &apos;read </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23946"/>
        <source>input register&apos;.Input registers (fct 4) usually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23947"/>
        <source> are from 30000-39999.Most devices hold data in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23948"/>
        <source>2 byte integer registers. A temperature of 145.2C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23949"/>
        <source>is often sent as 1452. In that case you have to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23950"/>
        <source>use the symbolic assignment &apos;x/10&apos;. Few devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23951"/>
        <source>hold data as 4 byte floats in two registers.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Radio Button</name>
    <message>
        <location filename="artisanlib/main.py" line="24547"/>
        <source>Meter</source>
        <translation>Μετρητης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24548"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20312"/>
        <source>Arduino TC4</source>
        <translation type="obsolete">Arduino TC4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24550"/>
        <source>Program</source>
        <translation>Προγραμμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24549"/>
        <source>TC4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Annotation</name>
    <message>
        <location filename="artisanlib/main.py" line="700"/>
        <source>Speed</source>
        <translation>Ταχυτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="701"/>
        <source>Heater</source>
        <translation>Θερμαστρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="702"/>
        <source>Damper</source>
        <translation>Κλαπετο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="703"/>
        <source>Fan</source>
        <translation>Ανεμιστηρας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3002"/>
        <source>START 00:00</source>
        <translation type="obsolete">ΕΝΑΡΞΗ 00:00</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3803"/>
        <source>DE %1</source>
        <translation>DE%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3859"/>
        <source>FCs %1</source>
        <translation>FCε%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3914"/>
        <source>FCe %1</source>
        <translation>FCλ%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3967"/>
        <source>SCs %1</source>
        <translation>SCε%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4025"/>
        <source>SCe %1</source>
        <translation>SCλ%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3340"/>
        <source>END %1</source>
        <translation type="obsolete">ΛΗΞΗ%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4164"/>
        <source>CE %1</source>
        <translation>ΛΨ%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3724"/>
        <source>CHARGE 00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3764"/>
        <source>TP %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4085"/>
        <source>DROP %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Title</name>
    <message>
        <location filename="artisanlib/main.py" line="10484"/>
        <source>Roaster Scope</source>
        <translation>Καταγραφεας</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="artisanlib/main.py" line="19977"/>
        <source>Playback Aid set OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28486"/>
        <source>Ready</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29285"/>
        <source>Decimal position successfully set to 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29288"/>
        <source>Problem setting decimal position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29339"/>
        <source>Problem setting thermocouple type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30318"/>
        <source>setting autotune...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30345"/>
        <source>Autotune successfully turned OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30348"/>
        <source>Autotune successfully turned ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30246"/>
        <source>wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27989"/>
        <source>PID OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27992"/>
        <source>PID ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28016"/>
        <source>SV successfully set to %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28027"/>
        <source>Empty SV box</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28036"/>
        <source>Unable to read SV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30141"/>
        <source>Ramp/Soak operation cancelled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30144"/>
        <source>No RX data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30182"/>
        <source>RS ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30156"/>
        <source>Need to change pattern mode...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30165"/>
        <source>Pattern has been changed. Wait 5 secs.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30168"/>
        <source>Pattern could not be changed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30199"/>
        <source>RampSoak could not be changed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30202"/>
        <source>RS OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30212"/>
        <source>RS successfully turned OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28249"/>
        <source>setONOFFrampsoak(): Ramp Soak could not be set OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28275"/>
        <source>getsegment(): problem reading ramp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28288"/>
        <source>getsegment(): problem reading soak</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28301"/>
        <source>getallsegments(): problem reading R/S </source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30314"/>
        <source>Finished reading Ramp/Soak val.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28344"/>
        <source>Finished reading pid values</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28396"/>
        <source>setpid(): There was a problem setting %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28470"/>
        <source>Ramp/Soak successfully written</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29248"/>
        <source>Time Units successfully set to MM:SS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29251"/>
        <source>Problem setting time units</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29336"/>
        <source>Thermocouple type successfully set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29529"/>
        <source>SV%1 set to %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29533"/>
        <source>Problem setting SV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29535"/>
        <source>Cancelled svN change</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29553"/>
        <source>PID already using sv%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29556"/>
        <source>setNsv(): bad response</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29593"/>
        <source>setNpid(): bad confirmation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29597"/>
        <source>Cancelled pid change</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29615"/>
        <source>PID was already using pid %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29618"/>
        <source>setNpid(): Unable to set pid %1 </source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29695"/>
        <source>SV%1 successfully set to %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29703"/>
        <source>setsv(): Unable to set SV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29821"/>
        <source>pid #%1 successfully set to (%2,%3,%4)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29829"/>
        <source>pid command failed. Bad data at pid%1 (8,8,8): (%2,%3,%4) </source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29907"/>
        <source>getallpid(): Unable to read pid values</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29935"/>
        <source>PID is using pid = %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29938"/>
        <source>getallpid(): Unable to read current sv</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30002"/>
        <source>PID is using SV = %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30215"/>
        <source>Ramp Soak could not be set OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30256"/>
        <source>PID set to OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30259"/>
        <source>PID set to ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30262"/>
        <source>Unable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30266"/>
        <source>No data received</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30306"/>
        <source>Reading Ramp/Soak %1 ...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30311"/>
        <source>problem reading Ramp/Soak</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30327"/>
        <source>Current pid = %1. Proceed with autotune command?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30331"/>
        <source>Autotune cancelled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30350"/>
        <source>UNABLE to set Autotune</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>SV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>Ramp (MM:SS)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>Soak (MM:SS)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30422"/>
        <source>Ramp/Soak successfully writen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31376"/>
        <source>Work in Progress</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29839"/>
        <source>sending commands for p%1 i%2 d%3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28387"/>
        <source>%1 successfully sent to pid </source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29590"/>
        <source>pid changed to %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Tab</name>
    <message>
        <location filename="artisanlib/main.py" line="15114"/>
        <source>HUD</source>
        <translation>ΠΡΟΒΟΛΕΑΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15117"/>
        <source>Plotter</source>
        <translation>ΠΛΟΤΕΡ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15120"/>
        <source>Math</source>
        <translation>Μαθηματικα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15123"/>
        <source>UI</source>
        <translation>UI</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31393"/>
        <source>General</source>
        <translation>Γενικα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16395"/>
        <source>Notes</source>
        <translation>Σημειωσεις</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19947"/>
        <source>Events</source>
        <translation>Συμβαντα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19950"/>
        <source>Data</source>
        <translation>Πληροφορια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19944"/>
        <source>Config</source>
        <translation>Διαμορφωση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18512"/>
        <source>Buttons</source>
        <translation>Πληκτρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18515"/>
        <source>Sliders</source>
        <translation>Σλαιντερς</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18521"/>
        <source>Palettes</source>
        <translation>Προτυπα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18527"/>
        <source>Style</source>
        <translation>Στυλ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24886"/>
        <source>ET/BT</source>
        <translation>ΕΤ/ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29102"/>
        <source>Extra</source>
        <translation>Εξτρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24243"/>
        <source>Modbus</source>
        <translation>Modbus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24246"/>
        <source>Scale</source>
        <translation>Κλιμακα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24889"/>
        <source>Extra Devices</source>
        <translation>Εξτρα Συσκαυες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24892"/>
        <source>Symb ET/BT</source>
        <translation>Συμβ ΕΤ/ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26093"/>
        <source>Graph</source>
        <translation>Γραφημα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26096"/>
        <source>LCDs</source>
        <translation>LCDς</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29090"/>
        <source>RS</source>
        <translation>RS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29093"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31263"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29099"/>
        <source>Set RS</source>
        <translation>Δημ RS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18518"/>
        <source>Quantifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24249"/>
        <source>Color</source>
        <translation type="unfinished">Χρωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31267"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24895"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Table</name>
    <message>
        <location filename="artisanlib/main.py" line="17041"/>
        <source>Abs Time</source>
        <translation type="obsolete">Χρονος Abs</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17041"/>
        <source>Rel Time</source>
        <translation type="obsolete">Χρονος Rel</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17041"/>
        <source>DeltaET (d/m)</source>
        <translation type="obsolete">DeltaET(d/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17041"/>
        <source>DeltaBT (d/m)</source>
        <translation type="obsolete">DeltaBT(d/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17079"/>
        <source>%1 START</source>
        <translation type="obsolete">%1ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17083"/>
        <source>%1 DRY END</source>
        <translation type="obsolete">%1ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17088"/>
        <source>%1 FC START</source>
        <translation type="obsolete">%1 ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17092"/>
        <source>%1 FC END</source>
        <translation type="obsolete">%1 ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17096"/>
        <source>%1 SC START</source>
        <translation type="obsolete">%1 ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17100"/>
        <source>%1 SC END</source>
        <translation type="obsolete">%1 ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17104"/>
        <source>%1 END</source>
        <translation type="obsolete">%1 ΛΗΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17109"/>
        <source>%1 EVENT #%2 %3%4</source>
        <translation type="obsolete">%1 ΣΥΜΒΑΝ #%2 %3%4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Time</source>
        <translation>Χρονος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Description</source>
        <translation>Περιγραφη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20093"/>
        <source>Type</source>
        <translation>Τυπος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20093"/>
        <source>Value</source>
        <translation>Τιμη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Label</source>
        <translation>Επιγραφη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Action</source>
        <translation>Ενεργεια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Documentation</source>
        <translation>Τεκμηριωση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Visibility</source>
        <translation>Ορατοτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Color</source>
        <translation>Χρωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Text Color</source>
        <translation>Χρωμα Κειμενου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Device</source>
        <translation>Συσκευη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Comm Port</source>
        <translation>Θυρα Επικοινωνιας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Baud Rate</source>
        <translation>Τιμη Baud</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Byte Size</source>
        <translation>Mεγεθος Byte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Parity</source>
        <translation>Ισοτιμια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Stopbits</source>
        <translation>Stopbits</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Timeout</source>
        <translation>Χρονικη Ληξη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Color 1</source>
        <translation>Χρωμα1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Color 2</source>
        <translation>Χρωμα2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Label 1</source>
        <translation>Επιγραφη1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Label 2</source>
        <translation>Επιγραφη2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>y1(x)</source>
        <translation>y1(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>y2(x)</source>
        <translation>y2(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>LCD 1</source>
        <translation>LCD1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>LCD 2</source>
        <translation>LCD2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Curve 1</source>
        <translation>Καμπυλη1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Curve 2</source>
        <translation>Καμπυλη2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Parent</source>
        <translation>Γονικος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Width</source>
        <translation>Πλατος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Opaqueness</source>
        <translation>Διαφανεια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Delete Wheel</source>
        <translation>Διαγραφη Ροδας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Edit Labels</source>
        <translation>Επεξεργασια  Επιγραφων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Update Labels</source>
        <translation>Ενημερωση Επιγραφων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Properties</source>
        <translation>Ιδιοτητες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Radius</source>
        <translation>Ακτινα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Starting angle</source>
        <translation>Γωνια Εναρξης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Projection</source>
        <translation>Προβολη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Text Size</source>
        <translation>Μεγεθος Κειμενου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Color Pattern</source>
        <translation>Προτυπο Χρωματων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Status</source>
        <translation>Θεση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>If Alarm</source>
        <translation>Εαν συναγερμος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>From</source>
        <translation>Απο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Source</source>
        <translation>Πηγη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Condition</source>
        <translation>Κατασταση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Temp</source>
        <translation>Θερμοκρασια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>Ramp HH:MM</source>
        <translation>Γραφημα Ramp ΩΩ:ΛΛ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>Soak HH:MM</source>
        <translation>Γραφημα Soak ΩΩ:ΛΛ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13761"/>
        <source>START</source>
        <translation type="obsolete">ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20175"/>
        <source>DRY END</source>
        <translation>ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20178"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20181"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20184"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20187"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20193"/>
        <source>COOL</source>
        <translation>ΨΥΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16549"/>
        <source>EVENT #%2 %3%4</source>
        <translation>ΣΥΜΒΑΝ #%2 %3%4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20190"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>DeltaET</source>
        <translation type="unfinished">DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>DeltaBT</source>
        <translation type="unfinished">DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20172"/>
        <source>CHARGE</source>
        <translation type="unfinished">ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20197"/>
        <source>EVENT #%1 %2%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>But Not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Beep</source>
        <translation type="unfinished">Beep</translation>
    </message>
</context>
<context>
    <name>Textbox</name>
    <message>
        <location filename="artisanlib/main.py" line="437"/>
        <source>Acidity</source>
        <translation>Οξυτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="423"/>
        <source>Aftertaste</source>
        <translation>Μεταγευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="395"/>
        <source>Clean Cup</source>
        <translation>Καθαριοτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="358"/>
        <source>Head</source>
        <translation>Κεφαλη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="435"/>
        <source>Fragrance</source>
        <translation>Ευωδια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="427"/>
        <source>Sweetness</source>
        <translation>Γλυκητητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="419"/>
        <source>Aroma</source>
        <translation>Αρωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="442"/>
        <source>Balance</source>
        <translation>Ισορροπια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="436"/>
        <source>Body</source>
        <translation>Σωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="368"/>
        <source>Sour</source>
        <translation>Ξυνιλα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="413"/>
        <source>Flavor</source>
        <translation>Γευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="370"/>
        <source>Critical
Stimulus</source>
        <translation>Κρισιμο κινητρο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="372"/>
        <source>Bitter</source>
        <translation>Πικρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="373"/>
        <source>Astringency</source>
        <translation>Στυπτικοτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="374"/>
        <source>Solubles
Concentration</source>
        <translation>Περιεκτηκοτητα διαλυματων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="421"/>
        <source>Mouthfeel</source>
        <translation>Στο στομα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="376"/>
        <source>Other</source>
        <translation>Αλλα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="377"/>
        <source>Aromatic
Complexity</source>
        <translation>Αρωματικη Πολυπλοκοτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="378"/>
        <source>Roast
Color</source>
        <translation>Χρωμα Ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="379"/>
        <source>Aromatic
Pungency</source>
        <translation>Αρωματικη Οξυτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="380"/>
        <source>Sweet</source>
        <translation>Γλυκα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="382"/>
        <source>pH</source>
        <translation>ΡΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="385"/>
        <source>Fragance</source>
        <translation>Ευωδια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="392"/>
        <source>Dry Fragrance</source>
        <translation>Ξηρα Ευωδια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="393"/>
        <source>Uniformity</source>
        <translation>Ομοιομορφια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="394"/>
        <source>Complexity</source>
        <translation>Πολυπλοκοτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="430"/>
        <source>Finish</source>
        <translation>Τελειωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="400"/>
        <source>Brightness</source>
        <translation>Λαμπεροτητα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="401"/>
        <source>Wet Aroma</source>
        <translation>Υγρο Αρωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="405"/>
        <source>Taste</source>
        <translation>Γευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="406"/>
        <source>Nose</source>
        <translation>Οσφρηση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="411"/>
        <source>Fragrance-Aroma</source>
        <translation>Ευωδια-Αρωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="422"/>
        <source>Flavour</source>
        <translation>Γευση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="432"/>
        <source>Roast Color</source>
        <translation>Χρωμα Ψησιματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="433"/>
        <source>Crema Texture</source>
        <translation>Υφη Κρεμας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="434"/>
        <source>Crema Volume</source>
        <translation>Ογκος Κρεμας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="438"/>
        <source>Bitterness</source>
        <translation>Πικριλα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="439"/>
        <source>Defects</source>
        <translation>Ελαττωματα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="440"/>
        <source>Aroma Intensity</source>
        <translation>Ενταση Αρωματος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="441"/>
        <source>Aroma Persistence</source>
        <translation>Πιστοτητα Αρωματος</translation>
    </message>
</context>
<context>
    <name>Tooltip</name>
    <message>
        <location filename="artisanlib/main.py" line="3527"/>
        <source>Stop monitoring</source>
        <translation>Παυση παρακολουθησης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7374"/>
        <source>Start monitoring</source>
        <translation>Εναρξηπαρακολουθησης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3632"/>
        <source>Stop recording</source>
        <translation>Παυση καταγραφης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7387"/>
        <source>Start recording</source>
        <translation>Εναρξη καταγραφης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7402"/>
        <source>Marks the begining of First Crack (FCs)</source>
        <translation>Μαρκαρει εναρξη FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7409"/>
        <source>Marks the end of First Crack (FCs)</source>
        <translation>Μαρκαρει ληξη FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7416"/>
        <source>Marks the begining of Second Crack (SCs)</source>
        <translation>Μαρκαρει εναρξη SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7423"/>
        <source>Marks the end of Second Crack (SCe)</source>
        <translation>Μαρκαρει ληξη SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7436"/>
        <source>Reset</source>
        <translation>Επαναφορα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7444"/>
        <source>Marks the begining of the roast (beans in)</source>
        <translation>Μαρκαρει εναρξη ψησιματος/φορτωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7452"/>
        <source>Marks the end of the roast (drop beans)</source>
        <translation>Μαρκαρει ληξη ψησιματος.ξεφορτωμα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7467"/>
        <source>Marks an Event</source>
        <translation>Μαρκαρει συμβαν</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7476"/>
        <source>Increases the current SV value by 5</source>
        <translation>Αυξανει τιμη SV κατα 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7484"/>
        <source>Increases the current SV value by 10</source>
        <translation>Αυξανει τιμη SV κατα 10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7492"/>
        <source>Increases the current SV value by 20</source>
        <translation>Αυξανει τιμη SV κατα 20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7500"/>
        <source>Decreases the current SV value by 20</source>
        <translation>Μειωνει τιμη SV κατα 20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7508"/>
        <source>Decreases the current SV value by 10</source>
        <translation>Μειωνει τιμη SV κατα 10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7516"/>
        <source>Decreases the current SV value by 5</source>
        <translation>Μειωνει τιμη SV κατα 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7530"/>
        <source>Turns ON/OFF the HUD</source>
        <translation>Ανοιγει/Κλεινει Προβολεα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7539"/>
        <source>Marks the end of the Drying phase (DRYEND)</source>
        <translation>Μαρκαρει ληξη περιοδου ξηρανσης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7547"/>
        <source>Marks the end of the Cooling phase (COOLEND)</source>
        <translation>Μαρκαρει ληξη περιοδου ψυξης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7597"/>
        <source>Timer</source>
        <translation>Χρονομετρο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7598"/>
        <source>ET Temperature</source>
        <translation>Θερμοκρασια ΕΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7599"/>
        <source>BT Temperature</source>
        <translation>Θερμοκρασια ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7600"/>
        <source>ET/time (degrees/min)</source>
        <translation>ΕΤ/Χρονος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7601"/>
        <source>BT/time (degrees/min)</source>
        <translation>ΒΤ/Χρονος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7602"/>
        <source>Value of SV in PID</source>
        <translation>Τιμη SV στο PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7603"/>
        <source>PID power %</source>
        <translation>Δυναμη PID%</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7687"/>
        <source>Number of events found</source>
        <translation>αΡΙΘΜΟς ΣΥΜΒΑΝΤΩΝ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7698"/>
        <source>Type of event</source>
        <translation>Τυπος συμβαντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7705"/>
        <source>Value of event</source>
        <translation>Τιμη συμβαντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7719"/>
        <source>Updates the event</source>
        <translation>Ενημερωνει συμβαντα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14392"/>
        <source>&lt;b&gt;Label&lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14393"/>
        <source>&lt;b&gt;Description &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Περιγραφη &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14395"/>
        <source>&lt;b&gt;Type &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14396"/>
        <source>&lt;b&gt;Value &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14397"/>
        <source>&lt;b&gt;Documentation &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14398"/>
        <source>&lt;b&gt;Button# &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26358"/>
        <source>Save image using current graph size to a png format</source>
        <translation>Αποθυκευση φραφηματος σε μορφη png</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14954"/>
        <source>linear: linear interpolation
cubic: 3rd order spline interpolation
nearest: y value of the nearest point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17065"/>
        <source>ON/OFF logs serial communication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17155"/>
        <source>Automatic generated name = This text + date + time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17158"/>
        <source>ON/OFF of automatic saving when pressing keyboard letter [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17169"/>
        <source>Sets the directory to store batch profiles when using the letter [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17771"/>
        <source>Allows to enter a description of the last event</source>
        <translation>Επιτρεπει εισαγωγη περιγραφης τελευταιου συμβαντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17977"/>
        <source>Add new extra Event button</source>
        <translation>Προσθηκη νεου πληκτρου συμβαντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17982"/>
        <source>Delete the last extra Event button</source>
        <translation>Διαγραφη τελευταιου πληκτρου συμβαντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26854"/>
        <source>Show help</source>
        <translation>Εμφανιση βοηθειας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18019"/>
        <source>Backup all palettes to a text file</source>
        <translation>Backup ολων προτυπων σε μορφη κειμενου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18317"/>
        <source>Action Type</source>
        <translation>Τυπος ενεργειας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18322"/>
        <source>Action String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26311"/>
        <source>Aspect Ratio</source>
        <translation>Αναλογια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24982"/>
        <source>Example: 100 + 2*x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24983"/>
        <source>Example: 100 + x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26300"/>
        <source>Erases wheel parent hierarchy</source>
        <translation>Διαγραφη ιεραρχιας ροδας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26304"/>
        <source>Sets graph hierarchy child-&gt;parent instead of parent-&gt;child</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26318"/>
        <source>Increase size of text in all the graph</source>
        <translation>Αυξανει μεγεθος κειμενων γραφηματων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26321"/>
        <source>Decrease size of text in all the graph</source>
        <translation>Μειωνει μεγεθος κειμενων γραφηματων</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26325"/>
        <source>Decorative edge beween wheels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26331"/>
        <source>Line thickness</source>
        <translation>Παχος γραμμης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26336"/>
        <source>Line color</source>
        <translation>Χρωμα γραμμης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26340"/>
        <source>Apply color pattern to whole graph</source>
        <translation>Εφαρμογη προτυπυ χρωματων το γραφημα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26346"/>
        <source>Add new wheel</source>
        <translation>Προσθηκη ροδας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26349"/>
        <source>Rotate graph 1 degree counter clockwise</source>
        <translation>Περιστροφη γραφηματος δεξιοστροφα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26352"/>
        <source>Rotate graph 1 degree clockwise</source>
        <translation>Περιστροφη γραφηματος αριστεροστροφα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26356"/>
        <source>Save graph to a text file.wg</source>
        <translation>Αποθυκευση γραφηματος σε μορφη κειμενου.wg</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26361"/>
        <source>Sets Wheel graph to view mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26364"/>
        <source>open graph file.wg</source>
        <translation>Ανοιγμα γραφηματος  απο wg</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26367"/>
        <source>Close wheel graph editor</source>
        <translation>Κλεισιμο διαμορφωσης ροδας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18020"/>
        <source>Restore all palettes from a text file</source>
        <translation>Επαναφορα προτυπων απο κειμενο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26859"/>
        <source>Clear alarms table</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
