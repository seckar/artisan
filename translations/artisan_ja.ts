<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="ja" sourcelanguage="">
<context>
    <name>About</name>
    <message>
        <location filename="artisanlib/main.py" line="10743"/>
        <source>%1, linux binary</source>
        <translation type="obsolete">%1, linux バイナリ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10744"/>
        <source>%1, documentation</source>
        <translation type="obsolete">%1, 関連文章</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10745"/>
        <source>%1, TEVA18B, DTA support</source>
        <translation type="obsolete">%1, TEVA18B, DTA サポート</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10746"/>
        <source>%1, DTA support</source>
        <translation type="obsolete">%1, DTA サポート</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10747"/>
        <source>%1, Swedish localization</source>
        <translation type="obsolete">%1, スウェーデン語ローカライズ対応</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10748"/>
        <source>%1, Spanish localization</source>
        <translation type="obsolete">%1, スペイン語ローカライズ対応</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10750"/>
        <source>%1, Arduino/TC4</source>
        <translation type="obsolete">%1, Arduino/TC4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>About</source>
        <translation>Artisan について</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11059"/>
        <source>Version:</source>
        <translation type="obsolete">バージョン:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>Core developers:</source>
        <translation>主要開発者:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>Contributors:</source>
        <translation>貢献者:</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="artisanlib/main.py" line="7372"/>
        <source>ON</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7385"/>
        <source>START</source>
        <translation>START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3526"/>
        <source>OFF</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7398"/>
        <source>FC
START</source>
        <translation>FC
START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7405"/>
        <source>FC
END</source>
        <translation>FC
END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7412"/>
        <source>SC
START</source>
        <translation>SC
START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7419"/>
        <source>SC
END</source>
        <translation>SC
END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7427"/>
        <source>RESET</source>
        <translation>RESET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7440"/>
        <source>CHARGE</source>
        <translation>CHARGE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7448"/>
        <source>DROP</source>
        <translation>DROP</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7456"/>
        <source>Control</source>
        <translation>コントロール</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7463"/>
        <source>EVENT</source>
        <translation>EVENT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7471"/>
        <source>SV +5</source>
        <translation>SV +5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7479"/>
        <source>SV +10</source>
        <translation>SV +10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7487"/>
        <source>SV +20</source>
        <translation>SV +20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7495"/>
        <source>SV -20</source>
        <translation>SV -20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7503"/>
        <source>SV -10</source>
        <translation>SV -10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7511"/>
        <source>SV -5</source>
        <translation>SV -5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7519"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7534"/>
        <source>DRY
END</source>
        <translation>DRY
END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7543"/>
        <source>COOL
END</source>
        <translation>COOL
END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26608"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14566"/>
        <source>PID Help</source>
        <translation type="obsolete">PID ヘルプ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31249"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28813"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14895"/>
        <source>Color</source>
        <translation>カラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14898"/>
        <source>Plot</source>
        <translation>プロット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25931"/>
        <source>Background</source>
        <translation>背景</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14904"/>
        <source>Virtual Device</source>
        <translation>仮想デバイス</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19555"/>
        <source>Save Image</source>
        <translation>画像を保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27124"/>
        <source>Help</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14963"/>
        <source>Info</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31101"/>
        <source>Set</source>
        <translation>セット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25886"/>
        <source>Defaults</source>
        <translation>デフォルト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15869"/>
        <source>Order</source>
        <translation>順序</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26830"/>
        <source>Add</source>
        <translation>追加</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26839"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16129"/>
        <source>in</source>
        <translation>焙煎前</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16136"/>
        <source>out</source>
        <translation>焙煎後</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17013"/>
        <source>Search</source>
        <translation>検索</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17166"/>
        <source>Path</source>
        <translation>パス</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18001"/>
        <source>Transfer To</source>
        <translation>パターンを転送</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18003"/>
        <source>Restore From</source>
        <translation>パターンを復元</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31229"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31226"/>
        <source>Load</source>
        <translation>ロード</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19552"/>
        <source>Del</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19784"/>
        <source>Align</source>
        <translation>整列</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19834"/>
        <source>Up</source>
        <translation>上へ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19836"/>
        <source>Down</source>
        <translation>下へ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19838"/>
        <source>Left</source>
        <translation>左へ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19840"/>
        <source>Right</source>
        <translation>右へ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24656"/>
        <source>Reset</source>
        <translation>リセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27501"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23270"/>
        <source>Create</source>
        <translation>作成</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20700"/>
        <source>Scan for Ports</source>
        <translation type="obsolete">ポートスキャン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26610"/>
        <source>Select</source>
        <translation>選択</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25775"/>
        <source>Grid</source>
        <translation>グリッド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25782"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25789"/>
        <source>Y Label</source>
        <translation>Y軸ラベル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25796"/>
        <source>X Label</source>
        <translation>X軸ラベル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25803"/>
        <source>Drying Phase</source>
        <translation>Drying フェーズ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25810"/>
        <source>Maillard Phase</source>
        <translation>Maillard フェーズ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25817"/>
        <source>Development Phase</source>
        <translation>Development フェーズ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25824"/>
        <source>Cooling Phase</source>
        <translation>Cooling フェーズ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25831"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25838"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25845"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25852"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25859"/>
        <source>Markers</source>
        <translation>マーカー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25866"/>
        <source>Text</source>
        <translation>テキスト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25873"/>
        <source>Watermarks</source>
        <translation>透かし</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25880"/>
        <source>C Lines</source>
        <translation>C ライン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25889"/>
        <source>Grey</source>
        <translation>グレー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25943"/>
        <source>LED</source>
        <translation>LED</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25975"/>
        <source>B/W</source>
        <translation>白黒</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26299"/>
        <source>Reset Parents</source>
        <translation>ペアレントをリセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26303"/>
        <source>Reverse Hierarchy</source>
        <translation>階層を反転</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26317"/>
        <source>+</source>
        <translation>＋</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26320"/>
        <source>-</source>
        <translation>－</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26335"/>
        <source>Line Color</source>
        <translation>ラインカラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26348"/>
        <source>&lt;</source>
        <translation>＜</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26351"/>
        <source>&gt;</source>
        <translation>＞</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26354"/>
        <source>Save File</source>
        <translation>ファイルを保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26357"/>
        <source>Save Img</source>
        <translation>画像を保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26360"/>
        <source>View Mode</source>
        <translation>表示モード</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26363"/>
        <source>Open</source>
        <translation>開く</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26633"/>
        <source>Set Color</source>
        <translation>カラーをセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26824"/>
        <source>All On</source>
        <translation>全て On</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26827"/>
        <source>All Off</source>
        <translation>全て Off</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27496"/>
        <source>Read Ra/So values</source>
        <translation>Ra/So の値を読み込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28550"/>
        <source>RampSoak ON</source>
        <translation>RampSoak を ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28552"/>
        <source>RampSoak OFF</source>
        <translation>RampSoak を OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28556"/>
        <source>PID OFF</source>
        <translation>PID を OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28558"/>
        <source>PID ON</source>
        <translation>PID を ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28673"/>
        <source>Write SV</source>
        <translation>SV に書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27512"/>
        <source>SV Buttons ON</source>
        <translation>SV ボタンを ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27514"/>
        <source>SV Buttons OFF</source>
        <translation>SV ボタンを OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27516"/>
        <source>Read SV</source>
        <translation>SV から読み込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27533"/>
        <source>Set p</source>
        <translation>p をセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27534"/>
        <source>Set i</source>
        <translation>i をセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27535"/>
        <source>Set d</source>
        <translation>d をセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28809"/>
        <source>Autotune ON</source>
        <translation>Autotune を ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28811"/>
        <source>Autotune OFF</source>
        <translation>Autotune を OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27550"/>
        <source>Read PID Values</source>
        <translation>PID の値を読み込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31380"/>
        <source>Read</source>
        <translation>読み込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28940"/>
        <source>Set ET PID to 1 decimal point</source>
        <translation>ET PID を小数第一位までにセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28942"/>
        <source>Set BT PID to 1 decimal point</source>
        <translation>BT PID を小数第一位までにセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28546"/>
        <source>Read RS values</source>
        <translation>RS の値を読み込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28609"/>
        <source>Write SV1</source>
        <translation>SV1 に書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28611"/>
        <source>Write SV2</source>
        <translation>SV2 に書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28613"/>
        <source>Write SV3</source>
        <translation>SV3 に書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28615"/>
        <source>Write SV4</source>
        <translation>SV4 に書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28617"/>
        <source>Write SV5</source>
        <translation>SV5 に書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28619"/>
        <source>Write SV6</source>
        <translation>SV6 に書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28621"/>
        <source>Write SV7</source>
        <translation>SV7 に書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28677"/>
        <source>ON SV buttons</source>
        <translation>ON SV ボタン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28680"/>
        <source>OFF SV buttons</source>
        <translation>OFF SV ボタン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28683"/>
        <source>Read SV (7-0)</source>
        <translation>SV (7-0) から読み込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28791"/>
        <source>pid 1</source>
        <translation>pid 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28793"/>
        <source>pid 2</source>
        <translation>pid 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28795"/>
        <source>pid 3</source>
        <translation>pid 3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28797"/>
        <source>pid 4</source>
        <translation>pid 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28799"/>
        <source>pid 5</source>
        <translation>pid 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28801"/>
        <source>pid 6</source>
        <translation>pid 6</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28803"/>
        <source>pid 7</source>
        <translation>pid 7</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24951"/>
        <source>Read All</source>
        <translation type="obsolete">全て読み込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28944"/>
        <source>Set ET PID to MM:SS time units</source>
        <translation>ET PID の時間単位を「分:秒 (MM:SS)」にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31381"/>
        <source>Write</source>
        <translation>書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26834"/>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26858"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16150"/>
        <source>scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28542"/>
        <source>Write All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28548"/>
        <source>Write RS values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28685"/>
        <source>Write SV (7-0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28805"/>
        <source>Read PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28807"/>
        <source>Write PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18208"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31251"/>
        <source>On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31253"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckBox</name>
    <message>
        <location filename="artisanlib/main.py" line="16089"/>
        <source>Heavy FC</source>
        <translation>Heavy FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16092"/>
        <source>Low FC</source>
        <translation>Low FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16095"/>
        <source>Light Cut</source>
        <translation>Light Cut</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16098"/>
        <source>Dark Cut</source>
        <translation>Dark Cut</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16101"/>
        <source>Drops</source>
        <translation>Drops</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16104"/>
        <source>Oily</source>
        <translation>Oily</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16107"/>
        <source>Uneven</source>
        <translation>Uneven</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16109"/>
        <source>Tipping</source>
        <translation>Tipping</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16111"/>
        <source>Scorching</source>
        <translation>Scorching</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16113"/>
        <source>Divots</source>
        <translation>Divots</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19772"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19773"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14644"/>
        <source>Smooth Spikes</source>
        <translation>スムーススパイク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14654"/>
        <source>Drop Spikes</source>
        <translation>ドロップスパイク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14659"/>
        <source>Limits</source>
        <translation>リミット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14688"/>
        <source>Projection</source>
        <translation>プロジェクション</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19769"/>
        <source>Show</source>
        <translation>表示</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15092"/>
        <source>Beep</source>
        <translation>ビープ音</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15860"/>
        <source>Delete roast properties on RESET</source>
        <translation>RESET 時に焙煎プロパティを削除する</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17064"/>
        <source>Serial Log ON/OFF</source>
        <translation>シリアルポートログの ON/OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17156"/>
        <source>Autosave [a]</source>
        <translation>オートセーブ [a]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17290"/>
        <source>Lock Max</source>
        <translation>最大を固定</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17764"/>
        <source>Button</source>
        <translation>ボタン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17770"/>
        <source>Mini Editor</source>
        <translation>ミニエディタ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15733"/>
        <source>Automatic CHARGE/DROP</source>
        <translation type="obsolete">オートマチック CHARGE/DROP</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18251"/>
        <source>CHARGE</source>
        <translation>CHARGE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23070"/>
        <source>DRY END</source>
        <translation>DRY END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23072"/>
        <source>FC START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23074"/>
        <source>FC END</source>
        <translation>FC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23076"/>
        <source>SC START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23078"/>
        <source>SC END</source>
        <translation>SC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18305"/>
        <source>DROP</source>
        <translation>DROP</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18314"/>
        <source>COOL END</source>
        <translation>COOL END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19362"/>
        <source>Auto Adjusted</source>
        <translation>オートアジャスト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19365"/>
        <source>Watermarks</source>
        <translation>透かし</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19560"/>
        <source>Background</source>
        <translation>背景</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19770"/>
        <source>Text</source>
        <translation>テキスト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19771"/>
        <source>Events</source>
        <translation>イベント</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19868"/>
        <source>Playback Aid</source>
        <translation>プレーバック AID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20225"/>
        <source>Time</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20226"/>
        <source>Bar</source>
        <translation>バー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20227"/>
        <source>d/m</source>
        <translation>d/m</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20228"/>
        <source>ETBTa</source>
        <translation>ETBTa</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20229"/>
        <source>Evaluation</source>
        <translation>評価</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20230"/>
        <source>Characteristics</source>
        <translation>特徴</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24521"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24524"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19763"/>
        <source>TC4_56</source>
        <translation type="obsolete">TC4_56</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26862"/>
        <source>Load alarms from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17946"/>
        <source>Auto CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17949"/>
        <source>Auto DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17952"/>
        <source>Mark TP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19367"/>
        <source>Phases LCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19369"/>
        <source>Auto DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19371"/>
        <source>Auto FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14765"/>
        <source>Decimal Places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24595"/>
        <source>Modbus Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14649"/>
        <source>Smooth2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31182"/>
        <source>Start PID on CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31237"/>
        <source>Load Ramp/Soak table from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24628"/>
        <source>Control Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24703"/>
        <source>Ratiometric</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComboBox</name>
    <message>
        <location filename="artisanlib/main.py" line="810"/>
        <source>Speed</source>
        <translation>Speed</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="811"/>
        <source>Power</source>
        <translation>Power</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="812"/>
        <source>Damper</source>
        <translation>Damper</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="813"/>
        <source>Fan</source>
        <translation>Fan</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>linear</source>
        <translation>linear</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14690"/>
        <source>newton</source>
        <translation>newton</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15688"/>
        <source>metrics</source>
        <translation>metrics</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15690"/>
        <source>thermal</source>
        <translation>thermal</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>cubic</source>
        <translation>cubic</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>nearest</source>
        <translation>nearest</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17555"/>
        <source>g</source>
        <translation>g</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17556"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16771"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16004"/>
        <source>l</source>
        <translation>l</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27196"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27197"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17268"/>
        <source>upper right</source>
        <translation>上段右</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17269"/>
        <source>upper left</source>
        <translation>上段左</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17270"/>
        <source>lower left</source>
        <translation>下段左</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17271"/>
        <source>lower right</source>
        <translation>下段右</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17272"/>
        <source>right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17273"/>
        <source>center left</source>
        <translation>中段左</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17274"/>
        <source>center right</source>
        <translation>中段右</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17275"/>
        <source>lower center</source>
        <translation>下段中央</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17276"/>
        <source>upper center</source>
        <translation>上段中央</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17277"/>
        <source>center</source>
        <translation>中央</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17295"/>
        <source>30 seconds</source>
        <translation>30秒</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17296"/>
        <source>1 minute</source>
        <translation>1分</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17297"/>
        <source>2 minute</source>
        <translation>2分</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17298"/>
        <source>3 minute</source>
        <translation>3分</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17299"/>
        <source>4 minute</source>
        <translation>4分</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17300"/>
        <source>5 minute</source>
        <translation>5分</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17323"/>
        <source>solid</source>
        <translation>実線</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17324"/>
        <source>dashed</source>
        <translation>破線</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17325"/>
        <source>dashed-dot</source>
        <translation>破点線</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17326"/>
        <source>dotted</source>
        <translation>点線</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>None</source>
        <translation>無し</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17535"/>
        <source>Event #0</source>
        <translation>Event #0</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17537"/>
        <source>Event #%1</source>
        <translation>Event #%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17557"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17578"/>
        <source>liter</source>
        <translation>liter</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17579"/>
        <source>gallon</source>
        <translation>gallon</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17580"/>
        <source>quart</source>
        <translation>quart</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17581"/>
        <source>pint</source>
        <translation>pint</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17582"/>
        <source>cup</source>
        <translation>cup</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17583"/>
        <source>cm^3</source>
        <translation>cm^3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17776"/>
        <source>Type</source>
        <translation>タイプ別</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17777"/>
        <source>Value</source>
        <translation>値別</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Serial Command</source>
        <translation>シリアルコマンド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Modbus Command</source>
        <translation>Modbus コマンド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>DTA Command</source>
        <translation>DTA コマンド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Call Program</source>
        <translation>プログラム呼び出し</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>OFF</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>ON</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Multiple Event</source>
        <translation>複合イベント</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27195"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27194"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23252"/>
        <source>SV Commands</source>
        <translation>SV コマンド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23252"/>
        <source>Ramp Commands</source>
        <translation>Ramp コマンド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23912"/>
        <source>little-endian</source>
        <translation>リトルエンディアン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25896"/>
        <source>grey</source>
        <translation>灰色</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25897"/>
        <source>Dark Grey</source>
        <translation>暗いグレー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25898"/>
        <source>Slate Grey</source>
        <translation>ねずみ色</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25899"/>
        <source>Light Gray</source>
        <translation>明るいグレー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25900"/>
        <source>Black</source>
        <translation>ブラック</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25901"/>
        <source>White</source>
        <translation>ホワイト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25902"/>
        <source>Transparent</source>
        <translation>透過</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Flat</source>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Perpendicular</source>
        <translation>垂直</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Radial</source>
        <translation>放射状</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>START</source>
        <translation>START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>CHARGE</source>
        <translation>CHARGE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>TP</source>
        <translation>TP</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>DRY END</source>
        <translation>DRY END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>FC START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>FC END</source>
        <translation>FC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>SC START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>SC END</source>
        <translation>SC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>DROP</source>
        <translation>DROP</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>COOL</source>
        <translation>COOL</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27254"/>
        <source>below</source>
        <translation>値以上</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27254"/>
        <source>above</source>
        <translation>値以下</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Pop Up</source>
        <translation>ポップアップ表示</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Event Button</source>
        <translation>イベントボタン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Slider</source>
        <translation>スライダー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14831"/>
        <source>classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14831"/>
        <source>xkcd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Default</source>
        <translation type="unfinished">デフォルト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Humor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>FCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>SCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>SCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>COOL END</source>
        <translation type="unfinished">COOL END</translation>
    </message>
</context>
<context>
    <name>Contextual Menu</name>
    <message>
        <location filename="artisanlib/main.py" line="5080"/>
        <source>Create</source>
        <translation>プロファイルを作成</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5084"/>
        <source>Config...</source>
        <translation>デザイナー構成...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5094"/>
        <source>Add point</source>
        <translation>ポイント追加</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5098"/>
        <source>Remove point</source>
        <translation>ポイントを削除</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5104"/>
        <source>Reset Designer</source>
        <translation>デザイナーをリセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5108"/>
        <source>Exit Designer</source>
        <translation>デザイナーを終了</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5685"/>
        <source>Add to Cupping Notes</source>
        <translation>カッピングノートに追加</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5689"/>
        <source>Add to Roasting Notes</source>
        <translation>焙煎ノートに追加</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5693"/>
        <source>Cancel selection</source>
        <translation>選択をキャンセル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5697"/>
        <source>Edit Mode</source>
        <translation>編集モードへ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5701"/>
        <source>Exit</source>
        <translation>ホイールグラフエディタを終了</translation>
    </message>
</context>
<context>
    <name>Directory</name>
    <message>
        <location filename="artisanlib/main.py" line="878"/>
        <source>edit text</source>
        <translation type="obsolete">edit text</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14034"/>
        <source>profiles</source>
        <translation>profiles</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14024"/>
        <source>other</source>
        <translation>other</translation>
    </message>
</context>
<context>
    <name>Error Message</name>
    <message>
        <location filename="artisanlib/main.py" line="31311"/>
        <source>Exception:</source>
        <translation>例外 :</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22958"/>
        <source>Value Error:</source>
        <translation>値のエラー :</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26787"/>
        <source>IO Error:</source>
        <translation>IO エラー :</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12447"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16804"/>
        <source>Unable to move CHARGE to a value that does not exist</source>
        <translation>値が存在しないため、CHARGE に移行できません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20865"/>
        <source>Modbus Error:</source>
        <translation>Modbus エラー :</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23014"/>
        <source>Serial Exception:</source>
        <translation>シリアルポートの例外 :</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21150"/>
        <source>F80h Error</source>
        <translation>F80h エラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21160"/>
        <source>CRC16 data corruption ERROR. TX does not match RX. Check wiring</source>
        <translation>エラー CRC16 データが破損しています。TX と RX は一致しません。配線を確認してください</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21163"/>
        <source>No RX data received</source>
        <translation>RX データーが受信されませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21291"/>
        <source>DTAcommand(): %1 bytes received but 15 needed</source>
        <translation>DTAcommand(): %1 バイト受信しましたが、15 必要です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21564"/>
        <source>Unable to open serial port</source>
        <translation>シリアルポートが開けません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20841"/>
        <source>HH806AUtemperature(): %1 bytes received but 14 needed</source>
        <translation type="obsolete">HH806AUtemperature(): %1 バイト受信しましたが、 14 必要です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21714"/>
        <source>HH806Wtemperature(): Unable to initiate device</source>
        <translation>HH806Wtemperature(): デバイスを始動できません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21843"/>
        <source>HH506RAGetID: %1 bytes received but 5 needed</source>
        <translation>HH506RAGetID: %1 バイト受信しましたが、 5 必要です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21868"/>
        <source>HH506RAtemperature(): Unable to get id from HH506RA device </source>
        <translation>HH506RAtemperature(): HH506RAデバイスから ID を取得できません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21888"/>
        <source>HH506RAtemperature(): %1 bytes received but 14 needed</source>
        <translation>HH506RAtemperature(): %1 バイト受信しましたが、 14 必要です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21943"/>
        <source>CENTER302temperature(): %1 bytes received but 7 needed</source>
        <translation>CENTER302temperature(): %1 バイト受信しましたが、 7 必要です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22013"/>
        <source>CENTER303temperature(): %1 bytes received but 8 needed</source>
        <translation>CENTER303temperature(): %1 バイト受信しましたが、 8 必要です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22083"/>
        <source>CENTER306temperature(): %1 bytes received but 10 needed</source>
        <translation>CENTER306temperature(): %1 バイト受信しましたが、 10 必要です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22169"/>
        <source>CENTER309temperature(): %1 bytes received but 45 needed</source>
        <translation>CENTER309temperature(): %1 バイト受信しましたが、 45 必要です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22594"/>
        <source>Arduino could not set channels</source>
        <translation>Arduino はチャンネルをセットできませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22605"/>
        <source>Arduino could not set temperature unit</source>
        <translation>Arduino は温度ユニットをセットできませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24398"/>
        <source>Serial Exception: invalid comm port</source>
        <translation>シリアルポートの例外 : 無効なシリアルポート</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24403"/>
        <source>Serial Exception: timeout</source>
        <translation>シリアルポートの例外 : タイムアウト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21184"/>
        <source>Device error</source>
        <translation type="obsolete">デバイスエラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30912"/>
        <source>Segment values could not be written into PID</source>
        <translation>セグメントの値を PID に書き込めませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30690"/>
        <source>RampSoak could not be changed</source>
        <translation>RampSoak を変更できませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30968"/>
        <source>pid.readoneword(): %1 RX bytes received (7 needed) for unit ID=%2</source>
        <translation>pid.readoneword(): ユニットID=%2 は %1 RX バイト受信（7 必要）</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15340"/>
        <source>Univariate: no profile data available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15512"/>
        <source>Polyfit: no profile data available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21614"/>
        <source>MS6514temperature(): %1 bytes received but 16 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21659"/>
        <source>HH806AUtemperature(): %1 bytes received but 16 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9247"/>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Flavor Scope Label</name>
    <message>
        <location filename="artisanlib/main.py" line="13236"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13237"/>
        <source>Grassy</source>
        <translation>Grassy</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13238"/>
        <source>Leathery</source>
        <translation>Leathery</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13239"/>
        <source>Toasty</source>
        <translation>Toasty</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13240"/>
        <source>Bready</source>
        <translation>Bready</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13241"/>
        <source>Acidic</source>
        <translation>Acidic</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13242"/>
        <source>Flat</source>
        <translation>Flat</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13243"/>
        <source>Fracturing</source>
        <translation>Fracturing</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13244"/>
        <source>Sweet</source>
        <translation>Sweet</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13245"/>
        <source>Less Sweet</source>
        <translation>Less Sweet</translation>
    </message>
</context>
<context>
    <name>Form Caption</name>
    <message>
        <location filename="artisanlib/main.py" line="14586"/>
        <source>Extras</source>
        <translation>エクストラ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15722"/>
        <source>Roast Properties</source>
        <translation>焙煎プロパティー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16964"/>
        <source>Artisan Platform</source>
        <translation>Artisan の実行環境</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17004"/>
        <source>Settings Viewer</source>
        <translation>設定ビューアー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17063"/>
        <source>Serial Log</source>
        <translation>シリアルポートログ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17107"/>
        <source>Error Log</source>
        <translation>エラーログ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17132"/>
        <source>Message History</source>
        <translation>メッセージ履歴</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17153"/>
        <source>Keyboard Autosave [a]</source>
        <translation>キーボードオートセーブ [a]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17191"/>
        <source>AutoSave Path</source>
        <translation>オートセーブパスの参照</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17214"/>
        <source>Axes</source>
        <translation>グラフパーツ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17520"/>
        <source>Roast Calculator</source>
        <translation>焙煎計算機</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17760"/>
        <source>Events</source>
        <translation>イベント</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19310"/>
        <source>Roast Phases</source>
        <translation>焙煎フェーズ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19533"/>
        <source>Cup Profile</source>
        <translation>カッププロファイル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19761"/>
        <source>Profile Background</source>
        <translation>プロファイルの背景</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20222"/>
        <source>Statistics</source>
        <translation>統計</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23064"/>
        <source>Designer Config</source>
        <translation>デザイナー構成</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23603"/>
        <source>Manual Temperature Logger</source>
        <translation>手動による温度記録</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23729"/>
        <source>Serial Ports Configuration</source>
        <translation>シリアルポート構成</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24511"/>
        <source>Device Assignment</source>
        <translation>デバイスの割り当て</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25762"/>
        <source>Colors</source>
        <translation>カラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26290"/>
        <source>Wheel Graph Editor</source>
        <translation>ホイールグラフエディタ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26819"/>
        <source>Alarms</source>
        <translation>アラーム</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27467"/>
        <source>Fuji PXR PID Control</source>
        <translation>富士電機PXR PID Control</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28483"/>
        <source>Fuji PXG PID Control</source>
        <translation>富士電機PXG PID Control</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31373"/>
        <source>Delta DTA PID Control</source>
        <translation>Delta DTA PID Control</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31007"/>
        <source>Arduino Control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupBox</name>
    <message>
        <location filename="artisanlib/main.py" line="24533"/>
        <source>Curves</source>
        <translation>カーブ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24542"/>
        <source>LCDs</source>
        <translation>LCDs</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14799"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14813"/>
        <source>Input Filters</source>
        <translation>入力フィルタ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15015"/>
        <source>Interpolate</source>
        <translation>Interpolate</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15021"/>
        <source>Univariate</source>
        <translation>Univariate</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15079"/>
        <source>Appearance</source>
        <translation>外観</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15087"/>
        <source>Resolution</source>
        <translation>解像度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15099"/>
        <source>Sound</source>
        <translation>サウンド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16336"/>
        <source>Times</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17386"/>
        <source>Time Axis</source>
        <translation>時間軸</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17388"/>
        <source>Temperature Axis</source>
        <translation>温度軸</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17390"/>
        <source>DeltaBT/DeltaET Axis</source>
        <translation>DeltaBT/DeltaET 軸</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17392"/>
        <source>Legend Location</source>
        <translation>凡例の表示位置</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17394"/>
        <source>Grid</source>
        <translation>グリッド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17633"/>
        <source>Rate of Change</source>
        <translation>変化率の算出</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17635"/>
        <source>Temperature Conversion</source>
        <translation>温度単位の変換</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17637"/>
        <source>Weight Conversion</source>
        <translation>重量単位の変換</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17639"/>
        <source>Volume Conversion</source>
        <translation>容積単位の変換</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18244"/>
        <source>Event Types</source>
        <translation>イベントタイプ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18351"/>
        <source>Default Buttons</source>
        <translation>デフォルトボタン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18394"/>
        <source>Management</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20339"/>
        <source>Evaluation</source>
        <translation>評価</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20341"/>
        <source>Display</source>
        <translation>表示</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23326"/>
        <source>Initial Settings</source>
        <translation>初期設定</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24113"/>
        <source>Input 1</source>
        <translation>入力 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24127"/>
        <source>Input 2</source>
        <translation>入力 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24140"/>
        <source>Input 3</source>
        <translation>入力 3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24153"/>
        <source>Input 4</source>
        <translation>入力 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24801"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24816"/>
        <source>Arduino TC4</source>
        <translation>Arduino TC4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24823"/>
        <source>External Program</source>
        <translation>外部プログラム</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24829"/>
        <source>Symbolic Assignments</source>
        <translation>シンボルの割り当て</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26057"/>
        <source>Timer LCD</source>
        <translation>タイマー LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26060"/>
        <source>ET LCD</source>
        <translation>ET LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26063"/>
        <source>BT LCD</source>
        <translation>BT LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26066"/>
        <source>DeltaET LCD</source>
        <translation>DeltaET LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26069"/>
        <source>DeltaBT LCD</source>
        <translation>DeltaBT LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26072"/>
        <source>Extra Devices / PID SV LCD</source>
        <translation>エクストラデバイス / PID SV LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26376"/>
        <source>Label Properties</source>
        <translation>ラベルプロパティ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15048"/>
        <source>Polyfit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14858"/>
        <source>Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24698"/>
        <source>1048 Probe Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24777"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24784"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31010"/>
        <source>p-i-d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31173"/>
        <source>Set Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24749"/>
        <source>Phidget IO</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTML Report Template</name>
    <message>
        <location filename="artisanlib/main.py" line="12657"/>
        <source>Roasting Report</source>
        <translation>Roasting Report</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12682"/>
        <source>Date:</source>
        <translation>Date:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12686"/>
        <source>Beans:</source>
        <translation>Beans:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12690"/>
        <source>Size:</source>
        <translation>Size:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12694"/>
        <source>Weight:</source>
        <translation>Weight:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12698"/>
        <source>Degree:</source>
        <translation>Degree:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12702"/>
        <source>Volume:</source>
        <translation>Volume:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12706"/>
        <source>Density:</source>
        <translation>Density:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12710"/>
        <source>Humidity:</source>
        <translation>Humidity:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12714"/>
        <source>Roaster:</source>
        <translation>Roaster:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12718"/>
        <source>Operator:</source>
        <translation>Operator:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12722"/>
        <source>Cupping:</source>
        <translation>Cupping:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12726"/>
        <source>Color:</source>
        <translation>Color:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12734"/>
        <source>CHARGE:</source>
        <translation>CHARGE:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12742"/>
        <source>DRY:</source>
        <translation>DRY:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12746"/>
        <source>FCs:</source>
        <translation>FCs:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12750"/>
        <source>FCe:</source>
        <translation>FCe:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12754"/>
        <source>SCs:</source>
        <translation>SCs:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12758"/>
        <source>SCe:</source>
        <translation>SCe:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12762"/>
        <source>DROP:</source>
        <translation>DROP:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12766"/>
        <source>COOL:</source>
        <translation>COOL:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12770"/>
        <source>RoR:</source>
        <translation>RoR:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12774"/>
        <source>ETBTa:</source>
        <translation>ETBTa:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12786"/>
        <source>Drying:</source>
        <translation>Drying:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12790"/>
        <source>Maillard:</source>
        <translation>Maillard:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12794"/>
        <source>Development:</source>
        <translation>Development:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12798"/>
        <source>Cooling:</source>
        <translation>Cooling:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12824"/>
        <source>Roasting Notes</source>
        <translation>Roasting Notes</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12834"/>
        <source>Cupping Notes</source>
        <translation>Cupping Notes</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12738"/>
        <source>TP:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12804"/>
        <source>Events</source>
        <translation type="unfinished">イベント</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12778"/>
        <source>CM:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12804"/>
        <source>Background:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="artisanlib/main.py" line="6036"/>
        <source>deg/min</source>
        <translation>度/分</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23238"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23614"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2912"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2919"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16066"/>
        <source>at</source>
        <translation>at</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23067"/>
        <source>CHARGE</source>
        <translation>CHARGE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15758"/>
        <source>DRY END</source>
        <translation>DRY END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15772"/>
        <source>FC START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15787"/>
        <source>FC END</source>
        <translation>FC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15801"/>
        <source>SC START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15815"/>
        <source>SC END</source>
        <translation>SC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23080"/>
        <source>DROP</source>
        <translation>DROP</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1302"/>
        <source>EVENT</source>
        <translation>EVENT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2709"/>
        <source>BackgroundET</source>
        <translation>背景の ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2713"/>
        <source>BackgroundBT</source>
        <translation>背景の BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2743"/>
        <source>BackgroundDeltaET</source>
        <translation>背景の DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2747"/>
        <source>BackgroundDeltaBT</source>
        <translation>背景の DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12910"/>
        <source>d/m</source>
        <translation>d/m</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3793"/>
        <source>BT=%1-%2 (%3)   ET=%4-%5 (%6)   T=%7   RoR=%8d/m   ETBTa=%9 [%11-%12]</source>
        <translation type="obsolete">BT=%1-%2 (%3)   ET=%4-%5 (%6)   T=%7   RoR=%8d/m   ETBTa=%9 [%11-%12]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23095"/>
        <source>Time</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5519"/>
        <source>BT %1 d/m for %2</source>
        <translation>BT %1 d/m for %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5535"/>
        <source>ET %1 d/m for %2</source>
        <translation>ET %1 d/m for %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7624"/>
        <source>PID SV</source>
        <translation>PID SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7628"/>
        <source>PID %</source>
        <translation>PID %</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7681"/>
        <source>Event #&lt;b&gt;0 &lt;/b&gt;</source>
        <translation>Event #&lt;b&gt;0 &lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9279"/>
        <source>Event #&lt;b&gt;%1 &lt;/b&gt;</source>
        <translation>Event #&lt;b&gt;%1 &lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13801"/>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13803"/>
        <source>City+</source>
        <translation>City+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13805"/>
        <source>Full City</source>
        <translation>Full City</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13807"/>
        <source>Full City+</source>
        <translation>Full City+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13809"/>
        <source>Light French</source>
        <translation>Light French</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13811"/>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14098"/>
        <source>%1 to reach ET target %2</source>
        <translation>ET ターゲット %2 に到達まで %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14109"/>
        <source> at %1</source>
        <translation>%1 で</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14111"/>
        <source>%1 to reach BT target %2</source>
        <translation>BT ターゲット %2 に到達まで %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14121"/>
        <source>%1 after FCs</source>
        <translation>FCs 後の %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14128"/>
        <source>%1 after FCe</source>
        <translation>FCe 後の %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14168"/>
        <source>ET - BT = %1</source>
        <translation>ET - BT = %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14219"/>
        <source>ET - BT = %1%2</source>
        <translation>ET - BT = %1%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14602"/>
        <source>ET Target 1</source>
        <translation>ET ターゲット 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14604"/>
        <source>BT Target 1</source>
        <translation>BT ターゲット 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14606"/>
        <source>ET Target 2</source>
        <translation>ET ターゲット 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14608"/>
        <source>BT Target 2</source>
        <translation>BT ターゲット 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31104"/>
        <source>Mode</source>
        <translation>モード</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14612"/>
        <source>ET p-i-d 1</source>
        <translation>ET p-i-d 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14619"/>
        <source>Smooth Deltas</source>
        <translation>スムースデルタ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14627"/>
        <source>Smooth Curves</source>
        <translation>スムースカーブ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31125"/>
        <source>min</source>
        <translation>下限</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31133"/>
        <source>max</source>
        <translation>上限</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14873"/>
        <source>Y(x)</source>
        <translation>Y(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15846"/>
        <source>COOL</source>
        <translation>COOL</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15889"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15892"/>
        <source>Date</source>
        <translation>日時</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15898"/>
        <source>Beans</source>
        <translation>豆</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15908"/>
        <source>Weight</source>
        <translation>重さ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15943"/>
        <source> in</source>
        <translation> 焙煎前</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15944"/>
        <source> out</source>
        <translation> 焙煎後</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15957"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15942"/>
        <source>Volume</source>
        <translation>体積</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15980"/>
        <source>Density</source>
        <translation>比重</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15995"/>
        <source>per</source>
        <translation>per</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16015"/>
        <source>Bean Size</source>
        <translation>豆サイズ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16021"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16023"/>
        <source>Whole Color</source>
        <translation>豆の色</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16029"/>
        <source>Ground Color</source>
        <translation>粉の色</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16040"/>
        <source>Storage Conditions</source>
        <translation>保管状態</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16060"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16058"/>
        <source>Ambient Conditions</source>
        <translation>周囲状態</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16077"/>
        <source>Roaster</source>
        <translation>焙煎機</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16079"/>
        <source>Operator</source>
        <translation>オペレーター</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16080"/>
        <source>Roasting Notes</source>
        <translation>焙煎ノート</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16084"/>
        <source>Cupping Notes</source>
        <translation>カッピングノート</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16161"/>
        <source>Ambient Source</source>
        <translation>「周囲」のソース</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16761"/>
        <source>                 Density in: %1  g/l   =&gt;   Density out: %2 g/l</source>
        <translation>                 焙煎前比重 : %1  g/l   ⇒   焙煎後比重 : %2 g/l</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16775"/>
        <source>(%1 g/l)</source>
        <translation>(%1 g/l)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17114"/>
        <source>Number of errors found %1</source>
        <translation>%1 個のエラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18126"/>
        <source>Max</source>
        <translation>最大</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18124"/>
        <source>Min</source>
        <translation>最小</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17258"/>
        <source>Rotation</source>
        <translation>回転</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17281"/>
        <source>Initial Max</source>
        <translation>初期最大</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17313"/>
        <source>Step</source>
        <translation>ステップ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17321"/>
        <source>Style</source>
        <translation>スタイル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17331"/>
        <source>Width</source>
        <translation>線幅</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19793"/>
        <source>Opaqueness</source>
        <translation>不透明度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17522"/>
        <source>Enter two times along profile</source>
        <translation>プロファイルに沿って2つの時間を入力してください</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17525"/>
        <source>Start (00:00)</source>
        <translation>開始時間 (00:00)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17526"/>
        <source>End (00:00)</source>
        <translation>終了時間 (00:00)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17545"/>
        <source>Fahrenheit</source>
        <translation>華氏 (F)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17546"/>
        <source>Celsius</source>
        <translation>摂氏 (C)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17673"/>
        <source>Time syntax error. Time not valid</source>
        <translation>時間構文エラー: 時間が正しくありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17677"/>
        <source>Error: End time smaller than Start time</source>
        <translation>エラー: 終了時間が開始時間より小さい</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17694"/>
        <source>Best approximation was made from %1 to %2</source>
        <translation>最も近い値: %1 ～ %2 から求めました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14306"/>
        <source>deg/sec = %1    deg/min = &lt;b&gt;%2&lt;<byte value="x8"/>&gt;</source>
        <translation type="obsolete">度/秒 = %1    度/分 = &lt;b&gt;%2&lt;<byte value="x8"/>&gt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17699"/>
        <source>No profile found</source>
        <translation>プロファイルがありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17774"/>
        <source>Bars</source>
        <translation>バー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17846"/>
        <source>Color</source>
        <translation>カラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23093"/>
        <source>Marker</source>
        <translation>マーカー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17850"/>
        <source>Thickness</source>
        <translation>線幅</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17852"/>
        <source>Opacity</source>
        <translation>不透明度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17854"/>
        <source>Size</source>
        <translation>サイズ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17965"/>
        <source>Max buttons per row</source>
        <translation>列中のボタンの数</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17993"/>
        <source>Color Pattern</source>
        <translation>カラーパターン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18005"/>
        <source>palette #</source>
        <translation>パターン #</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18120"/>
        <source>Event</source>
        <translation>イベント</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18028"/>
        <source>Action</source>
        <translation>アクション</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18030"/>
        <source>Command</source>
        <translation>コマンド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18032"/>
        <source>Offset</source>
        <translation>オフセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18034"/>
        <source>Factor</source>
        <translation>ファクター</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20271"/>
        <source>Drying</source>
        <translation>Drying</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20272"/>
        <source>Maillard</source>
        <translation>Maillard</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20273"/>
        <source>Development</source>
        <translation>Development</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19534"/>
        <source>Default</source>
        <translation>デフォルト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19564"/>
        <source>Aspect Ratio</source>
        <translation>縦横比</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19806"/>
        <source>ET Color</source>
        <translation>ET カラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19813"/>
        <source>BT Color</source>
        <translation>BT カラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19820"/>
        <source>DeltaET Color</source>
        <translation>DeltaET カラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19827"/>
        <source>DeltaBT Color</source>
        <translation>DeltaBT カラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19872"/>
        <source>Text Warning</source>
        <translation>文字による警告</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19873"/>
        <source>sec</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20274"/>
        <source>Cooling</source>
        <translation>Cooling</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23237"/>
        <source>Curviness</source>
        <translation>カーブ強度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23250"/>
        <source>Events Playback</source>
        <translation>イベントプレーバック</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24011"/>
        <source>Comm Port</source>
        <translation>ポート</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24015"/>
        <source>Baud Rate</source>
        <translation>通信速度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24021"/>
        <source>Byte Size</source>
        <translation>データー長</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24027"/>
        <source>Parity</source>
        <translation>パリティービット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24034"/>
        <source>Stopbits</source>
        <translation>ストップビット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24040"/>
        <source>Timeout</source>
        <translation>タイムアウト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23764"/>
        <source>Settings for non-Modbus devices</source>
        <translation>Modbus デバイス以外の設定</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23885"/>
        <source>Slave</source>
        <translation>スレーブ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23890"/>
        <source>Register</source>
        <translation>レジスタ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23895"/>
        <source>Float</source>
        <translation>float型</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23896"/>
        <source>Function</source>
        <translation>ファンクション</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24001"/>
        <source>Device</source>
        <translation>デバイス</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24575"/>
        <source>Control ET</source>
        <translation>Control ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24579"/>
        <source>Read BT</source>
        <translation>Read BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24583"/>
        <source>Type</source>
        <translation>タイプ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24584"/>
        <source>RS485 Unit ID</source>
        <translation>RS485 ユニットID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24600"/>
        <source>ET Channel</source>
        <translation>ET チャンネル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24603"/>
        <source>BT Channel</source>
        <translation>BT チャンネル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24623"/>
        <source>AT Channel</source>
        <translation>AT チャンネル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24637"/>
        <source>ET Y(x)</source>
        <translation>ET Y(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24638"/>
        <source>BT Y(x)</source>
        <translation>BT Y(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26309"/>
        <source>Ratio</source>
        <translation>比率</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26316"/>
        <source>Text</source>
        <translation>テキスト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26323"/>
        <source>Edge</source>
        <translation>境界</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26329"/>
        <source>Line</source>
        <translation>線幅</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26338"/>
        <source>Color pattern</source>
        <translation>カラーパターン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26618"/>
        <source> dg</source>
        <translation> dg</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26981"/>
        <source>Enter description</source>
        <translation>記述の入力</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27472"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(1-4)</source>
        <translation>Ramp Soak (時:分)&lt;br&gt;(1-4)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27477"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(5-8)</source>
        <translation>Ramp Soak (時:分)&lt;br&gt;(5-8)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27479"/>
        <source>Ramp/Soak Pattern</source>
        <translation>Ramp/Soak パターン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>WARNING</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>Writing eeprom memory</source>
        <translation>EEPROMメモリへ書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>&lt;u&gt;Max life&lt;/u&gt; 10,000 writes</source>
        <translation>&lt;u&gt;限界寿命は&lt;/u&gt; 10,000 書き換え程度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>Infinite read life.</source>
        <translation>読み込みに限界寿命はありません。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>After &lt;u&gt;writing&lt;/u&gt; an adjustment,&lt;br&gt;never power down the pid&lt;br&gt;for the next 5 seconds &lt;br&gt;or the pid may never recover.</source>
        <translation>調整事項の&lt;u&gt;書き込み&lt;/u&gt;後、&lt;br&gt;5秒間は決してPIDの&lt;br&gt;電源を落さないで下さい、&lt;br&gt; PIDが回復しなくなる場合があります。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>Read operations manual</source>
        <translation>操作マニュアルを読む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29056"/>
        <source>ET Thermocouple type</source>
        <translation>ET 熱電対タイプ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29063"/>
        <source>BT Thermocouple type</source>
        <translation>BT 熱電対タイプ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28846"/>
        <source>NOTE: BT Thermocouple type is not stored in the Artisan settings</source>
        <translation>注: BT の熱電対タイプが Artisan の設定に保存されていません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28949"/>
        <source>Artisan uses 1 decimal point</source>
        <translation>Artisan は小数点以下第一位までを使用します</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28491"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(1-7)</source>
        <translation>Ramp Soak (分:秒)&lt;br&gt;(1-7)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28497"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(8-16)</source>
        <translation>Ramp Soak (分:秒)&lt;br&gt;(8-16)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28544"/>
        <source>Pattern</source>
        <translation>パターン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28600"/>
        <source>SV (7-0)</source>
        <translation>SV (7-0)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28722"/>
        <source>Write</source>
        <translation>書込み</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28704"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28710"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28716"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23313"/>
        <source>NOTE: BT Thermocouple type is not stored in the Artisan seetings</source>
        <translation type="obsolete">注: BT の熱電対タイプが Artisan の設定に保存されていません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28950"/>
        <source>Artisan Fuji PXG uses MINUTES:SECONDS units in Ramp/Soaks</source>
        <translation>Artisan 富士電気PXG は Ramp/Soaks で「分:秒」単位を使用します</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31377"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>ETBTa</source>
        <translation type="unfinished">ETBTa</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>RoR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6041"/>
        <source>Curves</source>
        <translation type="unfinished">カーブ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6045"/>
        <source>Delta Curves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14984"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14985"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17695"/>
        <source>&lt;b&gt;%1&lt;/b&gt; deg/sec, &lt;b&gt;%2&lt;/b&gt; deg/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14816"/>
        <source>Path Effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14836"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8436"/>
        <source>TP</source>
        <translation type="unfinished">TP</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8464"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8498"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10219"/>
        <source>Charge the beans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10205"/>
        <source>Start recording</source>
        <translation type="unfinished">記録を開始</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17162"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31037"/>
        <source>Source</source>
        <translation type="unfinished">ソース</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4626"/>
        <source>CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14635"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24673"/>
        <source>1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24677"/>
        <source>2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24681"/>
        <source>3:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24685"/>
        <source>4:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31053"/>
        <source>Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31099"/>
        <source>Lookahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31106"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31107"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31108"/>
        <source>Background</source>
        <translation type="unfinished">背景</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31113"/>
        <source>SV Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31116"/>
        <source>SV Slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18128"/>
        <source>Coarse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23928"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23933"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14598"/>
        <source>HUD Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24735"/>
        <source>Raw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24736"/>
        <source>Data rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24737"/>
        <source>Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24756"/>
        <source>ServerId:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24758"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="const/UIconst.py" line="35"/>
        <source>Services</source>
        <translation>サービス</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="36"/>
        <source>Hide %1</source>
        <translation>%1を隠す</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="37"/>
        <source>Hide Others</source>
        <translation>ほかを隠す</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="38"/>
        <source>Show All</source>
        <translation>すべてを表示</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="39"/>
        <source>Preferences...</source>
        <translation>環境設定...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="61"/>
        <source>Quit %1</source>
        <translation>%1 を終了</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="141"/>
        <source>About %1</source>
        <translation>%1 について</translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="artisanlib/main.py" line="17813"/>
        <source>Circle</source>
        <translation>円形</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17814"/>
        <source>Square</source>
        <translation>四角形</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17815"/>
        <source>Pentagon</source>
        <translation>五角形</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17816"/>
        <source>Diamond</source>
        <translation>ダイア型</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17817"/>
        <source>Star</source>
        <translation>星型</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17818"/>
        <source>Hexagon 1</source>
        <translation>六角形１</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17819"/>
        <source>Hexagon 2</source>
        <translation>六角形２</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17820"/>
        <source>+</source>
        <translation>＋</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17821"/>
        <source>x</source>
        <translation>×</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17822"/>
        <source>None</source>
        <translation>無し</translation>
    </message>
</context>
<context>
    <name>Menu</name>
    <message>
        <location filename="artisanlib/main.py" line="6923"/>
        <source>CSV...</source>
        <translation>CSV...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6927"/>
        <source>JSON...</source>
        <translation>JSON...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6931"/>
        <source>RoastLogger...</source>
        <translation>RoastLogger...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6896"/>
        <source>HH506RA...</source>
        <translation>HH506RA...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6900"/>
        <source>K202...</source>
        <translation>K202...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6904"/>
        <source>K204...</source>
        <translation>K204...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="44"/>
        <source>File</source>
        <translation>ファイル</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="47"/>
        <source>New</source>
        <translation>新規</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="48"/>
        <source>Open...</source>
        <translation>開く...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="49"/>
        <source>Open Recent</source>
        <translation>最近使用したファイルを開く</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="50"/>
        <source>Import</source>
        <translation>取り込み</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="51"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="52"/>
        <source>Save As...</source>
        <translation>別名で保存...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="53"/>
        <source>Export</source>
        <translation>書き出し</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="54"/>
        <source>Save Graph</source>
        <translation>グラフを保存</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="55"/>
        <source>Full Size...</source>
        <translation>最大サイズ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="56"/>
        <source>Roasting Report</source>
        <translation>焙煎リポート</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="57"/>
        <source>Print...</source>
        <translation>印刷...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="64"/>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="67"/>
        <source>Cut</source>
        <translation>切り抜き</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="68"/>
        <source>Copy</source>
        <translation>コピー</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="69"/>
        <source>Paste</source>
        <translation>貼り付け</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="72"/>
        <source>Roast</source>
        <translation>焙煎</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="75"/>
        <source>Properties...</source>
        <translation>プロパティ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="76"/>
        <source>Background...</source>
        <translation>プロファイルの背景...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="77"/>
        <source>Cup Profile...</source>
        <translation>カッププロファイル...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="78"/>
        <source>Temperature</source>
        <translation>温度単位</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="79"/>
        <source>Convert to Fahrenheit</source>
        <translation>華氏 (F) へ変換</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="80"/>
        <source>Convert to Celsius</source>
        <translation>摂氏 (C) へ変換</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="81"/>
        <source>Fahrenheit Mode</source>
        <translation>華氏 (F) モード</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="82"/>
        <source>Celsius Mode</source>
        <translation>摂氏 (C) モード</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="86"/>
        <source>Config</source>
        <translation>構成</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="89"/>
        <source>Device...</source>
        <translation>デバイス...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="90"/>
        <source>Serial Port...</source>
        <translation>シリアルポート...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="91"/>
        <source>Sampling Interval...</source>
        <translation>サンプリング間隔...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="93"/>
        <source>Colors...</source>
        <translation>カラー...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="94"/>
        <source>Phases...</source>
        <translation>フェーズ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="95"/>
        <source>Events...</source>
        <translation>イベント...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="96"/>
        <source>Statistics...</source>
        <translation>統計...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="97"/>
        <source>Axes...</source>
        <translation>グラフパーツ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="98"/>
        <source>Autosave...</source>
        <translation>オートセーブ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="99"/>
        <source>Alarms...</source>
        <translation>アラーム...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="100"/>
        <source>Language</source>
        <translation>インターフェースの言語</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="124"/>
        <source>Tools</source>
        <translation>ツール</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="127"/>
        <source>Designer</source>
        <translation>デザイナー</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="128"/>
        <source>Calculator</source>
        <translation>計算機</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="129"/>
        <source>Wheel Graph</source>
        <translation>ホイールグラフ</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="130"/>
        <source>Extras...</source>
        <translation>エクストラ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="134"/>
        <source>Help</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="143"/>
        <source>Documentation</source>
        <translation>関連文書</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="133"/>
        <source>Blog</source>
        <translation type="obsolete">ブログ</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="145"/>
        <source>Keyboard Shortcuts</source>
        <translation>ショートカット</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="146"/>
        <source>Errors</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="147"/>
        <source>Messages</source>
        <translation>メッセージ</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="148"/>
        <source>Serial</source>
        <translation>シリアル</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="152"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="153"/>
        <source>Platform</source>
        <translation>実行環境</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="154"/>
        <source>Factory Reset</source>
        <translation>ファクトリリセット</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="142"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="83"/>
        <source>Switch Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="92"/>
        <source>Oversampling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="artisanlib/main.py" line="1495"/>
        <source>HUD OFF</source>
        <translation>HUD OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1508"/>
        <source>HUD ON</source>
        <translation>HUD ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1191"/>
        <source>Need some data for HUD to work</source>
        <translation type="obsolete">HUD の動作にはいくつかのデーターが必要です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1587"/>
        <source>Alarm notice</source>
        <translation>アラーム通知</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1592"/>
        <source>Alarm is calling: %1</source>
        <translation>アラーム呼び出し : %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1599"/>
        <source>Alarm trigger button error, description &apos;%1&apos; not a number</source>
        <translation>アラームトリガーボタンエラー、記述 &apos;%1&apos; は数値ではありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1627"/>
        <source>Alarm trigger slider error, description &apos;%1&apos; not a valid number [0-100]</source>
        <translation>アラームトリガースライダーエラー、記述 &apos;%1&apos; は [0-100] の範囲ではありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2001"/>
        <source>Save the profile, Discard the profile (Reset), or Cancel?</source>
        <translation>プロファイルを保存するか、プロファイルを破棄（リセット）するか、キャンセルしますか ?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2002"/>
        <source>Profile unsaved</source>
        <translation>プロファイルは保存されませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2010"/>
        <source>Action canceled</source>
        <translation>アクションはキャンセルされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2076"/>
        <source>Scope has been reset</source>
        <translation>スコープはリセットされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3181"/>
        <source>Time format error encountered</source>
        <translation>時間書式エラーが発生しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3282"/>
        <source>Convert profile data to Fahrenheit?</source>
        <translation>プロファイルのデーターを華氏に変換しますか ?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3360"/>
        <source>Convert Profile Temperature</source>
        <translation>プロファイルの温度単位を変換しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3319"/>
        <source>Profile changed to Fahrenheit</source>
        <translation>プロファイルを華氏に変更しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3322"/>
        <source>Unable to comply. You already are in Fahrenheit</source>
        <translation>すでに華氏なので、対応できません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3362"/>
        <source>Profile not changed</source>
        <translation>プロファイルは変更されませんした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3328"/>
        <source>Convert profile data to Celsius?</source>
        <translation>プロファイルのデーターを摂氏に変換しますか ?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3360"/>
        <source>Unable to comply. You already are in Celsius</source>
        <translation>すでに摂氏なので、対応できません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3366"/>
        <source>Profile changed to Celsius</source>
        <translation>プロファイルを摂氏に変更しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3371"/>
        <source>Convert Profile Scale</source>
        <translation>プロファイルのスケールを変換しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3371"/>
        <source>No profile data found</source>
        <translation>プロファイルにデーターがありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3388"/>
        <source>Colors set to defaults</source>
        <translation>カラーはデフォルトにセットされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3393"/>
        <source>Colors set to grey</source>
        <translation>カラーは「グレー」にセットされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3521"/>
        <source>Scope monitoring...</source>
        <translation>スコープをモニタ中...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3564"/>
        <source>Scope stopped</source>
        <translation>スコープを停止</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3629"/>
        <source>Scope recording...</source>
        <translation>スコープを記録中...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3669"/>
        <source>Scope recording stopped</source>
        <translation>スコープの記録を停止</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3719"/>
        <source>Not enough variables collected yet. Try again in a few seconds</source>
        <translation>ここまでに必要十分な変数は集まりませんでした。数秒後に再開します</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3752"/>
        <source>Roast time starts now 00:00 BT = %1</source>
        <translation>焙煎を開始します 00:00 BT = %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4172"/>
        <source>Scope is OFF</source>
        <translation>スコープは OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3833"/>
        <source>[DRY END] recorded at %1 BT = %2</source>
        <translation>[DRY END] を記録しました %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3892"/>
        <source>[FC START] recorded at %1 BT = %2</source>
        <translation>[FC START] を記録しました %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3946"/>
        <source>[FC END] recorded at %1 BT = %2</source>
        <translation>[FC END] を記録しました %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4004"/>
        <source>[SC START] recorded at %1 BT = %2</source>
        <translation>[SC START] を記録しました %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4061"/>
        <source>[SC END] recorded at %1 BT = %2</source>
        <translation>[SC END] を記録しました %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4141"/>
        <source>Roast ended at %1 BT = %2</source>
        <translation>焙煎を終了しました %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4205"/>
        <source>[COOL END] recorded at %1 BT = %2</source>
        <translation>[COOL END] を記録しました %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4319"/>
        <source>Event # %1 recorded at BT = %2 Time = %3</source>
        <translation>Event # %1 を記録しました BT = %2 Time = %3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4334"/>
        <source>Timer is OFF</source>
        <translation>タイマー OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4366"/>
        <source>Computer Event # %1 recorded at BT = %2 Time = %3</source>
        <translation>Computer Event # %1 を記録しました BT = %2 Time = %3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4445"/>
        <source>Statistics cancelled: need complete profile [CHARGE] + [DROP]</source>
        <translation>統計をキャンセル: 完全なプロファイルが必要です [CHARGE] + [DROP]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4676"/>
        <source>Unable to move background</source>
        <translation>背景は移動できません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4735"/>
        <source>No finished profile found</source>
        <translation>完了したプロファイルが見つかりません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4753"/>
        <source>Polynomial coefficients (Horner form):</source>
        <translation>多項式係数（ホーナー形式）:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4756"/>
        <source>Knots:</source>
        <translation>Knots:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4759"/>
        <source>Residual:</source>
        <translation>Residual:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4762"/>
        <source>Roots:</source>
        <translation>Roots:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4766"/>
        <source>Profile information</source>
        <translation>プロフィル情報</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4919"/>
        <source>Designer Start</source>
        <translation>デザイナーを開始</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4341"/>
        <source>Importing a profile in to Designer will decimate
all data except the main [points].
Continue?</source>
        <translation type="obsolete">デザイナー内でプロファイルを取り込む場合、
メイン ［ポイント］以外の全データーは decimate されます。
続けますか?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4963"/>
        <source>Designer Init</source>
        <translation>デザイナーを初期化しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4963"/>
        <source>Unable to start designer.
Profile missing [CHARGE] or [DROP]</source>
        <translation>デザイナーを開始できません。
プロファイルに [CHARGE] か [DROP] が見つかりません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5179"/>
        <source>[ CHARGE ]</source>
        <translation>[ CHARGE ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5182"/>
        <source>[ DRY END ]</source>
        <translation>[ DRY END ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5185"/>
        <source>[ FC START ]</source>
        <translation>[ FC START ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5188"/>
        <source>[ FC END ]</source>
        <translation>[ FC END ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5191"/>
        <source>[ SC START ]</source>
        <translation>[ SC START ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5194"/>
        <source>[ SC END ]</source>
        <translation>[ SC END ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5197"/>
        <source>[ DROP ]</source>
        <translation>[ DROP ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5460"/>
        <source>New profile created</source>
        <translation>新しいプロファイルが作成されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26791"/>
        <source>Open Wheel Graph</source>
        <translation>ホイールグラフを開く</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5647"/>
        <source> added to cupping notes</source>
        <translation> カッピングノートを追加しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5653"/>
        <source> added to roasting notes</source>
        <translation> 焙煎ノートを追加しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5920"/>
        <source>Mouse Cross ON: move mouse around</source>
        <translation>マウスクロス ON: マウスを動かせます</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5932"/>
        <source>Mouse cross OFF</source>
        <translation>マウスクロス OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8775"/>
        <source>Do you want to reset all settings?</source>
        <translation>あなたは全設定をリセットしますか?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8776"/>
        <source>Factory Reset</source>
        <translation>ファクトリーリセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8967"/>
        <source>Exit Designer?</source>
        <translation>デザイナーを終了しますか ?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8968"/>
        <source>Designer Mode ON</source>
        <translation>デザイナーモード ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8931"/>
        <source>Extra Event Button Palette</source>
        <translation type="obsolete">エクストライベントボタンパレット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9145"/>
        <source>Keyboard moves turned ON</source>
        <translation>キーボードでの移動を ON に</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9154"/>
        <source>Keyboard moves turned OFF</source>
        <translation>キーボードでの移動を OFF に</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9236"/>
        <source>Profile %1 saved in: %2</source>
        <translation>プロファイル %1 がセーブされました: %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9243"/>
        <source>Empty path or box unchecked in Autosave</source>
        <translation>パスが空かオートセーブが無効になっています</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9250"/>
        <source>&lt;b&gt;[ENTER]&lt;/b&gt; = Turns ON/OFF Keyboard Shortcuts</source>
        <translation>&lt;b&gt;[ENTER]&lt;/b&gt; = キーボードショートカットの ON/OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9251"/>
        <source>&lt;b&gt;[SPACE]&lt;/b&gt; = Choses current button</source>
        <translation>&lt;b&gt;[SPACE]&lt;/b&gt; = 現在のボタンを選択</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9252"/>
        <source>&lt;b&gt;[LEFT]&lt;/b&gt; = Move to the left</source>
        <translation>&lt;b&gt;[LEFT]&lt;/b&gt; = 左へ移動</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9253"/>
        <source>&lt;b&gt;[RIGHT]&lt;/b&gt; = Move to the right</source>
        <translation>&lt;b&gt;[RIGHT]&lt;/b&gt; = 右へ移動</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9254"/>
        <source>&lt;b&gt;[a]&lt;/b&gt; = Autosave</source>
        <translation>&lt;b&gt;[a]&lt;/b&gt; = オートセーブ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9255"/>
        <source>&lt;b&gt;[CRTL N]&lt;/b&gt; = Autosave + Reset + START</source>
        <translation>&lt;b&gt;[CRTL N]&lt;/b&gt; = オートセーブ + リセット + START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9256"/>
        <source>&lt;b&gt;[t]&lt;/b&gt; = Mouse cross lines</source>
        <translation>&lt;b&gt;[t]&lt;/b&gt; = マウスクロスライン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9258"/>
        <source>&lt;b&gt;[b]&lt;/b&gt; = Shows/Hides Extra Event Buttons</source>
        <translation>&lt;b&gt;[b]&lt;/b&gt; = エクストライベントボタンの 表示/非表示</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9259"/>
        <source>&lt;b&gt;[s]&lt;/b&gt; = Shows/Hides Event Sliders</source>
        <translation type="unfinished">&lt;b&gt;[s]&lt;/b&gt; = イベントスライダーの 表示/非表示</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9260"/>
        <source>&lt;b&gt;[i]&lt;/b&gt; = Retrieve Weight In from Scale</source>
        <translation>&lt;b&gt;[i]&lt;/b&gt; = はかりから焙煎前重量を取得</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9261"/>
        <source>&lt;b&gt;[o]&lt;/b&gt; = Retrieve Weight Out from Scale</source>
        <translation>&lt;b&gt;[o]&lt;/b&gt; = はかりから焙煎後重量を取得</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9262"/>
        <source>&lt;b&gt;[0-9]&lt;/b&gt; = Changes Event Button Palettes</source>
        <translation>&lt;b&gt;[0-9]&lt;/b&gt; = イベントボタンのパレットを変更</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9263"/>
        <source>&lt;b&gt;[;]&lt;/b&gt; = Application ScreenShot</source>
        <translation>&lt;b&gt;[;]&lt;/b&gt; = アプリケーションスクリーンショット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9264"/>
        <source>&lt;b&gt;[:]&lt;/b&gt; = Desktop ScreenShot</source>
        <translation>&lt;b&gt;[:]&lt;/b&gt; = デスクトップスクリーンショット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9268"/>
        <source>Keyboard Shotcuts</source>
        <translation>キーボードショートカット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9349"/>
        <source>Event #%1:  %2 has been updated</source>
        <translation>Event #%1:  %2 は更新されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9436"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9445"/>
        <source>Select Directory</source>
        <translation>ディレクトリを選択</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16692"/>
        <source>No profile found</source>
        <translation>プロファイルがありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9498"/>
        <source>%1 has been saved. New roast has started</source>
        <translation>%1 は保存され、新しい焙煎を開始しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9680"/>
        <source>Invalid artisan format</source>
        <translation>Artisan 形式ではありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9540"/>
        <source>%1  loaded </source>
        <translation>%1  はロードされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9676"/>
        <source>Background %1 loaded successfully %2</source>
        <translation>背景用 %1 は正しくロードされました %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9790"/>
        <source>Artisan CSV file loaded successfully</source>
        <translation>Artisan CSVファイルは正しくロードされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10369"/>
        <source>To load this profile the extra devices configuration needs to be changed.
Continue?</source>
        <translation>このプロファイルをロードすると、エクストラデバイスの構成が変更されます。
続けますか?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10370"/>
        <source>Found a different number of curves</source>
        <translation>異なる数のカーブを見つけました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11063"/>
        <source>Save Profile</source>
        <translation>プロファイルを保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11070"/>
        <source>Profile saved</source>
        <translation>プロファイルは保存されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11109"/>
        <source>Cancelled</source>
        <translation>キャンセルされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11086"/>
        <source>Readings exported</source>
        <translation>計測値は書き出されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11094"/>
        <source>Export CSV</source>
        <translation>CSV の書き出し</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11097"/>
        <source>Export JSON</source>
        <translation>JSON の書き出し</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11100"/>
        <source>Export RoastLogger</source>
        <translation>RoastLogger の書き出し</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11107"/>
        <source>Readings imported</source>
        <translation>計測値は取り込まれました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11115"/>
        <source>Import CSV</source>
        <translation>CSV の取り込み</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11118"/>
        <source>Import JSON</source>
        <translation>JSON の取り込み</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11121"/>
        <source>Import RoastLogger</source>
        <translation>RoastLogger の取り込み</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13524"/>
        <source>Sampling Interval</source>
        <translation>サンプリング間隔</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13524"/>
        <source>Seconds</source>
        <translation>秒数</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13730"/>
        <source>Alarm Config</source>
        <translation>アラーム構成</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13730"/>
        <source>Alarms are not available for device None</source>
        <translation>デバイスがないのでアラームは利用できません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13783"/>
        <source>Switch Language</source>
        <translation>インターフェース言語の切り替え</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13783"/>
        <source>Language successfully changed. Restart the application.</source>
        <translation>インターフェース言語を切り替えました。アプリケーションをリスタートしてください。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13815"/>
        <source>Import K202 CSV</source>
        <translation>K202 CSV の取り込み</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13865"/>
        <source>K202 file loaded successfully</source>
        <translation>K202 ファイルを正しくロードしました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13879"/>
        <source>Import K204 CSV</source>
        <translation>K204 CSV の取り込み</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13943"/>
        <source>K204 file loaded successfully</source>
        <translation>K204 ファイルを正しくロードしました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13957"/>
        <source>Import HH506RA CSV</source>
        <translation>HH506RA CSV の取り込み</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14006"/>
        <source>HH506RA file loaded successfully</source>
        <translation>HH506RA ファイルを正しくロードしました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14047"/>
        <source>Save Graph as PNG</source>
        <translation>グラフを PNG 形式で保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14054"/>
        <source>%1  size(%2,%3) saved</source>
        <translation>%1  サイズ（%2 %3）を保存しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14063"/>
        <source>Save Graph as SVG</source>
        <translation>グラフを SVG 形式で保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14068"/>
        <source>%1 saved</source>
        <translation>%1 は保存されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14276"/>
        <source>Invalid Wheel graph format</source>
        <translation>ホイールグラフ形式ではありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14279"/>
        <source>Wheel Graph succesfully open</source>
        <translation>ホイールグラフを開きました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14298"/>
        <source>Return the absolute value of x.</source>
        <translation>x の絶対値を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14299"/>
        <source>Return the arc cosine (measured in radians) of x.</source>
        <translation>x の逆コサイン（ラジアン）を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14300"/>
        <source>Return the arc sine (measured in radians) of x.</source>
        <translation>x の逆サイン（ラジアン）を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14301"/>
        <source>Return the arc tangent (measured in radians) of x.</source>
        <translation>x の逆タンジェント（ラジアン）を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14302"/>
        <source>Return the cosine of x (measured in radians).</source>
        <translation>x のコサイン（ラジアン）を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14303"/>
        <source>Convert angle x from radians to degrees.</source>
        <translation>角度 x をラジアンから度数（degrees）に変換します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14304"/>
        <source>Return e raised to the power of x.</source>
        <translation>e (e は自然対数の底)の x 乗を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14305"/>
        <source>Return the logarithm of x to the given base. </source>
        <translation>与えられた底で x の対数を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14306"/>
        <source>Return the base 10 logarithm of x.</source>
        <translation>底を 10 とする x の対数を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14309"/>
        <source>Return x**y (x to the power of y).</source>
        <translation>x**y （x の y 乗）を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14310"/>
        <source>Convert angle x from degrees to radians.</source>
        <translation>角度 x を度数（degrees）からラジアンに変換します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14311"/>
        <source>Return the sine of x (measured in radians).</source>
        <translation>x のサイン（ラジアン）を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14312"/>
        <source>Return the square root of x.</source>
        <translation>x の平方根を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14313"/>
        <source>Return the tangent of x (measured in radians).</source>
        <translation>x のタンジェント（ラジアン）を返します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14316"/>
        <source>previous ET value</source>
        <translation>以前の ET 値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14317"/>
        <source>previous BT value</source>
        <translation>以前の BT 値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14318"/>
        <source>previous Extra #1 T1 value</source>
        <translation>以前の エクストラ #1 T1 値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14319"/>
        <source>previous Extra #1 T2 value</source>
        <translation>以前の エクストラ #1 T2 値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14320"/>
        <source>previous Extra #2 T1 value</source>
        <translation>以前の エクストラ #2 T1 値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14321"/>
        <source>previous Extra #2 T2 value</source>
        <translation>以前の エクストラ #2 T2 値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14328"/>
        <source>MATHEMATICAL FUNCTIONS</source>
        <translation>数 学 関 数</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14330"/>
        <source>SYMBOLIC VARIABLES</source>
        <translation>シ ン ボ リ ッ ク 変 数</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14332"/>
        <source>Symbolic Functions</source>
        <translation>シンボリック関数</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14470"/>
        <source>Save Palettes</source>
        <translation>パレットを保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14474"/>
        <source>Palettes saved</source>
        <translation>パレットは保存されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14514"/>
        <source>Invalid palettes file format</source>
        <translation>パレットファイル形式ではありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14517"/>
        <source>Palettes loaded</source>
        <translation>パレットはロードされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14529"/>
        <source>Load Palettes</source>
        <translation>パレットをロード</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14550"/>
        <source>Alarms loaded</source>
        <translation>アラームはロードされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15526"/>
        <source>Interpolation failed: no profile available</source>
        <translation>補間に失敗: プロファイルがありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15535"/>
        <source>Sound turned ON</source>
        <translation>サウンドを ON に</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15539"/>
        <source>Sound turned OFF</source>
        <translation>サウンドを OFF に</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15705"/>
        <source>[ET target 1 = %1] [BT target 1 = %2] [ET target 2 = %3] [BT target 2 = %4]</source>
        <translation>[ETターゲット 1 = %1] [BTターゲット 1 = %2] [ETターゲット 2 = %3] [BTターゲット 2 = %4]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16689"/>
        <source>Event #%1 added</source>
        <translation>Event #%1 は追加されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16712"/>
        <source> Event #%1 deleted</source>
        <translation> Event #%1 は削除されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16717"/>
        <source>No events found</source>
        <translation>イベントがありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16952"/>
        <source>Roast properties updated but profile not saved to disk</source>
        <translation>焙煎プロパティは更新されましたが、プロファイルはディスクに保存されていません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17199"/>
        <source>Autosave ON. Prefix: %1</source>
        <translation>オートセーブ ON プレフィクス: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17203"/>
        <source>Autosave OFF</source>
        <translation>オートセーブ OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17491"/>
        <source>xlimit = (%3,%4) ylimit = (%1,%2) zlimit = (%5,%6)</source>
        <translation>xlimit = (%3,%4) ylimit = (%1,%2) zlimit = (%5,%6)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18536"/>
        <source>&lt;b&gt;Event&lt;/b&gt; hide or show the corresponding slider</source>
        <translation>&lt;b&gt;イベント&lt;/b&gt; 対応するスライダーの表示/非表示</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18537"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action on slider release</source>
        <translation>&lt;b&gt;アクション&lt;/b&gt; スライダーリリースに実行されるアクション</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18538"/>
        <source>&lt;b&gt;Command&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt;)</source>
        <translation>&lt;b&gt;コマンド&lt;/b&gt; 「アクション」毎に異なる（&apos;{}&apos; は &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt; に置換）</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19294"/>
        <source>Serial Command: ASCII serial command or binary a2b_uu(serial command)</source>
        <translation>シリアルコマンド : ASCII かバイナリ a2b_uu のシリアルコマンド</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19297"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) writes values to the registers in slaves specified by the given ids</source>
        <translation>Modbus コマンド: write([スレーブID, レジスタ, 値],..,[スレーブID, レジスタ, 値]) ID を与えて指定のスレーブ内レジスタに値を書き込む</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19298"/>
        <source>DTA Command: Insert Data address : value, ex. 4701:1000 and sv is 100. always multiply with 10 if value Unit: 0.1 / ex. 4719:0 stops heating</source>
        <translation>DTA Command: Insert Data address : value, ex. 4701:1000 and sv is 100. always multiply with 10 if value Unit: 0.1 / ex. 4719:0 stops heating</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18542"/>
        <source>&lt;b&gt;Offset&lt;/b&gt; added as offset to the slider value</source>
        <translation>&lt;b&gt;オフセット&lt;/b&gt; スライダー値にオフセット値として加算される</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18543"/>
        <source>&lt;b&gt;Factor&lt;/b&gt; multiplicator of the slider value</source>
        <translation>&lt;b&gt;ファクター&lt;/b&gt; スライダー値に乗算される</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19301"/>
        <source>Event custom buttons</source>
        <translation>イベントカスタムボタン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19265"/>
        <source>Event configuration saved</source>
        <translation>イベントの構成を保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19268"/>
        <source>Found empty event type box</source>
        <translation>イベントタイプのボックスが空です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19288"/>
        <source>&lt;b&gt;Button Label&lt;/b&gt; Enter \n to create labels with multiple lines.</source>
        <translation>&lt;b&gt;ラベル&lt;/b&gt; 改行コード（\n）で複数行に渡るボタンラベルを作成。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19289"/>
        <source>&lt;b&gt;Event Description&lt;/b&gt; Description of the Event to be recorded.</source>
        <translation>&lt;b&gt;記述&lt;/b&gt; イベント内容の記述として記録される。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19290"/>
        <source>&lt;b&gt;Event type&lt;/b&gt; Type of event to be recorded.</source>
        <translation>&lt;b&gt;タイプ&lt;/b&gt; イベントタイプとして記録される。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19291"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-10) to be recorded</source>
        <translation>&lt;b&gt;値&lt;/b&gt; イベントの値（1-10）として記録される</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19292"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action at the time of the event</source>
        <translation>&lt;b&gt;アクション&lt;/b&gt; そのイベントで実行されるアクション</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19293"/>
        <source>&lt;b&gt;Documentation&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by the event value):</source>
        <translation>&lt;b&gt;関連文書&lt;/b&gt; 「アクション」毎に異なる（&apos;{}&apos;はイベントの値に置き換え）:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19295"/>
        <source>Call Program: A program/script path (absolute or relative)</source>
        <translation>プログラム呼び出し : プログラム/スクリプトへのパス（絶対/相対パス）</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19296"/>
        <source>Multiple Event: Adds events of other button numbers separated by a comma: 1,2,3, etc.</source>
        <translation>複合イベント : カンマ区切りのボタン番号（1,2,3,...等）で複数のイベントを追加。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19299"/>
        <source>&lt;b&gt;Button Visibility&lt;/b&gt; Hides/shows individual button</source>
        <translation>&lt;b&gt;可視性&lt;/b&gt; 個々のボタンの表示/非表示</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19300"/>
        <source>&lt;b&gt;Keyboard Shorcut: &lt;/b&gt; [b] Hides/shows Extra Button Rows</source>
        <translation>&lt;b&gt;キーボードショートカット: &lt;/b&gt; [b] エクストラボタン列の表示/非表示</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19516"/>
        <source>Phases changed to %1 default: %2</source>
        <translation>フェーズは %1 に変更されました、デフォルト: %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19651"/>
        <source>Background profile not found</source>
        <translation>背景用プロファイルが見つかりません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19656"/>
        <source>Background does not match number of labels</source>
        <translation>背景のラベルの数が合いません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19974"/>
        <source>Playback Aid set ON at %1 secs</source>
        <translation>プレーバック AIDを %1 秒でONにセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19982"/>
        <source>No profile background found</source>
        <translation>プロファイルの背景が見つかりません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20076"/>
        <source>Reading background profile...</source>
        <translation>背景用プロファイルを読み込んでいます...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23346"/>
        <source>Not enough time points for an ET curviness of %1. Set curviness to %2</source>
        <translation>ET のカーブ精度 %1 では時間ポイントが足りないので、カーブ精度を %2 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23556"/>
        <source>Designer Config</source>
        <translation>デザイナー構成</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23353"/>
        <source>Not enough time points for an BT curviness of %1. Set curviness to %2</source>
        <translation>BT のカーブ精度 %1 では時間ポイントが足りないので、カーブ精度を %2 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23548"/>
        <source>CHARGE</source>
        <translation>CHARGE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23549"/>
        <source>DRY END</source>
        <translation>DRY END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23550"/>
        <source>FC START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23551"/>
        <source>FC END</source>
        <translation>FC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23552"/>
        <source>SC START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23553"/>
        <source>SC END</source>
        <translation>SC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23554"/>
        <source>DROP</source>
        <translation>DROP</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23368"/>
        <source>Incorrect time format. Please recheck %1 time</source>
        <translation>時間書式が不正です。 %1 時間を再確認してください</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23555"/>
        <source>Times need to be in ascending order. Please recheck %1 time</source>
        <translation>時間は昇順である必要があります。 %1 時間を再確認してください</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23520"/>
        <source>Designer has been reset</source>
        <translation>デザイナーはリセットされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23873"/>
        <source>These serial settings are used for all Modbus communication.</source>
        <translation type="obsolete">これらのシリアル設定は全ての Modbus 通信に使われます。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23874"/>
        <source>The MODBUS device corresponds to input channels 1 and 2.</source>
        <translation type="obsolete">MODBUS デバイスは入力チャンネル 1 と 2 に対応します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23875"/>
        <source>The +MODBUS_34 extra device adds input channels 3 and 4.</source>
        <translation type="obsolete">+MODBUS_34 エクストラデバイスは入力チャンネル 3 と 4 を追加します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23876"/>
        <source>Inputs with slave id set to 0 are turned off.</source>
        <translation type="obsolete">スレーブID に 0 を入力すると OFF になります。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23877"/>
        <source>Modbus function 3 &apos;read holding register&apos; is the standard.</source>
        <translation type="obsolete">Modbus ファンクション3 「保持レジスタ読み込み」は標準です。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23878"/>
        <source>Modbus function 4 triggers the use of &apos;read input register&apos;.</source>
        <translation type="obsolete">Modbus ファンクション4 は「入力レジスター読み込み」の使用の引き金となります。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23879"/>
        <source>Input registers (fct 4) usually are from the range 30000-39999.</source>
        <translation type="obsolete">入力レジスタ（fct 4）は通常 30000-39999 の範囲内です。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23880"/>
        <source>Most devices hold data in 2 byte integer registers.</source>
        <translation type="obsolete">殆どのデバイスは2バイトの整数レジスタにデータを保持します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19226"/>
        <source>A temperature of 145.2C is often send as 1452.</source>
        <translation type="obsolete">温度 145.2℃は、多くの場合 1452 として送信されます。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23882"/>
        <source>In that case you have to use the symbolic assignment &apos;x/10&apos;.</source>
        <translation type="obsolete">この場合、あなたは記号的な割り当て &apos;x/10&apos; を使わなければなりません。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23883"/>
        <source>Few devices hold data as 4 byte floats in two registers.</source>
        <translation type="obsolete">少数のデバイスでは二つのレジスタに 4 バイト float 型データーを保持します。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23952"/>
        <source>Tick the Float flag in this case.</source>
        <translation>この場合は「float型」フラグをチェックします。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24396"/>
        <source>Serial Port Settings: %1, %2, %3, %4, %5, %6</source>
        <translation>シリアルポート設定: %1, %2, %3, %4, %5, %6</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21105"/>
        <source>Port scan on this platform not yet supported</source>
        <translation type="obsolete">ポートスキャンはこのプラットホームではサポートされていません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25038"/>
        <source>External program</source>
        <translation>外部プログラム</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25245"/>
        <source>Device not set</source>
        <translation>デバイスがセットされていません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25302"/>
        <source>PID to control ET set to %1 %2 ; PID to read BT set to %3 %4</source>
        <translation>PID to control ET set to %1 %2 ; PID to read BT set to %3 %4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25526"/>
        <source>Device set to %1. Now, check Serial Port settings</source>
        <translation>デバイスを %1 にセットし、シリアルポートの設定を確認</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25621"/>
        <source>Device set to %1. Now, chose serial port</source>
        <translation>デバイスを %1 にセットし、シリアルポートを選択しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25378"/>
        <source>Device set to CENTER 305, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation>デバイスをCENTER 305にセットし（CENTER306と同等）、シリアルポートを選択しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25432"/>
        <source>Device set to %1, which is equivalent to CENTER 309. Now, chose serial port</source>
        <translation>デバイスを %1 にセットし（CENTER309と同等）、シリアルポートを選択しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25459"/>
        <source>Device set to %1, which is equivalent to CENTER 303. Now, chose serial port</source>
        <translation>デバイスを %1 にセットし（CENTER303と同等）、シリアルポートを選択しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25441"/>
        <source>Device set to %1, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation>デバイスを %1 にセットし（CENTER306と同等）、シリアルポートを選択しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25468"/>
        <source>Device set to %1, which is equivalent to Omega HH506RA. Now, chose serial port</source>
        <translation>デバイスを %1 にセットし（Omega HH506RAと同等）、シリアルポートを選択しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25569"/>
        <source>Device set to %1, which is equivalent to Omega HH806AU. Now, chose serial port</source>
        <translation>デバイスを %1 にセットし（Omega HH806AUと同等）、シリアルポートを選択しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25603"/>
        <source>Device set to %1</source>
        <translation>デバイスを %1 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25499"/>
        <source>Device set to %1%2</source>
        <translation>デバイスを %1%2 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25560"/>
        <source>Device set to %1, which is equivalent to CENTER 302. Now, chose serial port</source>
        <translation>デバイスを %1 にセットし（CENTER302と同等）、シリアルポートを選択しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26276"/>
        <source>Color of %1 set to %2</source>
        <translation>カラー %1 を %2 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26781"/>
        <source>Save Wheel graph</source>
        <translation>セーブホイールグラフ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26785"/>
        <source>Wheel Graph saved</source>
        <translation>ホイールグラフは保存されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27043"/>
        <source>Load Alarms</source>
        <translation>アラームをロード</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27081"/>
        <source>Save Alarms</source>
        <translation>アラームをセーブ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27113"/>
        <source>&lt;b&gt;Status:&lt;/b&gt; activate or deactive alarm</source>
        <translation>&lt;b&gt;状態:&lt;/b&gt; アラームの有効/無効</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27114"/>
        <source>&lt;b&gt;If Alarm:&lt;/b&gt; alarm triggered only if the alarm with the given number was triggered before. Use 0 for no guard.</source>
        <translation>&lt;b&gt;ＩＦアラーム:&lt;/b&gt; アラームは「ＩＦアラーム」に指定された回数を超えている場合のみ起動。0 は無条件。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27116"/>
        <source>&lt;b&gt;From:&lt;/b&gt; alarm only triggered after the given event</source>
        <translation>&lt;b&gt;From:&lt;/b&gt; アラームは「From」に指定されたイベント後のみ起動</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27117"/>
        <source>&lt;b&gt;Time:&lt;/b&gt; if not 00:00, alarm is triggered mm:ss after the event &apos;From&apos; happend</source>
        <translation>&lt;b&gt;時間:&lt;/b&gt; 00:00 以外の場合、イベント「From」後で指定時間（分:秒[mm:ss]）が過ぎるとアラーム起動</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27118"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; the temperature source that is observed</source>
        <translation>&lt;b&gt;ソース:&lt;/b&gt; 温度監視の情報源</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27119"/>
        <source>&lt;b&gt;Condition:&lt;/b&gt; alarm is triggered if source rises above or below the specified temperature</source>
        <translation>&lt;b&gt;条件:&lt;/b&gt; 「ソース」の閾値「温度」対するアラームの起動条件（上回るか/下回るか）</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27120"/>
        <source>&lt;b&gt;Temp:&lt;/b&gt; the speficied temperature limit</source>
        <translation>&lt;b&gt;温度:&lt;/b&gt; 温度監視の閾値を指定</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27121"/>
        <source>&lt;b&gt;Action:&lt;/b&gt; if all conditions are fulfilled the alarm triggeres the corresponding action</source>
        <translation>&lt;b&gt;アクション:&lt;/b&gt; 全ての条件が満たされアラームが起動した時に実行されるアクション</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23510"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button (if 0 the COOL event is triggered ) or the new value of the slider</source>
        <translation type="obsolete">&lt;b&gt;記述:&lt;/b&gt; 「ポップアップ」では表示テキスト、「プログラム呼び出し」ではプログラムの名前、「イベントボタン」ではボタン番号（0 の場合は COOL イベントが起動）、各「スライダー」ではスライダーの新しい値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27123"/>
        <source>&lt;b&gt;NOTE:&lt;/b&gt; each alarm is only triggered once</source>
        <translation>&lt;b&gt;注意:&lt;/b&gt; 各アラームは一度だけ起動します</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30102"/>
        <source>OFF</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30095"/>
        <source>CONTINUOUS CONTROL</source>
        <translation>CONTINUOUS CONTROL</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30108"/>
        <source>ON</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30107"/>
        <source>STANDBY MODE</source>
        <translation>STANDBY MODE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28144"/>
        <source>The rampsoak-mode tells how to start and end the ramp/soak</source>
        <translation>rampsoak-モードは、ramp/soak を開始および終了する方法について説明します</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28145"/>
        <source>Your rampsoak mode in this pid is:</source>
        <translation>あなたの rampsoak モードのPIDは:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28146"/>
        <source>Mode = %1</source>
        <translation>モード = %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28148"/>
        <source>Start to run from PV value: %1</source>
        <translation>実行開始の PV値: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28149"/>
        <source>End output status at the end of ramp/soak: %1</source>
        <translation>ステータス出力を終了、終了時の ramp/soak: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28150"/>
        <source>Output status while ramp/soak operation set to OFF: %1</source>
        <translation>ramp/soak 操作が OFF の間の出力ステータス: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28151"/>
        <source>
Repeat Operation at the end: %1</source>
        <translation>
繰り返し操作を終わります: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28153"/>
        <source>Recomended Mode = 0</source>
        <translation>推奨モード = 0</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28154"/>
        <source>If you need to change it, change it now and come back later</source>
        <translation>If you need to change it, change it now and come back later</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28155"/>
        <source>Use the Parameter Loader Software by Fuji if you need to

</source>
        <translation>富士電機のパラメーターローダーソフトウェアを使用する場合

</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28156"/>
        <source>Continue?</source>
        <translation>続けますか?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28157"/>
        <source>RampSoak Mode</source>
        <translation>RampSoak モード</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29512"/>
        <source>Current sv = %1. Change now to sv = %2?</source>
        <translation>現在は sv = %1 です。 sv = %2 に変更しますか?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29574"/>
        <source>Change svN</source>
        <translation>svN を変更</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29573"/>
        <source>Current pid = %1. Change now to pid =%2?</source>
        <translation>現在は pid = %1 ですが、pid = %2 に変更しますか?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30328"/>
        <source>Ramp Soak start-end mode</source>
        <translation>Ramp Soak start-end モード</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30234"/>
        <source>Pattern changed to %1</source>
        <translation>パターンを %1 に変更</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30236"/>
        <source>Pattern did not changed</source>
        <translation>パターンは変更されませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30239"/>
        <source>Ramp/Soak was found ON! Turn it off before changing the pattern</source>
        <translation>Ramp/Soak を ON で見つけました！パターンを変更する前にオフにします</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30241"/>
        <source>Ramp/Soak was found in Hold! Turn it off before changing the pattern</source>
        <translation>Ramp/Soak を Hold で見つけました！パターンを変更する前にオフにします</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30582"/>
        <source>Activate PID front buttons</source>
        <translation>PIDフロントボタンをアクティブにします</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30582"/>
        <source>Remember SV memory has a finite
life of ~10,000 writes.

Proceed?</source>
        <translation>SVメモリには10,000回程度の書き換え限界寿命
があることを覚えておいてください。

続行しますか?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30684"/>
        <source>RS ON</source>
        <translation>RS ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30686"/>
        <source>RS OFF</source>
        <translation>RS OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30688"/>
        <source>RS on HOLD</source>
        <translation>RS は HOLD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30709"/>
        <source>PXG sv#%1 set to %2</source>
        <translation>PXG sv#%1 を %2 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30730"/>
        <source>PXR sv set to %1</source>
        <translation>PXR sv を %1 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30765"/>
        <source>SV%1 changed from %2 to %3)</source>
        <translation>SV%1 は %2 から %3 に変更されました)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30773"/>
        <source>Unable to set sv%1</source>
        <translation>sv%1 をセットすることができません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30784"/>
        <source>SV changed from %1 to %2</source>
        <translation>SVは %1 から %2 に変更されました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30792"/>
        <source>Unable to set sv</source>
        <translation>sv をセットすることができません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30794"/>
        <source>Unable to set new sv</source>
        <translation>新しい sv をセットすることができません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1581"/>
        <source>Alarm %1 triggered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9266"/>
        <source>&lt;b&gt;[f]&lt;/b&gt; = Full Screen Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14061"/>
        <source>Save Graph as PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22227"/>
        <source>Phidget Temperature Sensor 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22235"/>
        <source>Phidget Temperature Sensor 4-input not attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22323"/>
        <source>Phidget Bridge 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22331"/>
        <source>Phidget Bridge 4-input not attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25551"/>
        <source>Device set to %1. Now, chose Modbus serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27115"/>
        <source>&lt;b&gt;But Not:&lt;/b&gt; alarm triggered only if the alarm with the given number was not triggered before. Use 0 for no guard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3772"/>
        <source>[TP] recorded at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4919"/>
        <source>Importing a profile in to Designer will decimate all data except the main [points].
Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27122"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button or the new value of the slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29111"/>
        <source>Load PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29195"/>
        <source>Save PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13531"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13531"/>
        <source>A tight sampling interval might lead to instability on some machines. We suggest a minimum of 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13514"/>
        <source>Oversampling is only active with a sampling interval equal or larger than 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14323"/>
        <source>current background ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14324"/>
        <source>current background BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9257"/>
        <source>&lt;b&gt;[d]&lt;/b&gt; = Toggle xy scale (T/Delta)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22415"/>
        <source>Phidget 1018 IO attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22423"/>
        <source>Phidget 1018 IO not attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31275"/>
        <source>Load Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31294"/>
        <source>Save Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31500"/>
        <source>PID turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31513"/>
        <source>PID turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9265"/>
        <source>&lt;b&gt;[q,w,e,r + &lt;i&gt;nn&lt;/i&gt;]&lt;/b&gt; = Quick Custom Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14307"/>
        <source>Return the minimum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14308"/>
        <source>Return the maximum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23940"/>
        <source>The MODBUS device corresponds to input channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23941"/>
        <source>1 and 2.. The MODBUS_34 extra device adds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23942"/>
        <source>input channels 3 and 4. Inputs with slave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23943"/>
        <source>id set to 0 are turned off. Modbus function 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23944"/>
        <source>&apos;read holding register&apos; is the standard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23945"/>
        <source>Modbus function 4 triggers the use of &apos;read </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23946"/>
        <source>input register&apos;.Input registers (fct 4) usually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23947"/>
        <source> are from 30000-39999.Most devices hold data in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23948"/>
        <source>2 byte integer registers. A temperature of 145.2C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23949"/>
        <source>is often sent as 1452. In that case you have to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23950"/>
        <source>use the symbolic assignment &apos;x/10&apos;. Few devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23951"/>
        <source>hold data as 4 byte floats in two registers.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Radio Button</name>
    <message>
        <location filename="artisanlib/main.py" line="24547"/>
        <source>Meter</source>
        <translation>メーター</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24548"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23500"/>
        <source>Arduino TC4</source>
        <translation type="obsolete">Arduino TC4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24550"/>
        <source>Program</source>
        <translation>プログラム</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24549"/>
        <source>TC4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Annotation</name>
    <message>
        <location filename="artisanlib/main.py" line="700"/>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="701"/>
        <source>Heater</source>
        <translation>ヒーター</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="702"/>
        <source>Damper</source>
        <translation>ダンパー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="703"/>
        <source>Fan</source>
        <translation>ファン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3031"/>
        <source>START 00:00</source>
        <translation type="obsolete">START 00:00</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3803"/>
        <source>DE %1</source>
        <translation>DE %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3859"/>
        <source>FCs %1</source>
        <translation>FCs %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3914"/>
        <source>FCe %1</source>
        <translation>FCe %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3967"/>
        <source>SCs %1</source>
        <translation>SCs %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4025"/>
        <source>SCe %1</source>
        <translation>SCe %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3369"/>
        <source>END %1</source>
        <translation type="obsolete">END %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4164"/>
        <source>CE %1</source>
        <translation>CE %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3724"/>
        <source>CHARGE 00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4085"/>
        <source>DROP %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3764"/>
        <source>TP %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Title</name>
    <message>
        <location filename="artisanlib/main.py" line="10484"/>
        <source>Roaster Scope</source>
        <translation>焙煎記録</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="artisanlib/main.py" line="19977"/>
        <source>Playback Aid set OFF</source>
        <translation>プレーバック AID を OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28486"/>
        <source>Ready</source>
        <translation>準備完了</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29285"/>
        <source>Decimal position successfully set to 1</source>
        <translation>小数部の桁を1 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29288"/>
        <source>Problem setting decimal position</source>
        <translation>小数部の桁の設定に問題があります</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29339"/>
        <source>Problem setting thermocouple type</source>
        <translation>熱電対タイプの設定に問題があります</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30318"/>
        <source>setting autotune...</source>
        <translation>autotune を設定...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30345"/>
        <source>Autotune successfully turned OFF</source>
        <translation>Autotune は OFF に切り替わりました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30348"/>
        <source>Autotune successfully turned ON</source>
        <translation>Autotune は ON に切り替わりました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30246"/>
        <source>wait...</source>
        <translation>wait...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27989"/>
        <source>PID OFF</source>
        <translation>PID OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27992"/>
        <source>PID ON</source>
        <translation>PID ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28016"/>
        <source>SV successfully set to %1</source>
        <translation>SV を %1 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28027"/>
        <source>Empty SV box</source>
        <translation>SV ボックスが空です</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28036"/>
        <source>Unable to read SV</source>
        <translation>sv を読むことができません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30141"/>
        <source>Ramp/Soak operation cancelled</source>
        <translation>Ramp/Soak 操作はキャンセルされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30144"/>
        <source>No RX data</source>
        <translation>RX データーがありません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30182"/>
        <source>RS ON</source>
        <translation>RS ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30156"/>
        <source>Need to change pattern mode...</source>
        <translation>パターンモードの変更が必要...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30165"/>
        <source>Pattern has been changed. Wait 5 secs.</source>
        <translation>パターンは変更されました。 5秒待ってください。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30168"/>
        <source>Pattern could not be changed</source>
        <translation>パターンは変更できませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30199"/>
        <source>RampSoak could not be changed</source>
        <translation>RampSoak を変更できませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30202"/>
        <source>RS OFF</source>
        <translation>RS OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30212"/>
        <source>RS successfully turned OFF</source>
        <translation>RS は OFF に切り替わりました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28249"/>
        <source>setONOFFrampsoak(): Ramp Soak could not be set OFF</source>
        <translation>setONOFFrampsoak(): Ramp Soak を OFF に出来ませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28275"/>
        <source>getsegment(): problem reading ramp</source>
        <translation>getsegment(): ramp の読み込みに問題があります</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28288"/>
        <source>getsegment(): problem reading soak</source>
        <translation>getsegment(): soak の読み込みに問題があります</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28301"/>
        <source>getallsegments(): problem reading R/S </source>
        <translation>getallsegment(): R/S の読み込みに問題があります </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30314"/>
        <source>Finished reading Ramp/Soak val.</source>
        <translation>Ramp/Soak の値読み込みは終了しました。</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28344"/>
        <source>Finished reading pid values</source>
        <translation type="unfinished">pid の値読み込みは終了しました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22912"/>
        <source>%1 successfully send to pid </source>
        <translation type="obsolete">%1 を pid に送信 </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28396"/>
        <source>setpid(): There was a problem setting %1</source>
        <translation>setpid(): 設定に問題があります %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28470"/>
        <source>Ramp/Soak successfully written</source>
        <translation>Ramp/Soak は正しく書き込まれました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29248"/>
        <source>Time Units successfully set to MM:SS</source>
        <translation>時間単位を「分:秒 (MM:SS)」にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29251"/>
        <source>Problem setting time units</source>
        <translation>時間単位の設定に問題があります</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29336"/>
        <source>Thermocouple type successfully set</source>
        <translation>熱電対タイプをセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29529"/>
        <source>SV%1 set to %2</source>
        <translation>SV%1 を %2 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29533"/>
        <source>Problem setting SV</source>
        <translation>SV の設定に問題があります</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29535"/>
        <source>Cancelled svN change</source>
        <translation>svN の変更はキャンセルされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29553"/>
        <source>PID already using sv%1</source>
        <translation>PID は sv%1 をすでに使用</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29556"/>
        <source>setNsv(): bad response</source>
        <translation>setNsv(): レスポンスが悪い</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23830"/>
        <source>pid%1 changed to %2</source>
        <translation type="obsolete">pid%1 を %2 に変更</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29593"/>
        <source>setNpid(): bad confirmation</source>
        <translation>setNpid(): bad confirmation</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29597"/>
        <source>Cancelled pid change</source>
        <translation>pid の変更はキャンセルされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29615"/>
        <source>PID was already using pid %1</source>
        <translation>PID は pid %1 をすでに使用</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29618"/>
        <source>setNpid(): Unable to set pid %1 </source>
        <translation>setNpid(): pid %1 をセットできません </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29695"/>
        <source>SV%1 successfully set to %2</source>
        <translation>SV%1 を %2 にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29703"/>
        <source>setsv(): Unable to set SV</source>
        <translation>setsv(): SV をセットできません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29821"/>
        <source>pid #%1 successfully set to (%2,%3,%4)</source>
        <translation>pid #%1 を (%2,%3,%4) にセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29829"/>
        <source>pid command failed. Bad data at pid%1 (8,8,8): (%2,%3,%4) </source>
        <translation>pid コマンドが失敗。 不正データー pid%1 (8,8,8): (%2,%3,%4) </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29907"/>
        <source>getallpid(): Unable to read pid values</source>
        <translation>getallpid(): pid の値が読み込めません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29935"/>
        <source>PID is using pid = %1</source>
        <translation>PID は pid = %1 を使用</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29938"/>
        <source>getallpid(): Unable to read current sv</source>
        <translation>getallpid(): 現在の sv から読み込めません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30002"/>
        <source>PID is using SV = %1</source>
        <translation>PID は SV = %1 を使用</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30215"/>
        <source>Ramp Soak could not be set OFF</source>
        <translation>Ramp Soak を OFF にできませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30256"/>
        <source>PID set to OFF</source>
        <translation>PID を OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30259"/>
        <source>PID set to ON</source>
        <translation>PID を ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30262"/>
        <source>Unable</source>
        <translation>Unable</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30266"/>
        <source>No data received</source>
        <translation>データーが受信されませんでした</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30306"/>
        <source>Reading Ramp/Soak %1 ...</source>
        <translation>Ramp/Soak %1 を読み込んでいます...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30311"/>
        <source>problem reading Ramp/Soak</source>
        <translation>Ramp/Soak の読み込みに問題があります</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30327"/>
        <source>Current pid = %1. Proceed with autotune command?</source>
        <translation>現在の pid = %1です。 autotune コマンドを続行しますか?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30331"/>
        <source>Autotune cancelled</source>
        <translation>Autotune はキャンセルされました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30350"/>
        <source>UNABLE to set Autotune</source>
        <translation>Autotune をセットすることができません</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>Ramp (MM:SS)</source>
        <translation>Ramp (分:秒)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>Soak (MM:SS)</source>
        <translation>Soak (分:秒)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30422"/>
        <source>Ramp/Soak successfully writen</source>
        <translation>Ramp/Soak は正しく書き込まれました</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31376"/>
        <source>Work in Progress</source>
        <translation>処理中</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29839"/>
        <source>sending commands for p%1 i%2 d%3</source>
        <translation>送信コマンド p%1 i%2 d%3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28387"/>
        <source>%1 successfully sent to pid </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29590"/>
        <source>pid changed to %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tab</name>
    <message>
        <location filename="artisanlib/main.py" line="15114"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15117"/>
        <source>Plotter</source>
        <translation>プロッター</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15120"/>
        <source>Math</source>
        <translation>計算</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15123"/>
        <source>UI</source>
        <translation>UI</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31393"/>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16395"/>
        <source>Notes</source>
        <translation>ノート</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19947"/>
        <source>Events</source>
        <translation>イベント</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19950"/>
        <source>Data</source>
        <translation>データー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19944"/>
        <source>Config</source>
        <translation>構成</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18512"/>
        <source>Buttons</source>
        <translation>ボタン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18515"/>
        <source>Sliders</source>
        <translation>スライダー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18521"/>
        <source>Palettes</source>
        <translation>パレット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18527"/>
        <source>Style</source>
        <translation>スタイル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24886"/>
        <source>ET/BT</source>
        <translation>ET/BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29102"/>
        <source>Extra</source>
        <translation>エクストラ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24243"/>
        <source>Modbus</source>
        <translation>Modbus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24246"/>
        <source>Scale</source>
        <translation>はかり</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24889"/>
        <source>Extra Devices</source>
        <translation>エクストラデバイス</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24892"/>
        <source>Symb ET/BT</source>
        <translation>ET/BT シンボル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26093"/>
        <source>Graph</source>
        <translation>グラフ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26096"/>
        <source>LCDs</source>
        <translation>LCDs</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29090"/>
        <source>RS</source>
        <translation>RS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29093"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31263"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29099"/>
        <source>Set RS</source>
        <translation>RSをセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24249"/>
        <source>Color</source>
        <translation type="unfinished">カラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18518"/>
        <source>Quantifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31267"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24895"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Table</name>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>Abs Time</source>
        <translation type="obsolete">絶対時間</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>Rel Time</source>
        <translation type="obsolete">相対時間</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>DeltaET (d/m)</source>
        <translation type="obsolete">DeltaET (d/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>DeltaBT (d/m)</source>
        <translation type="obsolete">DeltaBT (d/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18713"/>
        <source>%1 START</source>
        <translation type="obsolete">%1 START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18717"/>
        <source>%1 DRY END</source>
        <translation type="obsolete">%1 DRY END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18722"/>
        <source>%1 FC START</source>
        <translation type="obsolete">%1 FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18726"/>
        <source>%1 FC END</source>
        <translation type="obsolete">%1 FC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18730"/>
        <source>%1 SC START</source>
        <translation type="obsolete">%1 SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18734"/>
        <source>%1 SC END</source>
        <translation type="obsolete">%1 SC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18738"/>
        <source>%1 END</source>
        <translation type="obsolete">%1 END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13234"/>
        <source>%1 COOL</source>
        <translation type="obsolete">%1 COOL</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18743"/>
        <source>%1 EVENT #%2 %3%4</source>
        <translation type="obsolete">%1 EVENT #%2 %3%4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Time</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Description</source>
        <translation>記述</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20093"/>
        <source>Type</source>
        <translation>タイプ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20093"/>
        <source>Value</source>
        <translation>値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Label</source>
        <translation>ラベル</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Action</source>
        <translation>アクション</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Documentation</source>
        <translation>関連文章</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Visibility</source>
        <translation>可視性</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Color</source>
        <translation>カラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Text Color</source>
        <translation>テキストカラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Device</source>
        <translation>デバイス</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Comm Port</source>
        <translation>ポート</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Baud Rate</source>
        <translation>通信速度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Byte Size</source>
        <translation>データ長</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Parity</source>
        <translation>パリティビット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Stopbits</source>
        <translation>ストップビット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Timeout</source>
        <translation>タイムアウト</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Color 1</source>
        <translation>カラー 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Color 2</source>
        <translation>カラー 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Label 1</source>
        <translation>ラベル 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Label 2</source>
        <translation>ラベル 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>y1(x)</source>
        <translation>y1(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>y2(x)</source>
        <translation>y2(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>LCD 1</source>
        <translation>LCD 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>LCD 2</source>
        <translation>LCD 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Curve 1</source>
        <translation>カーブ 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Curve 2</source>
        <translation>カーブ 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Parent</source>
        <translation>ペアレント</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Width</source>
        <translation>幅</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Opaqueness</source>
        <translation>不透明度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Delete Wheel</source>
        <translation>ホイールを削除</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Edit Labels</source>
        <translation>ラベル編集</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Update Labels</source>
        <translation>ラベル更新</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Properties</source>
        <translation>プロパティ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Radius</source>
        <translation>半径範囲</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Starting angle</source>
        <translation>開始角度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Projection</source>
        <translation>プロジェクション</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Text Size</source>
        <translation>テキストサイズ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Color Pattern</source>
        <translation>カラーパターン</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Status</source>
        <translation>状態</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>If Alarm</source>
        <translation>ＩＦアラーム</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>From</source>
        <translation>From</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Source</source>
        <translation>ソース</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Condition</source>
        <translation>条件</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Temp</source>
        <translation>温度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>Ramp HH:MM</source>
        <translation>Ramp 時:分</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>Soak HH:MM</source>
        <translation>Soak 時:分</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20175"/>
        <source>DRY END</source>
        <translation type="unfinished">DRY END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20178"/>
        <source>FC START</source>
        <translation type="unfinished">FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20181"/>
        <source>FC END</source>
        <translation type="unfinished">FC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20184"/>
        <source>SC START</source>
        <translation type="unfinished">SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20187"/>
        <source>SC END</source>
        <translation type="unfinished">SC END</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20190"/>
        <source>DROP</source>
        <translation type="unfinished">DROP</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20193"/>
        <source>COOL</source>
        <translation type="unfinished">COOL</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13822"/>
        <source>START</source>
        <translation type="obsolete">START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16549"/>
        <source>EVENT #%2 %3%4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20172"/>
        <source>CHARGE</source>
        <translation type="unfinished">CHARGE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>But Not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>DeltaET</source>
        <translation type="unfinished">DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>DeltaBT</source>
        <translation type="unfinished">DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20197"/>
        <source>EVENT #%1 %2%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Beep</source>
        <translation type="unfinished">ビープ音</translation>
    </message>
</context>
<context>
    <name>Textbox</name>
    <message>
        <location filename="artisanlib/main.py" line="437"/>
        <source>Acidity</source>
        <translation>Acidity</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="423"/>
        <source>Aftertaste</source>
        <translation>Aftertaste</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="395"/>
        <source>Clean Cup</source>
        <translation>Clean Cup</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="358"/>
        <source>Head</source>
        <translation>Head</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="435"/>
        <source>Fragrance</source>
        <translation>Fragrance</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="427"/>
        <source>Sweetness</source>
        <translation>Sweetness</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="419"/>
        <source>Aroma</source>
        <translation>Aroma</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="442"/>
        <source>Balance</source>
        <translation>Balance</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="436"/>
        <source>Body</source>
        <translation>Body</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="368"/>
        <source>Sour</source>
        <translation>Sour</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="413"/>
        <source>Flavor</source>
        <translation>Flavor</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="370"/>
        <source>Critical
Stimulus</source>
        <translation>Critical
Stimulus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="372"/>
        <source>Bitter</source>
        <translation>Bitter</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="373"/>
        <source>Astringency</source>
        <translation>Astringency</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="374"/>
        <source>Solubles
Concentration</source>
        <translation>Solubles
Concentration</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="421"/>
        <source>Mouthfeel</source>
        <translation>Mouthfeel</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="376"/>
        <source>Other</source>
        <translation>Other</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="377"/>
        <source>Aromatic
Complexity</source>
        <translation>Aromatic
Complexity</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="378"/>
        <source>Roast
Color</source>
        <translation>Roast
Colo</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="379"/>
        <source>Aromatic
Pungency</source>
        <translation>Aromatic
Pungency</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="380"/>
        <source>Sweet</source>
        <translation>Sweet</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="382"/>
        <source>pH</source>
        <translation>pH</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="385"/>
        <source>Fragance</source>
        <translation>Fragance</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="392"/>
        <source>Dry Fragrance</source>
        <translation>Dry Fragrance</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="393"/>
        <source>Uniformity</source>
        <translation>Uniformity</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="394"/>
        <source>Complexity</source>
        <translation>Complexity</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="430"/>
        <source>Finish</source>
        <translation>Finish</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="400"/>
        <source>Brightness</source>
        <translation>Brightness</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="401"/>
        <source>Wet Aroma</source>
        <translation>Wet Aroma</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="405"/>
        <source>Taste</source>
        <translation>Taste</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="406"/>
        <source>Nose</source>
        <translation>Nose</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="411"/>
        <source>Fragrance-Aroma</source>
        <translation>Fragrance-Aroma</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="422"/>
        <source>Flavour</source>
        <translation>Flavour</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="432"/>
        <source>Roast Color</source>
        <translation>Roast Color</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="433"/>
        <source>Crema Texture</source>
        <translation>Crema Texture</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="434"/>
        <source>Crema Volume</source>
        <translation>Crema Volume</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="438"/>
        <source>Bitterness</source>
        <translation>Bitterness</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="439"/>
        <source>Defects</source>
        <translation>Defects</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="440"/>
        <source>Aroma Intensity</source>
        <translation>Aroma Intensity</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="441"/>
        <source>Aroma Persistence</source>
        <translation>Aroma Persistence</translation>
    </message>
</context>
<context>
    <name>Tooltip</name>
    <message>
        <location filename="artisanlib/main.py" line="3527"/>
        <source>Stop monitoring</source>
        <translation>モニタリングを停止</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7374"/>
        <source>Start monitoring</source>
        <translation>モニタリングを開始</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3632"/>
        <source>Stop recording</source>
        <translation>記録を停止</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7387"/>
        <source>Start recording</source>
        <translation>記録を開始</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7402"/>
        <source>Marks the begining of First Crack (FCs)</source>
        <translation>ファーストクラックの始まりをマーク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7409"/>
        <source>Marks the end of First Crack (FCs)</source>
        <translation>ファーストクラックの終わりをマーク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7416"/>
        <source>Marks the begining of Second Crack (SCs)</source>
        <translation>セカンドクラックの始まりをマーク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7423"/>
        <source>Marks the end of Second Crack (SCe)</source>
        <translation>セカンドクラックの終わりをマーク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7436"/>
        <source>Reset</source>
        <translation>リセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7444"/>
        <source>Marks the begining of the roast (beans in)</source>
        <translation>焙煎の開始（生豆投入）をマーク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7452"/>
        <source>Marks the end of the roast (drop beans)</source>
        <translation>焙煎の終了（焙煎豆排出）をマーク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7467"/>
        <source>Marks an Event</source>
        <translation>イベントをマーク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7476"/>
        <source>Increases the current SV value by 5</source>
        <translation>現在の SV 値を 5 増加</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7484"/>
        <source>Increases the current SV value by 10</source>
        <translation>現在の SV 値を 10 増加</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7492"/>
        <source>Increases the current SV value by 20</source>
        <translation>現在の SV 値を 20 増加</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7500"/>
        <source>Decreases the current SV value by 20</source>
        <translation>現在の SV 値を 20 減少</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7508"/>
        <source>Decreases the current SV value by 10</source>
        <translation>現在の SV 値を 10 減少</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7516"/>
        <source>Decreases the current SV value by 5</source>
        <translation>現在の SV 値を 5 減少</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7530"/>
        <source>Turns ON/OFF the HUD</source>
        <translation>HUN の ON/OFF を切り替え</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7539"/>
        <source>Marks the end of the Drying phase (DRYEND)</source>
        <translation>乾燥フェーズの終了 (DRYEND) をマーク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7547"/>
        <source>Marks the end of the Cooling phase (COOLEND)</source>
        <translation>冷却フェーズの終了 (COOLEND) をマーク</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7597"/>
        <source>Timer</source>
        <translation>タイマー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7598"/>
        <source>ET Temperature</source>
        <translation>ET 温度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7599"/>
        <source>BT Temperature</source>
        <translation>BT 温度</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7600"/>
        <source>ET/time (degrees/min)</source>
        <translation>ET/時間 (度/分)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7601"/>
        <source>BT/time (degrees/min)</source>
        <translation>BT/時間 (度/分)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7602"/>
        <source>Value of SV in PID</source>
        <translation>PID の SV 値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7603"/>
        <source>PID power %</source>
        <translation>PID パワー %</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7687"/>
        <source>Number of events found</source>
        <translation>イベントの数</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7698"/>
        <source>Type of event</source>
        <translation>イベントのタイプ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7705"/>
        <source>Value of event</source>
        <translation>イベントの値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7719"/>
        <source>Updates the event</source>
        <translation>イベントを更新</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14392"/>
        <source>&lt;b&gt;Label&lt;/b&gt;= </source>
        <translation>&lt;b&gt;ラベル &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14393"/>
        <source>&lt;b&gt;Description &lt;/b&gt;= </source>
        <translation>&lt;b&gt;記述 &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14395"/>
        <source>&lt;b&gt;Type &lt;/b&gt;= </source>
        <translation>&lt;b&gt;タイプ &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14396"/>
        <source>&lt;b&gt;Value &lt;/b&gt;= </source>
        <translation>&lt;b&gt;値 &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14397"/>
        <source>&lt;b&gt;Documentation &lt;/b&gt;= </source>
        <translation>&lt;b&gt;関連文書 &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14398"/>
        <source>&lt;b&gt;Button# &lt;/b&gt;= </source>
        <translation>&lt;b&gt;ボタン# &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26358"/>
        <source>Save image using current graph size to a png format</source>
        <translation>現在のグラフサイズを使い、png 形式で画像を保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14954"/>
        <source>linear: linear interpolation
cubic: 3rd order spline interpolation
nearest: y value of the nearest point</source>
        <translation>linear: リニア補間法
cubic: 三次精度スプライン補間法
nearest: 最近傍点の y 値</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17065"/>
        <source>ON/OFF logs serial communication</source>
        <translation>シリアル通信ログの ON/OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17155"/>
        <source>Automatic generated name = This text + date + time</source>
        <translation>自動生成される名前 = このテキスト + 日付 + 時間</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17158"/>
        <source>ON/OFF of automatic saving when pressing keyboard letter [a]</source>
        <translation>キーボードの文字 [a] を押した時の、オートセーブの ON/OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17169"/>
        <source>Sets the directory to store batch profiles when using the letter [a]</source>
        <translation>キーボードの文字 [a] を押した時にバッチプロファイルを保存するためのディレクトリをセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17771"/>
        <source>Allows to enter a description of the last event</source>
        <translation>最終イベントの「記述」の入力を許可</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17977"/>
        <source>Add new extra Event button</source>
        <translation>新しいエクストライベントボタンを追加</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17982"/>
        <source>Delete the last extra Event button</source>
        <translation>最後のエクストライベントボタンを削除</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26854"/>
        <source>Show help</source>
        <translation>ヘルプを表示</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18019"/>
        <source>Backup all palettes to a text file</source>
        <translation>全てのパターンをテキストファイルにバックアップ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14629"/>
        <source>Restore all palettes from a text</source>
        <translation type="obsolete">全てのパターンをテキストファイルからリストア</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18317"/>
        <source>Action Type</source>
        <translation>アクションタイプ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18322"/>
        <source>Action String</source>
        <translation>アクション文字列</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26311"/>
        <source>Aspect Ratio</source>
        <translation>縦横比</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24982"/>
        <source>Example: 100 + 2*x</source>
        <translation>例: 100 + 2*x</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24983"/>
        <source>Example: 100 + x</source>
        <translation>例: 100 + x</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26300"/>
        <source>Erases wheel parent hierarchy</source>
        <translation>ホイールのペアレントされた階層構造を消去</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26304"/>
        <source>Sets graph hierarchy child-&gt;parent instead of parent-&gt;child</source>
        <translation>グラフの階層構造 子→親 の代わりに 親→子 をセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26318"/>
        <source>Increase size of text in all the graph</source>
        <translation>全てのグラフのテキストサイズを拡大</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26321"/>
        <source>Decrease size of text in all the graph</source>
        <translation>全てのグラフのテキストサイズを縮小</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26325"/>
        <source>Decorative edge beween wheels</source>
        <translation>装飾的なホイール間のエッジ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26331"/>
        <source>Line thickness</source>
        <translation>ラインの太さ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26336"/>
        <source>Line color</source>
        <translation>ラインカラー</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26340"/>
        <source>Apply color pattern to whole graph</source>
        <translation>グラフ全体にカラーパターンを適用</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26346"/>
        <source>Add new wheel</source>
        <translation>新しいホイールを追加</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26349"/>
        <source>Rotate graph 1 degree counter clockwise</source>
        <translation>反時計回りに 1度グラフを回転</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26352"/>
        <source>Rotate graph 1 degree clockwise</source>
        <translation>時計回りに 1度グラフを回転</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26356"/>
        <source>Save graph to a text file.wg</source>
        <translation>テキストファイル .wg にグラフを保存</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26361"/>
        <source>Sets Wheel graph to view mode</source>
        <translation>ホイールグラフを表示モードにセット</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26364"/>
        <source>open graph file.wg</source>
        <translation>グラフファイル .wg を開く</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26367"/>
        <source>Close wheel graph editor</source>
        <translation>ホイールグラフエディタを閉じる</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18020"/>
        <source>Restore all palettes from a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26859"/>
        <source>Clear alarms table</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
