<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="ar" sourcelanguage="">
<context>
    <name>About</name>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>About</source>
        <translation>نبذة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11059"/>
        <source>Version:</source>
        <translation type="obsolete">نسخة:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>Core developers:</source>
        <translation>المطورون الأساسيون:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>Contributors:</source>
        <translation>المشاركون:</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="artisanlib/main.py" line="7372"/>
        <source>ON</source>
        <translation>شغّل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7385"/>
        <source>START</source>
        <translation>إبدأ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3526"/>
        <source>OFF</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7398"/>
        <source>FC
START</source>
        <translation>يدء الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7405"/>
        <source>FC
END</source>
        <translation>نهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7412"/>
        <source>SC
START</source>
        <translation>بدء الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7419"/>
        <source>SC
END</source>
        <translation>نهاية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7427"/>
        <source>RESET</source>
        <translation>إعادة تعيين</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7440"/>
        <source>CHARGE</source>
        <translation>تحميل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7448"/>
        <source>DROP</source>
        <translation>إخراج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7456"/>
        <source>Control</source>
        <translation>تحكم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7463"/>
        <source>EVENT</source>
        <translation>حدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7471"/>
        <source>SV +5</source>
        <translation>اس في + 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7479"/>
        <source>SV +10</source>
        <translation>اس في + 10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7487"/>
        <source>SV +20</source>
        <translation>اس في + 20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7495"/>
        <source>SV -20</source>
        <translation>اس في - 20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7503"/>
        <source>SV -10</source>
        <translation>اس في - 10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7511"/>
        <source>SV -5</source>
        <translation>اس في - 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7519"/>
        <source>HUD</source>
        <translation>نظام هد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7534"/>
        <source>DRY
END</source>
        <translation>إنتهاء التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7543"/>
        <source>COOL
END</source>
        <translation>إنتهاء التبريد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26608"/>
        <source>Update</source>
        <translation>تحديث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14566"/>
        <source>PID Help</source>
        <translation type="obsolete">مساعدة بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31249"/>
        <source>OK</source>
        <translation>موافق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28813"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14895"/>
        <source>Color</source>
        <translation>ألوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14898"/>
        <source>Plot</source>
        <translation>رسم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25931"/>
        <source>Background</source>
        <translation>خلفية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14904"/>
        <source>Virtual Device</source>
        <translation>جهاز إفتراضي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19555"/>
        <source>Save Image</source>
        <translation>حفظ الصورة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27124"/>
        <source>Help</source>
        <translation>مساعدة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14963"/>
        <source>Info</source>
        <translation>معلومات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31101"/>
        <source>Set</source>
        <translation>ضبط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25886"/>
        <source>Defaults</source>
        <translation>الإفتراضات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15869"/>
        <source>Order</source>
        <translation>ترتيب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26830"/>
        <source>Add</source>
        <translation>إضافة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26839"/>
        <source>Delete</source>
        <translation>إزالة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16129"/>
        <source>in</source>
        <translation>داخل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16136"/>
        <source>out</source>
        <translation>خارج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17013"/>
        <source>Search</source>
        <translation>بحث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17166"/>
        <source>Path</source>
        <translation>مسار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18001"/>
        <source>Transfer To</source>
        <translation>حول إلى</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18003"/>
        <source>Restore From</source>
        <translation>إعادة من</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31229"/>
        <source>Save</source>
        <translation>حفظ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31226"/>
        <source>Load</source>
        <translation>تحميل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19552"/>
        <source>Del</source>
        <translation>مسح</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19784"/>
        <source>Align</source>
        <translation>موازاة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19834"/>
        <source>Up</source>
        <translation>فوق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19836"/>
        <source>Down</source>
        <translation>تحت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19838"/>
        <source>Left</source>
        <translation>يسار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19840"/>
        <source>Right</source>
        <translation>يمين</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24656"/>
        <source>Reset</source>
        <translation>إعادة تعيين</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27501"/>
        <source>Close</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23270"/>
        <source>Create</source>
        <translation>إنشاء</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20700"/>
        <source>Scan for Ports</source>
        <translation type="obsolete">بحث منافذ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26610"/>
        <source>Select</source>
        <translation>إختيار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25775"/>
        <source>Grid</source>
        <translation>شبكة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25782"/>
        <source>Title</source>
        <translation>عنوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25789"/>
        <source>Y Label</source>
        <translation>عنوان ص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25796"/>
        <source>X Label</source>
        <translation>عنوان س</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25803"/>
        <source>Drying Phase</source>
        <translation>مرحلة التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25810"/>
        <source>Maillard Phase</source>
        <translation>مرحلة ميلارد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25817"/>
        <source>Development Phase</source>
        <translation>مرحلة الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25824"/>
        <source>Cooling Phase</source>
        <translation>مرحلة التبريد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25831"/>
        <source>ET</source>
        <translation>حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25838"/>
        <source>BT</source>
        <translation>حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25845"/>
        <source>DeltaET</source>
        <translation>فارق حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25852"/>
        <source>DeltaBT</source>
        <translation>فارق حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25859"/>
        <source>Markers</source>
        <translation>علامات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25866"/>
        <source>Text</source>
        <translation>نص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25873"/>
        <source>Watermarks</source>
        <translation>غلامات مائية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25880"/>
        <source>C Lines</source>
        <translation>خطوط سي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25889"/>
        <source>Grey</source>
        <translation>مظلل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25943"/>
        <source>LED</source>
        <translation>إضاءة خلفية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25975"/>
        <source>B/W</source>
        <translation>أبيض وأسود</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26299"/>
        <source>Reset Parents</source>
        <translation>إعادة تعيين الأصل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26303"/>
        <source>Reverse Hierarchy</source>
        <translation>عكس الترتيب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26317"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26320"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26335"/>
        <source>Line Color</source>
        <translation>لون الخط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26348"/>
        <source>&lt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26351"/>
        <source>&gt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26354"/>
        <source>Save File</source>
        <translation>حفظ الملف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26357"/>
        <source>Save Img</source>
        <translation>حفظ الصورة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26360"/>
        <source>View Mode</source>
        <translation>وضع العرض</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26363"/>
        <source>Open</source>
        <translation>إفتح</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26633"/>
        <source>Set Color</source>
        <translation>ظبط اللون</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26824"/>
        <source>All On</source>
        <translation>تشغيل الكل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26827"/>
        <source>All Off</source>
        <translation>إيقاف الكل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27496"/>
        <source>Read Ra/So values</source>
        <translation>قراءة قيمة آر أي و اس أو</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28550"/>
        <source>RampSoak ON</source>
        <translation>تشغيل التدرج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28552"/>
        <source>RampSoak OFF</source>
        <translation>إيقاف التدرج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28556"/>
        <source>PID OFF</source>
        <translation>تشغيل بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28558"/>
        <source>PID ON</source>
        <translation>إيقاف بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28673"/>
        <source>Write SV</source>
        <translation>كتابة أس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27512"/>
        <source>SV Buttons ON</source>
        <translation>تشغيل زر اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27514"/>
        <source>SV Buttons OFF</source>
        <translation>إيقاف زر اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27516"/>
        <source>Read SV</source>
        <translation>قراءة اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27533"/>
        <source>Set p</source>
        <translation>تحديد اس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27534"/>
        <source>Set i</source>
        <translation>تحديد آي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27535"/>
        <source>Set d</source>
        <translation>تحديد دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28809"/>
        <source>Autotune ON</source>
        <translation>تفعيل التوازن التلقائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28811"/>
        <source>Autotune OFF</source>
        <translation>إيقاف التوازن التلقائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27550"/>
        <source>Read PID Values</source>
        <translation>قراءة قيم بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31380"/>
        <source>Read</source>
        <translation>إقرأ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28940"/>
        <source>Set ET PID to 1 decimal point</source>
        <translation>تحديد خانة صفرية لل إي تي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28942"/>
        <source>Set BT PID to 1 decimal point</source>
        <translation>تحديد خانة صفرية لل بي تي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28546"/>
        <source>Read RS values</source>
        <translation>قراءة قيم الآر أس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28609"/>
        <source>Write SV1</source>
        <translation>كتابة أس قي 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28611"/>
        <source>Write SV2</source>
        <translation>كتابة أس في 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28613"/>
        <source>Write SV3</source>
        <translation>كتابة اس في 3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28615"/>
        <source>Write SV4</source>
        <translation>كتابة اس في 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28617"/>
        <source>Write SV5</source>
        <translation>كتابة اس في 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28619"/>
        <source>Write SV6</source>
        <translation>كتابة اس في 6</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28621"/>
        <source>Write SV7</source>
        <translation>كتابة اس في 7</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28677"/>
        <source>ON SV buttons</source>
        <translation>تشغيل أزرة اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28680"/>
        <source>OFF SV buttons</source>
        <translation>إيقاف أزرة اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28683"/>
        <source>Read SV (7-0)</source>
        <translation>قراءة اس في (0-7)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28791"/>
        <source>pid 1</source>
        <translation>بي آي دي 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28793"/>
        <source>pid 2</source>
        <translation>بي آي دي 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28795"/>
        <source>pid 3</source>
        <translation>بي آي دي 3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28797"/>
        <source>pid 4</source>
        <translation>بي آي دي 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28799"/>
        <source>pid 5</source>
        <translation>بي آي دي 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28801"/>
        <source>pid 6</source>
        <translation>بي آي دي 6</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28803"/>
        <source>pid 7</source>
        <translation>بي آي دي 7</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24951"/>
        <source>Read All</source>
        <translation type="obsolete">قراءة الكل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28944"/>
        <source>Set ET PID to MM:SS time units</source>
        <translation>تحديد البي آي دي لدقائق وثواني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31381"/>
        <source>Write</source>
        <translation>كتابة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26834"/>
        <source>Insert</source>
        <translation>إدخال</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26858"/>
        <source>Clear</source>
        <translation>إزالة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16150"/>
        <source>scan</source>
        <translation>بحث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28542"/>
        <source>Write All</source>
        <translation>كتابة الكل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28548"/>
        <source>Write RS values</source>
        <translation>كتابة قيم الآر أس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28685"/>
        <source>Write SV (7-0)</source>
        <translation>كتابة اس في (0-7)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28805"/>
        <source>Read PIDs</source>
        <translation>قراءة قيم بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28807"/>
        <source>Write PIDs</source>
        <translation>كتابة قيم بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18208"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31251"/>
        <source>On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31253"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckBox</name>
    <message>
        <location filename="artisanlib/main.py" line="16089"/>
        <source>Heavy FC</source>
        <translation>صدع اول قوي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16092"/>
        <source>Low FC</source>
        <translation>صدع أول خفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16095"/>
        <source>Light Cut</source>
        <translation>قطع فاتح</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16098"/>
        <source>Dark Cut</source>
        <translation>قطع غامق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16101"/>
        <source>Drops</source>
        <translation>قطرات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16104"/>
        <source>Oily</source>
        <translation>مدهن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16107"/>
        <source>Uneven</source>
        <translation>غير متساوي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16109"/>
        <source>Tipping</source>
        <translation>شقوق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16111"/>
        <source>Scorching</source>
        <translation>حروق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16113"/>
        <source>Divots</source>
        <translation>حفر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19772"/>
        <source>DeltaET</source>
        <translation>فارق حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19773"/>
        <source>DeltaBT</source>
        <translation>فارق حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14644"/>
        <source>Smooth Spikes</source>
        <translation>قفزات ناعمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14654"/>
        <source>Drop Spikes</source>
        <translation>إهمال القفزات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14659"/>
        <source>Limits</source>
        <translation>حدود</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14688"/>
        <source>Projection</source>
        <translation>إسقاط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19769"/>
        <source>Show</source>
        <translation>عرض</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15092"/>
        <source>Beep</source>
        <translation>نغمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15860"/>
        <source>Delete roast properties on RESET</source>
        <translation>إزالة خصائص الحماصة عند إعادة التعيين</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17064"/>
        <source>Serial Log ON/OFF</source>
        <translation>تشغيل/إيقاف التسجيل التسلسلي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17156"/>
        <source>Autosave [a]</source>
        <translation>حفظ تلقائي [أ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17290"/>
        <source>Lock Max</source>
        <translation>الحد الأعلى للقفل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17764"/>
        <source>Button</source>
        <translation>زر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17770"/>
        <source>Mini Editor</source>
        <translation>محرر صغير</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15733"/>
        <source>Automatic CHARGE/DROP</source>
        <translation type="obsolete">تحميل و إخراج تلقائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18251"/>
        <source>CHARGE</source>
        <translation>تحميل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23070"/>
        <source>DRY END</source>
        <translation>إنتهاء التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23072"/>
        <source>FC START</source>
        <translation>يدء الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23074"/>
        <source>FC END</source>
        <translation>نهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23076"/>
        <source>SC START</source>
        <translation>بدء الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23078"/>
        <source>SC END</source>
        <translation>نهاية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18305"/>
        <source>DROP</source>
        <translation>إخراج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18314"/>
        <source>COOL END</source>
        <translation>إنتهاء التبريد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19362"/>
        <source>Auto Adjusted</source>
        <translation>تعديل تلقائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19365"/>
        <source>Watermarks</source>
        <translation>غلامات مائية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19560"/>
        <source>Background</source>
        <translation>خلفية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19770"/>
        <source>Text</source>
        <translation>نص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19771"/>
        <source>Events</source>
        <translation>أحداث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19868"/>
        <source>Playback Aid</source>
        <translation>مساعدة الإعادة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20225"/>
        <source>Time</source>
        <translation>الوقت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20226"/>
        <source>Bar</source>
        <translation>شريط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20227"/>
        <source>d/m</source>
        <translation>درجة بالدقيقة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20228"/>
        <source>ETBTa</source>
        <translation>المساحة الحرارية بين المحيط والبن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20229"/>
        <source>Evaluation</source>
        <translation>تقييم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20230"/>
        <source>Characteristics</source>
        <translation>خصائص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24521"/>
        <source>ET</source>
        <translation>حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24524"/>
        <source>BT</source>
        <translation>حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26862"/>
        <source>Load alarms from profile</source>
        <translation>تحميل الإنذارات من المسار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17946"/>
        <source>Auto CHARGE</source>
        <translation>تحميل تلقائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17949"/>
        <source>Auto DROP</source>
        <translation>إخراج تلقائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17952"/>
        <source>Mark TP</source>
        <translation>علامة التحوّل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19367"/>
        <source>Phases LCDs</source>
        <translation>شاشة المراحل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19369"/>
        <source>Auto DRY</source>
        <translation>جفاف تلقائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19371"/>
        <source>Auto FCs</source>
        <translation>صدع اول تلقائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14765"/>
        <source>Decimal Places</source>
        <translation>خانات عشرية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24595"/>
        <source>Modbus Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14649"/>
        <source>Smooth2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31182"/>
        <source>Start PID on CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31237"/>
        <source>Load Ramp/Soak table from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24628"/>
        <source>Control Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24703"/>
        <source>Ratiometric</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComboBox</name>
    <message>
        <location filename="artisanlib/main.py" line="810"/>
        <source>Speed</source>
        <translation>السرعة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="811"/>
        <source>Power</source>
        <translation>الطاقة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="812"/>
        <source>Damper</source>
        <translation>الصمام</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="813"/>
        <source>Fan</source>
        <translation>مروحة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>linear</source>
        <translation>خطّي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14690"/>
        <source>newton</source>
        <translation>نيوتن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15688"/>
        <source>metrics</source>
        <translation>متري</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15690"/>
        <source>thermal</source>
        <translation>حراري</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>cubic</source>
        <translation>مكعب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>nearest</source>
        <translation>تقريب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17555"/>
        <source>g</source>
        <translation>غرام</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17556"/>
        <source>Kg</source>
        <translation>كيلوغرام</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16771"/>
        <source>ml</source>
        <translation>ميلليلتر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16004"/>
        <source>l</source>
        <translation>لتر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27196"/>
        <source>ET</source>
        <translation>حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27197"/>
        <source>BT</source>
        <translation>حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17268"/>
        <source>upper right</source>
        <translation>اليمين العلوي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17269"/>
        <source>upper left</source>
        <translation>اليسار العلوي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17270"/>
        <source>lower left</source>
        <translation>اليسار السفلي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17271"/>
        <source>lower right</source>
        <translation>اليمين السفلي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17272"/>
        <source>right</source>
        <translation>يمين</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17273"/>
        <source>center left</source>
        <translation>يسار الوسط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17274"/>
        <source>center right</source>
        <translation>يمين الوسط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17275"/>
        <source>lower center</source>
        <translation>أسفل الوسط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17276"/>
        <source>upper center</source>
        <translation>أعاى الوسط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17277"/>
        <source>center</source>
        <translation>الوسط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17295"/>
        <source>30 seconds</source>
        <translation>ثلاثين ثانية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17296"/>
        <source>1 minute</source>
        <translation>دقيقة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17297"/>
        <source>2 minute</source>
        <translation>دقيقتان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17298"/>
        <source>3 minute</source>
        <translation>ثلاث دقائق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17299"/>
        <source>4 minute</source>
        <translation>أربع دقائق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17300"/>
        <source>5 minute</source>
        <translation>خمس دقائق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17323"/>
        <source>solid</source>
        <translation>صلب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17324"/>
        <source>dashed</source>
        <translation>مخطط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17325"/>
        <source>dashed-dot</source>
        <translation>مخطط منقط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17326"/>
        <source>dotted</source>
        <translation>منقط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>None</source>
        <translation>لا شيئ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17535"/>
        <source>Event #0</source>
        <translation>حدث #0</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17537"/>
        <source>Event #%1</source>
        <translation>حدث #%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17557"/>
        <source>lb</source>
        <translation>رطل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17578"/>
        <source>liter</source>
        <translation>لتر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17579"/>
        <source>gallon</source>
        <translation>جالون</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17580"/>
        <source>quart</source>
        <translation>ربع جالون</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17581"/>
        <source>pint</source>
        <translation>نصف لتر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17582"/>
        <source>cup</source>
        <translation>كوب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17583"/>
        <source>cm^3</source>
        <translation>سنتيمتر مكعب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17776"/>
        <source>Type</source>
        <translation>نوع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17777"/>
        <source>Value</source>
        <translation>قيمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Serial Command</source>
        <translation>أمر تسلسلي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Modbus Command</source>
        <translation>أمر مودبس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>DTA Command</source>
        <translation>أمر دي تي أي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Call Program</source>
        <translation>إستدعاء برنامج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>OFF</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>ON</source>
        <translation>شغّل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Multiple Event</source>
        <translation>أحداث متعددة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27195"/>
        <source>DeltaBT</source>
        <translation>فارق حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27194"/>
        <source>DeltaET</source>
        <translation>فارق حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23252"/>
        <source>SV Commands</source>
        <translation>إوامر اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23252"/>
        <source>Ramp Commands</source>
        <translation>إوامر تدرّج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23912"/>
        <source>little-endian</source>
        <translation>إنديات صغير</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25896"/>
        <source>grey</source>
        <translation>رمادي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25897"/>
        <source>Dark Grey</source>
        <translation>رمادي عامق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25898"/>
        <source>Slate Grey</source>
        <translation>رمادي داكن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25899"/>
        <source>Light Gray</source>
        <translation>رمادي فاتح</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25900"/>
        <source>Black</source>
        <translation>أسود</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25901"/>
        <source>White</source>
        <translation>أبيض</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25902"/>
        <source>Transparent</source>
        <translation>شفاف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Flat</source>
        <translation>لا طعم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Perpendicular</source>
        <translation>عمدي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Radial</source>
        <translation>شعاعي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>START</source>
        <translation>إبدأ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>CHARGE</source>
        <translation>تحميل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>TP</source>
        <translation>نقطة تحول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>DRY END</source>
        <translation>إنتهاء التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>FC START</source>
        <translation>يدء الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>FC END</source>
        <translation>نهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>SC START</source>
        <translation>بدء الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>SC END</source>
        <translation>نهاية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>DROP</source>
        <translation>إخراج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>COOL</source>
        <translation>تبريد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27254"/>
        <source>below</source>
        <translation>أسفل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27254"/>
        <source>above</source>
        <translation>أعلى</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Pop Up</source>
        <translation>إبراز</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Event Button</source>
        <translation>زر الحدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Slider</source>
        <translation>مزلاج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14831"/>
        <source>classic</source>
        <translation>تقليدي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14831"/>
        <source>xkcd</source>
        <translation>اكس كي سي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Default</source>
        <translation>إفتراضي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Humor</source>
        <translation>فكاهي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Comic</source>
        <translation>رسوم متحركة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>DRY</source>
        <translation>جفاف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>FCs</source>
        <translation>بدء الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>FCe</source>
        <translation>نهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>SCs</source>
        <translation>بدء الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>SCe</source>
        <translation>نهاية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>COOL END</source>
        <translation>إنتهاء التجفيف</translation>
    </message>
</context>
<context>
    <name>Contextual Menu</name>
    <message>
        <location filename="artisanlib/main.py" line="5080"/>
        <source>Create</source>
        <translation>إنشاء</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5084"/>
        <source>Config...</source>
        <translation>ترتيب ...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5094"/>
        <source>Add point</source>
        <translation>إضافة نقطة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5098"/>
        <source>Remove point</source>
        <translation>إزالة نقطة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5104"/>
        <source>Reset Designer</source>
        <translation>إعادة المصمم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5108"/>
        <source>Exit Designer</source>
        <translation>خروج من المصمم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5685"/>
        <source>Add to Cupping Notes</source>
        <translation>إضافة إلى ملاحظات التذوق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5689"/>
        <source>Add to Roasting Notes</source>
        <translation>إضاقة إلى ملاحظات الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5693"/>
        <source>Cancel selection</source>
        <translation>إلغاء الإختيار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5697"/>
        <source>Edit Mode</source>
        <translation>نظام التحرير</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5701"/>
        <source>Exit</source>
        <translation>خروج</translation>
    </message>
</context>
<context>
    <name>Directory</name>
    <message>
        <location filename="artisanlib/main.py" line="878"/>
        <source>edit text</source>
        <translation type="obsolete">تحرير نص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14034"/>
        <source>profiles</source>
        <translation>ملامح</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14024"/>
        <source>other</source>
        <translation>أخرى</translation>
    </message>
</context>
<context>
    <name>Error Message</name>
    <message>
        <location filename="artisanlib/main.py" line="31311"/>
        <source>Exception:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22958"/>
        <source>Value Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26787"/>
        <source>IO Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12447"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16804"/>
        <source>Unable to move CHARGE to a value that does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20865"/>
        <source>Modbus Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23014"/>
        <source>Serial Exception:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21150"/>
        <source>F80h Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21160"/>
        <source>CRC16 data corruption ERROR. TX does not match RX. Check wiring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21163"/>
        <source>No RX data received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21291"/>
        <source>DTAcommand(): %1 bytes received but 15 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21564"/>
        <source>Unable to open serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21714"/>
        <source>HH806Wtemperature(): Unable to initiate device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21843"/>
        <source>HH506RAGetID: %1 bytes received but 5 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21868"/>
        <source>HH506RAtemperature(): Unable to get id from HH506RA device </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21888"/>
        <source>HH506RAtemperature(): %1 bytes received but 14 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21943"/>
        <source>CENTER302temperature(): %1 bytes received but 7 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22013"/>
        <source>CENTER303temperature(): %1 bytes received but 8 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22083"/>
        <source>CENTER306temperature(): %1 bytes received but 10 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22169"/>
        <source>CENTER309temperature(): %1 bytes received but 45 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22594"/>
        <source>Arduino could not set channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22605"/>
        <source>Arduino could not set temperature unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24398"/>
        <source>Serial Exception: invalid comm port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24403"/>
        <source>Serial Exception: timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30912"/>
        <source>Segment values could not be written into PID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30690"/>
        <source>RampSoak could not be changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30968"/>
        <source>pid.readoneword(): %1 RX bytes received (7 needed) for unit ID=%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15340"/>
        <source>Univariate: no profile data available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15512"/>
        <source>Polyfit: no profile data available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21614"/>
        <source>MS6514temperature(): %1 bytes received but 16 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21659"/>
        <source>HH806AUtemperature(): %1 bytes received but 16 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9247"/>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Flavor Scope Label</name>
    <message>
        <location filename="artisanlib/main.py" line="13236"/>
        <source>OK</source>
        <translation>موافق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13237"/>
        <source>Grassy</source>
        <translation>عشبي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13238"/>
        <source>Leathery</source>
        <translation>جلدي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13239"/>
        <source>Toasty</source>
        <translation>محموص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13240"/>
        <source>Bready</source>
        <translation>خبزي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13241"/>
        <source>Acidic</source>
        <translation>حامض</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13242"/>
        <source>Flat</source>
        <translation>لا طعم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13243"/>
        <source>Fracturing</source>
        <translation>تشقق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13244"/>
        <source>Sweet</source>
        <translation>حالي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13245"/>
        <source>Less Sweet</source>
        <translation>أقل حلاوة</translation>
    </message>
</context>
<context>
    <name>Form Caption</name>
    <message>
        <location filename="artisanlib/main.py" line="14586"/>
        <source>Extras</source>
        <translation>إضافات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15722"/>
        <source>Roast Properties</source>
        <translation>خصائص الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16964"/>
        <source>Artisan Platform</source>
        <translation>منصة آرتيزان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17004"/>
        <source>Settings Viewer</source>
        <translation>عارض الخصائص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17063"/>
        <source>Serial Log</source>
        <translation>سجل تسلسلي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17107"/>
        <source>Error Log</source>
        <translation>سجل الأخطاء</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17132"/>
        <source>Message History</source>
        <translation>تاريخ الرسائل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17153"/>
        <source>Keyboard Autosave [a]</source>
        <translation>حفظ تلقائي للمفاتيح [أ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17191"/>
        <source>AutoSave Path</source>
        <translation>مسار الحفظ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17214"/>
        <source>Axes</source>
        <translation>محاور</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17520"/>
        <source>Roast Calculator</source>
        <translation>حاسبة الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17760"/>
        <source>Events</source>
        <translation>أحداث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19310"/>
        <source>Roast Phases</source>
        <translation>مراحل الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19533"/>
        <source>Cup Profile</source>
        <translation>ملمح التذوق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19761"/>
        <source>Profile Background</source>
        <translation>خلفية الرسمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20222"/>
        <source>Statistics</source>
        <translation>إحصائيات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23064"/>
        <source>Designer Config</source>
        <translation>إعداد المصمم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23603"/>
        <source>Manual Temperature Logger</source>
        <translation>مسجل حرارة يدوي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23729"/>
        <source>Serial Ports Configuration</source>
        <translation>إعدادات منفذ تسلسلي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24511"/>
        <source>Device Assignment</source>
        <translation>تحديد الأجهزة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25762"/>
        <source>Colors</source>
        <translation>ألوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26290"/>
        <source>Wheel Graph Editor</source>
        <translation>محرر رسم العجلة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26819"/>
        <source>Alarms</source>
        <translation>إنذارات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27467"/>
        <source>Fuji PXR PID Control</source>
        <translation>تحكم فوجي بي اكس آر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28483"/>
        <source>Fuji PXG PID Control</source>
        <translation>تحكم فوجي بي اكس جي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31373"/>
        <source>Delta DTA PID Control</source>
        <translation>تحكم دلتا دي تي أي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31007"/>
        <source>Arduino Control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupBox</name>
    <message>
        <location filename="artisanlib/main.py" line="24533"/>
        <source>Curves</source>
        <translation>منحنيات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24542"/>
        <source>LCDs</source>
        <translation>شاشات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14799"/>
        <source>HUD</source>
        <translation>نظام الإستقراء هد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14813"/>
        <source>Input Filters</source>
        <translation>تصفية الإدخال</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15015"/>
        <source>Interpolate</source>
        <translation>استوفي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15021"/>
        <source>Univariate</source>
        <translation>وحيد المنغير</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15079"/>
        <source>Appearance</source>
        <translation>المظهر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15087"/>
        <source>Resolution</source>
        <translation>الدقة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15099"/>
        <source>Sound</source>
        <translation>الصوت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16336"/>
        <source>Times</source>
        <translation>الأوقات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17386"/>
        <source>Time Axis</source>
        <translation>محور الوقت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17388"/>
        <source>Temperature Axis</source>
        <translation>محور الحرارة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17390"/>
        <source>DeltaBT/DeltaET Axis</source>
        <translation>محور فارق حرارة المحيط/البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17392"/>
        <source>Legend Location</source>
        <translation>موقع الدلبل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17394"/>
        <source>Grid</source>
        <translation>شبكة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17633"/>
        <source>Rate of Change</source>
        <translation>معدّل التغيّر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17635"/>
        <source>Temperature Conversion</source>
        <translation>تحويل الحرارة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17637"/>
        <source>Weight Conversion</source>
        <translation>تحويل الوزن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17639"/>
        <source>Volume Conversion</source>
        <translation>تحويل الحجم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18244"/>
        <source>Event Types</source>
        <translation>نوع الحدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18351"/>
        <source>Default Buttons</source>
        <translation>الأزارير الإفتراضية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18394"/>
        <source>Management</source>
        <translation>إدارة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20339"/>
        <source>Evaluation</source>
        <translation>تقييم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20341"/>
        <source>Display</source>
        <translation>عرض</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23326"/>
        <source>Initial Settings</source>
        <translation>إعدادات إفتراضية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24113"/>
        <source>Input 1</source>
        <translation>مدخل 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24127"/>
        <source>Input 2</source>
        <translation>مدخل 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24140"/>
        <source>Input 3</source>
        <translation>مدخل 3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24153"/>
        <source>Input 4</source>
        <translation>مدخل 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24801"/>
        <source>PID</source>
        <translation>بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24816"/>
        <source>Arduino TC4</source>
        <translation>أردبنو تي سي 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24823"/>
        <source>External Program</source>
        <translation>برنامج خارجي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24829"/>
        <source>Symbolic Assignments</source>
        <translation>تحديدات رمزية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26057"/>
        <source>Timer LCD</source>
        <translation>شاشة الوقت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26060"/>
        <source>ET LCD</source>
        <translation>شاشة حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26063"/>
        <source>BT LCD</source>
        <translation>شاشة حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26066"/>
        <source>DeltaET LCD</source>
        <translation>شاشة فارق حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26069"/>
        <source>DeltaBT LCD</source>
        <translation>شاشة فارق حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26072"/>
        <source>Extra Devices / PID SV LCD</source>
        <translation>أجهزة إضافية/بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26376"/>
        <source>Label Properties</source>
        <translation>خصائص العنوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15048"/>
        <source>Polyfit</source>
        <translation>معادلة بوليفت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14858"/>
        <source>Look</source>
        <translation>المظهر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24698"/>
        <source>1048 Probe Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24777"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24784"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31010"/>
        <source>p-i-d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31173"/>
        <source>Set Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24749"/>
        <source>Phidget IO</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTML Report Template</name>
    <message>
        <location filename="artisanlib/main.py" line="12657"/>
        <source>Roasting Report</source>
        <translation>تقرير الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12682"/>
        <source>Date:</source>
        <translation>تاريخ:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12686"/>
        <source>Beans:</source>
        <translation>البن:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12690"/>
        <source>Size:</source>
        <translation>المقاس:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12694"/>
        <source>Weight:</source>
        <translation>الوزن:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12698"/>
        <source>Degree:</source>
        <translation>الدرجة:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12702"/>
        <source>Volume:</source>
        <translation>الحجك:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12706"/>
        <source>Density:</source>
        <translation>الكثافة:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12710"/>
        <source>Humidity:</source>
        <translation>الرطوبة:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12714"/>
        <source>Roaster:</source>
        <translation>الحمّاصة:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12718"/>
        <source>Operator:</source>
        <translation>المشغّل:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12722"/>
        <source>Cupping:</source>
        <translation>التذوّق:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12726"/>
        <source>Color:</source>
        <translation>اللون:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12734"/>
        <source>CHARGE:</source>
        <translation>تحميل:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12742"/>
        <source>DRY:</source>
        <translation>جفاف:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12746"/>
        <source>FCs:</source>
        <translation>بدء الصدع الأول:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12750"/>
        <source>FCe:</source>
        <translation>نهاية الصدع الأول:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12754"/>
        <source>SCs:</source>
        <translation>بدء الصدع الثاني:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12758"/>
        <source>SCe:</source>
        <translation>نهاية الصدع الثاني:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12762"/>
        <source>DROP:</source>
        <translation>إخراج:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12766"/>
        <source>COOL:</source>
        <translation>تبريد:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12770"/>
        <source>RoR:</source>
        <translation>معدّل إرتفاع:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12774"/>
        <source>ETBTa:</source>
        <translation>المساحة الحرارية بين المحيط والبن:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12786"/>
        <source>Drying:</source>
        <translation>التجفيف:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12790"/>
        <source>Maillard:</source>
        <translation>مرحلة ميلارد:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12794"/>
        <source>Development:</source>
        <translation>مرحلة الحمص:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12798"/>
        <source>Cooling:</source>
        <translation>التبريد:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12824"/>
        <source>Roasting Notes</source>
        <translation>ملاحظات الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12834"/>
        <source>Cupping Notes</source>
        <translation>ملاحظات التذوق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12738"/>
        <source>TP:</source>
        <translation>نقطة تحول:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12804"/>
        <source>Events</source>
        <translation type="unfinished">أحداث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12778"/>
        <source>CM:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12804"/>
        <source>Background:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="artisanlib/main.py" line="6036"/>
        <source>deg/min</source>
        <translation>درجة بالدقيقة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23238"/>
        <source>ET</source>
        <translation>حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23614"/>
        <source>BT</source>
        <translation>حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2912"/>
        <source>DeltaET</source>
        <translation>فارق حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2919"/>
        <source>DeltaBT</source>
        <translation>فارق حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16066"/>
        <source>at</source>
        <translation>عند</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23067"/>
        <source>CHARGE</source>
        <translation>تحميل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15758"/>
        <source>DRY END</source>
        <translation>إنتهاء التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15772"/>
        <source>FC START</source>
        <translation>يدء الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15787"/>
        <source>FC END</source>
        <translation>نهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15801"/>
        <source>SC START</source>
        <translation>بدء الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15815"/>
        <source>SC END</source>
        <translation>نهاية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23080"/>
        <source>DROP</source>
        <translation>إخراج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1302"/>
        <source>EVENT</source>
        <translation>حدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2709"/>
        <source>BackgroundET</source>
        <translation>خلفية حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2713"/>
        <source>BackgroundBT</source>
        <translation>خلفية حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2743"/>
        <source>BackgroundDeltaET</source>
        <translation>خلفية فارق حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2747"/>
        <source>BackgroundDeltaBT</source>
        <translation>خلفية فارق حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12910"/>
        <source>d/m</source>
        <translation>درجة / دقيقة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23095"/>
        <source>Time</source>
        <translation>الوقت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5519"/>
        <source>BT %1 d/m for %2</source>
        <translation>ةحرارة البن %1 درجة\دقيقة ل %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5535"/>
        <source>ET %1 d/m for %2</source>
        <translation>حرارة المحيط %1 درجة\دقيقة ل %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7624"/>
        <source>PID SV</source>
        <translation>بي آي دي أس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7628"/>
        <source>PID %</source>
        <translation>بي آي دي %</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7681"/>
        <source>Event #&lt;b&gt;0 &lt;/b&gt;</source>
        <translation>حدث # 0</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9279"/>
        <source>Event #&lt;b&gt;%1 &lt;/b&gt;</source>
        <translation>حدث # %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13801"/>
        <source>City</source>
        <translation>مدينة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13803"/>
        <source>City+</source>
        <translation>مدينة+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13805"/>
        <source>Full City</source>
        <translation>مدينة كامله</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13807"/>
        <source>Full City+</source>
        <translation>مدينة كامله +</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13809"/>
        <source>Light French</source>
        <translation>فرنسي خفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13811"/>
        <source>French</source>
        <translation>فرنسي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14098"/>
        <source>%1 to reach ET target %2</source>
        <translation>%1 لوصول هدف حرارة المحيط %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14109"/>
        <source> at %1</source>
        <translation>عند %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14111"/>
        <source>%1 to reach BT target %2</source>
        <translation>%1 لوصول هدف حرارة البن %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14121"/>
        <source>%1 after FCs</source>
        <translation>%1 بعد يداية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14128"/>
        <source>%1 after FCe</source>
        <translation>%1 بعد نهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14168"/>
        <source>ET - BT = %1</source>
        <translation>حرارة المحيط - حرارة البن = %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14219"/>
        <source>ET - BT = %1%2</source>
        <translation>حرارة المحيط- حرارة البن = %1%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14602"/>
        <source>ET Target 1</source>
        <translation>هدف حرارة المحيط 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14604"/>
        <source>BT Target 1</source>
        <translation>هدف حرارة البن 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14606"/>
        <source>ET Target 2</source>
        <translation>هدف حرارة المحيط 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14608"/>
        <source>BT Target 2</source>
        <translation>هدف حرارة البن 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31104"/>
        <source>Mode</source>
        <translation>حالة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14612"/>
        <source>ET p-i-d 1</source>
        <translation>حرارة المحيط بي آي دي 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14619"/>
        <source>Smooth Deltas</source>
        <translation>فروقات ناعمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14627"/>
        <source>Smooth Curves</source>
        <translation>منحنيات ناعمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31125"/>
        <source>min</source>
        <translation>أدنى</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31133"/>
        <source>max</source>
        <translation>أعلى</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14873"/>
        <source>Y(x)</source>
        <translation>ص(س)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15846"/>
        <source>COOL</source>
        <translation>تبريد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15889"/>
        <source>Title</source>
        <translation>عنوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15892"/>
        <source>Date</source>
        <translation>تاريخ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15898"/>
        <source>Beans</source>
        <translation>البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15908"/>
        <source>Weight</source>
        <translation>الوزن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15943"/>
        <source> in</source>
        <translation>داخل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15944"/>
        <source> out</source>
        <translation>خارج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15957"/>
        <source> %</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15942"/>
        <source>Volume</source>
        <translation>الحجم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15980"/>
        <source>Density</source>
        <translation>الكثافة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15995"/>
        <source>per</source>
        <translation>لكل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16015"/>
        <source>Bean Size</source>
        <translation>مقاس البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16021"/>
        <source>mm</source>
        <translation>مليمتر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16023"/>
        <source>Whole Color</source>
        <translation>لون البن المحموص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16029"/>
        <source>Ground Color</source>
        <translation>لون البن المطحون</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16040"/>
        <source>Storage Conditions</source>
        <translation>حالة التخزين</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16060"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16058"/>
        <source>Ambient Conditions</source>
        <translation>حال الطقس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16077"/>
        <source>Roaster</source>
        <translation>الحماصة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16079"/>
        <source>Operator</source>
        <translation>المشغل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16080"/>
        <source>Roasting Notes</source>
        <translation>ملاحظات الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16084"/>
        <source>Cupping Notes</source>
        <translation>ملاحظات التذوق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16161"/>
        <source>Ambient Source</source>
        <translation>مصدر الطقس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16761"/>
        <source>                 Density in: %1  g/l   =&gt;   Density out: %2 g/l</source>
        <translation>الكثافة عند الدخول: %1 غرام\لتر =&gt; الكثافة عند الخروج: %2 غرام\لتر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16775"/>
        <source>(%1 g/l)</source>
        <translation>(%1 غرام\لتر)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17114"/>
        <source>Number of errors found %1</source>
        <translation>عدد الأخطاء %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18126"/>
        <source>Max</source>
        <translation>حد أعلى</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18124"/>
        <source>Min</source>
        <translation>حد أدنى</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17258"/>
        <source>Rotation</source>
        <translation>تدوير</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17281"/>
        <source>Initial Max</source>
        <translation>الحد الأعلى الإبتدائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17313"/>
        <source>Step</source>
        <translation>خطوة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17321"/>
        <source>Style</source>
        <translation>أسلوب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17331"/>
        <source>Width</source>
        <translation>العرض</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19793"/>
        <source>Opaqueness</source>
        <translation>عدم الشفافية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17522"/>
        <source>Enter two times along profile</source>
        <translation>ادخل توقيتين على المسار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17525"/>
        <source>Start (00:00)</source>
        <translation>البداية 00:00</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17526"/>
        <source>End (00:00)</source>
        <translation>النهاية 00:00</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17545"/>
        <source>Fahrenheit</source>
        <translation>فهرنهايت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17546"/>
        <source>Celsius</source>
        <translation>سلسيوس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17673"/>
        <source>Time syntax error. Time not valid</source>
        <translation>صيغة الوقت خاطئة. الوقت غير صحيح</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17677"/>
        <source>Error: End time smaller than Start time</source>
        <translation>خطأ: الوقت ينتهي قبل البداية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17694"/>
        <source>Best approximation was made from %1 to %2</source>
        <translation>أفضل تقريب إستخدم %1 إلى %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17699"/>
        <source>No profile found</source>
        <translation>لم يوجد أي مسار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17774"/>
        <source>Bars</source>
        <translation>أشرطة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17846"/>
        <source>Color</source>
        <translation>ألوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23093"/>
        <source>Marker</source>
        <translation>علامات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17850"/>
        <source>Thickness</source>
        <translation>غلاظة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17852"/>
        <source>Opacity</source>
        <translation>عتمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17854"/>
        <source>Size</source>
        <translation>المقاس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17965"/>
        <source>Max buttons per row</source>
        <translation>الحد الأعلى للأزرار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17993"/>
        <source>Color Pattern</source>
        <translation>نمط اللون</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18005"/>
        <source>palette #</source>
        <translation>لوحة #</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18120"/>
        <source>Event</source>
        <translation>حدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18028"/>
        <source>Action</source>
        <translation>فعل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18030"/>
        <source>Command</source>
        <translation>أمر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18032"/>
        <source>Offset</source>
        <translation>عوض</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18034"/>
        <source>Factor</source>
        <translation>عامل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20271"/>
        <source>Drying</source>
        <translation>التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20272"/>
        <source>Maillard</source>
        <translation>مرحلة ميلارد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20273"/>
        <source>Development</source>
        <translation>مرحلة الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19534"/>
        <source>Default</source>
        <translation>الإفتراضات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19564"/>
        <source>Aspect Ratio</source>
        <translation>نسبة العرض للإرتفاع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19806"/>
        <source>ET Color</source>
        <translation>لون حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19813"/>
        <source>BT Color</source>
        <translation>لون حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19820"/>
        <source>DeltaET Color</source>
        <translation>لون فارق حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19827"/>
        <source>DeltaBT Color</source>
        <translation>لون فارق حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19872"/>
        <source>Text Warning</source>
        <translation>نص التحذير</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19873"/>
        <source>sec</source>
        <translation>ثانية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20274"/>
        <source>Cooling</source>
        <translation>التبريد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23237"/>
        <source>Curviness</source>
        <translation>إنحنائية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23250"/>
        <source>Events Playback</source>
        <translation>إعادة الحدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24011"/>
        <source>Comm Port</source>
        <translation>منفذ الإتصال</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24015"/>
        <source>Baud Rate</source>
        <translation>معدل الباود</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24021"/>
        <source>Byte Size</source>
        <translation>حجم البايت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24027"/>
        <source>Parity</source>
        <translation>تكافؤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24034"/>
        <source>Stopbits</source>
        <translation>بت التوقف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24040"/>
        <source>Timeout</source>
        <translation>وقت مستقطع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23764"/>
        <source>Settings for non-Modbus devices</source>
        <translation>إعدادات لغير أجهزة المودبس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23885"/>
        <source>Slave</source>
        <translation>خادم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23890"/>
        <source>Register</source>
        <translation>تسجيل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23895"/>
        <source>Float</source>
        <translation>الطفو</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23896"/>
        <source>Function</source>
        <translation>وظيفة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24001"/>
        <source>Device</source>
        <translation>الجهاز</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24575"/>
        <source>Control ET</source>
        <translation>تحكم حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24579"/>
        <source>Read BT</source>
        <translation>إقرأ حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24583"/>
        <source>Type</source>
        <translation>نوع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24584"/>
        <source>RS485 Unit ID</source>
        <translation>معرّف وحدة آرأس485</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24600"/>
        <source>ET Channel</source>
        <translation>قناة حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24603"/>
        <source>BT Channel</source>
        <translation>قناة حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24623"/>
        <source>AT Channel</source>
        <translation>قناة أي تي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24637"/>
        <source>ET Y(x)</source>
        <translation>حرارة المحيط ص(س)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24638"/>
        <source>BT Y(x)</source>
        <translation>حرارة البن ص(س)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26309"/>
        <source>Ratio</source>
        <translation>معدّل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26316"/>
        <source>Text</source>
        <translation>نص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26323"/>
        <source>Edge</source>
        <translation>حافّة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26329"/>
        <source>Line</source>
        <translation>خط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26338"/>
        <source>Color pattern</source>
        <translation>نمط اللون</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26618"/>
        <source> dg</source>
        <translation>درجة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26981"/>
        <source>Enter description</source>
        <translation>أدخل وصف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27472"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(1-4)</source>
        <translation>تدرّج و إشباع ساعة:دقيقة (1-4)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27477"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(5-8)</source>
        <translation>تدرّج و إشباع ساعة:دقيقة (5-8)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27479"/>
        <source>Ramp/Soak Pattern</source>
        <translation>نمط التدرج\الإشباع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>WARNING</source>
        <translation>تحذير</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>Writing eeprom memory</source>
        <translation>نكت لذاكرة ايبروم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>&lt;u&gt;Max life&lt;/u&gt; 10,000 writes</source>
        <translation>الحد النهائي 10,000 كتابة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>Infinite read life.</source>
        <translation>قراءة ملف لا نهائي.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>After &lt;u&gt;writing&lt;/u&gt; an adjustment,&lt;br&gt;never power down the pid&lt;br&gt;for the next 5 seconds &lt;br&gt;or the pid may never recover.</source>
        <translation>بعد كتابة تعديل لا تظفئ البي آي دي لخمس ثواني وإلا قد لا يعاود البي آي دي العمل.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>Read operations manual</source>
        <translation>إقرأ دليل المستخدم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29056"/>
        <source>ET Thermocouple type</source>
        <translation>نوع مستشعر حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29063"/>
        <source>BT Thermocouple type</source>
        <translation>نوع مستشعر حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28846"/>
        <source>NOTE: BT Thermocouple type is not stored in the Artisan settings</source>
        <translation>ملاحظة: نوعية مستشعر حرارة البن لا تخزّن في إعدادات أرتيزان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28949"/>
        <source>Artisan uses 1 decimal point</source>
        <translation>في برنامج أرتزان نستخدم خانه عشريّة واحدة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28491"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(1-7)</source>
        <translation>تدرّج و إشباع دقيقة:ثانية (1-4)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28497"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(8-16)</source>
        <translation>تدرّج و إشباع دقيقة:ثانية (5-8)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28544"/>
        <source>Pattern</source>
        <translation>نمط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28600"/>
        <source>SV (7-0)</source>
        <translation>اس في (0-7)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28722"/>
        <source>Write</source>
        <translation>كتابة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28704"/>
        <source>P</source>
        <translation>بي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28710"/>
        <source>I</source>
        <translation>آي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28716"/>
        <source>D</source>
        <translation>دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28950"/>
        <source>Artisan Fuji PXG uses MINUTES:SECONDS units in Ramp/Soaks</source>
        <translation>في برنامج أرتيزان فوجي بي اكس جي يستخدم وحدات دقائق:ثواني في تدرّج الصعود و الإشباع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31377"/>
        <source>SV</source>
        <translation>اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6041"/>
        <source>Curves</source>
        <translation>منحنيات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6045"/>
        <source>Delta Curves</source>
        <translation>منحنيات الفروق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>T</source>
        <translation>الوقت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>RoR</source>
        <translation>معدّل الصعود</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>ETBTa</source>
        <translation>المساحة الحرارية بين المحيط والبن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17695"/>
        <source>&lt;b&gt;%1&lt;/b&gt; deg/sec, &lt;b&gt;%2&lt;/b&gt; deg/min</source>
        <translation>%1 درجة\ثانية , %2 درجة\دقيقة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14984"/>
        <source>Start</source>
        <translation>بداية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14985"/>
        <source>End</source>
        <translation>نهاية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14816"/>
        <source>Path Effects</source>
        <translation>مؤثّرات المسار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14836"/>
        <source>Font</source>
        <translation>الخط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8436"/>
        <source>TP</source>
        <translation>نقطة تحول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8464"/>
        <source>DRY</source>
        <translation>جفاف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8498"/>
        <source>FCs</source>
        <translation>بدء الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10219"/>
        <source>Charge the beans</source>
        <translation>تحميل البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10205"/>
        <source>Start recording</source>
        <translation type="unfinished">يدء التسجيل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17162"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31037"/>
        <source>Source</source>
        <translation type="unfinished">مصدر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4626"/>
        <source>CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14635"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24673"/>
        <source>1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24677"/>
        <source>2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24681"/>
        <source>3:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24685"/>
        <source>4:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31053"/>
        <source>Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31099"/>
        <source>Lookahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31106"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31107"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31108"/>
        <source>Background</source>
        <translation type="unfinished">خلفية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31113"/>
        <source>SV Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31116"/>
        <source>SV Slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18128"/>
        <source>Coarse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23928"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23933"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14598"/>
        <source>HUD Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24735"/>
        <source>Raw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24736"/>
        <source>Data rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24737"/>
        <source>Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24756"/>
        <source>ServerId:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24758"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="const/UIconst.py" line="35"/>
        <source>Services</source>
        <translation>خدمات</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="36"/>
        <source>Hide %1</source>
        <translation>إخقاء %1</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="37"/>
        <source>Hide Others</source>
        <translation>إخقاء آخر</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="38"/>
        <source>Show All</source>
        <translation>إظهار الكل</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="39"/>
        <source>Preferences...</source>
        <translation>تفضيلات ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="61"/>
        <source>Quit %1</source>
        <translation>توقف %1</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="141"/>
        <source>About %1</source>
        <translation>نيذة %1</translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="artisanlib/main.py" line="17813"/>
        <source>Circle</source>
        <translation>دائرة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17814"/>
        <source>Square</source>
        <translation>مربع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17815"/>
        <source>Pentagon</source>
        <translation>مخمس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17816"/>
        <source>Diamond</source>
        <translation>معين الزوايا</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17817"/>
        <source>Star</source>
        <translation>نجمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17818"/>
        <source>Hexagon 1</source>
        <translation>مسدس 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17819"/>
        <source>Hexagon 2</source>
        <translation>مسدس 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17820"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17821"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17822"/>
        <source>None</source>
        <translation>لا شيئ</translation>
    </message>
</context>
<context>
    <name>Menu</name>
    <message>
        <location filename="artisanlib/main.py" line="6923"/>
        <source>CSV...</source>
        <translation>ملف سي اس في ...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6927"/>
        <source>JSON...</source>
        <translation>ملف جي سن ...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6931"/>
        <source>RoastLogger...</source>
        <translation>روست لوقر ...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6896"/>
        <source>HH506RA...</source>
        <translation>جهاز اتش اتش 506 ار اي ...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6900"/>
        <source>K202...</source>
        <translation>كي 202 ...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6904"/>
        <source>K204...</source>
        <translation>كي 204 ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="44"/>
        <source>File</source>
        <translation>ملف</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="47"/>
        <source>New</source>
        <translation>جديد</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="48"/>
        <source>Open...</source>
        <translation>إفتح ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="49"/>
        <source>Open Recent</source>
        <translation>إفتح مؤخرا</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="50"/>
        <source>Import</source>
        <translation>إستيراد</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="51"/>
        <source>Save</source>
        <translation>حفظ</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="52"/>
        <source>Save As...</source>
        <translation>حفظ بإسم ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="53"/>
        <source>Export</source>
        <translation>تصدير</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="54"/>
        <source>Save Graph</source>
        <translation>إحفظ الرسم</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="55"/>
        <source>Full Size...</source>
        <translation>حجم كامل ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="56"/>
        <source>Roasting Report</source>
        <translation>تقرير الحمص</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="57"/>
        <source>Print...</source>
        <translation>إطبع ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="64"/>
        <source>Edit</source>
        <translation>تحرير</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="67"/>
        <source>Cut</source>
        <translation>قص</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="68"/>
        <source>Copy</source>
        <translation>نسخ</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="69"/>
        <source>Paste</source>
        <translation>لزق</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="72"/>
        <source>Roast</source>
        <translation>حمص</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="75"/>
        <source>Properties...</source>
        <translation>خصائص ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="76"/>
        <source>Background...</source>
        <translation>خلفية ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="77"/>
        <source>Cup Profile...</source>
        <translation>تقييم التذوق...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="78"/>
        <source>Temperature</source>
        <translation>الحرارة</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="79"/>
        <source>Convert to Fahrenheit</source>
        <translation>تحويل لفهرنهايت</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="80"/>
        <source>Convert to Celsius</source>
        <translation>تحويل لسلسيوس</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="81"/>
        <source>Fahrenheit Mode</source>
        <translation>نظام الفهرنهايت</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="82"/>
        <source>Celsius Mode</source>
        <translation>نظام السلسيوس</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="86"/>
        <source>Config</source>
        <translation>إعداد</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="89"/>
        <source>Device...</source>
        <translation>الجهاز ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="90"/>
        <source>Serial Port...</source>
        <translation>منفذ تسلسلي ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="91"/>
        <source>Sampling Interval...</source>
        <translation>فترة العينات ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="93"/>
        <source>Colors...</source>
        <translation>ألوان ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="94"/>
        <source>Phases...</source>
        <translation>مراحل ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="95"/>
        <source>Events...</source>
        <translation>أحداث ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="96"/>
        <source>Statistics...</source>
        <translation>إحصائيات ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="97"/>
        <source>Axes...</source>
        <translation>محاور ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="98"/>
        <source>Autosave...</source>
        <translation>حفظ تلقائي ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="99"/>
        <source>Alarms...</source>
        <translation>إنذارات ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="100"/>
        <source>Language</source>
        <translation>اللغة</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="124"/>
        <source>Tools</source>
        <translation>أدوات</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="127"/>
        <source>Designer</source>
        <translation>مصمم</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="128"/>
        <source>Calculator</source>
        <translation>حاسبة</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="129"/>
        <source>Wheel Graph</source>
        <translation>رسم العجلة</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="130"/>
        <source>Extras...</source>
        <translation>إضافي ...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="134"/>
        <source>Help</source>
        <translation>مساعدة</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="143"/>
        <source>Documentation</source>
        <translation>توثيق</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="145"/>
        <source>Keyboard Shortcuts</source>
        <translation>إختصارات المفاتيح</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="146"/>
        <source>Errors</source>
        <translation>أخطاء</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="147"/>
        <source>Messages</source>
        <translation>رسائل</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="148"/>
        <source>Serial</source>
        <translation>متسلسل</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="152"/>
        <source>Settings</source>
        <translation>إعدادات</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="153"/>
        <source>Platform</source>
        <translation>منصة</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="154"/>
        <source>Factory Reset</source>
        <translation>الإعدادات الأصلية</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="142"/>
        <source>About Qt</source>
        <translation>نبذة عن كيوتي</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="83"/>
        <source>Switch Profiles</source>
        <translation>إستبدال المسارات</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="92"/>
        <source>Oversampling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="artisanlib/main.py" line="1495"/>
        <source>HUD OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1508"/>
        <source>HUD ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1587"/>
        <source>Alarm notice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1592"/>
        <source>Alarm is calling: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1599"/>
        <source>Alarm trigger button error, description &apos;%1&apos; not a number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1627"/>
        <source>Alarm trigger slider error, description &apos;%1&apos; not a valid number [0-100]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2001"/>
        <source>Save the profile, Discard the profile (Reset), or Cancel?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2002"/>
        <source>Profile unsaved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2010"/>
        <source>Action canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2076"/>
        <source>Scope has been reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3181"/>
        <source>Time format error encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3282"/>
        <source>Convert profile data to Fahrenheit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3360"/>
        <source>Convert Profile Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3319"/>
        <source>Profile changed to Fahrenheit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3322"/>
        <source>Unable to comply. You already are in Fahrenheit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3362"/>
        <source>Profile not changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3328"/>
        <source>Convert profile data to Celsius?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3360"/>
        <source>Unable to comply. You already are in Celsius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3366"/>
        <source>Profile changed to Celsius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3371"/>
        <source>Convert Profile Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3371"/>
        <source>No profile data found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3388"/>
        <source>Colors set to defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3393"/>
        <source>Colors set to grey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3521"/>
        <source>Scope monitoring...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3564"/>
        <source>Scope stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3629"/>
        <source>Scope recording...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3669"/>
        <source>Scope recording stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3719"/>
        <source>Not enough variables collected yet. Try again in a few seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3752"/>
        <source>Roast time starts now 00:00 BT = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4172"/>
        <source>Scope is OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3833"/>
        <source>[DRY END] recorded at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3892"/>
        <source>[FC START] recorded at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3946"/>
        <source>[FC END] recorded at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4004"/>
        <source>[SC START] recorded at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4061"/>
        <source>[SC END] recorded at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4141"/>
        <source>Roast ended at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4205"/>
        <source>[COOL END] recorded at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4319"/>
        <source>Event # %1 recorded at BT = %2 Time = %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4334"/>
        <source>Timer is OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4366"/>
        <source>Computer Event # %1 recorded at BT = %2 Time = %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4445"/>
        <source>Statistics cancelled: need complete profile [CHARGE] + [DROP]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4676"/>
        <source>Unable to move background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4735"/>
        <source>No finished profile found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4753"/>
        <source>Polynomial coefficients (Horner form):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4756"/>
        <source>Knots:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4759"/>
        <source>Residual:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4762"/>
        <source>Roots:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4766"/>
        <source>Profile information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4919"/>
        <source>Designer Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4963"/>
        <source>Designer Init</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4963"/>
        <source>Unable to start designer.
Profile missing [CHARGE] or [DROP]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5179"/>
        <source>[ CHARGE ]</source>
        <translation>[تحميل]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5182"/>
        <source>[ DRY END ]</source>
        <translation>[تم التجفيف]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5185"/>
        <source>[ FC START ]</source>
        <translation>[يدء الصدع الأول]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5188"/>
        <source>[ FC END ]</source>
        <translation>[نهاية الصدع الأول]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5191"/>
        <source>[ SC START ]</source>
        <translation>[بدء الصدع الثاني]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5194"/>
        <source>[ SC END ]</source>
        <translation>[نهاية الصدع الثاني]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5197"/>
        <source>[ DROP ]</source>
        <translation>[إخراج]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5460"/>
        <source>New profile created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26791"/>
        <source>Open Wheel Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5647"/>
        <source> added to cupping notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5653"/>
        <source> added to roasting notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5920"/>
        <source>Mouse Cross ON: move mouse around</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5932"/>
        <source>Mouse cross OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8775"/>
        <source>Do you want to reset all settings?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8776"/>
        <source>Factory Reset</source>
        <translation>الإعدادات الأصلية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8967"/>
        <source>Exit Designer?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8968"/>
        <source>Designer Mode ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9145"/>
        <source>Keyboard moves turned ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9154"/>
        <source>Keyboard moves turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9236"/>
        <source>Profile %1 saved in: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9243"/>
        <source>Empty path or box unchecked in Autosave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9250"/>
        <source>&lt;b&gt;[ENTER]&lt;/b&gt; = Turns ON/OFF Keyboard Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9251"/>
        <source>&lt;b&gt;[SPACE]&lt;/b&gt; = Choses current button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9252"/>
        <source>&lt;b&gt;[LEFT]&lt;/b&gt; = Move to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9253"/>
        <source>&lt;b&gt;[RIGHT]&lt;/b&gt; = Move to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9254"/>
        <source>&lt;b&gt;[a]&lt;/b&gt; = Autosave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9255"/>
        <source>&lt;b&gt;[CRTL N]&lt;/b&gt; = Autosave + Reset + START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9256"/>
        <source>&lt;b&gt;[t]&lt;/b&gt; = Mouse cross lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9258"/>
        <source>&lt;b&gt;[b]&lt;/b&gt; = Shows/Hides Extra Event Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9260"/>
        <source>&lt;b&gt;[i]&lt;/b&gt; = Retrieve Weight In from Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9261"/>
        <source>&lt;b&gt;[o]&lt;/b&gt; = Retrieve Weight Out from Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9262"/>
        <source>&lt;b&gt;[0-9]&lt;/b&gt; = Changes Event Button Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9263"/>
        <source>&lt;b&gt;[;]&lt;/b&gt; = Application ScreenShot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9264"/>
        <source>&lt;b&gt;[:]&lt;/b&gt; = Desktop ScreenShot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9268"/>
        <source>Keyboard Shotcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9349"/>
        <source>Event #%1:  %2 has been updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9436"/>
        <source>Save</source>
        <translation>حفظ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9445"/>
        <source>Select Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16692"/>
        <source>No profile found</source>
        <translation>لم يوجد أي مسار</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9498"/>
        <source>%1 has been saved. New roast has started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9680"/>
        <source>Invalid artisan format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9540"/>
        <source>%1  loaded </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9676"/>
        <source>Background %1 loaded successfully %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9790"/>
        <source>Artisan CSV file loaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10369"/>
        <source>To load this profile the extra devices configuration needs to be changed.
Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10370"/>
        <source>Found a different number of curves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11063"/>
        <source>Save Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11070"/>
        <source>Profile saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11109"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11086"/>
        <source>Readings exported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11094"/>
        <source>Export CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11097"/>
        <source>Export JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11100"/>
        <source>Export RoastLogger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11107"/>
        <source>Readings imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11115"/>
        <source>Import CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11118"/>
        <source>Import JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11121"/>
        <source>Import RoastLogger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13524"/>
        <source>Sampling Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13524"/>
        <source>Seconds</source>
        <translation>ثواني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13730"/>
        <source>Alarm Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13730"/>
        <source>Alarms are not available for device None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13783"/>
        <source>Switch Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13783"/>
        <source>Language successfully changed. Restart the application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13815"/>
        <source>Import K202 CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13865"/>
        <source>K202 file loaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13879"/>
        <source>Import K204 CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13943"/>
        <source>K204 file loaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13957"/>
        <source>Import HH506RA CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14006"/>
        <source>HH506RA file loaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14047"/>
        <source>Save Graph as PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14054"/>
        <source>%1  size(%2,%3) saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14063"/>
        <source>Save Graph as SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14068"/>
        <source>%1 saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14276"/>
        <source>Invalid Wheel graph format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14279"/>
        <source>Wheel Graph succesfully open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14298"/>
        <source>Return the absolute value of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14299"/>
        <source>Return the arc cosine (measured in radians) of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14300"/>
        <source>Return the arc sine (measured in radians) of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14301"/>
        <source>Return the arc tangent (measured in radians) of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14302"/>
        <source>Return the cosine of x (measured in radians).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14303"/>
        <source>Convert angle x from radians to degrees.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14304"/>
        <source>Return e raised to the power of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14305"/>
        <source>Return the logarithm of x to the given base. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14306"/>
        <source>Return the base 10 logarithm of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14309"/>
        <source>Return x**y (x to the power of y).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14310"/>
        <source>Convert angle x from degrees to radians.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14311"/>
        <source>Return the sine of x (measured in radians).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14312"/>
        <source>Return the square root of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14313"/>
        <source>Return the tangent of x (measured in radians).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14316"/>
        <source>previous ET value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14317"/>
        <source>previous BT value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14318"/>
        <source>previous Extra #1 T1 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14319"/>
        <source>previous Extra #1 T2 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14320"/>
        <source>previous Extra #2 T1 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14321"/>
        <source>previous Extra #2 T2 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14328"/>
        <source>MATHEMATICAL FUNCTIONS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14330"/>
        <source>SYMBOLIC VARIABLES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14332"/>
        <source>Symbolic Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14470"/>
        <source>Save Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14474"/>
        <source>Palettes saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14514"/>
        <source>Invalid palettes file format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14517"/>
        <source>Palettes loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14529"/>
        <source>Load Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14550"/>
        <source>Alarms loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15526"/>
        <source>Interpolation failed: no profile available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15535"/>
        <source>Sound turned ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15539"/>
        <source>Sound turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15705"/>
        <source>[ET target 1 = %1] [BT target 1 = %2] [ET target 2 = %3] [BT target 2 = %4]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16689"/>
        <source>Event #%1 added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16712"/>
        <source> Event #%1 deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16717"/>
        <source>No events found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16952"/>
        <source>Roast properties updated but profile not saved to disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17199"/>
        <source>Autosave ON. Prefix: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17203"/>
        <source>Autosave OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17491"/>
        <source>xlimit = (%3,%4) ylimit = (%1,%2) zlimit = (%5,%6)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18536"/>
        <source>&lt;b&gt;Event&lt;/b&gt; hide or show the corresponding slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18537"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action on slider release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18538"/>
        <source>&lt;b&gt;Command&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19294"/>
        <source>Serial Command: ASCII serial command or binary a2b_uu(serial command)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19297"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) writes values to the registers in slaves specified by the given ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19298"/>
        <source>DTA Command: Insert Data address : value, ex. 4701:1000 and sv is 100. always multiply with 10 if value Unit: 0.1 / ex. 4719:0 stops heating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18542"/>
        <source>&lt;b&gt;Offset&lt;/b&gt; added as offset to the slider value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18543"/>
        <source>&lt;b&gt;Factor&lt;/b&gt; multiplicator of the slider value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19301"/>
        <source>Event custom buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19265"/>
        <source>Event configuration saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19268"/>
        <source>Found empty event type box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19288"/>
        <source>&lt;b&gt;Button Label&lt;/b&gt; Enter \n to create labels with multiple lines.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19289"/>
        <source>&lt;b&gt;Event Description&lt;/b&gt; Description of the Event to be recorded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19290"/>
        <source>&lt;b&gt;Event type&lt;/b&gt; Type of event to be recorded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19291"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-10) to be recorded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19292"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action at the time of the event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19293"/>
        <source>&lt;b&gt;Documentation&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by the event value):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19295"/>
        <source>Call Program: A program/script path (absolute or relative)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19296"/>
        <source>Multiple Event: Adds events of other button numbers separated by a comma: 1,2,3, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19299"/>
        <source>&lt;b&gt;Button Visibility&lt;/b&gt; Hides/shows individual button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19300"/>
        <source>&lt;b&gt;Keyboard Shorcut: &lt;/b&gt; [b] Hides/shows Extra Button Rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19516"/>
        <source>Phases changed to %1 default: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19651"/>
        <source>Background profile not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19656"/>
        <source>Background does not match number of labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19974"/>
        <source>Playback Aid set ON at %1 secs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19982"/>
        <source>No profile background found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20076"/>
        <source>Reading background profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23346"/>
        <source>Not enough time points for an ET curviness of %1. Set curviness to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23556"/>
        <source>Designer Config</source>
        <translation>إعداد المصمم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23353"/>
        <source>Not enough time points for an BT curviness of %1. Set curviness to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23548"/>
        <source>CHARGE</source>
        <translation>تحميل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23549"/>
        <source>DRY END</source>
        <translation>إنتهاء التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23550"/>
        <source>FC START</source>
        <translation>يدء الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23551"/>
        <source>FC END</source>
        <translation>نهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23552"/>
        <source>SC START</source>
        <translation>بدء الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23553"/>
        <source>SC END</source>
        <translation>نهاية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23554"/>
        <source>DROP</source>
        <translation>إخراج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23368"/>
        <source>Incorrect time format. Please recheck %1 time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23555"/>
        <source>Times need to be in ascending order. Please recheck %1 time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23520"/>
        <source>Designer has been reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23952"/>
        <source>Tick the Float flag in this case.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24396"/>
        <source>Serial Port Settings: %1, %2, %3, %4, %5, %6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25038"/>
        <source>External program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25245"/>
        <source>Device not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25302"/>
        <source>PID to control ET set to %1 %2 ; PID to read BT set to %3 %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25526"/>
        <source>Device set to %1. Now, check Serial Port settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25621"/>
        <source>Device set to %1. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25378"/>
        <source>Device set to CENTER 305, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25432"/>
        <source>Device set to %1, which is equivalent to CENTER 309. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25459"/>
        <source>Device set to %1, which is equivalent to CENTER 303. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25441"/>
        <source>Device set to %1, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25468"/>
        <source>Device set to %1, which is equivalent to Omega HH506RA. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25569"/>
        <source>Device set to %1, which is equivalent to Omega HH806AU. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25603"/>
        <source>Device set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25499"/>
        <source>Device set to %1%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25560"/>
        <source>Device set to %1, which is equivalent to CENTER 302. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26276"/>
        <source>Color of %1 set to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26781"/>
        <source>Save Wheel graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26785"/>
        <source>Wheel Graph saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27043"/>
        <source>Load Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27081"/>
        <source>Save Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27113"/>
        <source>&lt;b&gt;Status:&lt;/b&gt; activate or deactive alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27114"/>
        <source>&lt;b&gt;If Alarm:&lt;/b&gt; alarm triggered only if the alarm with the given number was triggered before. Use 0 for no guard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27116"/>
        <source>&lt;b&gt;From:&lt;/b&gt; alarm only triggered after the given event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27117"/>
        <source>&lt;b&gt;Time:&lt;/b&gt; if not 00:00, alarm is triggered mm:ss after the event &apos;From&apos; happend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27118"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; the temperature source that is observed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27119"/>
        <source>&lt;b&gt;Condition:&lt;/b&gt; alarm is triggered if source rises above or below the specified temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27120"/>
        <source>&lt;b&gt;Temp:&lt;/b&gt; the speficied temperature limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27121"/>
        <source>&lt;b&gt;Action:&lt;/b&gt; if all conditions are fulfilled the alarm triggeres the corresponding action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27123"/>
        <source>&lt;b&gt;NOTE:&lt;/b&gt; each alarm is only triggered once</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30102"/>
        <source>OFF</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30095"/>
        <source>CONTINUOUS CONTROL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30108"/>
        <source>ON</source>
        <translation>شغّل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30107"/>
        <source>STANDBY MODE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28144"/>
        <source>The rampsoak-mode tells how to start and end the ramp/soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28145"/>
        <source>Your rampsoak mode in this pid is:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28146"/>
        <source>Mode = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28148"/>
        <source>Start to run from PV value: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28149"/>
        <source>End output status at the end of ramp/soak: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28150"/>
        <source>Output status while ramp/soak operation set to OFF: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28151"/>
        <source>
Repeat Operation at the end: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28153"/>
        <source>Recomended Mode = 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28154"/>
        <source>If you need to change it, change it now and come back later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28155"/>
        <source>Use the Parameter Loader Software by Fuji if you need to

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28156"/>
        <source>Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28157"/>
        <source>RampSoak Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29512"/>
        <source>Current sv = %1. Change now to sv = %2?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29574"/>
        <source>Change svN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29573"/>
        <source>Current pid = %1. Change now to pid =%2?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30328"/>
        <source>Ramp Soak start-end mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30234"/>
        <source>Pattern changed to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30236"/>
        <source>Pattern did not changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30239"/>
        <source>Ramp/Soak was found ON! Turn it off before changing the pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30241"/>
        <source>Ramp/Soak was found in Hold! Turn it off before changing the pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30582"/>
        <source>Activate PID front buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30582"/>
        <source>Remember SV memory has a finite
life of ~10,000 writes.

Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30684"/>
        <source>RS ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30686"/>
        <source>RS OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30688"/>
        <source>RS on HOLD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30709"/>
        <source>PXG sv#%1 set to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30730"/>
        <source>PXR sv set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30765"/>
        <source>SV%1 changed from %2 to %3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30773"/>
        <source>Unable to set sv%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30784"/>
        <source>SV changed from %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30792"/>
        <source>Unable to set sv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30794"/>
        <source>Unable to set new sv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9266"/>
        <source>&lt;b&gt;[f]&lt;/b&gt; = Full Screen Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14061"/>
        <source>Save Graph as PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1581"/>
        <source>Alarm %1 triggered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22227"/>
        <source>Phidget Temperature Sensor 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22235"/>
        <source>Phidget Temperature Sensor 4-input not attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22323"/>
        <source>Phidget Bridge 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22331"/>
        <source>Phidget Bridge 4-input not attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25551"/>
        <source>Device set to %1. Now, chose Modbus serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27115"/>
        <source>&lt;b&gt;But Not:&lt;/b&gt; alarm triggered only if the alarm with the given number was not triggered before. Use 0 for no guard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3772"/>
        <source>[TP] recorded at %1 BT = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4919"/>
        <source>Importing a profile in to Designer will decimate all data except the main [points].
Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27122"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button or the new value of the slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29111"/>
        <source>Load PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29195"/>
        <source>Save PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13531"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13531"/>
        <source>A tight sampling interval might lead to instability on some machines. We suggest a minimum of 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13514"/>
        <source>Oversampling is only active with a sampling interval equal or larger than 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14323"/>
        <source>current background ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14324"/>
        <source>current background BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9257"/>
        <source>&lt;b&gt;[d]&lt;/b&gt; = Toggle xy scale (T/Delta)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9259"/>
        <source>&lt;b&gt;[s]&lt;/b&gt; = Shows/Hides Event Sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22415"/>
        <source>Phidget 1018 IO attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22423"/>
        <source>Phidget 1018 IO not attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31275"/>
        <source>Load Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31294"/>
        <source>Save Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31500"/>
        <source>PID turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31513"/>
        <source>PID turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9265"/>
        <source>&lt;b&gt;[q,w,e,r + &lt;i&gt;nn&lt;/i&gt;]&lt;/b&gt; = Quick Custom Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14307"/>
        <source>Return the minimum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14308"/>
        <source>Return the maximum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23940"/>
        <source>The MODBUS device corresponds to input channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23941"/>
        <source>1 and 2.. The MODBUS_34 extra device adds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23942"/>
        <source>input channels 3 and 4. Inputs with slave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23943"/>
        <source>id set to 0 are turned off. Modbus function 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23944"/>
        <source>&apos;read holding register&apos; is the standard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23945"/>
        <source>Modbus function 4 triggers the use of &apos;read </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23946"/>
        <source>input register&apos;.Input registers (fct 4) usually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23947"/>
        <source> are from 30000-39999.Most devices hold data in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23948"/>
        <source>2 byte integer registers. A temperature of 145.2C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23949"/>
        <source>is often sent as 1452. In that case you have to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23950"/>
        <source>use the symbolic assignment &apos;x/10&apos;. Few devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23951"/>
        <source>hold data as 4 byte floats in two registers.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Radio Button</name>
    <message>
        <location filename="artisanlib/main.py" line="24547"/>
        <source>Meter</source>
        <translation>جهاز قياس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24548"/>
        <source>PID</source>
        <translation>بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23500"/>
        <source>Arduino TC4</source>
        <translation type="obsolete">أردبنو تي سي 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24550"/>
        <source>Program</source>
        <translation>برنامج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24549"/>
        <source>TC4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Annotation</name>
    <message>
        <location filename="artisanlib/main.py" line="700"/>
        <source>Speed</source>
        <translation>السرعة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="701"/>
        <source>Heater</source>
        <translation>السخّان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="702"/>
        <source>Damper</source>
        <translation>الصمام</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="703"/>
        <source>Fan</source>
        <translation>مروحة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3031"/>
        <source>START 00:00</source>
        <translation type="obsolete">البداية 00:00</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3803"/>
        <source>DE %1</source>
        <translation>تم التجفيف %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3859"/>
        <source>FCs %1</source>
        <translation>بدء الصدع الأول %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3914"/>
        <source>FCe %1</source>
        <translation>نهاية الصدع الأول %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3967"/>
        <source>SCs %1</source>
        <translation>بدء الصدع الثاني %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4025"/>
        <source>SCe %1</source>
        <translation>نهاية الصدع الثاني %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3369"/>
        <source>END %1</source>
        <translation type="obsolete">النهاية %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4164"/>
        <source>CE %1</source>
        <translation>نهاية التبريد %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3724"/>
        <source>CHARGE 00:00</source>
        <translation>تحميل 00:00</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4085"/>
        <source>DROP %1</source>
        <translation>إخراج %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3764"/>
        <source>TP %1</source>
        <translation>نقطة تحول %1</translation>
    </message>
</context>
<context>
    <name>Scope Title</name>
    <message>
        <location filename="artisanlib/main.py" line="10484"/>
        <source>Roaster Scope</source>
        <translation>نطاق الحمص</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="artisanlib/main.py" line="19977"/>
        <source>Playback Aid set OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28486"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29285"/>
        <source>Decimal position successfully set to 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29288"/>
        <source>Problem setting decimal position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29339"/>
        <source>Problem setting thermocouple type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30318"/>
        <source>setting autotune...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30345"/>
        <source>Autotune successfully turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30348"/>
        <source>Autotune successfully turned ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30246"/>
        <source>wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27989"/>
        <source>PID OFF</source>
        <translation>تشغيل بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27992"/>
        <source>PID ON</source>
        <translation>إيقاف بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28016"/>
        <source>SV successfully set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28027"/>
        <source>Empty SV box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28036"/>
        <source>Unable to read SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30141"/>
        <source>Ramp/Soak operation cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30144"/>
        <source>No RX data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30182"/>
        <source>RS ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30156"/>
        <source>Need to change pattern mode...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30165"/>
        <source>Pattern has been changed. Wait 5 secs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30168"/>
        <source>Pattern could not be changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30199"/>
        <source>RampSoak could not be changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30202"/>
        <source>RS OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30212"/>
        <source>RS successfully turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28249"/>
        <source>setONOFFrampsoak(): Ramp Soak could not be set OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28275"/>
        <source>getsegment(): problem reading ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28288"/>
        <source>getsegment(): problem reading soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28301"/>
        <source>getallsegments(): problem reading R/S </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30314"/>
        <source>Finished reading Ramp/Soak val.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28396"/>
        <source>setpid(): There was a problem setting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28470"/>
        <source>Ramp/Soak successfully written</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29248"/>
        <source>Time Units successfully set to MM:SS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29251"/>
        <source>Problem setting time units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29336"/>
        <source>Thermocouple type successfully set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29529"/>
        <source>SV%1 set to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29533"/>
        <source>Problem setting SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29535"/>
        <source>Cancelled svN change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29553"/>
        <source>PID already using sv%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29556"/>
        <source>setNsv(): bad response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29593"/>
        <source>setNpid(): bad confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29597"/>
        <source>Cancelled pid change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29615"/>
        <source>PID was already using pid %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29618"/>
        <source>setNpid(): Unable to set pid %1 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29695"/>
        <source>SV%1 successfully set to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29703"/>
        <source>setsv(): Unable to set SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29821"/>
        <source>pid #%1 successfully set to (%2,%3,%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29829"/>
        <source>pid command failed. Bad data at pid%1 (8,8,8): (%2,%3,%4) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29907"/>
        <source>getallpid(): Unable to read pid values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29935"/>
        <source>PID is using pid = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29938"/>
        <source>getallpid(): Unable to read current sv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30002"/>
        <source>PID is using SV = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30215"/>
        <source>Ramp Soak could not be set OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30256"/>
        <source>PID set to OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30259"/>
        <source>PID set to ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30262"/>
        <source>Unable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30266"/>
        <source>No data received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30306"/>
        <source>Reading Ramp/Soak %1 ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30311"/>
        <source>problem reading Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30327"/>
        <source>Current pid = %1. Proceed with autotune command?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30331"/>
        <source>Autotune cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30350"/>
        <source>UNABLE to set Autotune</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>SV</source>
        <translation>اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>Ramp (MM:SS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>Soak (MM:SS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30422"/>
        <source>Ramp/Soak successfully writen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31376"/>
        <source>Work in Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29839"/>
        <source>sending commands for p%1 i%2 d%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28387"/>
        <source>%1 successfully sent to pid </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29590"/>
        <source>pid changed to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28344"/>
        <source>Finished reading pid values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tab</name>
    <message>
        <location filename="artisanlib/main.py" line="15114"/>
        <source>HUD</source>
        <translation>إستقراء هد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15117"/>
        <source>Plotter</source>
        <translation>الرسّام</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15120"/>
        <source>Math</source>
        <translation>الحساب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15123"/>
        <source>UI</source>
        <translation>الواجهة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31393"/>
        <source>General</source>
        <translation>عام</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16395"/>
        <source>Notes</source>
        <translation>ملاحظات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19947"/>
        <source>Events</source>
        <translation>أحداث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19950"/>
        <source>Data</source>
        <translation>معلومات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19944"/>
        <source>Config</source>
        <translation>إعداد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18512"/>
        <source>Buttons</source>
        <translation>أزرّة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18515"/>
        <source>Sliders</source>
        <translation>زلاجات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18521"/>
        <source>Palettes</source>
        <translation>طاقم ألوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18527"/>
        <source>Style</source>
        <translation>أسلوب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24886"/>
        <source>ET/BT</source>
        <translation>حرارة المحيط/البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29102"/>
        <source>Extra</source>
        <translation>إضافي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24243"/>
        <source>Modbus</source>
        <translation>نظام مودبس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24246"/>
        <source>Scale</source>
        <translation>ميزان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24889"/>
        <source>Extra Devices</source>
        <translation>أجهزة إضافية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24892"/>
        <source>Symb ET/BT</source>
        <translation>رمز حرارة المحيط/البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26093"/>
        <source>Graph</source>
        <translation>الرسم البياني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26096"/>
        <source>LCDs</source>
        <translation>شاشات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29090"/>
        <source>RS</source>
        <translation>آر أس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29093"/>
        <source>SV</source>
        <translation>اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31263"/>
        <source>PID</source>
        <translation>بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29099"/>
        <source>Set RS</source>
        <translation>تحديد آر أس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24249"/>
        <source>Color</source>
        <translation>لون</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18518"/>
        <source>Quantifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31267"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24895"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Table</name>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>Abs Time</source>
        <translation type="obsolete">الوقت المطلق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>Rel Time</source>
        <translation type="obsolete">الوقت النسبي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>ET</source>
        <translation>حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>BT</source>
        <translation>حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>DeltaET (d/m)</source>
        <translation type="obsolete">فارق حرارة المحيط (درجة/دقيقة)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>DeltaBT (d/m)</source>
        <translation type="obsolete">فارق حرارة البن (درجة/دقيقة)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18713"/>
        <source>%1 START</source>
        <translation type="obsolete">%1 البداية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18717"/>
        <source>%1 DRY END</source>
        <translation type="obsolete">%1 إنتهاء التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18722"/>
        <source>%1 FC START</source>
        <translation type="obsolete">%1 يدء الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18726"/>
        <source>%1 FC END</source>
        <translation type="obsolete">%1 نهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18730"/>
        <source>%1 SC START</source>
        <translation type="obsolete">%1 بدء الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18734"/>
        <source>%1 SC END</source>
        <translation type="obsolete">%1 نهاية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18738"/>
        <source>%1 END</source>
        <translation type="obsolete">%1 النهاية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18743"/>
        <source>%1 EVENT #%2 %3%4</source>
        <translation type="obsolete">%1 جدث #%2 %3%4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Time</source>
        <translation>الوقت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Description</source>
        <translation>الوصف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20093"/>
        <source>Type</source>
        <translation>نوع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20093"/>
        <source>Value</source>
        <translation>قيمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Label</source>
        <translation>عنوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Action</source>
        <translation>فعل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Documentation</source>
        <translation>توثيق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Visibility</source>
        <translation>الظهور</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Color</source>
        <translation>ألوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Text Color</source>
        <translation>لون النص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Device</source>
        <translation>الجهاز</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Comm Port</source>
        <translation>منفذ الإتصال</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Baud Rate</source>
        <translation>معدل الباود</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Byte Size</source>
        <translation>حجم البايت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Parity</source>
        <translation>تكافؤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Stopbits</source>
        <translation>بت التوقف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Timeout</source>
        <translation>وقت مستقطع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Color 1</source>
        <translation>اللون 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Color 2</source>
        <translation>اللون 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Label 1</source>
        <translation>عنوان 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Label 2</source>
        <translation>عنوان 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>y1(x)</source>
        <translation>ص1(س)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>y2(x)</source>
        <translation>ص2(س)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>LCD 1</source>
        <translation>شاشة 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>LCD 2</source>
        <translation>شاشة 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Curve 1</source>
        <translation>منحنى 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Curve 2</source>
        <translation>منحنى 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Parent</source>
        <translation>أصل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Width</source>
        <translation>العرض</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Opaqueness</source>
        <translation>عدم الشفافية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Delete Wheel</source>
        <translation>حذف العجلة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Edit Labels</source>
        <translation>تحرير العنوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Update Labels</source>
        <translation>تحدبث العنوان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Properties</source>
        <translation>خصائص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Radius</source>
        <translation>نصف القطر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Starting angle</source>
        <translation>زاوية البداية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Projection</source>
        <translation>إسقاط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Text Size</source>
        <translation>حجم النص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Color Pattern</source>
        <translation>نمط اللون</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Status</source>
        <translation>حالة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>If Alarm</source>
        <translation>إذا التنبية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>From</source>
        <translation>من</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Source</source>
        <translation>مصدر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Condition</source>
        <translation>وضع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Temp</source>
        <translation>درجة الحرارة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>SV</source>
        <translation>اس في</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>Ramp HH:MM</source>
        <translation>تدرّج ساعة:دقيقة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>Soak HH:MM</source>
        <translation>إشباع ساعة:دقيقة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13822"/>
        <source>START</source>
        <translation type="obsolete">إبدأ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20175"/>
        <source>DRY END</source>
        <translation>إنتهاء التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20178"/>
        <source>FC START</source>
        <translation>يدء الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20181"/>
        <source>FC END</source>
        <translation>نهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20184"/>
        <source>SC START</source>
        <translation>بدء الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20187"/>
        <source>SC END</source>
        <translation>نهاية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20193"/>
        <source>COOL</source>
        <translation>تبريد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16549"/>
        <source>EVENT #%2 %3%4</source>
        <translation>جدث #%2 %3%4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20190"/>
        <source>DROP</source>
        <translation>إخراج</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20172"/>
        <source>CHARGE</source>
        <translation>تحميل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>But Not</source>
        <translation>ولكن ليس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>DeltaET</source>
        <translation>فارق حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>DeltaBT</source>
        <translation>فارق حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20197"/>
        <source>EVENT #%1 %2%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Beep</source>
        <translation type="unfinished">نغمة</translation>
    </message>
</context>
<context>
    <name>Textbox</name>
    <message>
        <location filename="artisanlib/main.py" line="437"/>
        <source>Acidity</source>
        <translation>الحمضيّة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="423"/>
        <source>Aftertaste</source>
        <translation>المذاق المتأخر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="395"/>
        <source>Clean Cup</source>
        <translation>شراب نظيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="358"/>
        <source>Head</source>
        <translation>رأس</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="435"/>
        <source>Fragrance</source>
        <translation>عبير</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="427"/>
        <source>Sweetness</source>
        <translation>حلاوة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="419"/>
        <source>Aroma</source>
        <translation>نكهة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="442"/>
        <source>Balance</source>
        <translation>توازن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="436"/>
        <source>Body</source>
        <translation>جسم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="368"/>
        <source>Sour</source>
        <translation>الحموضة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="413"/>
        <source>Flavor</source>
        <translation>طعم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="370"/>
        <source>Critical
Stimulus</source>
        <translation>حافز حاسم </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="372"/>
        <source>Bitter</source>
        <translation>مر</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="373"/>
        <source>Astringency</source>
        <translation>مقبّض</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="374"/>
        <source>Solubles
Concentration</source>
        <translation>تركيز الذوبان</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="421"/>
        <source>Mouthfeel</source>
        <translation>إحساس الفم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="376"/>
        <source>Other</source>
        <translation>آخرى</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="377"/>
        <source>Aromatic
Complexity</source>
        <translation>تركيب الرائحة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="378"/>
        <source>Roast
Color</source>
        <translation>لون الحمصة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="379"/>
        <source>Aromatic
Pungency</source>
        <translation>قوة العبير</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="380"/>
        <source>Sweet</source>
        <translation>حالي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="382"/>
        <source>pH</source>
        <translation>درجة الحموضة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="385"/>
        <source>Fragance</source>
        <translation>فراجانسي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="392"/>
        <source>Dry Fragrance</source>
        <translation>العبير الجاف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="393"/>
        <source>Uniformity</source>
        <translation>انتظام</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="394"/>
        <source>Complexity</source>
        <translation>تعقيد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="430"/>
        <source>Finish</source>
        <translation>النهاية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="400"/>
        <source>Brightness</source>
        <translation>إشراق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="401"/>
        <source>Wet Aroma</source>
        <translation>العبير الرطب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="405"/>
        <source>Taste</source>
        <translation>الطعم</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="406"/>
        <source>Nose</source>
        <translation>الأنف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="411"/>
        <source>Fragrance-Aroma</source>
        <translation>العبير و النكهة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="422"/>
        <source>Flavour</source>
        <translation>المذاق</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="432"/>
        <source>Roast Color</source>
        <translation>لون الحمص</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="433"/>
        <source>Crema Texture</source>
        <translation>ملمس الكريما</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="434"/>
        <source>Crema Volume</source>
        <translation>حجم الكريما</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="438"/>
        <source>Bitterness</source>
        <translation>المرارة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="439"/>
        <source>Defects</source>
        <translation>العيوب</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="440"/>
        <source>Aroma Intensity</source>
        <translation>حدّة النكهة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="441"/>
        <source>Aroma Persistence</source>
        <translation>إستمرارية النكهة</translation>
    </message>
</context>
<context>
    <name>Tooltip</name>
    <message>
        <location filename="artisanlib/main.py" line="3527"/>
        <source>Stop monitoring</source>
        <translation>إيقاف المراقية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7374"/>
        <source>Start monitoring</source>
        <translation>يدء المراقية</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3632"/>
        <source>Stop recording</source>
        <translation>إيقاف التسجيل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7387"/>
        <source>Start recording</source>
        <translation>يدء التسجيل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7402"/>
        <source>Marks the begining of First Crack (FCs)</source>
        <translation>مؤشّر لبداية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7409"/>
        <source>Marks the end of First Crack (FCs)</source>
        <translation>مؤشّر لنهاية الصدع الأول</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7416"/>
        <source>Marks the begining of Second Crack (SCs)</source>
        <translation>مؤشّر لبداية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7423"/>
        <source>Marks the end of Second Crack (SCe)</source>
        <translation>مؤشّر لنهاية الصدع الثاني</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7436"/>
        <source>Reset</source>
        <translation>إعادة تعيين</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7444"/>
        <source>Marks the begining of the roast (beans in)</source>
        <translation>مؤشّر لبداية الحمص (إدخال البن)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7452"/>
        <source>Marks the end of the roast (drop beans)</source>
        <translation>مؤشّر لنهاية الحمص (إخراج البن)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7467"/>
        <source>Marks an Event</source>
        <translation>مؤشّر لحدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7476"/>
        <source>Increases the current SV value by 5</source>
        <translation>كبّر قيمة اس في ب 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7484"/>
        <source>Increases the current SV value by 10</source>
        <translation>كبّر قيمة اس في ب 10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7492"/>
        <source>Increases the current SV value by 20</source>
        <translation>كبّر قيمة اس في ب 20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7500"/>
        <source>Decreases the current SV value by 20</source>
        <translation>قلّل قيمة اس في ب 20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7508"/>
        <source>Decreases the current SV value by 10</source>
        <translation>قلّل قيمة اس في ب 10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7516"/>
        <source>Decreases the current SV value by 5</source>
        <translation>قلّل قيمة اس في ب 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7530"/>
        <source>Turns ON/OFF the HUD</source>
        <translation>تفعيل\إغلاق نظام هد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7539"/>
        <source>Marks the end of the Drying phase (DRYEND)</source>
        <translation>مؤشّر لنهاية التجفيف</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7547"/>
        <source>Marks the end of the Cooling phase (COOLEND)</source>
        <translation>مؤشّر لنهاية التبريد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7597"/>
        <source>Timer</source>
        <translation>مؤقّت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7598"/>
        <source>ET Temperature</source>
        <translation>حرارة المحيط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7599"/>
        <source>BT Temperature</source>
        <translation>حرارة البن</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7600"/>
        <source>ET/time (degrees/min)</source>
        <translation>حرارة المحيط \الوقت (درجة\دقيقة)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7601"/>
        <source>BT/time (degrees/min)</source>
        <translation>حرارة البن \الوقت (درجة\دقيقة)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7602"/>
        <source>Value of SV in PID</source>
        <translation>قيمة اس في عند بي آي دي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7603"/>
        <source>PID power %</source>
        <translation>طاقة بي آي دي %</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7687"/>
        <source>Number of events found</source>
        <translation>عدد الأحداث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7698"/>
        <source>Type of event</source>
        <translation>نوع الحدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7705"/>
        <source>Value of event</source>
        <translation>قيمة الحدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7719"/>
        <source>Updates the event</source>
        <translation>تحديث الحدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14392"/>
        <source>&lt;b&gt;Label&lt;/b&gt;= </source>
        <translation>&lt;b&gt;عنوان&lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14393"/>
        <source>&lt;b&gt;Description &lt;/b&gt;= </source>
        <translation>&lt;b&gt;وصف &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14395"/>
        <source>&lt;b&gt;Type &lt;/b&gt;= </source>
        <translation>&lt;b&gt;نوع &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14396"/>
        <source>&lt;b&gt;Value &lt;/b&gt;= </source>
        <translation>&lt;b&gt;قيمة &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14397"/>
        <source>&lt;b&gt;Documentation &lt;/b&gt;= </source>
        <translation>&lt;b&gt;نوثيق &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14398"/>
        <source>&lt;b&gt;Button# &lt;/b&gt;= </source>
        <translation>&lt;b&gt;زر# &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26358"/>
        <source>Save image using current graph size to a png format</source>
        <translation>حفظ صورة بحجم الرسم الحالي يصيغة ال PNG</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14954"/>
        <source>linear: linear interpolation
cubic: 3rd order spline interpolation
nearest: y value of the nearest point</source>
        <translation>خطّي: إستيفاء خطّي
مكعّب: الدور الثالث للإستيفاء الخددي
تقريب: قيمة ص لأقرب نقطة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17065"/>
        <source>ON/OFF logs serial communication</source>
        <translation>تشغيل/إيقاف التسجيل التسلسلي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17155"/>
        <source>Automatic generated name = This text + date + time</source>
        <translation>الإسم المولّد تلقائيا = هذا-النص + تاريخ + وقت</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17158"/>
        <source>ON/OFF of automatic saving when pressing keyboard letter [a]</source>
        <translation>تشغيل/إيقاف الحفظ التلقائي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17169"/>
        <source>Sets the directory to store batch profiles when using the letter [a]</source>
        <translation>تحديد المجلد لحفظ حزم المسارات المستخدمة للحرف [أ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17771"/>
        <source>Allows to enter a description of the last event</source>
        <translation>يسمح بإضافة وصف لآخر حدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17977"/>
        <source>Add new extra Event button</source>
        <translation>إضافة زر حدث جديد</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17982"/>
        <source>Delete the last extra Event button</source>
        <translation>إزالة آخر زر إضافي للحدث</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26854"/>
        <source>Show help</source>
        <translation>إظهار المساعدة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18019"/>
        <source>Backup all palettes to a text file</source>
        <translation>حفظ كل اللوحات إلى ملف نصّي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18317"/>
        <source>Action Type</source>
        <translation>نوع فعل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18322"/>
        <source>Action String</source>
        <translation>جملة فعل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26311"/>
        <source>Aspect Ratio</source>
        <translation>نسبة العرض للإرتفاع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24982"/>
        <source>Example: 100 + 2*x</source>
        <translation>مثال: 100 + 2*س</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24983"/>
        <source>Example: 100 + x</source>
        <translation>مثال: 100 + س</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26300"/>
        <source>Erases wheel parent hierarchy</source>
        <translation>إزالة أصل العجلة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26304"/>
        <source>Sets graph hierarchy child-&gt;parent instead of parent-&gt;child</source>
        <translation>تعيين تسلسل الرسم فرع&gt;أصل بدلا من أصل&gt;فرع</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26318"/>
        <source>Increase size of text in all the graph</source>
        <translation>كبّر مقاس النص في كل الرسمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26321"/>
        <source>Decrease size of text in all the graph</source>
        <translation>صغّر مقاس النص في كل الرسمة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26325"/>
        <source>Decorative edge beween wheels</source>
        <translation>قاصل التجميل بين العجلات</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26331"/>
        <source>Line thickness</source>
        <translation>غلاظة الخط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26336"/>
        <source>Line color</source>
        <translation>لون الخط</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26340"/>
        <source>Apply color pattern to whole graph</source>
        <translation>تطبيق نمط اللون للعجلة بالكامل</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26346"/>
        <source>Add new wheel</source>
        <translation>إضافة عجلة جديدة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26349"/>
        <source>Rotate graph 1 degree counter clockwise</source>
        <translation>تدوير الرسم درجة عكس عقارب الساعة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26352"/>
        <source>Rotate graph 1 degree clockwise</source>
        <translation>تدوير الرسم درجة مع عقارب الساعة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26356"/>
        <source>Save graph to a text file.wg</source>
        <translation>حفظ الرسم إلى ملف نصّي .wg</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26361"/>
        <source>Sets Wheel graph to view mode</source>
        <translation>تحدبد العجلة لوضع المشاهدة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26364"/>
        <source>open graph file.wg</source>
        <translation>إقتح ملف الرسم (.wg)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26367"/>
        <source>Close wheel graph editor</source>
        <translation>أغلق محرر رسم العجلة</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18020"/>
        <source>Restore all palettes from a text file</source>
        <translation>إستعادة كل اللوحات من ملف نصّي</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26859"/>
        <source>Clear alarms table</source>
        <translation>إزالة جدول الإنذارات</translation>
    </message>
</context>
</TS>
