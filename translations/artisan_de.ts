<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="de" sourcelanguage="">
<context>
    <name>About</name>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>Core developers:</source>
        <translation>Hauptentwickler:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13441"/>
        <source>Contributors:</source>
        <translation>Beitragende:</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="artisanlib/main.py" line="26608"/>
        <source>Update</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28813"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26830"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26839"/>
        <source>Delete</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17166"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25886"/>
        <source>Defaults</source>
        <translation>Standardwerte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26357"/>
        <source>Save Img</source>
        <translation>Bild Speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31226"/>
        <source>Load</source>
        <translation>Laden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19784"/>
        <source>Align</source>
        <translation>Justieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14898"/>
        <source>Plot</source>
        <translation>Darstellen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27124"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27501"/>
        <source>Close</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23270"/>
        <source>Create</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25931"/>
        <source>Background</source>
        <translation>Profilvorlage</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25775"/>
        <source>Grid</source>
        <translation>Gitterlininen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25782"/>
        <source>Title</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25789"/>
        <source>Y Label</source>
        <translation>Y Achse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25796"/>
        <source>X Label</source>
        <translation>X Achse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25845"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25852"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25859"/>
        <source>Markers</source>
        <translation>Markierungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25873"/>
        <source>Watermarks</source>
        <translation>Wasserzeichen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25880"/>
        <source>C Lines</source>
        <translation>C Linien</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25889"/>
        <source>Grey</source>
        <translation>Graustufen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25975"/>
        <source>B/W</source>
        <translation>S/W</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26303"/>
        <source>Reverse Hierarchy</source>
        <translation>Hierarchie Umkehren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26335"/>
        <source>Line Color</source>
        <translation>Linienfarbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26354"/>
        <source>Save File</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26363"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28556"/>
        <source>PID OFF</source>
        <translation>PID AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28558"/>
        <source>PID ON</source>
        <translation>PID EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28677"/>
        <source>ON SV buttons</source>
        <translation>SV Tasten Anzeigen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28680"/>
        <source>OFF SV buttons</source>
        <translation>SV Tasten Verstecken</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26824"/>
        <source>All On</source>
        <translation>Alle AN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26827"/>
        <source>All Off</source>
        <translation>Alle AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19555"/>
        <source>Save Image</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19834"/>
        <source>Up</source>
        <translation>Oben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19836"/>
        <source>Down</source>
        <translation>Unten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19838"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19840"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14566"/>
        <source>PID Help</source>
        <translation type="obsolete">PID Hilfe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31249"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14904"/>
        <source>Virtual Device</source>
        <translation>Virtuelles Gerät</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14963"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31101"/>
        <source>Set</source>
        <translation>Aktivieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15869"/>
        <source>Order</source>
        <translation>Anordnen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16129"/>
        <source>in</source>
        <translation>Ein</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16136"/>
        <source>out</source>
        <translation>Aus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17013"/>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19552"/>
        <source>Del</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24656"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26610"/>
        <source>Select</source>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25831"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25838"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25866"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25943"/>
        <source>LED</source>
        <translation>LED</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26299"/>
        <source>Reset Parents</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26317"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26320"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26348"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26351"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26360"/>
        <source>View Mode</source>
        <translation>Anzeigemodus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26633"/>
        <source>Set Color</source>
        <translation>Farbe Aktivieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27496"/>
        <source>Read Ra/So values</source>
        <translation>Ra/So Werte Lesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28550"/>
        <source>RampSoak ON</source>
        <translation>Rampe/Haltezeit AN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28552"/>
        <source>RampSoak OFF</source>
        <translation>Rampe/Haltezeit AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28673"/>
        <source>Write SV</source>
        <translation>SV Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27533"/>
        <source>Set p</source>
        <translation>Wert p</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27534"/>
        <source>Set i</source>
        <translation>Wert i</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27535"/>
        <source>Set d</source>
        <translation>Wert d</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28809"/>
        <source>Autotune ON</source>
        <translation>Autotune EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28811"/>
        <source>Autotune OFF</source>
        <translation>Autotune AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31380"/>
        <source>Read</source>
        <translation>Lesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28940"/>
        <source>Set ET PID to 1 decimal point</source>
        <translation>ET PID  auf eine Dezimalstelle stellen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28942"/>
        <source>Set BT PID to 1 decimal point</source>
        <translation>BT PID  auf eine Dezimalstelle stellen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28546"/>
        <source>Read RS values</source>
        <translation>RS Werte Lesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28609"/>
        <source>Write SV1</source>
        <translation>SV1 Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28611"/>
        <source>Write SV2</source>
        <translation>SV2 Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28613"/>
        <source>Write SV3</source>
        <translation>SV3 Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28615"/>
        <source>Write SV4</source>
        <translation>SV4 Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28617"/>
        <source>Write SV5</source>
        <translation>SV5 Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28619"/>
        <source>Write SV6</source>
        <translation>SV6 Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28621"/>
        <source>Write SV7</source>
        <translation>SV7 Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28683"/>
        <source>Read SV (7-0)</source>
        <translation>SV (7-0) Lesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28791"/>
        <source>pid 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28793"/>
        <source>pid 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28795"/>
        <source>pid 3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28797"/>
        <source>pid 4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28799"/>
        <source>pid 5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28801"/>
        <source>pid 6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28803"/>
        <source>pid 7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28944"/>
        <source>Set ET PID to MM:SS time units</source>
        <translation>ET PID auf MM:SS Zeiteinheit setzen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31229"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7372"/>
        <source>ON</source>
        <translation>EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7385"/>
        <source>START</source>
        <translation>START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3526"/>
        <source>OFF</source>
        <translation>AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7398"/>
        <source>FC
START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7405"/>
        <source>FC
END</source>
        <translation>FC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7412"/>
        <source>SC
START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7419"/>
        <source>SC
END</source>
        <translation>SC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7427"/>
        <source>RESET</source>
        <translation>RESET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7440"/>
        <source>CHARGE</source>
        <translation>FÜLLEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7448"/>
        <source>DROP</source>
        <translation>LEEREN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7456"/>
        <source>Control</source>
        <translation>Steuerung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7463"/>
        <source>EVENT</source>
        <translation>EREIGNIS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7471"/>
        <source>SV +5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7479"/>
        <source>SV +10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7487"/>
        <source>SV +20</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7495"/>
        <source>SV -20</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7503"/>
        <source>SV -10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7511"/>
        <source>SV -5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7519"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7534"/>
        <source>DRY
END</source>
        <translation>TROCKEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7543"/>
        <source>COOL
END</source>
        <translation>ABGEKÜHLT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18001"/>
        <source>Transfer To</source>
        <translation>Übertragen nach</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18003"/>
        <source>Restore From</source>
        <translation>Wiederherstellen von</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27512"/>
        <source>SV Buttons ON</source>
        <translation>SV Tasten An</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27514"/>
        <source>SV Buttons OFF</source>
        <translation>SV Tasten Aus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27516"/>
        <source>Read SV</source>
        <translation>SV Lesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27550"/>
        <source>Read PID Values</source>
        <translation>PID Werte Lesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31381"/>
        <source>Write</source>
        <translation>Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25803"/>
        <source>Drying Phase</source>
        <translation>Trocknungsphase</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25810"/>
        <source>Maillard Phase</source>
        <translation>Maillardphase</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25817"/>
        <source>Development Phase</source>
        <translation>Entwicklungsphase</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25824"/>
        <source>Cooling Phase</source>
        <translation>Abkühlphase</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14895"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26834"/>
        <source>Insert</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26858"/>
        <source>Clear</source>
        <translation>Zurücksetzten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16150"/>
        <source>scan</source>
        <translation>messen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28542"/>
        <source>Write All</source>
        <translation>Alle Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28548"/>
        <source>Write RS values</source>
        <translation>RS Werte Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28685"/>
        <source>Write SV (7-0)</source>
        <translation>SV Werte Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28805"/>
        <source>Read PIDs</source>
        <translation>PIDs Lesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28807"/>
        <source>Write PIDs</source>
        <translation>PIDs Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18208"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31251"/>
        <source>On</source>
        <translation>An</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31253"/>
        <source>Off</source>
        <translation>Aus</translation>
    </message>
</context>
<context>
    <name>CheckBox</name>
    <message>
        <location filename="artisanlib/main.py" line="19772"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19773"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14688"/>
        <source>Projection</source>
        <translation>Projektion</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15092"/>
        <source>Beep</source>
        <translation>Piepston</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19362"/>
        <source>Auto Adjusted</source>
        <translation>Automatische Anpassung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19769"/>
        <source>Show</source>
        <translation>Anzeigen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19771"/>
        <source>Events</source>
        <translation>Ereignisse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20225"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20230"/>
        <source>Characteristics</source>
        <translation>Kenndaten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23070"/>
        <source>DRY END</source>
        <translation>TROCKEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23074"/>
        <source>FC END</source>
        <translation>FC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23078"/>
        <source>SC END</source>
        <translation>SC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17764"/>
        <source>Button</source>
        <translation>Taste</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19560"/>
        <source>Background</source>
        <translation>Profilvorlage</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17064"/>
        <source>Serial Log ON/OFF</source>
        <translation>Serielles Protokoll EIN/AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15860"/>
        <source>Delete roast properties on RESET</source>
        <translation>Lösche Rösteinstellungen bei RESET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17156"/>
        <source>Autosave [a]</source>
        <translation>Automatisches Speichern [a]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17770"/>
        <source>Mini Editor</source>
        <translation>Mini Editor</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18251"/>
        <source>CHARGE</source>
        <translation>FÜLLEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23072"/>
        <source>FC START</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23076"/>
        <source>SC START</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18305"/>
        <source>DROP</source>
        <translation>LEEREN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18314"/>
        <source>COOL END</source>
        <translation>ABGEKÜHLT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19770"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20226"/>
        <source>Bar</source>
        <translation>Balken</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20227"/>
        <source>d/m</source>
        <translation>d/m</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20228"/>
        <source>ETBTa</source>
        <translation>ETBTa</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20229"/>
        <source>Evaluation</source>
        <translation>Bewertung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24521"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24524"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19868"/>
        <source>Playback Aid</source>
        <translation>Wiedergabehilfe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16089"/>
        <source>Heavy FC</source>
        <translation>Lauter FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16092"/>
        <source>Low FC</source>
        <translation>Leiser FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16095"/>
        <source>Light Cut</source>
        <translation>Heller Schnitt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16098"/>
        <source>Dark Cut</source>
        <translation>Dunkler Schnitt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16101"/>
        <source>Drops</source>
        <translation>Öltröpfchen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16104"/>
        <source>Oily</source>
        <translation>Ölig</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16107"/>
        <source>Uneven</source>
        <translation>Ungleichmäßig</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16109"/>
        <source>Tipping</source>
        <translation>Versengte Spitzen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16111"/>
        <source>Scorching</source>
        <translation>Versengungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16113"/>
        <source>Divots</source>
        <translation>Abplatzer</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14654"/>
        <source>Drop Spikes</source>
        <translation>Spitzen Entfernen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14644"/>
        <source>Smooth Spikes</source>
        <translation>Spitzen Glätten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14659"/>
        <source>Limits</source>
        <translation>Limits</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19365"/>
        <source>Watermarks</source>
        <translation>Wasserzeichen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17290"/>
        <source>Lock Max</source>
        <translation>Max Sperre</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26862"/>
        <source>Load alarms from profile</source>
        <translation>Alarme von Profilen laden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17952"/>
        <source>Mark TP</source>
        <translation>TP Markieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19367"/>
        <source>Phases LCDs</source>
        <translation>Phasen LCDs</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19369"/>
        <source>Auto DRY</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19371"/>
        <source>Auto FCs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17946"/>
        <source>Auto CHARGE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17949"/>
        <source>Auto DROP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14765"/>
        <source>Decimal Places</source>
        <translation>Nachkommastellen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24595"/>
        <source>Modbus Port</source>
        <translation>Modbus Kanal</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14649"/>
        <source>Smooth2</source>
        <translation>Glätten2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31182"/>
        <source>Start PID on CHARGE</source>
        <translation>PID bei FÜLLEN starten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31237"/>
        <source>Load Ramp/Soak table from profile</source>
        <translation>Ramp/Soak Tabelle von Profilen laden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24628"/>
        <source>Control Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24703"/>
        <source>Ratiometric</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComboBox</name>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>None</source>
        <translation>Keines</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="811"/>
        <source>Power</source>
        <translation>Energie</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="812"/>
        <source>Damper</source>
        <translation>Luftklappe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="813"/>
        <source>Fan</source>
        <translation>Lüfter</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17268"/>
        <source>upper right</source>
        <translation>oben rechts</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17269"/>
        <source>upper left</source>
        <translation>oben links</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17270"/>
        <source>lower left</source>
        <translation>unten links</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17271"/>
        <source>lower right</source>
        <translation>unten rechts</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17272"/>
        <source>right</source>
        <translation>rechts</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17273"/>
        <source>center left</source>
        <translation>mitte links</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17274"/>
        <source>center right</source>
        <translation>mitte rechts</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17275"/>
        <source>lower center</source>
        <translation>unten mitte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17276"/>
        <source>upper center</source>
        <translation>oben mitte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17277"/>
        <source>center</source>
        <translation>mitte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25896"/>
        <source>grey</source>
        <translation>grau</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25897"/>
        <source>Dark Grey</source>
        <translation>Dunkelgrau</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25898"/>
        <source>Slate Grey</source>
        <translation>Schiefergrau</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25899"/>
        <source>Light Gray</source>
        <translation>Hellgrau</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25900"/>
        <source>Black</source>
        <translation>Schwarz</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25901"/>
        <source>White</source>
        <translation>Weiß</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25902"/>
        <source>Transparent</source>
        <translation>Transparenz</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Flat</source>
        <translation>Eben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>CHARGE</source>
        <translation>FÜLLEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>DRY END</source>
        <translation>TROCKEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>FC END</source>
        <translation>FC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>DROP</source>
        <translation>LEEREN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>OFF</source>
        <translation>AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>ON</source>
        <translation>EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17295"/>
        <source>30 seconds</source>
        <translation>30 Sekunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17296"/>
        <source>1 minute</source>
        <translation>1 Minute</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17297"/>
        <source>2 minute</source>
        <translation>2 Minuten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17298"/>
        <source>3 minute</source>
        <translation>3 Minuten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17299"/>
        <source>4 minute</source>
        <translation>4 Minuten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17300"/>
        <source>5 minute</source>
        <translation>5 Minuten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17323"/>
        <source>solid</source>
        <translation>durchgezogen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17324"/>
        <source>dashed</source>
        <translation>gestrichelt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17325"/>
        <source>dashed-dot</source>
        <translation>strichpunktiert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17326"/>
        <source>dotted</source>
        <translation>punktiert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>linear</source>
        <translation>linear</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14690"/>
        <source>newton</source>
        <translation>newton</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15688"/>
        <source>metrics</source>
        <translation>metrisch</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15690"/>
        <source>thermal</source>
        <translation>thermal</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>cubic</source>
        <translation>kubisch</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14951"/>
        <source>nearest</source>
        <translation>nächstliegend</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17555"/>
        <source>g</source>
        <translation>g</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17556"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16771"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16004"/>
        <source>l</source>
        <translation>l</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17535"/>
        <source>Event #0</source>
        <translation>Ereignis #0</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17537"/>
        <source>Event #%1</source>
        <translation>Ereignis #%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17557"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17578"/>
        <source>liter</source>
        <translation>liter</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17579"/>
        <source>gallon</source>
        <translation>gallon</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17580"/>
        <source>quart</source>
        <translation>quart</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17581"/>
        <source>pint</source>
        <translation>pint</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17582"/>
        <source>cup</source>
        <translation>cup</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17583"/>
        <source>cm^3</source>
        <translation>cm^3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17776"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17777"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Serial Command</source>
        <translation>Serieller Befehl</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Modbus Command</source>
        <translation>Modbus Befehl</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>DTA Command</source>
        <translation>DTA Befehl</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Call Program</source>
        <translation>Externes Programm</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18840"/>
        <source>Multiple Event</source>
        <translation>Ereignissequenz</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Perpendicular</source>
        <translation>Lotrecht</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26624"/>
        <source>Radial</source>
        <translation>Radial</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27194"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27195"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27196"/>
        <source>ET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27197"/>
        <source>BT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>START</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>TP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>FC START</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>SC START</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>SC END</source>
        <translation>SC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27227"/>
        <source>COOL</source>
        <translation>KÜHL</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Event Button</source>
        <translation>Ereignis Taste</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Slider</source>
        <translation>Regler</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27254"/>
        <source>below</source>
        <translation>unter</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27254"/>
        <source>above</source>
        <translation>über</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>Pop Up</source>
        <translation>Dialogfenster</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23252"/>
        <source>SV Commands</source>
        <translation>SV Befehle</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23252"/>
        <source>Ramp Commands</source>
        <translation>Ramp Befehle</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="810"/>
        <source>Speed</source>
        <translation>Drehzahl</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23912"/>
        <source>little-endian</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14831"/>
        <source>classic</source>
        <translation>klassisch</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14831"/>
        <source>xkcd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Default</source>
        <translation>Standardwerte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Humor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14843"/>
        <source>Comic</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>DRY</source>
        <translation>TROCKEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>FCs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>FCe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>SCs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>SCe</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27264"/>
        <source>COOL END</source>
        <translation>ABGEKÜHLT</translation>
    </message>
</context>
<context>
    <name>Contextual Menu</name>
    <message>
        <location filename="artisanlib/main.py" line="5094"/>
        <source>Add point</source>
        <translation>Punkt Hinzufügen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5098"/>
        <source>Remove point</source>
        <translation>Punkt Entfernen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5104"/>
        <source>Reset Designer</source>
        <translation>Designer Zurücksetzen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5108"/>
        <source>Exit Designer</source>
        <translation>Designer Beenden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5693"/>
        <source>Cancel selection</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5701"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5080"/>
        <source>Create</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5685"/>
        <source>Add to Cupping Notes</source>
        <translation>Zu den Notizen zur Verkostung Hinzufügen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5689"/>
        <source>Add to Roasting Notes</source>
        <translation>Zu den Notizen zur Röstung Hinzufügen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5697"/>
        <source>Edit Mode</source>
        <translation>Bearbeitungsmodus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5084"/>
        <source>Config...</source>
        <translation>Einstellungen...</translation>
    </message>
</context>
<context>
    <name>Directory</name>
    <message>
        <location filename="artisanlib/main.py" line="14034"/>
        <source>profiles</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14024"/>
        <source>other</source>
        <translation>Sonstiges</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="878"/>
        <source>edit text</source>
        <translation type="obsolete">text eingeben</translation>
    </message>
</context>
<context>
    <name>Error Message</name>
    <message>
        <location filename="artisanlib/main.py" line="16804"/>
        <source>Unable to move CHARGE to a value that does not exist</source>
        <translation>Versetzen von CHARGE auf einen Wert der nicht existiert fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20841"/>
        <source>HH806AUtemperature(): %1 bytes received but 14 needed</source>
        <translation type="obsolete">HH806AUtemperature(): %1 bytes empfangen aber 14 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21714"/>
        <source>HH806Wtemperature(): Unable to initiate device</source>
        <translation>HH806Wtemperature(): Gerät konnte nicht initalisiert werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21843"/>
        <source>HH506RAGetID: %1 bytes received but 5 needed</source>
        <translation>HH506RAGetID: %1 bytes empfangen aber 5 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21868"/>
        <source>HH506RAtemperature(): Unable to get id from HH506RA device </source>
        <translation>HH506RAtemperature(): Die ID konnte nicht von dem HH506RA Gerät gelesen werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21888"/>
        <source>HH506RAtemperature(): %1 bytes received but 14 needed</source>
        <translation>HH506RAtemperature(): %1 bytes empfangen aber 14 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30912"/>
        <source>Segment values could not be written into PID</source>
        <translation>Abschnittswerte konnten nicht in den PID geschrieben werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30690"/>
        <source>RampSoak could not be changed</source>
        <translation>Rampe/Haltezeit konnte nicht geändert werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30968"/>
        <source>pid.readoneword(): %1 RX bytes received (7 needed) for unit ID=%2</source>
        <translation>pid.readoneword(): %1 RX bytes empfangen (7 benötigt) für Gerät mit ID=%2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12447"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22958"/>
        <source>Value Error:</source>
        <translation>Falscher Wert:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31311"/>
        <source>Exception:</source>
        <translation>Fehler:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26787"/>
        <source>IO Error:</source>
        <translation>Ein-/Ausgabe Fehler:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20865"/>
        <source>Modbus Error:</source>
        <translation>Modbus Fehler:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23014"/>
        <source>Serial Exception:</source>
        <translation>Serieller Kommunikationsfehler:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21150"/>
        <source>F80h Error</source>
        <translation>F80h Fehler</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21160"/>
        <source>CRC16 data corruption ERROR. TX does not match RX. Check wiring</source>
        <translation>Prüfsummenfehler. Verkabelung kontrollieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21163"/>
        <source>No RX data received</source>
        <translation>Keine Daten empfangen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21564"/>
        <source>Unable to open serial port</source>
        <translation>Seriellerkanal konnte nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22013"/>
        <source>CENTER303temperature(): %1 bytes received but 8 needed</source>
        <translation>CENTER303temperature(): %1 bytes empfangen aber 8 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22083"/>
        <source>CENTER306temperature(): %1 bytes received but 10 needed</source>
        <translation>CENTER306temperature(): %1 bytes empfangen aber 10 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21291"/>
        <source>DTAcommand(): %1 bytes received but 15 needed</source>
        <translation>DTAcommand(): %1 bytes empfangen aber 15 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22169"/>
        <source>CENTER309temperature(): %1 bytes received but 45 needed</source>
        <translation>CENTER309temperature(): %1 bytes empfangen aber 45 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22594"/>
        <source>Arduino could not set channels</source>
        <translation>Arduino Kanalkonfiguration fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22605"/>
        <source>Arduino could not set temperature unit</source>
        <translation>Arduino Konfiguration der Temperatureinheiten fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21943"/>
        <source>CENTER302temperature(): %1 bytes received but 7 needed</source>
        <translation>CENTER302temperature(): %1 bytes empfangen aber 7 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24398"/>
        <source>Serial Exception: invalid comm port</source>
        <translation>Serieller Kommunikationsfehler: ungültiger Anschluss</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24403"/>
        <source>Serial Exception: timeout</source>
        <translation>Serieller Kommunikationsfehler: timeout</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15340"/>
        <source>Univariate: no profile data available</source>
        <translation>Univariate: kein Profil verfügbar</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15512"/>
        <source>Polyfit: no profile data available</source>
        <translation>Polyfit: kein Profil verfügbar</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21614"/>
        <source>MS6514temperature(): %1 bytes received but 16 needed</source>
        <translation>MS6514temperature(): %1 bytes empfangen aber 16 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21659"/>
        <source>HH806AUtemperature(): %1 bytes received but 16 needed</source>
        <translation>HH806AUtemperature(): %1 bytes empfangen aber 16 benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9247"/>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Flavor Scope Label</name>
    <message>
        <location filename="artisanlib/main.py" line="13236"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13237"/>
        <source>Grassy</source>
        <translation>Grasig</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13238"/>
        <source>Leathery</source>
        <translation>Lederartig</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13239"/>
        <source>Toasty</source>
        <translation>Toastig</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13240"/>
        <source>Bready</source>
        <translation>Brotartig</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13241"/>
        <source>Acidic</source>
        <translation>Säurereich</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13242"/>
        <source>Flat</source>
        <translation>Flach</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13243"/>
        <source>Fracturing</source>
        <translation>Bruchbildung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13244"/>
        <source>Sweet</source>
        <translation>Süß</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13245"/>
        <source>Less Sweet</source>
        <translation>Wenig Süß</translation>
    </message>
</context>
<context>
    <name>Form Caption</name>
    <message>
        <location filename="artisanlib/main.py" line="14586"/>
        <source>Extras</source>
        <translation>Erweiterungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15722"/>
        <source>Roast Properties</source>
        <translation>Eigenschaften der Röstung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17107"/>
        <source>Error Log</source>
        <translation>Fehlerprotokoll</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17132"/>
        <source>Message History</source>
        <translation>System Meldungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17191"/>
        <source>AutoSave Path</source>
        <translation>Automastisches Speicher Pfad</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17520"/>
        <source>Roast Calculator</source>
        <translation>Röstrechner</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17760"/>
        <source>Events</source>
        <translation>Ereignisse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19310"/>
        <source>Roast Phases</source>
        <translation>Röstphasen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19533"/>
        <source>Cup Profile</source>
        <translation>Verkostung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19761"/>
        <source>Profile Background</source>
        <translation>Profilvorlage</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20222"/>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23064"/>
        <source>Designer Config</source>
        <translation>Einstellungen Designer</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23603"/>
        <source>Manual Temperature Logger</source>
        <translation>Manueller Temperatur Logger</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23729"/>
        <source>Serial Ports Configuration</source>
        <translation>Einstellungen Serielle Anschlüsse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24511"/>
        <source>Device Assignment</source>
        <translation>Gerätezuordnung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25762"/>
        <source>Colors</source>
        <translation>Farben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26290"/>
        <source>Wheel Graph Editor</source>
        <translation>Kreisdiagramm Editor</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26819"/>
        <source>Alarms</source>
        <translation>Alarme</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28483"/>
        <source>Fuji PXG PID Control</source>
        <translation>Fuji PXG PID Steuerung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17214"/>
        <source>Axes</source>
        <translation>Achsen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31373"/>
        <source>Delta DTA PID Control</source>
        <translation>Delta DTA PID Steuerung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17063"/>
        <source>Serial Log</source>
        <translation>Datenübertragungs Protokoll</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16964"/>
        <source>Artisan Platform</source>
        <translation>Artisan Plattform</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17153"/>
        <source>Keyboard Autosave [a]</source>
        <translation>Automatisches Speichern [a]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17004"/>
        <source>Settings Viewer</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27467"/>
        <source>Fuji PXR PID Control</source>
        <translation>Fuji PXR PID Steuerung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31007"/>
        <source>Arduino Control</source>
        <translation>Arduino Steuerung</translation>
    </message>
</context>
<context>
    <name>GroupBox</name>
    <message>
        <location filename="artisanlib/main.py" line="24533"/>
        <source>Curves</source>
        <translation>Kurven</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15015"/>
        <source>Interpolate</source>
        <translation>Interpolation</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15021"/>
        <source>Univariate</source>
        <translation>Eindimensional</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16336"/>
        <source>Times</source>
        <translation>Zeiten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17392"/>
        <source>Legend Location</source>
        <translation>Position der Legende</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26066"/>
        <source>DeltaET LCD</source>
        <translation>DeltaET LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26069"/>
        <source>DeltaBT LCD</source>
        <translation>DeltaBT LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17386"/>
        <source>Time Axis</source>
        <translation>Zeitachse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17388"/>
        <source>Temperature Axis</source>
        <translation>Temperaturachse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17394"/>
        <source>Grid</source>
        <translation>Raster</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24542"/>
        <source>LCDs</source>
        <translation>LCDs</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14799"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15079"/>
        <source>Appearance</source>
        <translation>Oberfläche</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15087"/>
        <source>Resolution</source>
        <translation>Auflösung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17390"/>
        <source>DeltaBT/DeltaET Axis</source>
        <translation>DeltaBT/DeltaET Achse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17633"/>
        <source>Rate of Change</source>
        <translation>Änderungsrate</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17635"/>
        <source>Temperature Conversion</source>
        <translation>Temperaturumrechnung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17637"/>
        <source>Weight Conversion</source>
        <translation>Gewichtsumrechnung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17639"/>
        <source>Volume Conversion</source>
        <translation>Volumenumrechnung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18244"/>
        <source>Event Types</source>
        <translation>Ereignistypen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18351"/>
        <source>Default Buttons</source>
        <translation>Standard Tasten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18394"/>
        <source>Management</source>
        <translation>Verwaltung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20339"/>
        <source>Evaluation</source>
        <translation>Bewertung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20341"/>
        <source>Display</source>
        <translation>Anzeige</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23326"/>
        <source>Initial Settings</source>
        <translation>Standard Einstellungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24113"/>
        <source>Input 1</source>
        <translation>Eingang 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24127"/>
        <source>Input 2</source>
        <translation>Eingang 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24140"/>
        <source>Input 3</source>
        <translation>Eingang 3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24153"/>
        <source>Input 4</source>
        <translation>Eingang 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24801"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24816"/>
        <source>Arduino TC4</source>
        <translation>Arduino TC4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24823"/>
        <source>External Program</source>
        <translation>Externes Programm</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24829"/>
        <source>Symbolic Assignments</source>
        <translation>Symbolische Verknüpfung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26057"/>
        <source>Timer LCD</source>
        <translation>Timer LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26060"/>
        <source>ET LCD</source>
        <translation>ET LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26063"/>
        <source>BT LCD</source>
        <translation>BT LCD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26376"/>
        <source>Label Properties</source>
        <translation>Markierungseigenschaften</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15099"/>
        <source>Sound</source>
        <translation>Ton</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14813"/>
        <source>Input Filters</source>
        <translation>Eingangsfilter</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26072"/>
        <source>Extra Devices / PID SV LCD</source>
        <translation>Zusatzgeräte / PID SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15048"/>
        <source>Polyfit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14858"/>
        <source>Look</source>
        <translation>Aussehen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23644"/>
        <source>Phidgets 1048 Probe Types</source>
        <translation type="obsolete">Phidgets 1048 Fühler Typen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24698"/>
        <source>1048 Probe Types</source>
        <translation>1048 Fühlertypen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24777"/>
        <source>Network</source>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24784"/>
        <source>Phidgets</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31010"/>
        <source>p-i-d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31173"/>
        <source>Set Value</source>
        <translation>Zielwert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24749"/>
        <source>Phidget IO</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTML Report Template</name>
    <message>
        <location filename="artisanlib/main.py" line="12657"/>
        <source>Roasting Report</source>
        <translation>Röstprotokoll</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12682"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12686"/>
        <source>Beans:</source>
        <translation>Bohnen:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12694"/>
        <source>Weight:</source>
        <translation>Gewicht:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12702"/>
        <source>Volume:</source>
        <translation>Volumen:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12714"/>
        <source>Roaster:</source>
        <translation>Röstmaschine:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12718"/>
        <source>Operator:</source>
        <translation>Röstmeister:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12722"/>
        <source>Cupping:</source>
        <translation>Verkostung:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12742"/>
        <source>DRY:</source>
        <translation>TROCKEN:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12824"/>
        <source>Roasting Notes</source>
        <translation>Notizen zur Röstung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12834"/>
        <source>Cupping Notes</source>
        <translation>Notizen zur Verkostung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12690"/>
        <source>Size:</source>
        <translation>Grösse:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12698"/>
        <source>Degree:</source>
        <translation>Grad:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12706"/>
        <source>Density:</source>
        <translation>Dichte:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12710"/>
        <source>Humidity:</source>
        <translation>Feuchtigkeit:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12746"/>
        <source>FCs:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12750"/>
        <source>FCe:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12754"/>
        <source>SCs:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12758"/>
        <source>SCe:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12762"/>
        <source>DROP:</source>
        <translation>LEEREN:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12766"/>
        <source>COOL:</source>
        <translation>KÜHL:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12770"/>
        <source>RoR:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12774"/>
        <source>ETBTa:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12726"/>
        <source>Color:</source>
        <translation>Farbe:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12790"/>
        <source>Maillard:</source>
        <translation>Maillard:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12794"/>
        <source>Development:</source>
        <translation>Entwicklung:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12798"/>
        <source>Cooling:</source>
        <translation>Kühlung:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12786"/>
        <source>Drying:</source>
        <translation>Trocknung:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12734"/>
        <source>CHARGE:</source>
        <translation>FÜLLEN:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12738"/>
        <source>TP:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12804"/>
        <source>Events</source>
        <translation>Ereignisse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12778"/>
        <source>CM:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12804"/>
        <source>Background:</source>
        <translation>Vorlage:</translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="artisanlib/main.py" line="2912"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2919"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7681"/>
        <source>Event #&lt;b&gt;0 &lt;/b&gt;</source>
        <translation>Ereignis #&lt;b&gt;0 &lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23067"/>
        <source>CHARGE</source>
        <translation>FÜLLEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15758"/>
        <source>DRY END</source>
        <translation>TROCKEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15772"/>
        <source>FC START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15787"/>
        <source>FC END</source>
        <translation>FC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15801"/>
        <source>SC START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15815"/>
        <source>SC END</source>
        <translation>SC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23080"/>
        <source>DROP</source>
        <translation>LEEREN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15889"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15892"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15898"/>
        <source>Beans</source>
        <translation>Bohnen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15908"/>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15943"/>
        <source> in</source>
        <translation>rein</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15944"/>
        <source> out</source>
        <translation>raus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15942"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15980"/>
        <source>Density</source>
        <translation>Dichte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15995"/>
        <source>per</source>
        <translation>pro</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16066"/>
        <source>at</source>
        <translation>bei</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16077"/>
        <source>Roaster</source>
        <translation>Röstmaschine</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16079"/>
        <source>Operator</source>
        <translation>Röstmeister</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16080"/>
        <source>Roasting Notes</source>
        <translation>Notizen zur Röstung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16084"/>
        <source>Cupping Notes</source>
        <translation>Notizen zur Verkostung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16761"/>
        <source>                 Density in: %1  g/l   =&gt;   Density out: %2 g/l</source>
        <translation>Dichte rein: %1  g/l   =&gt;   Dichte raus: %2 g/l</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19793"/>
        <source>Opaqueness</source>
        <translation>Transparenz</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19813"/>
        <source>BT Color</source>
        <translation>BT Farbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24011"/>
        <source>Comm Port</source>
        <translation>Anschluss</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24015"/>
        <source>Baud Rate</source>
        <translation>Baudrate</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24021"/>
        <source>Byte Size</source>
        <translation>Datenbits</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24027"/>
        <source>Parity</source>
        <translation>Parität</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24034"/>
        <source>Stopbits</source>
        <translation>Stoppbits</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24040"/>
        <source>Timeout</source>
        <translation>Zeitlimit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24583"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26323"/>
        <source>Edge</source>
        <translation>Kante</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17331"/>
        <source>Width</source>
        <translation>Linienstärke</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24600"/>
        <source>ET Channel</source>
        <translation>ET Kanal</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24603"/>
        <source>BT Channel</source>
        <translation>BT Kanal</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19806"/>
        <source>ET Color</source>
        <translation>ET Farbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23238"/>
        <source>ET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23614"/>
        <source>BT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7624"/>
        <source>PID SV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7628"/>
        <source>PID %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9279"/>
        <source>Event #&lt;b&gt;%1 &lt;/b&gt;</source>
        <translation>Ereignis #&lt;b&gt;%1 &lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13801"/>
        <source>City</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13803"/>
        <source>City+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13805"/>
        <source>Full City</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13807"/>
        <source>Full City+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13809"/>
        <source>Light French</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13811"/>
        <source>French</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31104"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14873"/>
        <source>Y(x)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15846"/>
        <source>COOL</source>
        <translation>KÜHL</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15957"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16015"/>
        <source>Bean Size</source>
        <translation>Bohnengröße</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16021"/>
        <source>mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16060"/>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16775"/>
        <source>(%1 g/l)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17114"/>
        <source>Number of errors found %1</source>
        <translation>Fehleranzahl %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18126"/>
        <source>Max</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18124"/>
        <source>Min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17258"/>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17313"/>
        <source>Step</source>
        <translation>Schrittweite</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17321"/>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17522"/>
        <source>Enter two times along profile</source>
        <translation>Zwei Zeitpunkte des Profils eingeben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17525"/>
        <source>Start (00:00)</source>
        <translation>Start (00:00)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17526"/>
        <source>End (00:00)</source>
        <translation>Ende (00:00)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17545"/>
        <source>Fahrenheit</source>
        <translation>Fahrenheit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17546"/>
        <source>Celsius</source>
        <translation>Celsius</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17673"/>
        <source>Time syntax error. Time not valid</source>
        <translation>Zeitrepresentation Fehler. Zeitangabe is ungültig</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17677"/>
        <source>Error: End time smaller than Start time</source>
        <translation>Fehler: Endzeitpunkt vor Startzeitpunkt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17694"/>
        <source>Best approximation was made from %1 to %2</source>
        <translation>Die Beste Näherung wurde berechnet von %1 nach %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17699"/>
        <source>No profile found</source>
        <translation>Kein Profil gefunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17965"/>
        <source>Max buttons per row</source>
        <translation>Anz. Schalter/Zeile</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17993"/>
        <source>Color Pattern</source>
        <translation>Farbmuster</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18005"/>
        <source>palette #</source>
        <translation>Palette #</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18120"/>
        <source>Event</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18028"/>
        <source>Action</source>
        <translation>Aktion</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18030"/>
        <source>Command</source>
        <translation>Befehl</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18032"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18034"/>
        <source>Factor</source>
        <translation>Faktor</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19534"/>
        <source>Default</source>
        <translation>Standardwerte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19564"/>
        <source>Aspect Ratio</source>
        <translation>Verhältnis</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23093"/>
        <source>Marker</source>
        <translation>Marker</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23095"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23237"/>
        <source>Curviness</source>
        <translation>Rundheit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23250"/>
        <source>Events Playback</source>
        <translation>Ereigniswiedergabe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23885"/>
        <source>Slave</source>
        <translation>Slave</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23890"/>
        <source>Register</source>
        <translation>Register</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24001"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24575"/>
        <source>Control ET</source>
        <translation>Steuere ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24579"/>
        <source>Read BT</source>
        <translation>Lese BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24584"/>
        <source>RS485 Unit ID</source>
        <translation>RS485 Gerätenummer</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24623"/>
        <source>AT Channel</source>
        <translation>AT Kannal</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24637"/>
        <source>ET Y(x)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24638"/>
        <source>BT Y(x)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26309"/>
        <source>Ratio</source>
        <translation>Verhältnis</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26316"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26329"/>
        <source>Line</source>
        <translation>Linie</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26338"/>
        <source>Color pattern</source>
        <translation>Farbmuster</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27472"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(1-4)</source>
        <translation>Rampe Haltezeit HH:MM&lt;br&gt;(1-4)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27477"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(5-8)</source>
        <translation>Rampe Haltezeit HH:MM&lt;br&gt;(5-8)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27479"/>
        <source>Ramp/Soak Pattern</source>
        <translation>Rampe/Haltezeit Muster</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>WARNING</source>
        <translation>WARUNUNG</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>Writing eeprom memory</source>
        <translation>EEPROM Speicher wird geschrieben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>&lt;u&gt;Max life&lt;/u&gt; 10,000 writes</source>
        <translation>&lt;u&gt;Max Schreibzyklen&lt;/u&gt; 10,000</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27523"/>
        <source>Infinite read life.</source>
        <translation>Beliebige Anzahl von Lesezyklen.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>After &lt;u&gt;writing&lt;/u&gt; an adjustment,&lt;br&gt;never power down the pid&lt;br&gt;for the next 5 seconds &lt;br&gt;or the pid may never recover.</source>
        <translation>Nachdem &lt;u&gt;schreiben&lt;/u&gt; von Werten,&lt;br&gt; den PID die nächsten 5sec&lt;br&gt;nicht ausschalten&lt;br&gt;damit er keinen bleibenden Schaden nimmt.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27527"/>
        <source>Read operations manual</source>
        <translation>Benutzerhandbuch lesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29056"/>
        <source>ET Thermocouple type</source>
        <translation>ET Messfühler Typ </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29063"/>
        <source>BT Thermocouple type</source>
        <translation>BT Messfühler Typ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28949"/>
        <source>Artisan uses 1 decimal point</source>
        <translation>Artisan nutzt eine Nachkommastelle</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28491"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(1-7)</source>
        <translation>Rampe Haltezeit (MM:SS)&lt;br&gt;(1-7)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28497"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(8-16)</source>
        <translation>Rampe Haltezeit (MM:SS)&lt;br&gt;(8-16)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28544"/>
        <source>Pattern</source>
        <translation>Muster</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28600"/>
        <source>SV (7-0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28722"/>
        <source>Write</source>
        <translation>Schreiben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28704"/>
        <source>P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28710"/>
        <source>I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28716"/>
        <source>D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28950"/>
        <source>Artisan Fuji PXG uses MINUTES:SECONDS units in Ramp/Soaks</source>
        <translation>Artisan Fuji PXG nutzt MINUTEN:SEKUNDEN als Einheit für Rampe/Haltezeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19820"/>
        <source>DeltaET Color</source>
        <translation>DeltaET Farbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19827"/>
        <source>DeltaBT Color</source>
        <translation>DeltaBT Farbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19872"/>
        <source>Text Warning</source>
        <translation>Textwarnung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19873"/>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16040"/>
        <source>Storage Conditions</source>
        <translation>Lagerfeuchtigkeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16058"/>
        <source>Ambient Conditions</source>
        <translation>Umgebungsfeuchtigkeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16161"/>
        <source>Ambient Source</source>
        <translation>Quelle Umgebungstemperatur</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17846"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17850"/>
        <source>Thickness</source>
        <translation>Linenstärke</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17852"/>
        <source>Opacity</source>
        <translation>Transparenz</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17854"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31377"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17774"/>
        <source>Bars</source>
        <translation>Balken</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14627"/>
        <source>Smooth Curves</source>
        <translation>Kurven glätten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16023"/>
        <source>Whole Color</source>
        <translation>Bohnenfarbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16029"/>
        <source>Ground Color</source>
        <translation>Mahlgutfarbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23895"/>
        <source>Float</source>
        <translation>Gleitkomma</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23896"/>
        <source>Function</source>
        <translation>Funktion</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14619"/>
        <source>Smooth Deltas</source>
        <translation>Deltas glätten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6036"/>
        <source>deg/min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2709"/>
        <source>BackgroundET</source>
        <translation>VorlageET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2713"/>
        <source>BackgroundBT</source>
        <translation>VorlageBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2743"/>
        <source>BackgroundDeltaET</source>
        <translation>VorlageDeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2747"/>
        <source>BackgroundDeltaBT</source>
        <translation>VorlageDeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12910"/>
        <source>d/m</source>
        <translation>d/m</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5519"/>
        <source>BT %1 d/m for %2</source>
        <translation>BT %1 d/m für %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5535"/>
        <source>ET %1 d/m for %2</source>
        <translation>ET %1 d/m für %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14098"/>
        <source>%1 to reach ET target %2</source>
        <translation>%1 bis zum ET Ziel %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14109"/>
        <source> at %1</source>
        <translation> bei %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14111"/>
        <source>%1 to reach BT target %2</source>
        <translation>%1 bis zum BT Ziel %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14168"/>
        <source>ET - BT = %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14219"/>
        <source>ET - BT = %1%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26618"/>
        <source> dg</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26981"/>
        <source>Enter description</source>
        <translation>Beschreibung eingeben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28846"/>
        <source>NOTE: BT Thermocouple type is not stored in the Artisan settings</source>
        <translation>HINWEIS: BT Messfühler Typ wird nicht in den Programmeinstellungen gespeichert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14121"/>
        <source>%1 after FCs</source>
        <translation>%1 nach FCs</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14128"/>
        <source>%1 after FCe</source>
        <translation>%1 nach FCe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14602"/>
        <source>ET Target 1</source>
        <translation>ET Ziel 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14604"/>
        <source>BT Target 1</source>
        <translation>BT Ziel 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14606"/>
        <source>ET Target 2</source>
        <translation>ET Ziel 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14608"/>
        <source>BT Target 2</source>
        <translation>BT Ziel 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14612"/>
        <source>ET p-i-d 1</source>
        <translation>ET p-i-d 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31125"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31133"/>
        <source>max</source>
        <translation>max</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20271"/>
        <source>Drying</source>
        <translation>Trocknung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20272"/>
        <source>Maillard</source>
        <translation>Maillard</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20273"/>
        <source>Development</source>
        <translation>Entwicklung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20274"/>
        <source>Cooling</source>
        <translation>Kühlung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1302"/>
        <source>EVENT</source>
        <translation>EREIGNIS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17281"/>
        <source>Initial Max</source>
        <translation>Initiales Max</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23764"/>
        <source>Settings for non-Modbus devices</source>
        <translation>Einstellungen für nicht-Modbus Geräte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6041"/>
        <source>Curves</source>
        <translation>Kurven</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6045"/>
        <source>Delta Curves</source>
        <translation>Delta Kurven</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>RoR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4616"/>
        <source>ETBTa</source>
        <translation>ETBTa</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17695"/>
        <source>&lt;b&gt;%1&lt;/b&gt; deg/sec, &lt;b&gt;%2&lt;/b&gt; deg/min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14984"/>
        <source>Start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14985"/>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14816"/>
        <source>Path Effects</source>
        <translation>Kurveneffekte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14836"/>
        <source>Font</source>
        <translation>Schrift</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8436"/>
        <source>TP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8464"/>
        <source>DRY</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8498"/>
        <source>FCs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10219"/>
        <source>Charge the beans</source>
        <translation>Bohnen Einfüllen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10205"/>
        <source>Start recording</source>
        <translation>Aufzeichnung starten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17162"/>
        <source>Prefix</source>
        <translation>Präfix</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23620"/>
        <source>Probe 1</source>
        <translation type="obsolete">Fühler 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23624"/>
        <source>Probe 2</source>
        <translation type="obsolete">Fühler 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23628"/>
        <source>Probe 3</source>
        <translation type="obsolete">Fühler 3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23632"/>
        <source>Probe 4</source>
        <translation type="obsolete">Fühler 4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31037"/>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4626"/>
        <source>CM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14635"/>
        <source>Window</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24673"/>
        <source>1:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24677"/>
        <source>2:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24681"/>
        <source>3:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24685"/>
        <source>4:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31053"/>
        <source>Cycle</source>
        <translation>Interval</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31099"/>
        <source>Lookahead</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31106"/>
        <source>Manual</source>
        <translation>Manuell</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31107"/>
        <source>Ramp/Soak</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31108"/>
        <source>Background</source>
        <translation>Profilvorlage</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31113"/>
        <source>SV Buttons</source>
        <translation>SV Tasten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31116"/>
        <source>SV Slider</source>
        <translation>SV Regler</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18128"/>
        <source>Coarse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23928"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23933"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14598"/>
        <source>HUD Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24735"/>
        <source>Raw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24736"/>
        <source>Data rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24737"/>
        <source>Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24756"/>
        <source>ServerId:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24758"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="const/UIconst.py" line="35"/>
        <source>Services</source>
        <translation>Dienste</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="36"/>
        <source>Hide %1</source>
        <translation>%1 ausblenden</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="37"/>
        <source>Hide Others</source>
        <translation>Andere ausblenden</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="38"/>
        <source>Show All</source>
        <translation>Alle anzeigen</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="39"/>
        <source>Preferences...</source>
        <translation>Einstellungen...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="61"/>
        <source>Quit %1</source>
        <translation>%1 beenden</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="141"/>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="artisanlib/main.py" line="17813"/>
        <source>Circle</source>
        <translation>Kreis</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17814"/>
        <source>Square</source>
        <translation>Box</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17815"/>
        <source>Pentagon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17816"/>
        <source>Diamond</source>
        <translation>Diamand</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17817"/>
        <source>Star</source>
        <translation>Stern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17818"/>
        <source>Hexagon 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17819"/>
        <source>Hexagon 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17820"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17821"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17822"/>
        <source>None</source>
        <translation>Keines</translation>
    </message>
</context>
<context>
    <name>Menu</name>
    <message>
        <location filename="const/UIconst.py" line="44"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="47"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="48"/>
        <source>Open...</source>
        <translation>Öffnen...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="49"/>
        <source>Open Recent</source>
        <translation>Zuletzte geöffnete Dateien</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="50"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="51"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="52"/>
        <source>Save As...</source>
        <translation>Speichern Unter...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="57"/>
        <source>Print...</source>
        <translation>Drucken...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="72"/>
        <source>Roast</source>
        <translation>Röstung</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="75"/>
        <source>Properties...</source>
        <translation>Eigenschaften...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="76"/>
        <source>Background...</source>
        <translation>Profilvorlage...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="77"/>
        <source>Cup Profile...</source>
        <translation>Verkostung...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="78"/>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="128"/>
        <source>Calculator</source>
        <translation>Rechner</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="89"/>
        <source>Device...</source>
        <translation>Meßgerät...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="90"/>
        <source>Serial Port...</source>
        <translation>Serieller Anschluss...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="91"/>
        <source>Sampling Interval...</source>
        <translation>Abtastintervall...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="93"/>
        <source>Colors...</source>
        <translation>Farben...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="94"/>
        <source>Phases...</source>
        <translation>Phasen...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="95"/>
        <source>Events...</source>
        <translation>Ereignisse...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="96"/>
        <source>Statistics...</source>
        <translation>Statistiken...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="98"/>
        <source>Autosave...</source>
        <translation>Automatisches Speichern...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="130"/>
        <source>Extras...</source>
        <translation>Erweiterungen...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="134"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="143"/>
        <source>Documentation</source>
        <translation>Dokumentation</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="54"/>
        <source>Save Graph</source>
        <translation>Diagramm speichern</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="86"/>
        <source>Config</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="97"/>
        <source>Axes...</source>
        <translation>Achsen...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="146"/>
        <source>Errors</source>
        <translation>Fehlermeldungen</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="145"/>
        <source>Keyboard Shortcuts</source>
        <translation>Tastaturkürzel</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="147"/>
        <source>Messages</source>
        <translation>System Meldungen</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="99"/>
        <source>Alarms...</source>
        <translation>Alarme...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="64"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="67"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="68"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="69"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="55"/>
        <source>Full Size...</source>
        <translation>Volle Größe...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="127"/>
        <source>Designer</source>
        <translation>Designer</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="79"/>
        <source>Convert to Fahrenheit</source>
        <translation>Nach Fahrenheit Umrechnen</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="80"/>
        <source>Convert to Celsius</source>
        <translation>Nach Celsius Umrechnen</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="81"/>
        <source>Fahrenheit Mode</source>
        <translation>Fahrenheit Modus</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="82"/>
        <source>Celsius Mode</source>
        <translation>Celsius Modus</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="124"/>
        <source>Tools</source>
        <translation>Extras</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="129"/>
        <source>Wheel Graph</source>
        <translation>Kreisdiagramm</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="154"/>
        <source>Factory Reset</source>
        <translation>Werkseinstellungen</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="100"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="148"/>
        <source>Serial</source>
        <translation></translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="56"/>
        <source>Roasting Report</source>
        <translation>Röstprotokoll</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="152"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="153"/>
        <source>Platform</source>
        <translation>Plattform</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6923"/>
        <source>CSV...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6927"/>
        <source>JSON...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6931"/>
        <source>RoastLogger...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6896"/>
        <source>HH506RA...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6900"/>
        <source>K202...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6904"/>
        <source>K204...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="53"/>
        <source>Export</source>
        <translation>Exportieren</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="142"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="83"/>
        <source>Switch Profiles</source>
        <translation>Profile tauschen</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="92"/>
        <source>Oversampling</source>
        <translation>Überabtastung</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="artisanlib/main.py" line="5920"/>
        <source>Mouse Cross ON: move mouse around</source>
        <translation>Fadenkreuz AN: Aktivierung durch Mausbewegung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5932"/>
        <source>Mouse cross OFF</source>
        <translation>Fadenkreuz AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1495"/>
        <source>HUD OFF</source>
        <translation>HUD AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1508"/>
        <source>HUD ON</source>
        <translation>HUD EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1587"/>
        <source>Alarm notice</source>
        <translation>Alarmmeldung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1592"/>
        <source>Alarm is calling: %1</source>
        <translation>Alarm aktiviert: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1599"/>
        <source>Alarm trigger button error, description &apos;%1&apos; not a number</source>
        <translation>Alarm trigger Taste Fehler, Beschreibung &apos;%1&apos; keine Zahl</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1627"/>
        <source>Alarm trigger slider error, description &apos;%1&apos; not a valid number [0-100]</source>
        <translation>Alarm trigger Schieberegler Fehler, Beschreibung &apos;%1&apos; keine gültige Zahl [0-100]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2001"/>
        <source>Save the profile, Discard the profile (Reset), or Cancel?</source>
        <translation>Profile abspeichern, Verwerfe das Profil (Reset), or Abbrechen?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2002"/>
        <source>Profile unsaved</source>
        <translation>Profile nicht gesichert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2076"/>
        <source>Scope has been reset</source>
        <translation>Röstoskop wurde Zurückgesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3181"/>
        <source>Time format error encountered</source>
        <translation>Zeitformatfehler aufgetreten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3282"/>
        <source>Convert profile data to Fahrenheit?</source>
        <translation>Röstprofil in Fahrenheit umrechnen?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3360"/>
        <source>Convert Profile Temperature</source>
        <translation>Temperatureinheit Umstellen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3319"/>
        <source>Profile changed to Fahrenheit</source>
        <translation>Röstprofil wurde auf Fahrenheit umgestellt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3322"/>
        <source>Unable to comply. You already are in Fahrenheit</source>
        <translation>Umrechnung nicht möglich. Temperatureinheit ist bereits Fahrenheit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3362"/>
        <source>Profile not changed</source>
        <translation>Röstprofil nicht verändert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3328"/>
        <source>Convert profile data to Celsius?</source>
        <translation>Röstprofil in Celsius umrechnen?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3360"/>
        <source>Unable to comply. You already are in Celsius</source>
        <translation>Umrechnung nicht möglich. Temperatureinheit ist bereits Celsius</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3366"/>
        <source>Profile changed to Celsius</source>
        <translation>Röstprofil wurde auf Celcius umgestellt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3371"/>
        <source>Convert Profile Scale</source>
        <translation>Umrechnung der Profileskala</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3371"/>
        <source>No profile data found</source>
        <translation>Es wurden keine Profildaten gefunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3388"/>
        <source>Colors set to defaults</source>
        <translation>Farben auf Standardwerte gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3393"/>
        <source>Colors set to grey</source>
        <translation>Farben auf Graustufen gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3521"/>
        <source>Scope monitoring...</source>
        <translation>Röstoskop liest Werte...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3564"/>
        <source>Scope stopped</source>
        <translation>Röstoskop angehalten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3629"/>
        <source>Scope recording...</source>
        <translation>Röstoskop zeichnet auf...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3669"/>
        <source>Scope recording stopped</source>
        <translation>Röstoskop Aufzeichnung beendet</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3719"/>
        <source>Not enough variables collected yet. Try again in a few seconds</source>
        <translation>Noch nicht genug Messwerte verfügbar. In einige Sekunden Wiederholen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3752"/>
        <source>Roast time starts now 00:00 BT = %1</source>
        <translation>Röstung startet jetzt 00:00 BT = %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4172"/>
        <source>Scope is OFF</source>
        <translation>Röstoskop AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3833"/>
        <source>[DRY END] recorded at %1 BT = %2</source>
        <translation>[TROCKEN] erfasst um %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3892"/>
        <source>[FC START] recorded at %1 BT = %2</source>
        <translation>[FC START] erfasst um %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3946"/>
        <source>[FC END] recorded at %1 BT = %2</source>
        <translation>[FC ENDE] erfasst um %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4004"/>
        <source>[SC START] recorded at %1 BT = %2</source>
        <translation>[SC START] erfasst um %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4061"/>
        <source>[SC END] recorded at %1 BT = %2</source>
        <translation>[SC ENDE] erfasst um %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4141"/>
        <source>Roast ended at %1 BT = %2</source>
        <translation>Röstung beendet um %1 BT = %2 </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4205"/>
        <source>[COOL END] recorded at %1 BT = %2</source>
        <translation>[ABGEKÜHLT] erfasst um %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4319"/>
        <source>Event # %1 recorded at BT = %2 Time = %3</source>
        <translation>Event # %1 erfasst bei BT = %2 Zeit = %3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4334"/>
        <source>Timer is OFF</source>
        <translation>Timer ist AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4366"/>
        <source>Computer Event # %1 recorded at BT = %2 Time = %3</source>
        <translation>Event # %1 erfasst bei BT = %2 Zeit = %3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4445"/>
        <source>Statistics cancelled: need complete profile [CHARGE] + [DROP]</source>
        <translation>Statistik abgebrochen: vollständiges Profile mit [FÜLLEN] + [LEEREN] benötigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4676"/>
        <source>Unable to move background</source>
        <translation>Profilvorlage konnte nicht bewegt werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4735"/>
        <source>No finished profile found</source>
        <translation>Kein vollständiges Profile gefunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4753"/>
        <source>Polynomial coefficients (Horner form):</source>
        <translation>Polynomialkoeffizienten (Horner form):</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4756"/>
        <source>Knots:</source>
        <translation>Knoten:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4759"/>
        <source>Residual:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4762"/>
        <source>Roots:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4766"/>
        <source>Profile information</source>
        <translation>Profilinformation</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4919"/>
        <source>Designer Start</source>
        <translation>Designer Start</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4963"/>
        <source>Designer Init</source>
        <translation>Designer Initalizierung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4963"/>
        <source>Unable to start designer.
Profile missing [CHARGE] or [DROP]</source>
        <translation>Designer konnte nicht gestartet werden. Profl ohne [FÜLLEN] oder [LEEREN]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5179"/>
        <source>[ CHARGE ]</source>
        <translation>[ FÜLLEN ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5182"/>
        <source>[ DRY END ]</source>
        <translation>[ TROCKEN ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5185"/>
        <source>[ FC START ]</source>
        <translation>[ FC START ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5188"/>
        <source>[ FC END ]</source>
        <translation>[ FC ENDE ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5191"/>
        <source>[ SC START ]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5194"/>
        <source>[ SC END ]</source>
        <translation>[ SC ENDE ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5197"/>
        <source>[ DROP ]</source>
        <translation>[ LEEREN ]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5460"/>
        <source>New profile created</source>
        <translation>Neues Profile angelegt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26791"/>
        <source>Open Wheel Graph</source>
        <translation>Kreisdiagramm Laden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5647"/>
        <source> added to cupping notes</source>
        <translation>Den Notizen zur Verkostung hinzugefügt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5653"/>
        <source> added to roasting notes</source>
        <translation>Den Notizen zur Röstung hinzugefügt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8775"/>
        <source>Do you want to reset all settings?</source>
        <translation>Alle Einstellungen zurücksetzen?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8776"/>
        <source>Factory Reset</source>
        <translation>Werkseinstellungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9145"/>
        <source>Keyboard moves turned ON</source>
        <translation>Tastaturnavigation EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9154"/>
        <source>Keyboard moves turned OFF</source>
        <translation>Tastaturnavigation AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9236"/>
        <source>Profile %1 saved in: %2</source>
        <translation>Profile %1 gepeichert in: %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9243"/>
        <source>Empty path or box unchecked in Autosave</source>
        <translation>Leerer Verzeichnispfad oder Autosave ausgeschaltet</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9250"/>
        <source>&lt;b&gt;[ENTER]&lt;/b&gt; = Turns ON/OFF Keyboard Shortcuts</source>
        <translation>&lt;b&gt;[EINGABE]&lt;/b&gt; = Tastaturbefehle EIN/AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9251"/>
        <source>&lt;b&gt;[SPACE]&lt;/b&gt; = Choses current button</source>
        <translation>&lt;b&gt;[LEERTASTE]&lt;/b&gt; = Ausgewählte Taste aktivieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9252"/>
        <source>&lt;b&gt;[LEFT]&lt;/b&gt; = Move to the left</source>
        <translation>&lt;b&gt;[LINKS]&lt;/b&gt; = Springe nach links</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9253"/>
        <source>&lt;b&gt;[RIGHT]&lt;/b&gt; = Move to the right</source>
        <translation>&lt;b&gt;[RECHTS]&lt;/b&gt; = Springe nach rechts</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9254"/>
        <source>&lt;b&gt;[a]&lt;/b&gt; = Autosave</source>
        <translation>&lt;b&gt;[a]&lt;/b&gt; = Automatisches Speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9256"/>
        <source>&lt;b&gt;[t]&lt;/b&gt; = Mouse cross lines</source>
        <translation>&lt;b&gt;[t]&lt;/b&gt; = Fadenkreuz</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9258"/>
        <source>&lt;b&gt;[b]&lt;/b&gt; = Shows/Hides Extra Event Buttons</source>
        <translation>&lt;b&gt;[b]&lt;/b&gt; = Extra Ereignistasten Anzeigen/Verstecken</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9259"/>
        <source>&lt;b&gt;[s]&lt;/b&gt; = Shows/Hides Event Sliders</source>
        <translation>&lt;b&gt;[s]&lt;/b&gt; = Ereignisregler an/aus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9260"/>
        <source>&lt;b&gt;[i]&lt;/b&gt; = Retrieve Weight In from Scale</source>
        <translation>&lt;b&gt;[i]&lt;/b&gt; = Eingangsgewicht von Waage einlesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9261"/>
        <source>&lt;b&gt;[o]&lt;/b&gt; = Retrieve Weight Out from Scale</source>
        <translation>&lt;b&gt;[i]&lt;/b&gt; = Ausgangsgewicht von Waage einlesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9262"/>
        <source>&lt;b&gt;[0-9]&lt;/b&gt; = Changes Event Button Palettes</source>
        <translation>&lt;b&gt;[0-9]&lt;/b&gt; = Ereignispalette aktivieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9263"/>
        <source>&lt;b&gt;[;]&lt;/b&gt; = Application ScreenShot</source>
        <translation>&lt;b&gt;[;]&lt;/b&gt; = Bildschirmfoto Artisan Fenster</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9264"/>
        <source>&lt;b&gt;[:]&lt;/b&gt; = Desktop ScreenShot</source>
        <translation>&lt;b&gt;[:]&lt;/b&gt; = Bildschirmfoto Desktop</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9268"/>
        <source>Keyboard Shotcuts</source>
        <translation>Tastaturkürzel</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9349"/>
        <source>Event #%1:  %2 has been updated</source>
        <translation>Ereignis #%1:  %2 wurde geändert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9436"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9445"/>
        <source>Select Directory</source>
        <translation>Verzeichnis wählen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16692"/>
        <source>No profile found</source>
        <translation>Kein Profil gefunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9498"/>
        <source>%1 has been saved. New roast has started</source>
        <translation>%1 wurde gespeichert. Neue Röstung gestartet</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9680"/>
        <source>Invalid artisan format</source>
        <translation>Ungültiges Artisan Dateiformat</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9540"/>
        <source>%1  loaded </source>
        <translation>%1  geladen </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9676"/>
        <source>Background %1 loaded successfully %2</source>
        <translation>Profilvorlage %1 erfolgreich geladen %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9790"/>
        <source>Artisan CSV file loaded successfully</source>
        <translation>Artisan CSV Datei erfolgreich geladen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11063"/>
        <source>Save Profile</source>
        <translation>Profile speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11070"/>
        <source>Profile saved</source>
        <translation>Profile gespeichert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11109"/>
        <source>Cancelled</source>
        <translation>Abbgebrochen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11086"/>
        <source>Readings exported</source>
        <translation>Messwerte exportiert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11094"/>
        <source>Export CSV</source>
        <translation>Exportiere CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11097"/>
        <source>Export JSON</source>
        <translation>Exportiere JSON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11100"/>
        <source>Export RoastLogger</source>
        <translation>Exportiere RoastLogger</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11107"/>
        <source>Readings imported</source>
        <translation>Messwerte importiert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11115"/>
        <source>Import CSV</source>
        <translation>Importiere CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11118"/>
        <source>Import JSON</source>
        <translation>Importiere JSON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11121"/>
        <source>Import RoastLogger</source>
        <translation>Importiere RoastLogger</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13524"/>
        <source>Sampling Interval</source>
        <translation>Abtastintervall</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13524"/>
        <source>Seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13730"/>
        <source>Alarm Config</source>
        <translation>Alarmeinstellungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13730"/>
        <source>Alarms are not available for device None</source>
        <translation>Alarme sind für Gerät &apos;Kein&apos; nicht verfügbar</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13783"/>
        <source>Switch Language</source>
        <translation>Sprache umstellen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13783"/>
        <source>Language successfully changed. Restart the application.</source>
        <translation>Sprache erfolgreich umgestellt. Anwendung Neustarten.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13815"/>
        <source>Import K202 CSV</source>
        <translation>Importiere K202 CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13865"/>
        <source>K202 file loaded successfully</source>
        <translation>K202 Datei erfolgreich geladen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13879"/>
        <source>Import K204 CSV</source>
        <translation>Importiere K204 CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13943"/>
        <source>K204 file loaded successfully</source>
        <translation>K204 Datei erfolgreich geladen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13957"/>
        <source>Import HH506RA CSV</source>
        <translation>Importiere HH506RA CSV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14006"/>
        <source>HH506RA file loaded successfully</source>
        <translation>HH506RA Datei erfolgreich geladen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14047"/>
        <source>Save Graph as PNG</source>
        <translation>Diagramm als PNG speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14054"/>
        <source>%1  size(%2,%3) saved</source>
        <translation>%1  Grösse(%2,%3) gespeichert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14063"/>
        <source>Save Graph as SVG</source>
        <translation>Diagramm als SVG speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14068"/>
        <source>%1 saved</source>
        <translation>%1 gespeichert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14276"/>
        <source>Invalid Wheel graph format</source>
        <translation>Ungültiges Kreisdiagramm Datenformat</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14279"/>
        <source>Wheel Graph succesfully open</source>
        <translation>Kreisdiagramm geladen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14298"/>
        <source>Return the absolute value of x.</source>
        <translation>Liefert den absoluten x Wert.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14299"/>
        <source>Return the arc cosine (measured in radians) of x.</source>
        <translation>Liefert den arc cosinus (in Radians) von x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14300"/>
        <source>Return the arc sine (measured in radians) of x.</source>
        <translation>Liefert den arc sinus (in Radians) von x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14301"/>
        <source>Return the arc tangent (measured in radians) of x.</source>
        <translation>Liefert den arc tangens (in Radians) von x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14302"/>
        <source>Return the cosine of x (measured in radians).</source>
        <translation>Liefert den cosinus (in Radians) von x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14303"/>
        <source>Convert angle x from radians to degrees.</source>
        <translation>Konvertiert den Winkel x von Radians nach Grad.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14304"/>
        <source>Return e raised to the power of x.</source>
        <translation>Liefert e hoch x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14305"/>
        <source>Return the logarithm of x to the given base. </source>
        <translation>Liefert den Logarithmus von x zur gegebenen Basis.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14306"/>
        <source>Return the base 10 logarithm of x.</source>
        <translation>Liefert den 10er Logarithmus von x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14309"/>
        <source>Return x**y (x to the power of y).</source>
        <translation>Liefert x hoch y.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14310"/>
        <source>Convert angle x from degrees to radians.</source>
        <translation>Konvertiert den Winkel x von Grand nach Radians.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14311"/>
        <source>Return the sine of x (measured in radians).</source>
        <translation>Liefert den sinus von x (in Radians).</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14312"/>
        <source>Return the square root of x.</source>
        <translation>Liefert die Quadratwurzel von x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14313"/>
        <source>Return the tangent of x (measured in radians).</source>
        <translation>Liefert den tangens von x (in Radians).</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14328"/>
        <source>MATHEMATICAL FUNCTIONS</source>
        <translation>MATHEMATISCHE FUNKTIONEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14330"/>
        <source>SYMBOLIC VARIABLES</source>
        <translation>SYMBOLISCHE VARIABLEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14332"/>
        <source>Symbolic Functions</source>
        <translation>Symbolische Funktionen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14470"/>
        <source>Save Palettes</source>
        <translation>Paletten speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14474"/>
        <source>Palettes saved</source>
        <translation>Paletten gespeichert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14514"/>
        <source>Invalid palettes file format</source>
        <translation>Ungültige Paletten Datei</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14517"/>
        <source>Palettes loaded</source>
        <translation>Paletten geladen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14529"/>
        <source>Load Palettes</source>
        <translation>Paletten laden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14550"/>
        <source>Alarms loaded</source>
        <translation>Alarme geladen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15535"/>
        <source>Sound turned ON</source>
        <translation>Ton AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15539"/>
        <source>Sound turned OFF</source>
        <translation>Ton AN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16689"/>
        <source>Event #%1 added</source>
        <translation>Ereignis #%1 hinzugefügt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16712"/>
        <source> Event #%1 deleted</source>
        <translation>Ereignis #%1 gelöscht</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16717"/>
        <source>No events found</source>
        <translation>Keine Ereignisse gefunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16952"/>
        <source>Roast properties updated but profile not saved to disk</source>
        <translation>Profileattribute geändert aber nicht abgespeichert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17199"/>
        <source>Autosave ON. Prefix: %1</source>
        <translation>Autosave AN. Verzeichnis: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17203"/>
        <source>Autosave OFF</source>
        <translation>Autosave AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17491"/>
        <source>xlimit = (%3,%4) ylimit = (%1,%2) zlimit = (%5,%6)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18536"/>
        <source>&lt;b&gt;Event&lt;/b&gt; hide or show the corresponding slider</source>
        <translation>&lt;b&gt;Ereignis&lt;/b&gt; anzeigen oder ausblenden des zugehörigen Schiebereglers</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18537"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action on slider release</source>
        <translation>&lt;b&gt;Aktion&lt;/b&gt; Aktion wird ausgeführt bei loslassen des Schieberglers</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18538"/>
        <source>&lt;b&gt;Command&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt;)</source>
        <translation>&lt;b&gt;Befehl&lt;/b&gt; abhängig von dem Aktionstyp (&apos;{}&apos; wird durch &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt; ersetzt)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19294"/>
        <source>Serial Command: ASCII serial command or binary a2b_uu(serial command)</source>
        <translation>Serialerbefehl: ASCII serial befehl oder binar a2b_uu(serial befehl)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19297"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) writes values to the registers in slaves specified by the given ids</source>
        <translation>Modbus Befehl: write([slaveId,register,value],..,[slaveId,register,value]) schreibt Werte in die Register der Slaves gegegebn bei den Ids</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19298"/>
        <source>DTA Command: Insert Data address : value, ex. 4701:1000 and sv is 100. always multiply with 10 if value Unit: 0.1 / ex. 4719:0 stops heating</source>
        <translation>DTA Befehl: Datenadresse eingeben : Wert, bsp. 4701:1000 und sv ist 100. immer mit 10 multiplizieren falls  Werte mit Einheit: 0.1 / bsp. 4719:0 Heizung aus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18542"/>
        <source>&lt;b&gt;Offset&lt;/b&gt; added as offset to the slider value</source>
        <translation>&lt;b&gt;Offset&lt;/b&gt; wird dem Reglerwert hinzugefügt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18543"/>
        <source>&lt;b&gt;Factor&lt;/b&gt; multiplicator of the slider value</source>
        <translation>&lt;b&gt;Faktor&lt;/b&gt; Multiplikator des Reglerwertes</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19301"/>
        <source>Event custom buttons</source>
        <translation>Wählbare Ereignis Taste</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19265"/>
        <source>Event configuration saved</source>
        <translation>Ereigniskonfiguration gespeichert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19268"/>
        <source>Found empty event type box</source>
        <translation>Leerer Ereignistyp</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19288"/>
        <source>&lt;b&gt;Button Label&lt;/b&gt; Enter \n to create labels with multiple lines.</source>
        <translation>&lt;b&gt;Tastenname&lt;/b&gt; Eingabe von \n erlaubt mehrzeilige Namen.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19289"/>
        <source>&lt;b&gt;Event Description&lt;/b&gt; Description of the Event to be recorded.</source>
        <translation>&lt;b&gt;Tasten Beschreibung&lt;/b&gt; Beschreibung des Ereignis welches registriert werden soll.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19290"/>
        <source>&lt;b&gt;Event type&lt;/b&gt; Type of event to be recorded.</source>
        <translation>&lt;b&gt;Event Typ&lt;/b&gt; Typ des Ereignisses welches registriert werden soll.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19291"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-10) to be recorded</source>
        <translation>&lt;b&gt;Ereigniswert&lt;/b&gt; Wert des Ereignis (1-10) welches registriert werden soll</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19292"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action at the time of the event</source>
        <translation>&lt;b&gt;Aktion&lt;/b&gt; Aktion wird bei Registrierung des Ereignisses ausgeführt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19293"/>
        <source>&lt;b&gt;Documentation&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by the event value):</source>
        <translation>&lt;b&gt;Dokumentation&lt;/b&gt; abhängig vom Ereignistyp (&apos;{}&apos; wird durch den Ereigniswert ersetzt):</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19295"/>
        <source>Call Program: A program/script path (absolute or relative)</source>
        <translation>Programmaufruf: Der (absolute oder relative) Pfad zu einem Programm/Skript</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19296"/>
        <source>Multiple Event: Adds events of other button numbers separated by a comma: 1,2,3, etc.</source>
        <translation>Ereignisgruppe: kombiniert Ereignisse gegeben als Kommagetrente Liste von Tastennummern: 1,2,3,..</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19299"/>
        <source>&lt;b&gt;Button Visibility&lt;/b&gt; Hides/shows individual button</source>
        <translation>&lt;b&gt;Tasten Sichtbarkeit&lt;/b&gt; Anzeigen/Ausblenden von einzelnen Tasten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19300"/>
        <source>&lt;b&gt;Keyboard Shorcut: &lt;/b&gt; [b] Hides/shows Extra Button Rows</source>
        <translation>&lt;b&gt;Tastenkürzel: &lt;/b&gt; [b] Anzeigen/Ausblenden der Benutzerdefinierten Tastenreihen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19656"/>
        <source>Background does not match number of labels</source>
        <translation>Profilevorlage entspricht nicht der Anzahl der Kurven</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23346"/>
        <source>Not enough time points for an ET curviness of %1. Set curviness to %2</source>
        <translation>Nicht genug Datenelemente für ET Rundheit of %1. Setze Rundheit auf %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23556"/>
        <source>Designer Config</source>
        <translation>Designer Einstellungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23353"/>
        <source>Not enough time points for an BT curviness of %1. Set curviness to %2</source>
        <translation>Nicht genug Datenelemente für BT Rundheit of %1. Setze Rundheit auf %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23548"/>
        <source>CHARGE</source>
        <translation>FÜLLEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23549"/>
        <source>DRY END</source>
        <translation>TROCKNEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23550"/>
        <source>FC START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23551"/>
        <source>FC END</source>
        <translation>FC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23552"/>
        <source>SC START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23553"/>
        <source>SC END</source>
        <translation>SC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23554"/>
        <source>DROP</source>
        <translation>LEEREN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23368"/>
        <source>Incorrect time format. Please recheck %1 time</source>
        <translation>Falsches Zeitformat. Zeit %1 überprüfen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23555"/>
        <source>Times need to be in ascending order. Please recheck %1 time</source>
        <translation>Zeiten müssen in aufsteigender Reihe geordnet sein. Zeiten %1 überprüfen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23520"/>
        <source>Designer has been reset</source>
        <translation>Designer wurde zurückgesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24396"/>
        <source>Serial Port Settings: %1, %2, %3, %4, %5, %6</source>
        <translation>Serieller Anschluss: %1, %2, %3, %4, %5, %6</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25038"/>
        <source>External program</source>
        <translation>Externes Programm</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25302"/>
        <source>PID to control ET set to %1 %2 ; PID to read BT set to %3 %4</source>
        <translation>PID um den ET zu steuern gesetzt auf %1 %2 ; PID zum lesen des BT gesetzt auf %3 %4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25526"/>
        <source>Device set to %1. Now, check Serial Port settings</source>
        <translation>Gerät %1 ausgewählt. Überprüfe nun die Serielleneinstellungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25621"/>
        <source>Device set to %1. Now, chose serial port</source>
        <translation>Gerät %1 ausgewählt. Serielleeinstellungen vornehmen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25378"/>
        <source>Device set to CENTER 305, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation>Gerät CENTER 305 ausgewählt, equivalent zum CENTER 306 ist. Serielleeinstellungen vornehmen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25432"/>
        <source>Device set to %1, which is equivalent to CENTER 309. Now, chose serial port</source>
        <translation>Gerät %1 ausgewählt, equivalent zum CENTER 309. Serielleeinstellungen vornehmen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25459"/>
        <source>Device set to %1, which is equivalent to CENTER 303. Now, chose serial port</source>
        <translation>Gerät %1 ausgewählt, equivalent zum CENTER 303. Serielleeinstellungen vornehmen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25441"/>
        <source>Device set to %1, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation>Gerät %1 ausgewählt, equivalent zum CENTER 306. Serielleeinstellungen vornehmen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25468"/>
        <source>Device set to %1, which is equivalent to Omega HH506RA. Now, chose serial port</source>
        <translation>Gerät %1 ausgewählt, equivalent zum Omega HH506RA. Serielleeinstellungen vornehmen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25569"/>
        <source>Device set to %1, which is equivalent to Omega HH806AU. Now, chose serial port</source>
        <translation>Gerät %1 ausgewählt, equivalent zum Omega HH806AU. Serielleeinstellungen vornehmen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25603"/>
        <source>Device set to %1</source>
        <translation>Gerät %1 ausgewählt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25499"/>
        <source>Device set to %1%2</source>
        <translation>Gerät %1%2 ausgewählt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25560"/>
        <source>Device set to %1, which is equivalent to CENTER 302. Now, chose serial port</source>
        <translation>Gerät %1 ausgewählt, equivalent zum CENTER 302. Serielleeinstellungen vornehmen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26276"/>
        <source>Color of %1 set to %2</source>
        <translation>Farbe von %1 auf %2 gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26781"/>
        <source>Save Wheel graph</source>
        <translation>Kreisdiagramm Speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26785"/>
        <source>Wheel Graph saved</source>
        <translation>Kreisdiagramm gespeichert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27043"/>
        <source>Load Alarms</source>
        <translation>Alarme laden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27081"/>
        <source>Save Alarms</source>
        <translation>Alarme speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27113"/>
        <source>&lt;b&gt;Status:&lt;/b&gt; activate or deactive alarm</source>
        <translation>&lt;b&gt;Status:&lt;/b&gt; Alarm aktivieren oder deaktivieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27114"/>
        <source>&lt;b&gt;If Alarm:&lt;/b&gt; alarm triggered only if the alarm with the given number was triggered before. Use 0 for no guard.</source>
        <translation>&lt;b&gt;Wenn Alarm:&lt;/b&gt; Alarm ist freigeschaltet falls der Alarm der gegebenen Nummber (ausser 0) voher ausgeführt wurde.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27116"/>
        <source>&lt;b&gt;From:&lt;/b&gt; alarm only triggered after the given event</source>
        <translation>&lt;b&gt;Von:&lt;/b&gt; Alarm wird erst nach dem ausgewählten Ereignis freigeschaltet</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27117"/>
        <source>&lt;b&gt;Time:&lt;/b&gt; if not 00:00, alarm is triggered mm:ss after the event &apos;From&apos; happend</source>
        <translation>&lt;b&gt;Zeit:&lt;/b&gt; falls nicht 00:00, wird der Alarm mm:ss nach dem Ereignis &apos;Von&apos; ausgeführt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27118"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; the temperature source that is observed</source>
        <translation>&lt;b&gt;Quelle:&lt;/b&gt; der Temperaturkanal der beobachtet wird</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27119"/>
        <source>&lt;b&gt;Condition:&lt;/b&gt; alarm is triggered if source rises above or below the specified temperature</source>
        <translation>&lt;b&gt;Bedingung:&lt;/b&gt; der Alarm wird aktiviert sobald die Temperatur über bzw. unter den angegebenen Wert fällt bzw. steigt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27121"/>
        <source>&lt;b&gt;Action:&lt;/b&gt; if all conditions are fulfilled the alarm triggeres the corresponding action</source>
        <translation>&lt;b&gt;Aktion:&lt;/b&gt; die Aktion welche bei Aktivierung ausgeführt wird</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27123"/>
        <source>&lt;b&gt;NOTE:&lt;/b&gt; each alarm is only triggered once</source>
        <translation>&lt;b&gt;Anmerkung:&lt;/b&gt; jeder Alarm wird nur einmalig ausgeführt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30102"/>
        <source>OFF</source>
        <translation>AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30095"/>
        <source>CONTINUOUS CONTROL</source>
        <translation>KONTINUIERLICHE STEUERUNG</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30108"/>
        <source>ON</source>
        <translation>EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30107"/>
        <source>STANDBY MODE</source>
        <translation>STANDBY MODUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28144"/>
        <source>The rampsoak-mode tells how to start and end the ramp/soak</source>
        <translation>Der Rampe/Haltezeit Modus beschreibt wie dieser gestartet und gestopped werden kann</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28145"/>
        <source>Your rampsoak mode in this pid is:</source>
        <translation>Der Rampe/Haltezeit Modus dieses PID ist:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28146"/>
        <source>Mode = %1</source>
        <translation>Modus = %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28148"/>
        <source>Start to run from PV value: %1</source>
        <translation>Start von PV Wert: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28149"/>
        <source>End output status at the end of ramp/soak: %1</source>
        <translation>Beendigung des Ausgabestatus zum Ende der Rampe/Haltezeit: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28151"/>
        <source>
Repeat Operation at the end: %1</source>
        <translation>
OPeration am Ende wiederholen: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28153"/>
        <source>Recomended Mode = 0</source>
        <translation>Empfohlener Modus = 0</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28154"/>
        <source>If you need to change it, change it now and come back later</source>
        <translation>Fall es geändert werden muss, ändere es jetzt und verändere es später wieder</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28155"/>
        <source>Use the Parameter Loader Software by Fuji if you need to

</source>
        <translation>Benutzen Sie die Fuji Parameter Software falls nötig
</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28156"/>
        <source>Continue?</source>
        <translation>Fortfahren?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28157"/>
        <source>RampSoak Mode</source>
        <translation>Rampe/Haltezeit Modus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29512"/>
        <source>Current sv = %1. Change now to sv = %2?</source>
        <translation>sv Wert = %1. Jetzt auf sv = %2 setzen?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29574"/>
        <source>Change svN</source>
        <translation>svN ändern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29573"/>
        <source>Current pid = %1. Change now to pid =%2?</source>
        <translation>Ausgewählter pid = %1. Wechsele nun auf pid =%2?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30328"/>
        <source>Ramp Soak start-end mode</source>
        <translation>Rampe/Haltezeit Start End Modus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30234"/>
        <source>Pattern changed to %1</source>
        <translation>Muster geändert auf %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30236"/>
        <source>Pattern did not changed</source>
        <translation>Muster wurde nicht geändert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30239"/>
        <source>Ramp/Soak was found ON! Turn it off before changing the pattern</source>
        <translation>Rampe/Haltezeit ist EIN! Zum ändern des Musters ausschalten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30241"/>
        <source>Ramp/Soak was found in Hold! Turn it off before changing the pattern</source>
        <translation>Rampe/Haltezeit ist auf HATL! Zum ändern des Musters ausschalten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30582"/>
        <source>Activate PID front buttons</source>
        <translation>Aktiviere PID Tasten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30582"/>
        <source>Remember SV memory has a finite
life of ~10,000 writes.

Proceed?</source>
        <translation>Begrenzte Anzahl von SV Speicher Schreibzyklen beachten.

Fortfahren?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30686"/>
        <source>RS OFF</source>
        <translation>RS AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30688"/>
        <source>RS on HOLD</source>
        <translation>RS in Bereitschaft</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30709"/>
        <source>PXG sv#%1 set to %2</source>
        <translation>PXG sv#%1 auf %2 gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30730"/>
        <source>PXR sv set to %1</source>
        <translation>PXR sv auf %1 gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30765"/>
        <source>SV%1 changed from %2 to %3)</source>
        <translation>SV%1 geändert von %2 auf %3)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30773"/>
        <source>Unable to set sv%1</source>
        <translation>sv%1 konnte nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30792"/>
        <source>Unable to set sv</source>
        <translation>sv konnte nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30794"/>
        <source>Unable to set new sv</source>
        <translation>neuer sv Wert konnte nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8967"/>
        <source>Exit Designer?</source>
        <translation>Designer verlassen?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8968"/>
        <source>Designer Mode ON</source>
        <translation>Designer Modus EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8931"/>
        <source>Extra Event Button Palette</source>
        <translation type="obsolete">Extra Ereignis Tasten Palette</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27120"/>
        <source>&lt;b&gt;Temp:&lt;/b&gt; the speficied temperature limit</source>
        <translation>&lt;b&gt;Temp:&lt;/b&gt; das angegebene Temperaturlimit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="2010"/>
        <source>Action canceled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14316"/>
        <source>previous ET value</source>
        <translation>verheriger ET Wert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14317"/>
        <source>previous BT value</source>
        <translation>vorheriger BT Wert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14318"/>
        <source>previous Extra #1 T1 value</source>
        <translation>vorheriger Extra #1 T1 Wert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14319"/>
        <source>previous Extra #1 T2 value</source>
        <translation>vorheriger Extra #1 T2 Wert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14320"/>
        <source>previous Extra #2 T1 value</source>
        <translation>vorheriger Extra #2 T1 Wert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14321"/>
        <source>previous Extra #2 T2 value</source>
        <translation>vorheriger Extra #2 T2 Wert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15526"/>
        <source>Interpolation failed: no profile available</source>
        <translation>Interpolation fehlgeschlagen: kein Profil verfügbar</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19974"/>
        <source>Playback Aid set ON at %1 secs</source>
        <translation>Wiedergabehilfe AUS bei %1 secs</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19982"/>
        <source>No profile background found</source>
        <translation>Keine Profilvorlage geladen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20076"/>
        <source>Reading background profile...</source>
        <translation>Profilevorlage wird gelesen...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23873"/>
        <source>These serial settings are used for all Modbus communication.</source>
        <translation type="obsolete">Diese Serielleneinstellungen werden für jede Modbus-Verbindung verwendet.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23874"/>
        <source>The MODBUS device corresponds to input channels 1 and 2.</source>
        <translation type="obsolete">Das MODBUS Gerät bezieht sich auf die Kanäle 1 und 2.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23875"/>
        <source>The +MODBUS_34 extra device adds input channels 3 and 4.</source>
        <translation type="obsolete">Das +MODBUS_34 Gerät bezieht sich auf die Kanäle 3 und 4.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23876"/>
        <source>Inputs with slave id set to 0 are turned off.</source>
        <translation type="obsolete">Kanäle mit Slave id 0 werden nicht benutzt.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23877"/>
        <source>Modbus function 3 &apos;read holding register&apos; is the standard.</source>
        <translation type="obsolete">Modbus Funktion 3 &apos;read holding register&apos; ist der Standardfall.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23878"/>
        <source>Modbus function 4 triggers the use of &apos;read input register&apos;.</source>
        <translation type="obsolete">Modbus Funktion 4 wird für &apos;read input register&apos; verwendet.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23879"/>
        <source>Input registers (fct 4) usually are from the range 30000-39999.</source>
        <translation type="obsolete">Input Register (Fkt. 4) sind üblicherweise aus dem Bereich 30000-39999.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23880"/>
        <source>Most devices hold data in 2 byte integer registers.</source>
        <translation type="obsolete">Die meisten Geräte haben 2 byte Integer Register.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23882"/>
        <source>In that case you have to use the symbolic assignment &apos;x/10&apos;.</source>
        <translation type="obsolete">In diesem Fall muss die Symbolische Funktion &apos;x/10&apos;.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23883"/>
        <source>Few devices hold data as 4 byte floats in two registers.</source>
        <translation type="obsolete">Einige Geräte halten Werte in 4 byte Gleitkomma Registern.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23952"/>
        <source>Tick the Float flag in this case.</source>
        <translation>In diesem Falle muss das Gleitkomma Flag gesetzt werden.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25245"/>
        <source>Device not set</source>
        <translation>Gerät nicht ausgewählt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10369"/>
        <source>To load this profile the extra devices configuration needs to be changed.
Continue?</source>
        <translation>Um dieses Profil zu laden müssen die Extra Geräte Einstellungen geändert werden. Fortfahren?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="10370"/>
        <source>Found a different number of curves</source>
        <translation>Eine andere Kurvenanzahl gefunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15705"/>
        <source>[ET target 1 = %1] [BT target 1 = %2] [ET target 2 = %3] [BT target 2 = %4]</source>
        <translation>[ET Ziel 1 = %1] [BT Ziel 1 = %2] [ET Ziel 2 = %3] [BT Ziel 2 = %4]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19651"/>
        <source>Background profile not found</source>
        <translation>Profilevorlage nicht gefunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9255"/>
        <source>&lt;b&gt;[CRTL N]&lt;/b&gt; = Autosave + Reset + START</source>
        <translation>&lt;b&gt;[CRTL N]&lt;/b&gt; = Automatisches Speichern + Reset + START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30684"/>
        <source>RS ON</source>
        <translation>RS EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30784"/>
        <source>SV changed from %1 to %2</source>
        <translation>SV von %1 auf %2 gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28150"/>
        <source>Output status while ramp/soak operation set to OFF: %1</source>
        <translation>Ausgabestatus während Rampe/Haltezeit Operationen AUS: %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19516"/>
        <source>Phases changed to %1 default: %2</source>
        <translation>Röstphasen geändert auf %1 default: %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23881"/>
        <source>A temperature of 145.2C is often sent as 1452.</source>
        <translation type="obsolete">Ein Temperaturwert wie 145.2C wird oft als 1452 abgelegt.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9266"/>
        <source>&lt;b&gt;[f]&lt;/b&gt; = Full Screen Mode</source>
        <translation>&lt;b&gt;[f]&lt;/b&gt; = Vollbild Modus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14061"/>
        <source>Save Graph as PDF</source>
        <translation>Diagramm als PDF gespeichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="1581"/>
        <source>Alarm %1 triggered</source>
        <translation>Alarm %1 ausgelöst</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22227"/>
        <source>Phidget Temperature Sensor 4-input attached</source>
        <translation>Phidget 4-Kanal Temperatur Sensor verbunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22235"/>
        <source>Phidget Temperature Sensor 4-input not attached</source>
        <translation>Phidget 4-Kanal Temperatur Sensor nicht verbunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22323"/>
        <source>Phidget Bridge 4-input attached</source>
        <translation>Phidget 4-Kanal Bridge Sensor verbunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22331"/>
        <source>Phidget Bridge 4-input not attached</source>
        <translation>Phidget 4-Kanal Bridge Sensor nicht verbunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="25551"/>
        <source>Device set to %1. Now, chose Modbus serial port</source>
        <translation>Gerät %1 ausgewählt. Modbus Seriellenanschluss auswählen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27115"/>
        <source>&lt;b&gt;But Not:&lt;/b&gt; alarm triggered only if the alarm with the given number was not triggered before. Use 0 for no guard.</source>
        <translation>&lt;b&gt;Aber Nicht:&lt;/b&gt; Alarm wird nur ausgelöst falls der Alarm an der angegeben Position noch nicht ausgelöst wurde. Funktion deaktiviert falls auf 0 gesetzt.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3772"/>
        <source>[TP] recorded at %1 BT = %2</source>
        <translation>[TP] markiert bei %1 BT = %2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4919"/>
        <source>Importing a profile in to Designer will decimate all data except the main [points].
Continue?</source>
        <translation>Der Import eines Profiles in den Designer alle Daten verwerfen ausser den signifikanten. Weitermachen?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27122"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button or the new value of the slider</source>
        <translation>&lt;b&gt;Beschreibung:&lt;/b&gt;Text der Nachricht, name des Programms, id der Ereignisstaste, oder der neue Reglerwert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29111"/>
        <source>Load PID Settings</source>
        <translation>PID Einstellungen laden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29195"/>
        <source>Save PID Settings</source>
        <translation>PID Einstellungen sichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13531"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13531"/>
        <source>A tight sampling interval might lead to instability on some machines. We suggest a minimum of 3s.</source>
        <translation>Ein kurzes Abtastintervall kann zu Instabilitäten führen. Eine Einstellung von 3s wird empfohlen.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13514"/>
        <source>Oversampling is only active with a sampling interval equal or larger than 3s.</source>
        <translation>Überabtastung ist nur aktiv bei einem Abtastinterval größer als 2ss.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14323"/>
        <source>current background ET</source>
        <translation>Vorlage ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14324"/>
        <source>current background BT</source>
        <translation>Vorlage BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9257"/>
        <source>&lt;b&gt;[d]&lt;/b&gt; = Toggle xy scale (T/Delta)</source>
        <translation>&lt;b&gt;[d]&lt;/b&gt; = Wechsele xy Einheit (T/Delta)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22415"/>
        <source>Phidget 1018 IO attached</source>
        <translation>Phidget 1018 IO verbunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22423"/>
        <source>Phidget 1018 IO not attached</source>
        <translation>Phidget 1018 IO nicht verbunden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31275"/>
        <source>Load Ramp/Soak Table</source>
        <translation>Ramp/Soak Tabelle laden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31294"/>
        <source>Save Ramp/Soak Table</source>
        <translation>Ramp/Soak Tabelle speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31500"/>
        <source>PID turned on</source>
        <translation>PID AN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31513"/>
        <source>PID turned off</source>
        <translation>PID AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="9265"/>
        <source>&lt;b&gt;[q,w,e,r + &lt;i&gt;nn&lt;/i&gt;]&lt;/b&gt; = Quick Custom Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14307"/>
        <source>Return the minimum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14308"/>
        <source>Return the maximum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23940"/>
        <source>The MODBUS device corresponds to input channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23941"/>
        <source>1 and 2.. The MODBUS_34 extra device adds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23942"/>
        <source>input channels 3 and 4. Inputs with slave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23943"/>
        <source>id set to 0 are turned off. Modbus function 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23944"/>
        <source>&apos;read holding register&apos; is the standard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23945"/>
        <source>Modbus function 4 triggers the use of &apos;read </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23946"/>
        <source>input register&apos;.Input registers (fct 4) usually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23947"/>
        <source> are from 30000-39999.Most devices hold data in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23948"/>
        <source>2 byte integer registers. A temperature of 145.2C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23949"/>
        <source>is often sent as 1452. In that case you have to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23950"/>
        <source>use the symbolic assignment &apos;x/10&apos;. Few devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23951"/>
        <source>hold data as 4 byte floats in two registers.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Radio Button</name>
    <message>
        <location filename="artisanlib/main.py" line="24548"/>
        <source>PID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24550"/>
        <source>Program</source>
        <translation>Programm</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24547"/>
        <source>Meter</source>
        <translation>Messgerät</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24549"/>
        <source>TC4</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Scope Annotation</name>
    <message>
        <location filename="artisanlib/main.py" line="701"/>
        <source>Heater</source>
        <translation>Heizung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="702"/>
        <source>Damper</source>
        <translation>Luftklappe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="703"/>
        <source>Fan</source>
        <translation>Lüfter</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3803"/>
        <source>DE %1</source>
        <translation>DE %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3859"/>
        <source>FCs %1</source>
        <translation>FCs %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3914"/>
        <source>FCe %1</source>
        <translation>FCe %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3967"/>
        <source>SCs %1</source>
        <translation>SCs %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4025"/>
        <source>SCe %1</source>
        <translation>SCe %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4164"/>
        <source>CE %1</source>
        <translation>CE %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="700"/>
        <source>Speed</source>
        <translation>Drehzahl</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3724"/>
        <source>CHARGE 00:00</source>
        <translation>FÜLLEN 00:00</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4085"/>
        <source>DROP %1</source>
        <translation>LEEREN %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3764"/>
        <source>TP %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Scope Title</name>
    <message>
        <location filename="artisanlib/main.py" line="10484"/>
        <source>Roaster Scope</source>
        <translation>Röstoskop</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="artisanlib/main.py" line="28486"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27989"/>
        <source>PID OFF</source>
        <translation>PID AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27992"/>
        <source>PID ON</source>
        <translation>PID EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19977"/>
        <source>Playback Aid set OFF</source>
        <translation>Widergabehilfe AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29285"/>
        <source>Decimal position successfully set to 1</source>
        <translation>Dezimalposition auf 1 gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29288"/>
        <source>Problem setting decimal position</source>
        <translation>Setzten der Dezimalposition fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29339"/>
        <source>Problem setting thermocouple type</source>
        <translation>Setzen des Temperaturfühlertyp fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30318"/>
        <source>setting autotune...</source>
        <translation>Autotune eingeschaltet...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30345"/>
        <source>Autotune successfully turned OFF</source>
        <translation>Autotune AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30348"/>
        <source>Autotune successfully turned ON</source>
        <translation>Autotune EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30246"/>
        <source>wait...</source>
        <translation>warten...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28027"/>
        <source>Empty SV box</source>
        <translation>Leeres SV Feld</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28036"/>
        <source>Unable to read SV</source>
        <translation>SV konnte nicht gelesen werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30141"/>
        <source>Ramp/Soak operation cancelled</source>
        <translation>Rampe/Haltezeit Operation abgebrochen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30144"/>
        <source>No RX data</source>
        <translation>Keine Daten empfangen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30156"/>
        <source>Need to change pattern mode...</source>
        <translation>Muster modus muss gewechselt werden...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30165"/>
        <source>Pattern has been changed. Wait 5 secs.</source>
        <translation>Muster wurde geändert. Bitte 5 sec. warten.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30168"/>
        <source>Pattern could not be changed</source>
        <translation>Muster konnte nicht geändert werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30199"/>
        <source>RampSoak could not be changed</source>
        <translation>Rampe/Haltezeit konnte nicht geändert werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30212"/>
        <source>RS successfully turned OFF</source>
        <translation>RS wurde ausgeschaltet</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28249"/>
        <source>setONOFFrampsoak(): Ramp Soak could not be set OFF</source>
        <translation>setONOFFrampsoak(): Rampe/Haltezeit konnte nicht abgeschaltet werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28301"/>
        <source>getallsegments(): problem reading R/S </source>
        <translation>getallsegments(): fehler beim Lesen von R/S </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30314"/>
        <source>Finished reading Ramp/Soak val.</source>
        <translation>Rampe/Haltezeit erfolgreich gelesen.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28344"/>
        <source>Finished reading pid values</source>
        <translation>PID Werte erfolgreich gelesen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28396"/>
        <source>setpid(): There was a problem setting %1</source>
        <translation>setpid(): Ein Problem ist aufgetreten %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30422"/>
        <source>Ramp/Soak successfully writen</source>
        <translation>Rampe/Haltezeit erfolgreich geschrieben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29248"/>
        <source>Time Units successfully set to MM:SS</source>
        <translation>Zeiteinheit auf MM:SS gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29251"/>
        <source>Problem setting time units</source>
        <translation>Fehler beim setzen der Zeiteinheit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29336"/>
        <source>Thermocouple type successfully set</source>
        <translation>Temperaturfühlertyp geändert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29529"/>
        <source>SV%1 set to %2</source>
        <translation>SV%1 auf  %2 gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29533"/>
        <source>Problem setting SV</source>
        <translation>SV konnte nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29535"/>
        <source>Cancelled svN change</source>
        <translation>svN Änderung abgebrochen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29553"/>
        <source>PID already using sv%1</source>
        <translation>PID benutzt schon sv%1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29556"/>
        <source>setNsv(): bad response</source>
        <translation>setNsv(): fehlerhafte Antwort</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29593"/>
        <source>setNpid(): bad confirmation</source>
        <translation>setNpid(): fehlerhafter Bestätigung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29597"/>
        <source>Cancelled pid change</source>
        <translation>PID Änderung abgebrochen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29615"/>
        <source>PID was already using pid %1</source>
        <translation>Es wurde bereits PID %1 gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29618"/>
        <source>setNpid(): Unable to set pid %1 </source>
        <translation>setNpid(): PID %1 konnte nicht ausgewählt werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29695"/>
        <source>SV%1 successfully set to %2</source>
        <translation>SV%1 auf %2 gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29821"/>
        <source>pid #%1 successfully set to (%2,%3,%4)</source>
        <translation>pid #%1 auf (%2,%3,%4) gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29829"/>
        <source>pid command failed. Bad data at pid%1 (8,8,8): (%2,%3,%4) </source>
        <translation>pid Befehl fehlgeschlagen. Fehlerhafte Daten bei pid%1 (8,8,8): (%2,%3,%4)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29935"/>
        <source>PID is using pid = %1</source>
        <translation>PID verwendet pid = %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30002"/>
        <source>PID is using SV = %1</source>
        <translation>PID verwendet SV = %1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30215"/>
        <source>Ramp Soak could not be set OFF</source>
        <translation>Rampe/Haltezeit konnte nicht abgeschaltet werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30256"/>
        <source>PID set to OFF</source>
        <translation>PID AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30259"/>
        <source>PID set to ON</source>
        <translation>PID AN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30262"/>
        <source>Unable</source>
        <translation>Fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30266"/>
        <source>No data received</source>
        <translation>Keine Daten empfangen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30306"/>
        <source>Reading Ramp/Soak %1 ...</source>
        <translation>Lese Rampe/Haltezeit %1 ...</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30311"/>
        <source>problem reading Ramp/Soak</source>
        <translation>Probleme beim Lesen von Rampe/Haltezeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30327"/>
        <source>Current pid = %1. Proceed with autotune command?</source>
        <translation>Ausgewählter PID = %1. Autotune Kommando ausführen?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30331"/>
        <source>Autotune cancelled</source>
        <translation>Autotune abgebrochen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30350"/>
        <source>UNABLE to set Autotune</source>
        <translation>Autotune fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>Ramp (MM:SS)</source>
        <translation>Rampe (MM:SS)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30355"/>
        <source>Soak (MM:SS)</source>
        <translation>Haltezeit (MM:SS)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28275"/>
        <source>getsegment(): problem reading ramp</source>
        <translation>getsegment(): Rampe konnte nicht gelesen werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28288"/>
        <source>getsegment(): problem reading soak</source>
        <translation>getsegment(): Haltezeit konnte nicht gelesen werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29703"/>
        <source>setsv(): Unable to set SV</source>
        <translation>setsv(): SV konnte nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29907"/>
        <source>getallpid(): Unable to read pid values</source>
        <translation>getallpid(): PID Werte konnten nicht gelesen werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29938"/>
        <source>getallpid(): Unable to read current sv</source>
        <translation>getallpid(): SV Wert konnte nicht gelesen werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31376"/>
        <source>Work in Progress</source>
        <translation>In Arbeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28470"/>
        <source>Ramp/Soak successfully written</source>
        <translation>Rampe/Haltezeit erfolgreich geschrieben</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28016"/>
        <source>SV successfully set to %1</source>
        <translation>SV wurde auf %1 gesetzt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30182"/>
        <source>RS ON</source>
        <translation>RS EIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="30202"/>
        <source>RS OFF</source>
        <translation>RS AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29839"/>
        <source>sending commands for p%1 i%2 d%3</source>
        <translation>Befehle für p%1 i%2 d%3 werden gesendet</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28387"/>
        <source>%1 successfully sent to pid </source>
        <translation>%1 korrekt an PID übertragen </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29590"/>
        <source>pid changed to %1</source>
        <translation>pid geändert auf %1</translation>
    </message>
</context>
<context>
    <name>Tab</name>
    <message>
        <location filename="artisanlib/main.py" line="15120"/>
        <source>Math</source>
        <translation>Berechnung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31393"/>
        <source>General</source>
        <translation>Allgemeines</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16395"/>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19947"/>
        <source>Events</source>
        <translation>Ereignisse</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19950"/>
        <source>Data</source>
        <translation>Daten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19944"/>
        <source>Config</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26093"/>
        <source>Graph</source>
        <translation>Diagramm</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18512"/>
        <source>Buttons</source>
        <translation>Tasten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24889"/>
        <source>Extra Devices</source>
        <translation>Zusatzgeräte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15114"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15117"/>
        <source>Plotter</source>
        <translation>Kurvenschreiber</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18527"/>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18515"/>
        <source>Sliders</source>
        <translation>Regler</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18521"/>
        <source>Palettes</source>
        <translation>Paletten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24886"/>
        <source>ET/BT</source>
        <translation>ET/BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29102"/>
        <source>Extra</source>
        <translation>Extras</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24243"/>
        <source>Modbus</source>
        <translation>Modbus</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24246"/>
        <source>Scale</source>
        <translation>Waage</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24892"/>
        <source>Symb ET/BT</source>
        <translation>Symb ET/BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26096"/>
        <source>LCDs</source>
        <translation>LCDs</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29090"/>
        <source>RS</source>
        <translation>RS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29093"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="29099"/>
        <source>Set RS</source>
        <translation>RS Einstellungen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31263"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15123"/>
        <source>UI</source>
        <translation>UI</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24249"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18518"/>
        <source>Quantifiers</source>
        <translation>Quantifikatoren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31267"/>
        <source>Ramp/Soak</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24895"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Table</name>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>Abs Time</source>
        <translation type="obsolete">Abs Zeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>Rel Time</source>
        <translation type="obsolete">Rel Zeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>DeltaBT (d/m)</source>
        <translation type="obsolete">DeltaBT (d/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>DeltaET (d/m)</source>
        <translation type="obsolete">DeltaET (d/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18717"/>
        <source>%1 DRY END</source>
        <translation type="obsolete">%1 TROCKEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18726"/>
        <source>%1 FC END</source>
        <translation type="obsolete">%1 FC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18734"/>
        <source>%1 SC END</source>
        <translation type="obsolete">%1 SC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18738"/>
        <source>%1 END</source>
        <translation type="obsolete">%1 ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20093"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20093"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Action</source>
        <translation>Aktion</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Comm Port</source>
        <translation>Anschluss</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Baud Rate</source>
        <translation>Baudrate</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Byte Size</source>
        <translation>Datenbits</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Parity</source>
        <translation>Parität</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Stopbits</source>
        <translation>Stoppbits</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24274"/>
        <source>Timeout</source>
        <translation>Zeitlimit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Width</source>
        <translation>Linienstärke</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Opaqueness</source>
        <translation>Transparenz</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Documentation</source>
        <translation>Dokumentation</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Visibility</source>
        <translation>Sichtbarkeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Device</source>
        <translation>Meßgerät</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Color 1</source>
        <translation>Farbe 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Color 2</source>
        <translation>Farbe 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18713"/>
        <source>%1 START</source>
        <translation type="obsolete">%1 START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18722"/>
        <source>%1 FC START</source>
        <translation type="obsolete">%1 FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18730"/>
        <source>%1 SC START</source>
        <translation type="obsolete">%1 SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18743"/>
        <source>%1 EVENT #%2 %3%4</source>
        <translation type="obsolete">%1 EREIGNIS #%2 %3%4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Label</source>
        <translation>Beschriftung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18803"/>
        <source>Text Color</source>
        <translation>Textfarbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Label 1</source>
        <translation>Beschriftung 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Label 2</source>
        <translation>Beschriftung 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>y1(x)</source>
        <translation>y1(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>y2(x)</source>
        <translation>y2(x)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>LCD 1</source>
        <translation>LCD 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>LCD 2</source>
        <translation>LCD 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Curve 1</source>
        <translation>Kurve 1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24936"/>
        <source>Curve 2</source>
        <translation>Kurve 2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26423"/>
        <source>Parent</source>
        <translation>Vorgänger</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Delete Wheel</source>
        <translation>Kreisdiagramm Löschen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Edit Labels</source>
        <translation>Beschriftungen Editieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Update Labels</source>
        <translation>Beschriftungen Aktualisieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Properties</source>
        <translation>Egenschaften</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Radius</source>
        <translation>Radius</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Starting angle</source>
        <translation>Startwinkel</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Color Pattern</source>
        <translation>Farbmuster</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>If Alarm</source>
        <translation>Wenn Alarm</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>From</source>
        <translation>Von</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Condition</source>
        <translation>Bedingung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>Ramp HH:MM</source>
        <translation>Rampe HH:MM</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28403"/>
        <source>Soak HH:MM</source>
        <translation>Haltezeit HH:MM</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Projection</source>
        <translation>Projektion</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26587"/>
        <source>Text Size</source>
        <translation>Textgröße</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Temp</source>
        <translation>Temp</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20175"/>
        <source>DRY END</source>
        <translation>TROCKEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20178"/>
        <source>FC START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20181"/>
        <source>FC END</source>
        <translation>FC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20184"/>
        <source>SC START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20187"/>
        <source>SC END</source>
        <translation>SC ENDE</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20193"/>
        <source>COOL</source>
        <translation>KÜHL</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16549"/>
        <source>EVENT #%2 %3%4</source>
        <translation>EREIGNIS #%2 %3%4</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20190"/>
        <source>DROP</source>
        <translation>LEEREN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20172"/>
        <source>CHARGE</source>
        <translation>FÜLLEN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>But Not</source>
        <translation>Aber Nicht</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20133"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20197"/>
        <source>EVENT #%1 %2%3</source>
        <translation>EREIGNIS #%1 %2%3</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="27326"/>
        <source>Beep</source>
        <translation>Piepston</translation>
    </message>
</context>
<context>
    <name>Textbox</name>
    <message>
        <location filename="artisanlib/main.py" line="430"/>
        <source>Finish</source>
        <translation>Abschluss</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="437"/>
        <source>Acidity</source>
        <translation>Säure</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="395"/>
        <source>Clean Cup</source>
        <translation>Reinheit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="358"/>
        <source>Head</source>
        <translation>Blume</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="385"/>
        <source>Fragance</source>
        <translation>Duft</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="427"/>
        <source>Sweetness</source>
        <translation>Süße</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="419"/>
        <source>Aroma</source>
        <translation>Aroma</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="442"/>
        <source>Balance</source>
        <translation>Ausgewogenheit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="436"/>
        <source>Body</source>
        <translation>Körper</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="368"/>
        <source>Sour</source>
        <translation>Säure</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="413"/>
        <source>Flavor</source>
        <translation>Geschmack</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="423"/>
        <source>Aftertaste</source>
        <translation>Nachgeschmack</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="372"/>
        <source>Bitter</source>
        <translation>Bitter</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="373"/>
        <source>Astringency</source>
        <translation>Adstringenz</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="374"/>
        <source>Solubles
Concentration</source>
        <translation>Lösungskonzentration</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="421"/>
        <source>Mouthfeel</source>
        <translation>Mundgefühl</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="376"/>
        <source>Other</source>
        <translation>Sonstiges</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="380"/>
        <source>Sweet</source>
        <translation>Süße</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="382"/>
        <source>pH</source>
        <translation>pH</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="392"/>
        <source>Dry Fragrance</source>
        <translation>Duft Kaffeemehl</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="393"/>
        <source>Uniformity</source>
        <translation>Gleichmäßigkeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="394"/>
        <source>Complexity</source>
        <translation>Komplexität</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="400"/>
        <source>Brightness</source>
        <translation>Helligkeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="401"/>
        <source>Wet Aroma</source>
        <translation>Naßaroma</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="435"/>
        <source>Fragrance</source>
        <translation>Duft</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="405"/>
        <source>Taste</source>
        <translation>Geschmack</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="406"/>
        <source>Nose</source>
        <translation>Spitze</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="411"/>
        <source>Fragrance-Aroma</source>
        <translation>Duft-Aroma</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="422"/>
        <source>Flavour</source>
        <translation>Geschmack</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="432"/>
        <source>Roast Color</source>
        <translation>Röstfarbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="433"/>
        <source>Crema Texture</source>
        <translation>Cremastruktur</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="434"/>
        <source>Crema Volume</source>
        <translation>Cremavolumen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="438"/>
        <source>Bitterness</source>
        <translation>Bitterkeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="439"/>
        <source>Defects</source>
        <translation>Defekte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="440"/>
        <source>Aroma Intensity</source>
        <translation>Aromaintensität</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="441"/>
        <source>Aroma Persistence</source>
        <translation>Aromabeständigkeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="370"/>
        <source>Critical
Stimulus</source>
        <translation>EntscheidenderReiz</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="377"/>
        <source>Aromatic
Complexity</source>
        <translation>Aromatische Komplexität</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="378"/>
        <source>Roast
Color</source>
        <translation>Röstfarbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="379"/>
        <source>Aromatic
Pungency</source>
        <translation>Aromatische Schärfe</translation>
    </message>
</context>
<context>
    <name>Tooltip</name>
    <message>
        <location filename="artisanlib/main.py" line="7444"/>
        <source>Marks the begining of the roast (beans in)</source>
        <translation>Markiert den Begin der Röstung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7452"/>
        <source>Marks the end of the roast (drop beans)</source>
        <translation>Markiert das Ende der Röstung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7467"/>
        <source>Marks an Event</source>
        <translation>Markiert ein Ereignis</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7476"/>
        <source>Increases the current SV value by 5</source>
        <translation>Erhöht den SV Wert um 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7484"/>
        <source>Increases the current SV value by 10</source>
        <translation>Erhöht den SV Wert um 10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7492"/>
        <source>Increases the current SV value by 20</source>
        <translation>Erhöht den SV Wert um 20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7500"/>
        <source>Decreases the current SV value by 20</source>
        <translation>Verringert den SV Wert um 20</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7508"/>
        <source>Decreases the current SV value by 10</source>
        <translation>Verringert den SV Wert um 10</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7516"/>
        <source>Decreases the current SV value by 5</source>
        <translation>Verringert den SV Wert um 5</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7530"/>
        <source>Turns ON/OFF the HUD</source>
        <translation>Schaltet den HUD EIN/AUS</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7597"/>
        <source>Timer</source>
        <translation>Röstzeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7598"/>
        <source>ET Temperature</source>
        <translation>ET Temperatur</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7599"/>
        <source>BT Temperature</source>
        <translation>BT Temperatur</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7600"/>
        <source>ET/time (degrees/min)</source>
        <translation>ET/Zeit (grad/min)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7601"/>
        <source>BT/time (degrees/min)</source>
        <translation>BT/Zeit (grad/min)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7602"/>
        <source>Value of SV in PID</source>
        <translation>Wert von SV des PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7603"/>
        <source>PID power %</source>
        <translation>PID Energie %</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7687"/>
        <source>Number of events found</source>
        <translation>Anzahl der Ereignise</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7698"/>
        <source>Type of event</source>
        <translation>Typ des Ereignisses</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7705"/>
        <source>Value of event</source>
        <translation>Wert des Ereignisses</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7719"/>
        <source>Updates the event</source>
        <translation>Aktualisiert das Ereignis</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26358"/>
        <source>Save image using current graph size to a png format</source>
        <translation>Speichert eine Abbildung des Graphen in PNG Format</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14954"/>
        <source>linear: linear interpolation
cubic: 3rd order spline interpolation
nearest: y value of the nearest point</source>
        <translation>linear: Geradeninterpolation
cubic: Splineinterpolation 3. Ordnung
nearest: y-Wert des nächstliegenden Punkt</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17065"/>
        <source>ON/OFF logs serial communication</source>
        <translation>EIN/AUS Serielles Protokoll</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17155"/>
        <source>Automatic generated name = This text + date + time</source>
        <translation>Automatischgenerierter Name = &lt;text&gt; + Datum + Zeit</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17158"/>
        <source>ON/OFF of automatic saving when pressing keyboard letter [a]</source>
        <translation>EIN/AUS des Automatischen Speicherns durch drücken der Taste [a]</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17169"/>
        <source>Sets the directory to store batch profiles when using the letter [a]</source>
        <translation>Definiert das Verzeichnis in welchem Röstprofile automatisch gespeichert werden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17771"/>
        <source>Allows to enter a description of the last event</source>
        <translation>Beschreibung des letzten Ereignis</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17977"/>
        <source>Add new extra Event button</source>
        <translation>Fügt eine extra Event Taste hinzu</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17982"/>
        <source>Delete the last extra Event button</source>
        <translation>Löscht die letzte extra Event Taste</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26854"/>
        <source>Show help</source>
        <translation>Offnet eine Hilfeseite</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18019"/>
        <source>Backup all palettes to a text file</source>
        <translation>Speichert alle Paletten in eine Datei</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18317"/>
        <source>Action Type</source>
        <translation>Aktionstyp</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18322"/>
        <source>Action String</source>
        <translation>Aktionstext</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26311"/>
        <source>Aspect Ratio</source>
        <translation>Verhältnis</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24982"/>
        <source>Example: 100 + 2*x</source>
        <translation>Beispiel: 100 + 2*x</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24983"/>
        <source>Example: 100 + x</source>
        <translation>Beispiel: 100 + x</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26300"/>
        <source>Erases wheel parent hierarchy</source>
        <translation>Löscht die Übergeordnete Hierarchy</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26304"/>
        <source>Sets graph hierarchy child-&gt;parent instead of parent-&gt;child</source>
        <translation>Umkehr der Super-/Sub Knoten Beziehung</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26318"/>
        <source>Increase size of text in all the graph</source>
        <translation>Vergrösserung der Textgrösse in allen Diagrammen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26321"/>
        <source>Decrease size of text in all the graph</source>
        <translation>Verkleinerung der Textgrösse in allen Diagrammen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26325"/>
        <source>Decorative edge beween wheels</source>
        <translation>Dekorative Kante zwischen den Kreisen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26331"/>
        <source>Line thickness</source>
        <translation>Linenstärke</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26336"/>
        <source>Line color</source>
        <translation>Linenfarbe</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26340"/>
        <source>Apply color pattern to whole graph</source>
        <translation>Farbschema auf ganzes Diagramm anwenden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26346"/>
        <source>Add new wheel</source>
        <translation>Neuen Kreis hinzufügen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26349"/>
        <source>Rotate graph 1 degree counter clockwise</source>
        <translation>Graph gegen den Uhrzeigersinn rotieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26352"/>
        <source>Rotate graph 1 degree clockwise</source>
        <translation>Graph mit dem Uhrzeigersinn rotieren</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26356"/>
        <source>Save graph to a text file.wg</source>
        <translation>Kreisdiagramm in eine Datei speichern</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26361"/>
        <source>Sets Wheel graph to view mode</source>
        <translation>Kreisdiagram anzeigen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26364"/>
        <source>open graph file.wg</source>
        <translation>Kreisdiagramm aus einer Datei laden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26367"/>
        <source>Close wheel graph editor</source>
        <translation>Kreisdiagramm Editor schliessen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14392"/>
        <source>&lt;b&gt;Label&lt;/b&gt;= </source>
        <translation>&lt;b&gt;Beschriftung&lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14393"/>
        <source>&lt;b&gt;Description &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Beschreibung &lt;/b&gt;=</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14395"/>
        <source>&lt;b&gt;Type &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Typ &lt;/b&gt;=</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14396"/>
        <source>&lt;b&gt;Value &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Wert &lt;/b&gt;=</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14397"/>
        <source>&lt;b&gt;Documentation &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Dokumentation &lt;/b&gt;=</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14398"/>
        <source>&lt;b&gt;Button# &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Taste# &lt;/b&gt;=</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7402"/>
        <source>Marks the begining of First Crack (FCs)</source>
        <translation>Markiert den Beginn des ersten Knackens (FCs)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7409"/>
        <source>Marks the end of First Crack (FCs)</source>
        <translation>Markiert das Ende des ersten Knackens (FCs)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7416"/>
        <source>Marks the begining of Second Crack (SCs)</source>
        <translation>Markiert den Beginn des zweiten Knackens (SCs)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7423"/>
        <source>Marks the end of Second Crack (SCe)</source>
        <translation>Markiert das Ende des zweiten Knackens (SCs)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7539"/>
        <source>Marks the end of the Drying phase (DRYEND)</source>
        <translation>Markiert das Ende der Trocknungsphase (TROCKEN)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7547"/>
        <source>Marks the end of the Cooling phase (COOLEND)</source>
        <translation>Markiert das Ende der Kühlungsphase (ABGEKÜHLT)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3527"/>
        <source>Stop monitoring</source>
        <translation>Monitor starten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7374"/>
        <source>Start monitoring</source>
        <translation>Monitor beenden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3632"/>
        <source>Stop recording</source>
        <translation>Aufzeichnung beenden</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7387"/>
        <source>Start recording</source>
        <translation>Aufzeichnung starten</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="7436"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18020"/>
        <source>Restore all palettes from a text file</source>
        <translation>Läd alle Paletten von einer Datei</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="26859"/>
        <source>Clear alarms table</source>
        <translation>Alarmtabelle zurücksetzen</translation>
    </message>
</context>
</TS>
